<?
include('includes/conexion.php');
include('includes/idioma.php');
$lg=new idioma($_GET['lg']);
$lg->seccion(3);
include('includes/header.php')?>
<script>
lg=<?=json_encode($lg->seccion)?>
</script>
<style>
  .bkg{
    background-color: #2d2d2d;
    display: table;
  }
  .bkg .txt01{
    width: 49%;
    position: relative;
    display:table-cell;
    vertical-align: top;
    padding-top: 4em;
  }
  .bkg .txt03{
    text-align: center;
  }
  .bkg h1{
    font-family:AspiraBlack,Verdana;
    font-size:30em;
    color:#2d2d2d;
    margin:0px;
    padding:0px;
    display:inline-block;
    line-height: 63%;
    text-align: left;
    letter-spacing: -0.1em;
    width: 100%;
  }
  .bkg h1 span{
    color:#FFF;
    display: block;
    text-align: right;
  }
  .bkg .txt03{
    font-family: AspiraXWide-Medium,Verdana;
    font-size: 3em;
    color: #FFF;
    display: inline-block;
/*    width: 90%;*/
    padding-left: 10%;
    padding-right: 10%;
    padding-bottom: 5%
  }
  .bkg .padd{
    padding-left: 5%;
  }
  .section{
    background: #FFF;
    min-height: 500px;
  }
  section{
    display: block;
    max-width: 1233px;
    width: 85%;
    margin: auto;
    padding-top: 50px;
  }
  section header{
    background-image: url(img/fdospon.png);
    background-repeat: no-repeat;
    background-size: 100% 100%;
    height: 78px; /*108*/
    display: block;
    max-width: 1233px;
    width: 100%;
    border-radius: 25px;
    text-align: center;
    color:#FFF;
    font-family: AspiraBold,Verdana;
    font-size:5em;
    vertical-align: middle;
    padding-top: 30px;
  }
  section ul{
    list-style: none;
    display: block;
    margin: auto;
    border:0px solid #969696;
    padding:0;
    background: #FFF;
    width: 85%;
    margin-top: -30px;
    -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    border-bottom-left-radius: 40px;
    border-bottom-right-radius: 40px;
    position: relative;
  }
  section li{
    width: 100%;
    background: url('img/fdo_bottom_ofer.jpg') center bottom no-repeat;
  }
  .cell {
    cursor: pointer;
  }
  .sinBg{
    background: none !important;
  }
  .cell{
    display: table-cell;
    vertical-align: middle;
  }
  .he01{
    width: 22%;
    max-width: 278px;
    text-align: center;
  }
  .he01 img{
    max-width: 167px;
    width: 90%;
  }
  .he02{
    width: 72%;
  }
  .he03{
    max-width: 134px;
    text-align: left;
  }
  .he03 img{
    max-width: 43px;
    width: 90%;
  }
  .tabl{
    display: table;
    display: table;
    width: 100%;
    font-family: AspiraBold,Verdana;
    font-size:3.9em;
    color:#2d2d2d;
    height: 140px;
  }
  .descrip{
    /*font-family: AspiraLight,Verdana;*/
    font-family: Tahoma, Geneva, sans-serif;
    font-size: 16px;
    color:#606060;
    padding-left: 70px;
    padding-bottom: 35px;
    line-height: 1.2em;
    position: relative;
  }
  .descrip div{
    position: absolute;
    top: 15px;
    right: 100px;
      -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    width: 30%;
    padding: 20px;
  }
  .descrip table{
    width: 100%;
  }
  .descrip td{
    height: 50px;
    vertical-align: middle;
    border-bottom: 1px solid #606060;
  }
  .descrip div a{
    width: 265px;
    padding: 10px 0;
    background: #db9b31;
    color:#FFF;
    display: inline-block;
    text-align: center;
    vertical-align: middle;
    margin: auto;
    text-decoration: none;
    margin-top: 40px;
    border-radius: 25px;
  }
  .btndes {
    width: 265px;
    padding: 10px 0;
    background: #a9a9a9;
    color: #FFF;
    display: inline-block;
    text-align: center;
    vertical-align: middle;
    margin: auto;
    text-decoration: none;
    margin-top: 40px;
    border-radius: 25px;
  }
  .detalle {
    display: block;
    width: 40%;
    max-width: 500px;
  }
  .separador {
    display: block;
    height: 20px;
    background: #a6212f;
    /*margin-top: 120px;*/
  }
  @media screen and (max-width: 650px) {
    section ul{
      width:100%
    }
  }
  </style>
    <div class="content">
      <div class="separador"></div>
      <div class="bkg">
        <div class="txt01">
          <span class="txt03">
            <?php echo cargarBloque('sponsor-descrip', $lg->idioma); ?>
          </span>
        </div>
      </div>
      <div class="section">
      <?php include('servicios/patrocinios.php'); ?>
      </div>
    </div>
    <script type="text/javascript">$("li:last-child").addClass("sinBg");</script>
  <form class="preLogin" id="solicitarPaquete"  method="POST" enctype="multipart/form-data" >
    <div class="login-content">
      <span class="close" onclick="closeForm()">&times;</span>
      <div class="contentBorder">
        <div class="log_tit"><?=$lg->seccion->t_comp_spon?></div>
        <div class="log_descrip"><?=$lg->seccion->t_des_pat?></div>
        <div id="log_leyenda" class="log_descrip"></div>
        <div class="log_textImp"><?=$lg->seccion->t_logo?></div>
        <!--<center><label class="btn btn-primary btn-block my-2 btn-sm text-center" style="font-size: 3em; cursor: pointer; font-family: AspiraMediumIt,Verdana;" for="logo">
        <i class="fa fa-fw fa-cloud-upload"></i><?=$lg->seccion->t_subir?></label></center>-->
        <div class="log_textImp login-content" style="border-radius:unset">
        <input name="file" id="logo" type="file" size="35" style="border: 2px solid #e3bd69;
    background: #FFF;
    border-radius: 17px;
    font-size: 1em;
    margin-bottom: 18px;
    margin-top: 6px;
    width: 85%;
    outline: none;
    padding: 3px 5%;" />
        <br />
        URL: <input type="text" name="url" id="url_patro" required="" style="width: 85%;" /></div>
        <input type="hidden" id="logoPat" >
        <input type="hidden" id="paqPat" >
        <input type="hidden" id="txtbtnlog" value="<?=$lg->general->btn_signing ?>-<?=$lg->general->loading ?>">
        <div class="login_botones">
          <a class="botLink" onclick="cargaLogo()"><?=$lg->general->t_acep ?></a>
          <a class="botLink" onclick="closeForm()"><?=$lg->general->btn_cancel ?></a>
        </div>
      </div>
    </div>
  </form>
    <? include('includes/footer.php')?>