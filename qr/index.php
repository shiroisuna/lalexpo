<?
include('../includes/conexion.php');
if(!empty($_POST['ingresar'])){
  $descrip='6th Edition Lalexpo 2019';
  if ($_POST['tipo']==2) $descrip='2th Edition Lalexpo Workshop';
  $rs=$con->query("INSERT INTO asistentes
    SET
      id_usuario = '".$_POST['usuario']."',
      ticket = '".$_POST['codigo']."',
      descrip = '".$descrip."'");
?>
<style type="text/css">* { font-family: sans-serif, helvetica, Verdana; }</style>
<center>
  <img src="../img/logo.jpg" /><br/>
  <div id="result"><h2>Ingreso de asistente completado!</h2></div>
  <button onclick="document.location='index.php'">Volver</button>
<?
  exit;
}
if(!empty($_POST['get'])){
  $datos=explode('_', $_POST['datos']);

  $r=new stdClass;
  $rs=$con->query("SELECT u.*,t.estado estado_ticket, t.codigo, t.tipo
    FROM usuarios u
    INNER JOIN ticket t ON t.id_usuario=u.id
    WHERE SUBSTRING(u.token,1,40)='".$datos[0]."' AND t.codigo='".$datos[1]."' AND t.tipo='".$datos[2]."'' AND t.evento='".$datos[3]."'");  
  if($rs->num_rows>0){
    $rw=$rs->fetch_object();
    $ingreso=$con->query("SELECT COUNT(*) total FROM asistentes WHERE id_usuario='".$rw->id."' AND ticket='".$rw->codigo."'")->fetch_object()->total;
    $descrip='6th Edition Lalexpo 2019';
    if ($rw->tipo==2) $descrip='2th Edition Lalexpo Workshop';
    if($rw->eliminado==0){
      if($rw->estado_ticket==1){
        $r->estado=1;
        $r->datos=$rw;
        $r->ingreso=$ingreso;
        $r->evento=$descrip;
      }else{
        $r->estado=0;
        $r->msg='El codigo ingresado no se ha abonado';
      }
    }else{
      $r->estado=0;
      $r->msg='La persona ha sido eliminada de nuestra base de datos por un administrador';
    }
  }else{
    $r->estado=0;
    $r->msg='El codigo ingresado no pertenece a nuestra base de datos';
  }
  echo json_encode($r);
  exit;
}?>
<!DOCTYPE html>
<html>
  <head>
    <title>Lalexpo.com : Escaner Tickets</title>
    <meta name="theme-color" content="#b12128" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script type="text/javascript" src="instascan.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script>
    </script>
    <style>
    *{
      font-size: 14px;
      font-family: Arial,Verdana;
    }
    #preview{
      width: 350px;
      height: 170px;
      border:2px solid #b12128;
      max-width: 100%;
      margin: auto;
    }
    #camaras{
      margin: auto;
      text-align: center;
    }
    .tbDat td{
      border:1px solid #CCC;
      padding:3px;
      text-align: left;
      font-size: 14px;
    }
    </style>
  </head>
  <body>
    <div id="sec01">
      <center><img src="../img/logo.jpg" /><br/>
      <video id="preview"></video><br/>
      <div id="result"></div>
      <div id="camaras"></div></center>
    </div>
    <div id="sec02" style="text-align: center;">

    </div>

    <script type="text/javascript">
    $(document).ready(function(){
      try{
      scanner = new Instascan.Scanner({ video: document.getElementById('preview'), mirror: true, scanPeriod: 1, refractoryPeriod:200 });
      scanner.addListener('scan', function (content) {
        getDatos(content)
      });
      Instascan.Camera.getCameras().then(function (cameras) {
        __camss=cameras
        if (cameras.length > 0) {
          var t='',i;
          for(i=0;i<cameras.length;i++){
            t+='<button onclick="setCam('+i+')" onmousedown="setCam('+i+')">Camara '+(i+1)+'</button> ';
          }
          __i=0
          $('#camaras').html(t)
          scanner.start(cameras[__i]);
        } else {
          $('#result').html('No cameras found.');
        }
      }).catch(function (e) {
        $('#result').html(e);
      });
      }
      catch(err){
        $('#result').html(err.message)
      }
      })
      function setCam(i){
        __i=i
        scanner.start(__camss[__i]);
      }
      function volver(){
        $('#sec01').css('display','block')
        $('#sec02').css('display','none').html('')
        scanner.start(__camss[__i]);
      }
      function getDatos(datos){
        $('#sec01').css('display','none')
        $('#sec02').css('display','block').html('<img src="loading.gif" /><br ><span class="reciviendo">Recibiendo datos...</span>')
        scanner.stop();
        $.ajax({
          url:'index.php',type:'post',data:'get=1&datos='+datos,dataType:'json',
          success:function(d){
            if(d.estado==0){
              t='<img src="error.png" /><br /><span style="color:red">'+d.msg+'</span>'
            }else{
              var foto
              if(typeof d.datos.foto!='undefined' && d.foto!=''){
                foto='../'+d.datos.foto
              }else{
                foto='exito.png';
              }
              t='<img src="'+foto+'" style="max-height:120px" /><br /><span style="color:green">El usuario puede pasar</span>'
              t+='<table class="tbDat" cellpadding="0" cellspacing="0" style="margin:auto">'
              t+='<tr><td>Nombre:</td><td>'+d.datos.nombre+' '+d.datos.apellido+'</td></tr>'
              t+='<tr><td>Documento:</td><td>'+d.datos.dni+'</td></tr>'
              t+='<tr><td>Idioma:</td><td>'+d.datos.lengua+'</td></tr>'
              t+='<tr><td>Pais:</td><td>'+d.datos.pais+'</td></tr>'
              t+='<tr><td>Compania:</td><td>'+d.datos.company+'</td></tr>'
              t+='<tr><td>Evento:</td><td>'+d.evento+'</td></tr>'
              if (d.ingreso==0) {
              t+='<tr><td colspan="2"><center><form method="post" action="index.php"><input type="hidden" name="usuario" value="'+d.datos.id+'" /><input type="hidden" name="codigo" value="'+d.datos.codigo+'" /><input type="hidden" name="ingresar" value="1" /><input type="hidden" name="tipo" value="'+d.datos.tipo+'" /><input type="submit" value=" Ingresar asistente " /></form></center></td></tr>'
              } else {
                t+='<tr><td colspan="2"><b>Este usuario ya ingresó al evento</b></td></tr>'
              }
              t+='</table>'
            }
            t+='<br /><input type="button" value="Volver" onclick="volver()" />'
            $('#sec02').html(t);
          },error:function(d){
            $('#sec02').html('Hubo un error al intentar recibir datos: '+datos);
          }
        })
      }
    </script>
    <img src="exito.png" style="display: none;" />
    <img src="error.png" style="display: none;" />
    <img src="loading.gif" style="display: none;" />
  </body>
</html>