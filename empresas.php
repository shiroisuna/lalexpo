<?
include('includes/conexion.php');
if(!empty($_GET['ingr'])){
  $nombre=$con->query("select razonsocial as empresa_nombre from empresas
  where id=".((int)$_GET['ingr']))->fetch_object()->empresa_nombre;
  $_SESSION['empresa']=(int)$_GET['ingr'];
  $_SESSION['empresa_nombre']=$nombre;
  header('location: empresas.php');
  exit;
}
$limit=8;
$desde=((int)$_GET['p'])*$limit;
$rs=$con->query('SELECT e.id,e.razonsocial,
(SELECT COUNT(i.id) FROM usuarios i WHERE i.empresa=e.id) usuarios,
(SELECT COUNT(t.id) FROM empleados t WHERE t.id_empresa=e.id) empleados,
(SELECT COUNT(o.id) FROM obras o WHERE o.empresa=e.id) obras
FROM empresas e limit '.$desde.','.$limit);
$total=$con->query('SELECT COUNT(id) total FROM empresas')->fetch_object()->total;
include('includes/header.php');
include('includes/top.php');
include('includes/menu.php')?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="far fa-building nav-icon"></i> Empresas</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item active">Empresas</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listado de Empresas <a href="newempresa.php"  class="btn btn-info" style="float: right;color:#FFF">Crear nueva</a></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <tbody><tr>
                    <th style="width: 10px">#</th>
                    <th>Nombre</th>
                    <th style="width: 120px">Usuarios</th>
                    <th style="width: 120px">Obras</th>
                    <th style="width: 120px">Empleados</th>
                  </tr>
                  <?
                  if($rs->num_rows>0){
                  while($rw=$rs->fetch_object()){?>
                  <tr>
                    <td><?=$rw->id?>.</td>
                    <td><a href="newempresa.php?id=<?=$rw->id?>"><?=$rw->razonsocial?></a>&nbsp;&nbsp;<a href="empresas.php?ingr=<?=$rw->id?>" title="Ingresar"><img src="img/flecha.png" style="width: 12px;" /></td>
                    <td><?=$rw->usuarios?></td>
                    <td><?=$rw->obras?></td>
                    <td><?=$rw->empleados?></td>
                  </tr>
                  <? }}else{ ?>
                  <tr>
                    <td colspan="5">No se encontraron datos.</td>
                  </tr>
                  <? } ?>
                </tbody></table>
              </div>
              <!-- /.card-body -->
              <? $paginas=ceil($total/$limit)-1;
              if($paginas>0){?>
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <?
                  $act=(int)$_GET['p'];
                  if($_GET['p']>0){?>
                  <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.($act-1)?>">Anterior</a></li>
                  <? }
                  for($i=($act-5);$i<$act;$i++){
                    if($i<0) continue;?>
                    <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.$i?>"><?=$i?></a></li>
                  <? }?>
                  <li class="page-item active"><a class="page-link" href="javascript:;"><?=$act?></a></li>
                  <?for($i=$act+1;$i<=$i+5;$i++){
                    if($i>$paginas) break;?>
                    <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.$i?>"><?=$i?></a></li>
                  <? }?>
                  <? if($paginas>$act){?>
                  <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.($act+1)?>">Siguiente</a></li>
                  <? } ?>
                </ul>
              </div>
              <? } ?>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include('includes/footer.php')?>