<?
include('includes/conexion.php');
include('lib/TCPDF/tcpdf.php');
if(empty($_GET['cod'])){
  echo 'No se recibio codigo de peticion';
  exit;
}

if (empty($_GET['e'])) {
  $id_work=$con->query("SELECT id FROM workshop WHERE estado=1 LIMIT 1")->fetch_object()->id;
} else {
  $id_work=$_GET['e'];
}

if (!preg_match("/^[0-9]+$/", $_GET['cod'])) $id_work=1;

$rs=$con->query("SELECT * FROM ticket WHERE codigo='".$_GET['cod']."' AND evento='".$id_work."' LIMIT 1");
if($rs->num_rows==0){
  echo 'Codigo inexistente';
  exit;
}
$rw=$rs->fetch_object();
if($rw->estado==0){
  echo '<h1>El usuario no posee un ticket activo</h1>';
  exit;
}
$archimg=date("Ymdhis").'_'.$_GET['cod'];
$img_base64_encoded = 'data:image/png;base64,'.$rw->ticket;
// Include the main TCPDF library (search for installation path).
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('LALEXPO');
$pdf->SetTitle('Ticket de ingreso');
// set default header data
#$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);
// set header and footer fonts
#$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
#$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
#$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
#$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
#$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
#$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
#$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// set image scale factor
#$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// ---------------------------------------------------------
// set font
#$pdf->SetFont('dejavusans', '', 10);
// add a page
$infoadic='<p class="px-3">
          Le damos la Bienvenida a LALEXPO! Le estamos confirmando su participación en este evento y le informamos que para ingresar a la locación del Congreso, usted deberá presentar este tiquete ya sea impreso o en su teléfono, junto con su identificación (Cédula o Pasaporte).           De igual manera le recordamos que la acreditación de participantes iniciara a las 9:00 Am. Por ser un Evento de negocios y de carácter privado, pedimos seguir las siguientes normas para asegurar su participación en el transcurso del evento:          - No se permite el ingreso a menores de edad al evento. - Para el ingreso al Hotel y locaciones del Evento el uso de Escarapela es obligatorio. - Las escarapelas son solo para la persona registrada, no se pueden transferir. Esto también quiere decir que usted no podrá recoger escarapelas de otras personas.           - Solo se permite el ingreso de publicidad a los patrocinadores. - Si desea ser patrocinador contáctenos info@lalexpo.com , hay paquetes para todo tipo de Empresas y Presupuestos. - El ingreso de Cámaras y Videocámaras profesionales de cualquier tipo esta totalmente prohibido. - Por ser este evento de carácter participativo, social y formativo las buenas maneras son deseadas al fin de mantener el orden y la cordura durante el desarrollo de todo el programa.           - Durante el Evento se solicita vestimenta Formal, SemiFormal o adecuada para la ocasión. PREPÁRESE PARA HACER GRANDES NEGOCIOS, APRENDER INTENSIVAMENTE EN LOS SEMINARIOS Y FESTEJAR COMO NUNCA ANTES !!! </p>';
$path='banners/';
$base64 = explode(',', $img_base64_encoded);
$base64d = base64_decode($base64[1]);
$im = imagecreatefromstring($base64d);
if ($im !== false) {
    //header('Content-Type: image/png');
    imagejpeg($im, $path . $archimg.".jpg");
    imagedestroy($im);
}else {
    echo 'error al crear la imagen';
}
$pdf->AddPage();
$pdf->writeHTMLCell(0, 30, '', '', '<img src="' .$path . $archimg.'.jpg"/>'.$infoadic, 1, 1, 0, true, 'C', true);
unlink($path . $archimg.".jpg");
/*
$img = '<img src="' . $img_base64_encoded . '" />';
$pdf->writeHTML($img, true, false, true, false, '');*/
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// reset pointer to the last page
$pdf->lastPage();
// ---------------------------------------------------------
//Close and output PDF document
if(empty($_GET['desc'])){
  $_GET['desc']='I';
}
$pdf->Output('Ticket_de_ingreso.pdf', $_GET['desc']);
//============================================================+
// END OF FILE
//============================================================+