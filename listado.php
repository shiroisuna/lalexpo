<?
include('../includes/conexion.php');
include('includes/config.php');
$secAct=$sec->getSeccion($_GET['cat'],$_GET['obj']);
$limit=15;
$desde=((int)$_GET['p'])*$limit;

if (substr($secAct->selectList, 0, 4)=='CALL') {
  $query=$secAct->selectList;
} else {
  $query=$secAct->selectList.' limit '.$desde.','.$limit;
}

$rs=$con->query($query);
$errorConsulta='';
if(!empty($con->error)){
  $errorConsulta=$con->error;
}
if (substr($secAct->selectList, 0, 4)=='CALL') {
  $total=$rs->num_rows;
} else {  
  $total=$con->query('select FOUND_ROWS() total')->fetch_object()->total;
}
include('includes/header.php');
include('includes/top.php');
include('includes/menu.php');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="<?=$secAct->classIco?> nav-icon"></i> <?=$secAct->nombre?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item active"><?=$secAct->nombre?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listado de <?=$secAct->nombre?>
                <? if(isset($secAct->export)){ ?>
                <a href="contenido.php?cat=3&obj=7&getLista=1&_cat=<?=$_GET['cat']?>&_obj=<?=$_GET['obj']?>"  class="btn btn-info" style="float: right;background:#548a7f !important">Exportar</a>
                <? }?>
                <? if($secAct->enConstruccion!=1 && !empty($secAct->nuevo)){?>
                <a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>"  class="btn btn-info" style="float: right;color:#FFF; margin-right: 10px;"><?=$secAct->nuevo?></a>
                <? } ?>
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <? if($secAct->enConstruccion!=1){
                  if(empty($errorConsulta)){
                  $archivo='listados/list_'.$_GET['cat'].'_'.$_GET['obj'].'.php';
                  if(!is_file($archivo)){?>
                  <center style="font-size:2rem">No se encontró el archivo: <?=$archivo?></center>
                  <? }else{
                    include($archivo);
                  }
                  }else{?>
                  <center style="font-size:2rem;color:red"><?=$errorConsulta?></center>
                  <?}
                }else{?>
                  <center style="font-size:2rem">En construccion</center>
                <? }?>
              </div>
              <!-- /.card-body -->
              <? $paginas=ceil($total/$limit)-1;
              if($paginas>0){?>
              <div class="card-footer clearfix">
                <div class="float-left"><?=$total?> Registros</div>
                <ul class="pagination pagination-sm m-0 float-right">
                  <?
                  $act=(int)$_GET['p'];
                  if($_GET['p']>0){?>
                  <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.($act-1)?>&cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&filtro=<?=urlencode(serialize($_POST['filtro']))?>">Anterior</a></li>
                  <? }
                  for($i=($act-5);$i<$act;$i++){
                    if($i<0) continue;?>
                    <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.$i?>&cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&filtro=<?=urlencode(serialize($_POST['filtro']))?>"><?=$i+1?></a></li>
                  <? }?>
                  <li class="page-item active"><a class="page-link" href="javascript:;"><?=$act+1?></a></li>
                  <?for($i=$act+1;$i<=$act+5;$i++){
                    if($i>$paginas) break;?>
                    <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.$i?>&cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&filtro=<?=urlencode(serialize($_POST['filtro']))?>"><?=$i+1?></a></li>
                  <? }?>
                  <? if($paginas>$act){?>
                  <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.($act+1)?>&cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&filtro=<?=urlencode(serialize($_POST['filtro']))?>">Siguiente</a></li>
                  <? } ?>
                </ul>
              </div>
              <? } ?>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include('includes/footer.php')?>