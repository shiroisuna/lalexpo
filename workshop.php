<?
include('includes/conexion.php');
include('includes/idioma.php');
session_start();
$rw_work=$con->query("SELECT * FROM workshop WHERE estado=1 LIMIT 1")->fetch_object();
// $imgBannerPrin='/images/banners/'.$rw_work->imagen_en;

$lg=new idioma($_GET['lg']);
$lg->seccion(5);

$banner=$con->query("SELECT * FROM banners_principal WHERE banner='workshop' AND activo=1 LIMIT 1")->fetch_object();
if ($lg->idioma=='es') {
  $imgBannerPrin='/images/banners/'.$banner->archivo_es;
} else {
  $imgBannerPrin='/images/banners/'.$banner->archivo_en;
}

$numTickets=$con->query("SELECT COUNT(*) total FROM ticket WHERE tipo=2 AND evento=".$rw_work->id)->fetch_object()->total;
$workshop=$con->query("SELECT * FROM ticket WHERE id_usuario='".$_SESSION['id']."' AND tipo=2 AND evento='".$rw_work->id."' LIMIT 1 ")->fetch_object();

include('includes/header.php')?>
 <style>
  .bkg{
    background-color: #2d2d2d;
    display: table;
  }
  .bkg .txt01{
    width: 49%;
    position: relative;
    display:table-cell;
    vertical-align: top;
    padding-top: 4em;
  }
  .bkg .txt03{
    text-align: center;
  }
  .bkg h1{
    font-family:AspiraBlack,Verdana;
    font-size:8em;
    color:#d0a951;
    margin:0px;
    padding:0px;
    display:inline-block;
    line-height: 80%;
    text-align: center;
    letter-spacing: -0.1em;
    width: 100%;
  }
  .bkg h1 span{
    color:#FFF;
  }
  .bkg .txt03{
    font-family: AspiraLight,Verdana;
    font-size: 5em;
    color: #FFF;
    display: inline-block;
    padding: 0 9%;
  }
  .bkg .padd{
    text-align: center;
  }
  </style>
  <script>
  _urlLogin='workshop.php'
  </script>
    <div class="content">
      <div  class="section">
        <?
        if($_GET['lg']=='es'){
          $titulo=$rw_work->titulo_es;
          $descripcion=$rw_work->descripcion_es;
          $itinerario=$rw_work->itinerario_es;
          $imagen=$rw_work->imagen_es;
        }else{
          $titulo=$rw_work->titulo_en;
          $descripcion=$rw_work->descripcion_en;
          $itinerario=$rw_work->itinerario_en;
          $imagen=$rw_work->imagen_en;
        }

          ?>
        <table  style="margin:auto;margin-top:20px;font-size: 2em;font-family: Aspira,Verdana;font-size: 18px;width:85%;max-width: 1200px;">
          <tr>
            <td colspan="2" style="text-align: center;padding-bottom:30px;">
              <span style="font-family: AspiraBold,Verdana;font-size:2em;color:#a6212f;"><?=$titulo?></span>
            </td>
          </tr>
          <tr>
            <td style="width: 40%;vertical-align: top;padding:20px;padding-top:0">
              <? if(!empty($imagen)){?>
              <img src="/images/banners/<?=$imagen?>" style="width:100%;border:1px solid #777" />
              <? } ?>
              <div style="text-align:center;font-size: 0.9em;padding-left:10px;padding-top: 10px;">
              <? if ($rw_work->estado) { ?>
                <? if (empty($workshop) && $numTickets<$rw_work->tickets) { ?>
                <a class="botLink" onclick="$('html,body').animate({scrollTop:0});showForm('<?=(empty($_SESSION['id']))?'preLogin':'solicitudDatosWork'?>')" style="font-size:0.8em;padding: 8px 23px;">Reservar</a>
                <? } elseif (isset($_SESSION['login']) && !empty($workshop) && $workshop->estado==0) { ?>
                  <a class="botLink" onclick="window.location='payments.php#menu'" style="font-size:0.8em;padding: 8px 23px;">Pagar</a>
                <? } ?>
              <? } ?>
              </div>
            </td>
            <td style="vertical-align: top;text-align: justify;">
              <?=nl2br($descripcion)?>
            </td>
          </tr>
          <? if(!empty($itinerario)){?>
          <tr>
            <td colspan="2" style="padding: 0 50px 100px 50px;font-size: 0.9em;color: #333;">
            <span style="font-family: AspiraBold,Verdana;font-size:1.6em;color:#a6212f">Programación</span>
            <br />
              <span style="white-space:pre"><?=$itinerario?></span><br /><br />
              <center>
              <? if ($rw_work->estado) { ?>
                <? if (empty($workshop) && $numTickets<$rw_work->tickets) { ?>
                <a class="botLink" onclick="$('html,body').animate({scrollTop:0});showForm('<?=(empty($_SESSION['id']))?'preLogin':'solicitudDatosWork'?>')" style="font-size:1em">Reservar</a>
                <? } elseif (isset($_SESSION['login']) && !empty($workshop)  && $workshop->estado==0) { ?>
                  <a class="botLink" onclick="window.location='payments.php#menu'" style="font-size:1em">Pagar</a>
                <? } elseif (isset($_SESSION['login']) && $workshop->estado) { ?>
                  <img style="width: 100%" class="img-fluid" src="data:image/png;base64,<?=$workshop->ticket?>">
                  <a class="botLink" href="/getTicket.php?cod=<?=$workshop->codigo?>&desc=D">PDF</a>
                <? } ?>
              <? } ?>
              </center>
            </td>
          </tr>
          <? } ?>
        </table>
      </div>
      <? if ($rw_work->estado && $numTickets<$rw_work->tickets && !empty($_SESSION['id'])) { ?>
      <form class="preLogin" id="solicitudDatosWork" onsubmit="">
        <div class="login-content">
          <span class="close" onclick="closeForm()">&times;</span>
          <div class="log_encabezado">
            <img src="/img/logow.png" alt="LALEXPO" />
          </div>
          <div class="contentBorder">
            <div class="log_tit">Reservar</div>
            <div class="log_descrip">Para generar su cupon de pago, ingrese los datos del asistente.</div>
            <div id="log_leyenda" class="log_descrip" maxlength="100"></div>
            <div class="log_textImp">Nombre y apellido:</div>
            <input type="text" name="w_nomape" id="w_nomape" maxlength="100" />
            <div class="log_textImp">Telefono:</div>
            <input type="text" name="w_tel" id="w_tel" maxlength="100" />
            <div class="log_textImp">Documento:</div>
            <input type="text" name="w_doc" id="w_doc" maxlength="100" />
            <div id="memcomp1" class="login_botones">
              <input type="hidden" name="usrId" id="usrId" value="<?=$_SESSION['id']?>">
              <a class="botLink" onclick="generarLinkPagoWorkshop(<?=$rw_work->id?>);"><?=$lg->general->btn_buy ?></a>
              <br />
              <a class="botLink" onclick="closeForm()"><?=$lg->general->btn_cancel ?></a>
            </div>
            <div id="memcomp2" style="display: none;">
              <div class="log_descrip"><?=$lg->general->membership_buy_complete ?></div>
              <a class="botLink" onclick="$('#memcomp2').hide();$('#memcomp1').show();closeForm();window.location='payments.php#menu'"><?=$lg->general->membership_btn_finish ?></a>
            </div>
          </div>
        </div>
      </form>
      <? } ?>
    <? include('includes/footer.php')?>