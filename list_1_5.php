<table class="table table-bordered">
  <thead>
    <tr>
      <th style="width: 10px">#</th>
      <th>Nombre</th>
      <th>Email</th>
      <th>Pais</th>
      <th>Categoria</th>
      <th>Telefono</th>
      <th>Info-Alert</th>
      <th style="width:70px"></th>
    </tr>
  </thead>
  <tbody id="lista">
    <tr>
      <td colspan="8" style="text-align:center">Cargando datos...<br /><img src="img/loading.gif" /></td>
    </tr>
  </tbody>
</table>
<script>
$(document).ready(function(){
  getLista()
})
function getLista(){
  msg.text('Actualizando lista...<br /><img src="img/loading.gif" />').load()
  $.ajax({
    url:'contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>',
    type:'post',data:'getLista=1',dataType:'json',
    success:function(d){
      if(d.length==0){
        $('#lista').html('<tr><td colspan="8" style="text-align:center">No se encontraron datos.</td></tr>')
        msg.close()
        return false
      }
      var i,t='',c=1,add,add2
      for(i in d){
        add=(d[i].visto!='0')?'#FFF':'#ade7ff'
        if(d[i].visto=='0'){
          add2='<i title="Marcar como visto" onclick="marcarLeido('+d[i].id+')" class="far fa-square" style="cursor:pointer" aria-hidden="true"></i>'
        }else{
          add2='<i class="far fa-check-square" aria-hidden="true"></i>'
        }
        t+='<tr style="background:'+add+'">'
          t+='<td>'+c+'</td>'
          t+='<td>'+d[i].nombre+' '+d[i].apellido+'</td>'
          t+='<td>'+d[i].email+'</td>'
          t+='<td>'+d[i].pais+'</td>'
          t+='<td>'+d[i].categoria+'</td>'
          t+='<td>'+d[i].telefono+'</td>'
          t+='<td>'+d[i].encontro+'</td>'
          t+='<td>'+add2+'&nbsp; &nbsp; &nbsp; <i class="far fa-trash-alt" title="Eliminar" style="cursor:pointer" onclick="elim('+d[i].id+')"></i></td>'
        t+='</tr>'
        c++
      }
      $('#lista').html(t)
      msg.close()
    },error:function(d){
      msg.text(d.responseText).load().aceptar()
    }
  })
}
function marcarLeido(id){
  msg.text('¿Desea marcar como leido?').load().confirm(function(){
    msg.text('Marcando como leido...<br /><img src="img/loading.gif" />').load()
    $.ajax({
      url:'contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>',
      type:'post',data:'marcarLeido=1&id='+id,
      success:function(d){
        if(d==1){
          getLista()
        }else{
          msg.text(d).load().aceptar()
        }
      },error:function(d){
        msg.text(d.responseText).load().aceptar()
      }
    })
  })
}
function elim(id){
  msg.text('Este proceso es irreversible.<br /><br />¿Usted desea continuar con este procedimiento?').load().confirm(function(){
    msg.text('Eliminando...<br /><img src="img/loading.gif" />').load()
    $.ajax({
      url:'contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>',
      type:'post',data:'elim=1&id='+id,
      success:function(d){
        if(d==1){
          getLista()
        }else{
          msg.text(d).load().aceptar()
        }
      },error:function(d){
        msg.text(d.responseText).load().aceptar()
      }
    })
  })
}
</script>