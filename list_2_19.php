<form  method="post" action="<?=$_SERVER["REQUEST_URI"]?>" >
  <div class="col-10">    
      <label>Categoria</label>
      <select class="form-control" name="categoria" id="categoria"  style="display: inline-block !important; width: 50% !important; ">
        <option value="">Todas</option>
        <? 
        $rs_categorias=$con->query("SELECT id, nombre_es, nombre_en FROM premios_categorias WHERE estado=1");
        while($rw2=$rs_categorias->fetch_object()){
          $add=$rw2->id==$_POST['categoria']?'selected':'';
        ?>
        <option value="<?=$rw2->id?>" <?=$add?>><?=$rw2->nombre_es?></option>
        <? } ?>
      </select>
    <button type="submit" class="btn btn-info" >Ver</button>
  </div>
</form>
<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th>Nominado</th>
    <th>Categoría</th>
    <th>Votos</th>
    <th style="width: 130px"></th>
  </tr>
  <?
  if($rs->num_rows>0){
  $numfil=1;
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><?=$numfil?></td>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>"><?=$rw->usuario?></a></td>
    <td><?=$rw->categoria?></td>
    <td><?=$rw->votos?></td>
    <td><a href="javascript:;" onclick="msg.text('Indique la cantidad de votos a generar: <br/> <input type=number id=cv value=1 min=1 />').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&crear=1&cnt='+$('#cv').val()+'&nom=<?=$rw->id?>'})" title="Crear voto"><i class="fas fa-file-download"></i> Crear voto</a></td>
  </tr>
  <? $numfil++; } }else{ ?>
  <tr>
    <td colspan="5">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>