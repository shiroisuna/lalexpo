<?php
/**
 * Created by PhpStorm.
 * User: Wilderyut
 * Date: 26/07/2015
 * Time: 11:03
 */
require_once 'ConnectionModel.php';
require_once 'Query.php';

class ConnectionMySQL {

    private function connection(){
        $con= new ConnectionModel();

        $pdo="";
        try{
            $pdo = new PDO(
                'mysql:'.$con->host.'=hostname;dbname='.$con->database,
                $con->user,
                $con->pass,
                array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
                )
            );
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }catch(PDOException $e){
            echo "Problema para conectar a la base de datos.<br/>";
            error_log("(".DateUtil::getDate().") ERROR: " . $e->getMessage()."\n", 3, 'log/connection.log');
        }
        return $pdo;
    }

    public function Select($obj,$id,$field=null){
        $sql= new Query();
        $rs=null;
        try{
            $st= $this::connection()->query($sql->SelectById($obj,$id,$field));
            $rs= $st->fetch();
        }catch (PDOException $e){
            error_log("(".DateUtil::getDate().") ERROR: " . $e->getMessage()."\n", 3, 'log/connection.log');
        }
        return $rs;
    }

    public function SelectAll($table,$field=null){
        $sql= new Query();
        $rs=null;
        try{
            $st=$this::connection()->query($sql->Select($table,$field));
            $rs= $st->fetchAll();
        }catch (PDOException $e){
            error_log("(".DateUtil::getDate().") ERROR: " . $e->getMessage()."\n", 3, 'log/connection.log');
        }
        return $rs;
    }

    public function SelectWhere($table,$condition){
        $sql= new Query();
        $rs=null;
        try{
            $rs= $this::connection()->query($sql->Select($table)." where ".$condition);
        }catch (PDOException $e){
            error_log("(".DateUtil::getDate().") ERROR: " . $e->getMessage()."\n", 3, 'log/connection.log');
        }
        return $rs;
    }

    public function Update($table,$array,$id){
        $sql= new Query();
        $rs=null;
        try{
            $st= $this::connection()->prepare($sql->Update($table,$array,$id));
            $st->execute();
            if($this::connection()->errorCode()){
                $rs= array("update"=>"1","message"=>"Actualización completada");
            }else{
                $rs= array("update"=>"0","message"=>"Error al actualizar el registro, consulte al administrador del sistema");
            }
        }catch (PDOException $e){
            $rs= array("update"=>"0","message"=>"Error al insertar el registro, consulte al administrador del sistema");
            $rs["code"]=$e->getCode();
            error_log("(".DateUtil::getDate().") ERROR: " . $e->getMessage()."\n", 3, 'log/connection.log');
        }
        return $rs;
    }

    public function Insert($table,$array){
        $sql= new Query();
        $rs=null;
        try{

            $st= $this::connection()->prepare($sql->InsertInto($table,$array));
            $st->execute();
            if($this::connection()->errorCode()){
                $rs= array("update"=>"1","message"=>"Se insertó correctamente");
            }else{
                $rs= array("update"=>"0","message"=>"Error al insertar el registro en la tabla ".$table.", consulte al administrador del sistema");
            }
        }catch (PDOException $e){
            $rs= array("update"=>"0","message"=>"Error al insertar el registro en la tabla ".$table.", consulte al administrador del sistema");
            $rs["code"]=$e->getCode();
            error_log("(".DateUtil::getDate().") ERROR: " . $e->getMessage()."\n", 3, 'log/connection.log');
        }
        return $rs;
    }

    public function DeleteWhere($table,$condition){
        $sql= new Query();
        $rs=null;
        try{
            $st= $this::connection()->prepare($sql->DeleteWhere($table,$condition));
            $st->execute();
            if($this::connection()->errorCode()){
                $rs= array("update"=>"1","message"=>"Se eliminó correctamente");
            }else{
                $rs= array("update"=>"0","message"=>"Error al eliminar el registro, consulte al administrador del sistema");
            }
        }catch (PDOException $e){
            $rs= array("update"=>"0","message"=>"Error al insertar el registro, consulte al administrador del sistema");
            $rs["code"]=$e->getCode();
            error_log("(".DateUtil::getDate().") ERROR: " . $e->getMessage()."\n", 3, 'log/connection.log');
        }
        return $rs;
    }

    public function selectBySQL($sql){
        $rs=null;
        try{
            $st= $this::connection()->query($sql);
            $rs= $st->fetchAll();
        }catch (PDOException $e){
            error_log("(".DateUtil::getDate().") ERROR: " . $e->getMessage()."\n", 3, 'log/connection.log');
        }
        return $rs;
    }
    public function selectBySQLOne($sql){
        $rs=null;
        try{
            $st= $this::connection()->query($sql);
            $rs= $st->fetch();
        }catch (PDOException $e){
            error_log("(".DateUtil::getDate().") ERROR: " . $e->getMessage()."\n", 3, 'log/connection.log');
        }
        return $rs;
    }

    public function lastId($table){
        $rs=null;
        try{
            $rs= $this::connection()->lastInsertId($table);
        }catch (PDOException $e){
            error_log("(".DateUtil::getDate().") ERROR: " . $e->getMessage()."\n", 3, 'log/connection.log');
        }
        return $rs;
    }

    public function createDB(){
        $con= new ConnectionModel();
        $q= "CREATE DATABASE IF NOT EXISTS ".$con->database;
        $rs=false;
        try{
            $dbh = new PDO("mysql:host=".$con->host, $con->user, $con->pass);
            $dbh->exec($q)
            or die(print_r($dbh->errorInfo(), true));
            $rs= true;
        }catch (PDOException $e){

            error_log("(".DateUtil::getDate().") ERROR: " . $e->getMessage()."\n", 3, 'log/connection.log');
        }
        return $rs;
    }
}


?> 