<?php
/**
 * Created by PhpStorm.
 * User: waular
 * Date: 09/05/2016
 * Time: 09:45 AM
 */

class Util {

    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function  parseJson($path){
        $str_datos = file_get_contents($path);
        return json_decode($str_datos,true);
    }

    public static function myUrlEncode($string) {
        return strtolower(str_replace(" ","-", $string));
    }

    public static function showJson($array){

        header('Content-type: application/json; charset=utf-8');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: origin, content-type, accept, authorization');
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS, HEA');
        header('Access-Control-Max-Age: 1209600');

        echo json_encode($array);
    }

    public static function getJsonPost(){
        return  json_decode(file_get_contents('php://input'), true);
    }

    /**
     * Convierte un array en objeto
     * @param $array
     * @return stdClass
     */
    public static function toObject($array)
    {
        $object = new stdClass();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = Util::toObject($value);
            }
            $object->$key = $value;
        }
        return $object;
    }
    public static function toArrayObjectResult($rs){
        $result=array();
        foreach($rs as $row){
            array_push($result, Util::toObjectResult($row));
        }
        return $result;
    }

    public static function toObjectResult($rs){
        $rs = Util::toObject($rs);
        
        foreach($rs as $key=>$value){
            if(is_numeric($key)){
                unset($rs->$key);
            }
        }
        return $rs;
    }
    /**
     * Redirect with POST data.
     *
     * @param string $url URL.
     * @param array $post_data POST data. Example: array('foo' => 'var', 'id' => 123)
     * @param array $headers Optional. Extra headers to send.
     */
    public static function redirectPost($url, array $data, array $headers = null) {
        $params = array(
            'http' => array(
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );
        if (!is_null($headers)) {
            $params['http']['header'] = '';
            foreach ($headers as $k => $v) {
                $params['http']['header'] .= "$k: $v\n";
            }
        }
        $ctx = stream_context_create($params);
        $fp = @fopen($url, 'rb', false, $ctx);
        if ($fp) {
            echo @stream_get_contents($fp);
            die();
        } else {
            // Error
            throw new Exception("Error loading '$url', $php_errormsg");
        }
    }

}

?> 