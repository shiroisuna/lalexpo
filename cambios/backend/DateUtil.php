<?php
/**
 * Created by PhpStorm.
 * User: waular
 * Date: 22/08/2016
 * Time: 03:20 PM
 */

class DateUtil{
    public static function setTimeZone($zone){
        date_default_timezone_set($zone);
    }

    public static function getDate(){
        $date = new DateTime();
        return $date->format('Y-m-d H:i:s');
    }

    public static function addDay($date,$day){
        $date->add(new DateInterval('P'.$day.'D'));
        return $date;
    }

    public static function getTimestamp($date){
        return $date->getTimestamp();
    }

}
?>
 