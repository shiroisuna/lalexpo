<?php
	session_start();
	include('includes/conexion.php');
	include('includes/idioma.php');
	$lg=new idioma($_GET['lg']);
	$lg->seccion(7);
	include('includes/header.php');
	
    require_once 'backend/Controller.php';
    require_once 'backend/controllers/ProductController.php';
    $productController = new ProductController();
	
	$producto = null;
	if(isset($_GET["id"])){
		$producto = $productController->find($_GET["id"]);
		
    }else{
        //header('Location: store.php');
    }
 ?>
 <div class="product-detail">
    <!-- breadcrumb -->
	<div class="migaja">
		<a href="index.html" class="">
			Tienda
			<i class="fa fa-angle-right " aria-hidden="true"></i>
		</a>
		<span class="end-migaja">
			<?=$producto->nombre_es?>
		</span>
	</div>
    <!-- Product Detail -->
	<div class="product">
		<div class="product-img">
			<?php
				if (!is_readable('images/productos/'.$producto->archivo)) {
					$imagen_prod='../img/default.gif';
				} else {
					$imagen_prod='../images/productos/'.$producto->archivo;
				}
			?>
			<img src="<?=$imagen_prod?>" alt="">
		</div>
		<div class="product-content">
			<form action="backend/carrito.php?crear=true" method="post" id="form-product">
				<h3 class="title"><?=$producto->nombre_es?></h3>
				<h2 class="price"><strong>$ <?=$producto->valor?></strong></h2>
				<p class="description"><?=$producto->descripcion_es?></p>
				<input type="hidden" name="addcart" value="1" />
				<input type="hidden" name="codprod" value="<?=$producto->id?>" />
				<input type="hidden" name="crear" value="1" />
				<input type="hidden" name="valor" value="<?=$producto->valor?>" />
				<input type="hidden" name="nomprod" value="<?=$producto->nombre_es?>" />
				<div class="group-input">
					<label for="size">Tamaño</label>
					<select name="talla" id="size" class="input-form-control">
						<?php  foreach($producto->detalle as $detalle){ ?>
						<option value="<?=$detalle->talla?>"><?=strtoupper($detalle->talla)?></option>
						<?php } ?>
					</select>
				</div>
				<div class="group-input">
					<label for="color">Color</label>
					<select name="color" id="color" class="input-form-control">
					<?php  foreach($producto->detalle as $detalle){ ?>
						<option value="<?=$detalle->color?>"><?=strtoupper($detalle->color)?></option>
						<?php } ?>
					</select>
				</div>
				<div class="group-input">
					<label for="quantity">Cantidad</label>
					<input type="number" name="cantidad" id="quantity" class="input-form-control" value="1" min="1">
				</div>
				<div class="group-input">
					<button class="add-cart" type="submit" form="form-product" >
						Agregar al carrito
					</button>
				</div>
			</form>
		</div>
	</div>
 </div>
<div class="product-footer">
	<?php include('includes/footer.php')?>
</div>
 