<?php
	function cargarBloques($bloque, $idioma) {
		include('../includes/conexion.php');
		$page='';
		$fondo='';
		  $rs=$con->query("SELECT * FROM bloques WHERE estado=1 AND id='".$bloque."' LIMIT 1");
		  if ($rs->num_rows>0) {
	   	   	$rw=$rs->fetch_object();
	   	   	$fondo=' style="background-image:url(../images/banners/'.$rw->fondo.') !important;background-size: 100% 100%;background-repeat: no-repeat;" ';
	   	    $page.='<section class="bloque" id="'.$rw->nombre.'" '.$fondo.'>';
	   	    if ($idioma=='es') {
				$page.=$rw->contenido1;
	   	    } else {
				$page.=$rw->contenido2;
	   	    }
      		$page.='</section>';	   		
	   	  } 
	    return $page;
	}
	
	function cargarBloque($bloque, $idioma) {
		include('../includes/conexion.php');
		$page='';
		$fondo='';
		try {
		  $rs=$con->query("SELECT * FROM bloques WHERE estado=1 AND nombre='".$bloque."' LIMIT 1");
		  if ($rs->num_rows>0) {
	   	   	$rw=$rs->fetch_object();
	   	   	$fondo=' style="color:#fff;background:url(../images/banners/'.$rw->fondo.') no-repeat;background-size: 100% 100%;" ';
	   	    $page.='<section class="bloque" id="'.$rw->nombre.'" '.$fondo.'>';
	   	    if ($idioma=='es') {
				$page.=nl2br($rw->contenido1);
	   	    } else {
				$page.=nl2br($rw->contenido2);
	   	    }
      		$page.='</section>';	   		
	   	  } 
		} finally {
		}
	    return $page;
	}
?>