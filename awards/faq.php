<?php
	$txtpreg=array("es"=>"Preguntas", "en"=>"Questions");
	$txtresp=array("es"=>"Respuesta", "en"=>"Answer");
	$page='       <ul class="tabs">';
	$rs=$con->query("SELECT id, nombre_es, nombre_en FROM faq_seccion WHERE habilitado=1 AND id=9");
	$pest='';
  	while($rw=$rs->fetch_object()){
  		if ($lg->idioma=='es') {
  			$titulo=$rw->nombre_es;
  		} else {
			$titulo=$rw->nombre_en;
  		}
  		$pest.='<li><a href="#tab'.$rw->id.'">'.$titulo.'</a></li>';
$cpest.='<div class="tab_content" id="tab'.$rw->id.'">
            <div class="preguntas">
              <h1>'.$txtpreg[$lg->idioma].'</h1>
              <div class="cajaPregunta">';
		$preg='';
		$resp='';
    		$rs2=$con->query("SELECT id, id_seccion, preg_es, preg_en, resp_es, resp_en FROM faq WHERE estado=1 AND id_seccion='".$rw->id."'");
    		while($rw2=$rs2->fetch_object()){
	          if ($lg->idioma=='es') {
	            $nombre=$rw2->preg_es;
	            $descri=$rw2->resp_es;
	          } else {
	            $nombre=$rw2->preg_en;
	            $descri=$rw2->resp_en;
	          }
				$preg.='<p id="sp'.$rw->id.''.$rw2->id.'" onclick="verResp('.$rw->id.''.$rw2->id.')">'.$nombre.'</p>';
				$resp.='<p id="rsp'.$rw->id.''.$rw2->id.'">'.$descri.'</p>';
    		}
		$cpest.=$preg;
		$cpest.='              </div>
            </div>
            <div class="respuesta">
              <h1>'.$txtresp[$lg->idioma].'</h1>
              <div class="cajaRespuesta">
                '.$resp.'
              </div>
            </div>
          </div>';
    }
	$page.=$pest.'</ul>';
	$page.='<div class="tab_container">
			'.$cpest.'
        </div>';
    echo $page;
?>