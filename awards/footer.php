<nav>
      <ul id="menu" class="menu">
      <li><a href="index.php" <?=setAct('index.php')?> >Inicio</a></li>
      <li><a href="sponsor.php" <?=setAct('sponsor.php')?>>Patrocinios</a></li>
      <li><a href="awards.php" <?=setAct('awards.php')?>>Votar</a></li>
      <li><a href="about.php" <?=setAct('about.php')?>>Acerca de</a></li>
      <li><a href="info.php" <?=setAct('info.php')?>>Info</a></li>
      </ul>
</nav>
<?
$fondos=$con->query("select fdo_footer,fdo_contacto from parametros")->fetch_object()?>
<div class="section10" style="background-image:url(../images/banners/<?=$fondos->fdo_contacto?>) !important">
        <header>
          <h1><?=$lg->general->frm_contact_title ?></h1>
        </header>
        <div class="pre2">
          <form class="cell" action="" id="contactarF" onsubmit="contactarF('<?=$_GET['lg']?>');return false">
            <span><?=$lg->general->frm_contact_name?></span>
            <input type="text" name="nombre" required="" />
            <span><?=$lg->general->frm_contact_email?></span>
            <input type="email" name="email" required="" />
            <span><?=$lg->general->frm_contact_subject?></span>
            <input type="text" name="asunto" />
            <span><?=$lg->general->frm_contact_message?></span>
            <textarea name="comentario" required="" style="background: rgba(11, 11, 19, 0.5);"></textarea><br />
            <center style="display:inline-block;padding: 14px 0;width: 100%;"><a class="botLink submit" id="btnEnvFoFoot" onclick="$('#btnEnvFormFoot').click()"><?=$lg->general->btn_submit?></a></center>
            <input type="submit" style="display: none;" id="btnEnvFormFoot" />
          </form>
          <div id="infoContact" class="cell">
            <div class="tits right">
              <div class="_subtit">LALEXPO INTERNATIONAL<br />
              Cali - Colombia</div>
              <div class="derr">
                <div style="animation-delay: .1s">
                  <span>Phone</span>
                  <p>USA:<br />
                  +1 347 568 0230</p>
                </div>
                <div style="animation-delay: .2s">
                  <span>Phone</span>
                  <p>Colombia:<br />
                  +57 313 525 3836 / +57 2 380 97 03</p>
                </div>
                <div style="animation-delay: .3s">
                  <span>Envelope</span>
                  <p>info@lalexpo.com</p>
                </div>
                <div style="animation-delay: .4s">
                  <span class="brands">Skype</span>
                  <p><?=$lg->general->contact_skype?></p>
                </div>
              </div>
              <div class="log"><img src="../img/logow.png" alt="LALEXPO" style="animation-delay: 1s" /></div>
            </div>
          </div>
        </div>
      </div>
    <footer style="background-image:url(../images/banners/<?=$fondos->fdo_footer?>) !important">
      <div class="celda01">
        <img src="../img/logow.png" alt="LALEXPO" />
      </div>
      <div class="celda02" style="position:relative">
        <div class="pre">
          <div class="tit_news"><?=$lg->general->newsletter?></div>
          <div class="dvImpNews">
            <div class="_c imp">
              <input type="text" id="inpNews02" />
            </div>
            <div class="_c">
              <a onclick="regisNews('inpNews02')"><?=$lg->general->btn_submit?></a>
            </div>
          </div>
          <div class="copy">
            © Lalexpo 2014 is a property of LALEXPO International.<br />
            All rights reserved. Reproduction in whole or in part is prohibited <br />
            <a href="#" style="color: #f1cc7c;">Get your Banners </a> | <a href="../terms.php" style="color: #f1cc7c;"> Terms and Conditions </a>
          </div>
        </div>
      </div>
    </footer>
    <div class="powerby">
    <a href="javascript:;" id="top" class="botLink" style="position:relative;margin: auto;top:-60px;z-index: 1;display:inline-block" ><?=$lg->general->btn_up ?></a>
        Desarrollado con todo el <img src="../img/heart.svg" /> por <a href="http://lacasacreativa.co" target="_blank">lacasacreativa.co</a>
      </div>
  </div>
<div class="redes">
  <a href="https://www.facebook.com/Lalexpo" target="_blank" title="Lalexpo"><span><i class="fa fa-facebook"></i></span></a>
  <a href="https://www.twitter.com/@vivelalexpo" target="_blank" title="@vivelalexpo"><span><i class="fa fa-twitter"></i></span></a>
  <a href="https://www.instagram.com/lal_expo" target="_blank" title="@lal_expo"><span><i class="fa fa-instagram"></i></span></a>
</div>
<div class="redes2">
  <? if ($_SESSION['membresia']==0) { ?>
  <a href="#compraMembresia" onclick="showForm('compraMembresia')" title="<?=$lg->general->membership?>"><span><i class="fa fa-certificate"></i><i><?=$lg->general->membership?></i></span></a>
  <? } ?>
</div>
  <div id="login" class="login"></div>
  <? if(empty($_SESSION['id'])){?>
  <form class="preLogin" id="preRegistrer" action="#" method="post" onsubmit="return registro()">
    <div class="login-content">
      <span class="close"  style="color:#000;" onclick="closeForm()">&times;</span>
      <div class="log_encabezado"  style="background: #fff;">
        <?php echo cargarLogosPatrocinadores(26); ?>
      </div>
      <script type="text/javascript">
        var txtreg=new Object();
        txtreg.register_model='<?=$lg->general->register_model?>';
        txtreg.register_nick='<?=$lg->general->register_nick?>';
        txtreg.register_pages='<?=$lg->general->register_pages?>';
        txtreg.register_studio='<?=$lg->general->register_studio?>';
        txtreg.register_model='<?=$lg->general->register_model?>';
        txtreg.register_company_name='<?=$lg->general->register_company_name?>';
        txtreg.register_cmodels='<?=$lg->general->register_cmodels?>';
        txtreg.register_website='<?=$lg->general->register_website?>';
        txtreg.register_webmaster='<?=$lg->general->register_webmaster?>';
        txtreg.register_sitepromoting='<?=$lg->general->register_sitepromoting?>';
        txtreg.register_other='<?=$lg->general->register_other?>';
        txtreg.register_segment='<?=$lg->general->register_segment?>';
        txtreg.register_submit='<?=$lg->general->register_submit?>';
        txtreg.register_complete='<?=$lg->general->register_complete?>';
        txtreg.register_complete_msg='<?=$lg->general->register_complete_msg?>';
        txtreg.register_selterms='<?=$lg->general->register_selterms?>';
      </script>
      <div id="frmReg" class="contentBorder">
        <div class="log_tit"><?=$lg->general->register_title ?></div>
        <div class="log_descrip"><?=$lg->general->register_descrip ?></div>
      <div id="reg_leyenda" class="log_descrip"></div>
        <div class="tb">
          <div class="tbCell padd_">
            <div class="log_textImp"><?=$lg->general->register_name ?></div>
            <input type="text" name="nombre" id="nombre"/>
          </div>
          <div class="tbCell">
            <div class="log_textImp"><?=$lg->general->register_lastname ?></div>
            <input type="text" name="apellido" id="apellido" />
          </div>
        </div>
        <div class="log_textImp"><?=$lg->general->register_email ?></div>
        <input type="email" name="email" id="email1" />
        <div class="log_textImp"><?=$lg->general->register_email2 ?></div>
        <input type="email" id="email2" onpaste="return false;" autocomplete="off" />
        <div class="log_textImp"><?=$lg->general->register_password ?></div>
        <input type="password" name="pass" id="pass1" />
        <div class="log_textImp"><?=$lg->general->register_password2 ?></div>
        <input type="password" id="pass2" />
        <div class="log_textImp"><?=$lg->general->register_id ?></div>
        <input type="text" name="dni" id="dni" />
        <div class="log_textImp"><?=$lg->general->register_gender ?></div>
        <select name="genero" id="genero" >
          <option value="1"><?=$lg->general->register_opt_gender_1 ?></option>
          <option value="2"><?=$lg->general->register_opt_gender_2 ?></option>
          <option value="3"><?=$lg->general->register_opt_gender_3 ?></option>
        </select>
        <div class="log_textImp"><?=$lg->general->register_country ?></div>
        <select class="bfh-countries" id="pais" name="pais_cod" data-country="paises" data-country="CO" onchange="" ></select>
        <div class="log_textImp"><?=$lg->general->register_state ?></div>
        <select class="bfh-states" name="estado_cod" id="estado" data-country="pais" onchange="cargarCiudades()" ></select>
        <div class="log_textImp"><?=$lg->general->register_city ?></div>
        <!-- <select id="ciudad"></select> -->
        <input type="text" id="ciudad" name="ciudad" />
        <div class="log_textImp"><?=$lg->general->register_language ?></div>
        <select id="idioma" name="idioma" >
          <option style="display: none;"></option>
          <option>Español</option>
          <option>English</option>
        </select>
        <div class="log_textImp"><?=$lg->general->register_phone ?></div>
        <input type="text" id="telefono" name="telefono" />
        <div class="log_textImp"><?=$lg->general->register_category ?></div>
        <select name="categoria" id="categoria" >
          <option value=""></option>
          <option value="2"><?=$lg->general->register_opt_category_1 ?></option>
          <option value="3"><?=$lg->general->register_opt_category_2 ?></option>
          <option value="4"><?=$lg->general->register_opt_category_3 ?></option>
          <option value="5"><?=$lg->general->register_opt_category_4 ?></option>
        </select><br/>
        <input type="checkbox" name="termninos" id="terminos" style="vertical-align: middle;" /><label class="log_textImp" for="terminos" style="margin-left: 5px;"><a href="terms.php" style="color: #f1cc7c;text-decoration: underline;" target="_blank"><?=$lg->general->register_terms ?></a></label>
        <div class="login_botones">
          <a href="javascript:;" class="botLink" onclick="registro()"><?=$lg->general->btn_register ?></a>
        </div>
      </div>
    </div>
  </form>
  <? } ?>
  <div id="aviso" style="display:none">
    <table width="100%">
      <tbody>
        <tr>
          <td align="center">
            <span id="avisoTxt"></span>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <form class="preLogin" id="comprarTicket" action="#" method="post" onsubmit="return false;">
    <div class="login-content">
      <span class="close" onclick="closeForm()">&times;</span>
      <div class="log_encabezado">
        <div class="log_tit"><?=$lg->general->buy_ticket_title ?></div>
      </div>
      <div class="contentBorder">
        <div class="log_descrip2"><?=$lg->general->buy_ticket_descrip ?></div>
        <div class="login_botones">
          <div id="comprar1"><a class="botLink" style="width:60%;" onclick="comprarTicket(2)"><?=$lg->general->btn_buy ?></a></div>
          <div id="comprar2">
            <table width="100%">
              <tbody>
                <tr>
                  <td width="120" style="font-size: 2em;"><?=$lg->general->buy_ticket_text_2 ?></td>
                  <td width="40"><input type="number" id="cantidad" min="1" value="1" onkeyup="totalTiquetes(this.value)" onchange="totalTiquetes(this.value)" /></td>
                  <td width="20" style="font-size: 2em;"> x </td>
                  <td width="80" style="font-size: 2em;"><?=$_SESSION['moneda']?> $<?=number_format($_SESSION['valorTicket'],2,',','.')?></td>
                  <td width="20" style="font-size: 2em;"> = </td>
                  <td id="txTotal" style="font-size: 2em;" width="80"><?=$_SESSION['moneda']?> $<?=number_format($_SESSION['valorTicket'],2,',','.')?></td>
                </tr>
              </tbody>
            </table>
            <a class="botLink" onclick="comprarTicket(1)"> < <?=$lg->general->btn_back ?></a>
            <a class="botLink" onclick="comprarTicket(3)"><?=$lg->general->btn_pay ?> ></a>
          </div>
          <div id="comprar3">
            <a class="botLink" onclick="comprarTicket(2)"> < <?=$lg->general->btn_back ?></a>
            <input type="hidden" id="descto" value="0">
            <input type="hidden" id="total" value="<?=$_SESSION['valorTicket']?>" />
            <? if(!empty($_SESSION['id'])){
              $dat=$con->query("SELECT nombre,apellido,email,telefono FROM usuarios where id=".$_SESSION['id'])->fetch_object();
              ?>
              <script>
              _m='<?=$_SESSION['moneda']?>';_v=<?=$_SESSION['valorTicket']?>
              </script>
            <img src="../img/banner_payu.png" style="cursor: pointer;" width="60%" onclick="getIdPreGoPayu({
              emailComprador:'<?=$dat->email?>',
              nombreComprador:'<?=$dat->nombre.' '.$dat->apellido?>',
              telefonoComprador:'<?=$dat->telefono?>',
              valor:$('#total').val(),
              descripcion:'Compra de ticket x '+$('.login_botones #cantidad').val(),
              idProducto:'2',
              idUsuario:'<?=$_SESSION['id']?>',
              datoReferencia:$('.login_botones #cantidad').val(),
              moneda:'<?=$_SESSION['moneda']?>'
            })" />
            <? } ?>
          </div>
        </div>
        <div class="log_descrip2"><?=$lg->general->buy_ticket_text_1.' '.$_SESSION['moneda'].' $'.$_SESSION['valorTicket'] ?></div>
        <img src="../img/divisor_hor.png" />
        <div class="log_descrip2"><?=$lg->general->buy_ticket_code_1 ?></div>
        <input type="text" name="codigo" id="codigo" placeholder="<?=$lg->general->buy_ticket_enter_code ?>" />
        <div class="login_botones2">
          <a class="botLink" onclick="validarCodigoDescuento($('#codigo').val())"><?=$lg->general->buy_ticket_confirm_code ?></a>
        </div>
        <div class="log_descrip2"><?=$lg->general->buy_ticket_code_2 ?></div>
        <div class="log_descrip2" style="text-align: left;">
          <?=$lg->general->buy_ticket_code_3 ?>
        </div>
      </div>
    </div>
  </form>
  <?
  if(!empty($_SESSION['id'])){
    include('../includes/form_payu.php');
  }?>
  <form class="preLogin" id="preLogin" onsubmit="return login()">
    <div class="login-content">
      <span class="close" onclick="closeForm()">&times;</span>
      <div class="log_encabezado">
        <img src="../img/logow.png" alt="LALEXPO" />
      </div>
      <div class="contentBorder">
        <div class="log_tit"><?=$lg->general->title_login ?></div>
        <div class="log_descrip"><?=$lg->general->desc_login ?></div>
        <div id="log_leyenda" class="log_descrip"></div>
        <div class="log_textImp"><?=$lg->general->login_email ?></div>
        <input type="email" name="user" required="" />
        <div class="log_textImp"><?=$lg->general->login_password ?></div>
        <input type="password" name="pass" required="" />
        <div class="log_textImp" style="cursor: pointer;" onclick="closeForm();setTimeout('showForm(\'forgPassword\')', 500)"><?=$lg->general->login_forgot_password ?></div>
        <div class="log_textImp" style="position: relative;top: -12px;float: right;cursor: pointer;" onclick="closeForm();setTimeout('showForm(\'preRegistrer\')', 500)"><?=$lg->general->login_signup ?></div>
        <input type="hidden" id="txtbtnlog" value="<?=$lg->general->btn_signing ?>-<?=$lg->general->loading ?>">
        <div class="login_botones">
          <a class="botLink" onclick="$('#preLogin .hiddSub').click()"><?=$lg->general->btn_login ?></a>
          <input type="submit" class="hiddSub" />
          <br />
          <a class="botLink" onclick="closeForm()"><?=$lg->general->btn_cancel ?></a>
        </div>
      </div>
    </div>
  </form>
    <form class="preLogin" id="forgPassword" >
    <div class="login-content">
      <span class="close" onclick="closeForm()">&times;</span>
      <div class="log_encabezado">
        <img src="../img/logow.png" alt="LALEXPO" />
      </div>
      <div class="contentBorder">
        <input type="hidden" id="txtbtnfpp" value="<?=$lg->general->btn_signing ?>-<?=$lg->general->loading ?>">
        <div class="log_tit"><?=$lg->general->forgot_password_title ?></div>
        <div id="frmForgPass">
          <div class="log_descrip"><?=$lg->general->forgot_password_descrip ?></div>
          <div id="log_leyenda" class="log_descrip"></div>
          <div class="log_textImp"><?=$lg->general->login_email ?></div>
          <input type="hidden" name="lgidioma" id="lgidioma" value="<?=$lg->idioma?>">
          <input type="email" name="pcemail" id="pcemail" required="" />
          <div id="txterror" class="log_descrip"></div>
          <div id="btnrecpas" class="login_botones">
            <a href="javascript:;" class="botLink" onclick="passRecover()"><?=$lg->general->btn_submit ?></a>
            <br />
            <a href="javascript:;" class="botLink" onclick="closeForm()"><?=$lg->general->btn_cancel ?></a>
          </div>
        </div>
      </div>
    </div>
  </form>

  <form class="preLogin" id="compraMembresia" onsubmit="">
    <div class="login-content">
      <span class="close" onclick="closeForm()">&times;</span>
      <div class="log_encabezado">
        <img src="../img/logow.png" alt="LALEXPO" />
      </div>
      <div class="contentBorder">
        <div class="log_tit"><?=$lg->general->membership_buy_title ?></div>
        <div class="log_descrip" style="text-align: left;"><?=cargarBloque('membresia', $lg->idioma) ?></div>
        <div id="log_leyenda" class="log_descrip"></div>
        
        <div id="memcomp1" class="login_botones">
        <? if(!empty($_SESSION['id'])) { ?>
          <input type="hidden" name="usrId" id="usrId" value="<?=$_SESSION['id']?>">
          <a class="botLink" onclick="compMemb();"><?=$lg->general->btn_buy ?></a>
        <? } else { ?>
          <a class="botLink" onclick="closeForm();setTimeout('showForm(\'preLogin\')',500)"><?=$lg->general->btn_logintobuy ?></a>
        <? } ?>  
          <br />
          <a class="botLink" onclick="closeForm()"><?=$lg->general->btn_cancel ?></a>
        </div>
        <div id="memcomp2" style="display: none;">
          <div class="log_descrip"><?=$lg->general->membership_buy_complete ?></div>
          <center><a class="botLink" onclick="$('#memcomp2').hide();closeForm();window.location='payments.php#menu'"><?=$lg->general->membership_btn_finish ?></a></center>
        </div>        
      </div>
    </div>
  </form>
  
<!-- Estilo añadido para Popup Membresia -->
  <style>
  .prePopup{
		display: none;
		width: 100%;
		position: absolute;
		top: 0;
		padding-top: 100px;
		z-index: 80;
	  }
  .popup-content{
		background-color: #2d2d2d;
		margin: auto;
		border: 0px;
		width: 80%;
		max-width: 600px;
		border-radius: 29px;
		overflow: hidden;
		color: #ddb762;
		position: relative;
	}
	.popup {
    display: none;
    position: fixed;
    z-index: 50;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.6);
}
  </style>  
<!--formulario añadido para Popup Membresia -->
  <div id="popup" class="popup"></div>
  <form class="prePopup" id="popupMembresia" onsubmit="">
    <div class="popup-content">
      <span class="close" onclick="closePopup();showForm('comprarTicket')">&times;</span>
     <img src="../img/popup.png" width="600" alt="LALEXPO" />

        
        <div id="memcomp1" class="login_botones">
        <? if(!empty($_SESSION['id'])) { ?>
          <input type="hidden" name="usrId" id="usrId" value="<?=$_SESSION['id']?>">
          <a class="botLink" onclick="setTimeout('closePopup();closeform()',500);setTimeout('showForm(\'compraMembresia\')',500)"><?=$lg->general->btn_buy ?></a>
        <? } else { ?>
          <a class="botLink" onclick="setTimeout('closePopup();closeform()',500);setTimeout('showForm(\'compraMembresia\')',500)"><?=$lg->general->btn_logintobuy ?></a>
        <? } ?>  
          <br />
          <a class="botLink" onclick="closePopup();showForm('comprarTicket')"><?=$lg->general->btn_cancel ?></a>
        </div>       
      </div>
    </div>
  </form>

</body>
</html>