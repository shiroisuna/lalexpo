<?php
include('../includes/conexion.php');
include('../includes/idioma.php');
$lg=new idioma($_GET['lg']);
$lg->seccion(2);
include('header.php');
?>
<link rel="stylesheet" href="../css/home.css" />
<style type="text/css">
.color_bk_home_title_nominations_categories {
    color: #fff !important;
}
.title-nm-cat-aw-at {
    width: 651px;
    height: 94px;
    color: #fff;
    background: url(../img/cat-nm-aw.png) no-repeat center center;
    padding-top: 14px;
    margin: 0 auto;
    font-family: AspiraBlack, Verdana;
    font-size:3.5em;
    text-align: center;
    text-transform: uppercase;
}
.fichaCat{
    display: inline-block;
    width: 251px;
    height: 167px;
    margin-right: 10px;
    background-image: url("../img/cat_award.png");
    background-repeat: no-repeat;
    padding-top: 50px;
    cursor: pointer;
  }
  .tituloCat{
    display: block;
    width: 150px;
    height: 80px;
    max-height: 80px;
    overflow: hidden;
    margin-right: auto;
    margin-left: auto;
    color: #c43337;
    font-family: AspiraBlack,Verdana;
    font-size: 2.3em;
    text-align: center;
    padding-top: 10%;
  }

</style>
    <div class="content">
      <div class="bkg" style="display: block;padding-top: 0 !important;">
        <div class="txt01"><?=$lg->seccion->presen_patrocin?><span>
        <?
        $rs=$con->query("select * from banners_principal where banner='1' limit 1");
        if ($rs->num_rows==1) {
          $rw=$rs->fetch_object();
          $ext=substr(strrchr($rw->archivo_es, "."), 1);
          $arch=($_GET['lg']=='es')?$rw->archivo_es:$rw->archivo_en; ?>
            <a href="<?=$rw->link?>" target="_blank"><img src="../images/banners/<?=$arch?>" style="width: 100%" /></a>
       <? } ?>
        </span></div>
        <div class="txt01" style="margin-bottom: 80px;display: block;"><?=$lg->seccion->community_sponsor ?><span>
        <?
        $rs=$con->query("select * from banners_principal where banner='2' limit 1");
        if ($rs->num_rows==1) {
          $rw=$rs->fetch_object();
          $ext=substr(strrchr($rw->archivo_es, "."), 1);
          $arch=($_GET['lg']=='es')?$rw->archivo_es:$rw->archivo_en; ?>
            <a href="<?=$rw->link?>" target="_blank"><img src="../images/banners/<?=$arch?>" style="width: 100%" /></a>
       <? } ?>
        </span></div>
      </div>

        <?
        $rs=$con->query("select * from banners_principal where banner='premios_1' limit 1");
        if ($rs->num_rows==1) {
          $rw=$rs->fetch_object();
          $ext=substr(strrchr($rw->archivo_es, "."), 1);
          $arch=($_GET['lg']=='es')?$rw->archivo_es:$rw->archivo_en; ?>
            <a href="<?=$rw->link?>" target="_blank"><img src="../images/banners/<?=$arch?>" style="width: 100%" /></a>
       <? } ?>

      <?php echo cargarBloque('premios_2', $lg->idioma); ?>

      <section class="section02">
        <header>
          <h1> <?=$lg->idioma?'Patrocinadores Principales':'Main Sponsors' ?></h1>
        </header>
        <div class="gale">
          <?php echo cargarLogosPatrocinadores(40); ?>
        </div>
      </section>

        <?
        $rs=$con->query("select * from banners_principal where banner='premios_4' limit 1");
        if ($rs->num_rows==1) {
          $rw=$rs->fetch_object();
          $ext=substr(strrchr($rw->archivo_es, "."), 1);
          $arch=($_GET['lg']=='es')?$rw->archivo_es:$rw->archivo_en; ?>
            <a href="<?=$rw->link?>" target="_blank"><img src="../images/banners/<?=$arch?>" style="width: 100%" /></a>
       <? } ?>      

      <section class="section02">
        <header>
          <h1>Sponsorship</h1>
        </header>
        <div class="gale">
          <?php echo cargarLogosPatrocinadores(41); ?>
        </div>
      </section>
      <section class="section05">
        <div>
          <span><?=$lg->general->newsletter?></span>
        </div>
        <div>
          <input type="text" class="inpBusc01" id="inpNews01" placeholder="<?=$lg->general->newsletter_hint ?>" />
        </div>
        <div>
          <a class="botLink submit" onclick="regisNews('inpNews01')"><?=$lg->general->btn_submit ?></a>
        </div>
      </section>
      <section class="section02">
        <header>
          <h1><?=$lg->idioma?'Nominación a los premios (Por categoría)':'Awards Nomination (Per Category)' ?></h1>
        </header>
        <div class="gale">
          <?php echo cargarLogosPatrocinadores(42); ?>
        </div>
      </section>
      <section class="section14">
          <div id="3" class="flexslider">
            <ul class="slides">
          <?
              $rs=$con->query("select * from banners_principal where banner='3'");
              while($rw=$rs->fetch_object()){
                $ext=substr(strrchr($rw->archivo_es, "."), 1);
                $arch=($_GET['lg']=='es')?$rw->archivo_es:$rw->archivo_en; ?>
                 <li>
                  <? if($ext=='mp4'){ ?>
                      <video autoplay="" muted="" loop="" id="myVideo" style="width: 100%;">
                        <source src="../images/banners/<?=$arch?>" type="video/mp4" />
                      </video>
                  <? } elseif(in_array($ext,array('png','gif','jpg'))) { ?>
                      <img src="../images/banners/<?=$arch?>"  style="width: 100%;" />
                  <? } ?>
                </li>
          <?
                }    ?>
            </ul>
          </div>
      </section>


      <section class="section13">
        <header>
          <h1><?=$lg->seccion->media_partner ?></h1>
        </header>
        <div id="medspo" class="cont01">
          <div class="dv02"><?php echo cargarLogosPatrocinadores(44); ?></div>
          <div class="dv01"><?=$lg->seccion->media_sponsors ?> </div>
          <div class="dv02"><?=$lg->seccion->media_sponsors_text ?></div>
          <div class="dv03">
            <div id="logosPatrocinios" class="flexslider">
              <ul class="slides">
              <?php echo cargarLogosPatrocinadores(43); ?>
              </ul>
            </div>
          </div>
        </div>
      </section>

      <h2 class="text-uppercase font-bold text-center title-nm-cat-aw-at color_bk_home_title_nominations_categories">Categorías de las nominaciones:</h2>
      <section  style="display: block;width: 80%;margin-left: auto;margin-right: auto;text-align: center;">
        <section><a href="awards.php">
        <div class="fichaCat" ><span class="tituloCat">Best European Cam Site</span></div>
        <div class="fichaCat" ><span class="tituloCat">Best North American Cam Site</span></div>
        <div class="fichaCat" ><span class="tituloCat">Best Tipping Cam Site</span></div>
        <div class="fichaCat" ><span class="tituloCat">Best Private Cam Site</span></div></a>
        </section>
      </section>
      <script type="text/javascript">
$(window).load(function() {
  $('#3').flexslider({ animation: "slide", controlNav: false, directionNav: false, video: true });
  $('#4').flexslider({ animation: "slide", controlNav: false, directionNav: false, video: true });
});
      </script>
    <?php include('footer.php')?>