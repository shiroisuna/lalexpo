<?php
  @session_start();
  if(empty($_GET['lg'])){
    #$locale=locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']);
    #$leng=explode('_',$locale);
    $tieneEsp=strpos(strtolower($_SERVER["HTTP_ACCEPT_LANGUAGE"]),'es');
    if($tieneEsp!==false){
      header('location: /es'.$_SERVER['PHP_SELF']);
      exit;
    }else{
      header('location: /en'.$_SERVER['PHP_SELF']);
      exit;
    }
  }
  if(!isset($lg)){ echo 'no se instancio lengua';exit;}
  include('logosPatrocinios.php');
  include('bloques.php');
  if ($_GET['logout']==1) { session_destroy(); header('Location: index.php'); }
  $rs_menu=$con->query("select id,url,nombre_es,nombre_en,tipo from menu where habilitado=1 and id_seccion=12 order by orden asc");
  $menus=array();
  if($rs_menu)
  while($rw=$rs_menu->fetch_object()){
    $url=($rw->tipo==2)?'segmento.php?id='.$rw->id:$rw->url;
    $nom=($_GET['lg']=='es')?$rw->nombre_es:$rw->nombre_en;
    $menus[]=array('url'=>$url,'nombre'=>$nom);
  }
  $params=$con->query("SELECT * FROM parametros LIMIT 1")->fetch_object();

  $pag=explode('/',$_SERVER['PHP_SELF']);
  $pag=$pag[count($pag)-1];
  if (!$params->estado_votaciones && $pag=='awards.php') header('location: index.php');
  if (!$params->estado_nominaciones && $pag=='nominations.php') header('location: index.php');
  if (!$params->estado_ganadores && $pag=='winners.php') header('location: index.php');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?=$_GET['lg']?>" xml:lang="<?=$_GET['lg']?>" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta charset="utf-8" />
  <meta http-equiv="Content-Language" content="<?=$_GET['lg']?>"/>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <link rel="shortcut icon" href="/fav.ico" type="image/x-icon" />
  <meta content="follow, index, all" name="robots" />
  <title>LALEXPO Awards</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
  <script src="../js/jquery.nice-select.min.js"></script>
  <script src="../js/inviewport.jquery.js"></script>
  <script src="../js/bootstrap-formhelpers.min.js"></script>
  <script src="../js/jquery.slicknav.min.js"></script>
  <script src="../js/general.js?<?=rand(1,999999)?>"></script>
  <link rel="stylesheet" href="../css/fonts.css" />
  <link rel="stylesheet" href="../css/slideshow.css" />
  <link rel="stylesheet" href="../css/animate.css" />
  <link rel="stylesheet" href="../css/nice-select.css" />
  <link rel="stylesheet" href="../css/general.css" />
  <link rel="stylesheet" href="../css/bootstrap-formhelpers.min.css"  />
  <link rel="stylesheet" href="../css/flexslider.css"  />
  <link rel="stylesheet" href="../css/colorbox.css" />
  <link rel="stylesheet" href="../css/slicknav.min.css" />
  <link rel="stylesheet" href="../css/responsitive.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="../js/jquery.flexslider.js"></script>
  <script>
  _lengua='<?=$_GET['lg']?>';
  _len=<?=json_encode($lg->general)?>;
  </script>
</head>
<body itemscope itemtype="http://schema.org/WebPage">
  <div id="todo">
    <div class="topH">
      <div class="logo">
        <a href="index.php"><img src="../img/logo.jpg" alt="LalExpo" /></a>
      </div>
      <div class="botones">
        <?php //echo cargarLogosPatrocinadores(26); ?>
        <?php if (!isset($_SESSION['login'])) { ?>
          <a href="#" onclick="showForm('preRegistrer')"><?=$lg->general->btn_register?></a>
          <a href="#" onclick="showForm('preLogin')"><?=$lg->general->btn_login?></a>
        <?php } else { ?>
          <a href="../dashboard.php" ><?=$lg->general->btn_profile?></a>
          <a href="../servicios/logout.php" ><?=$lg->general->btn_logout?></a>
        <?php } ?>
        <div class="btnsIdiomas">
        <? #$pa=substr($_SERVER['PHP_SELF'],4);?>
          <a href="/en<?=$_SERVER['PHP_SELF']?>" title="English"><img src="../img/eng.jpg" alt="English" /></a>
          <a href="/es<?=$_SERVER['PHP_SELF']?>" title="Español"><img src="../img/esp.jpg" alt="Español" /></a>
        </div>
      </div>
    </div>
    <? if(empty($imgBannerPrin)){?>
    <div class="sepCont">
      <div class="max">
        <div class="contador">
          <div class="grupo">
            <span id="dias">00</span>
            <label><?=$lg->general->days ?></label>
            <div class="bggr"></div>
          </div>
          <div class="grupo" style="margin: 0 10px;">
            <span id="horas">00</span>
            <label><?=$lg->general->hours ?></label>
            <div class="bggr"></div>
          </div>
          <div class="grupo">
            <span id="minutos">00</span>
            <label><?=$lg->general->minutes ?></label>
            <div class="bggr"></div>
          </div>
        </div>
      </div>
    </div>
    <? } ?>
<div id="principal" class="flexslider">
<? if(empty($imgBannerPrin)){?>
  <ul class="slides">
<?
    $rs=$con->query("select * from banners_principal where banner='premios'");
    while($rw=$rs->fetch_object()){
      $ext=substr(strrchr($rw->archivo_es, "."), 1);
      $arch=($_GET['lg']=='es')?$rw->archivo_es:$rw->archivo_en; ?>
       <li>
        <? if($ext=='mp4'){ ?>
            <video autoplay="" muted="" loop="" id="myVideo" style="width: 100%;">
              <source src="../images/banners/<?=$arch?>" type="video/mp4" />
            </video>
        <? } elseif(in_array($ext,array('png','gif','jpg'))) { ?>
            <img src="../images/banners/<?=$arch?>"  style="width: 100%;" />
        <? } ?>
      </li>
<?
      }    ?>
  </ul>
  <? }else{ ?>
  <img src="<?=$imgBannerPrin?>" style="width: 100%" />
  <? } ?>
</div>
<?php
function setAct($p){
  $pag=explode('/',$_SERVER['PHP_SELF']);
  $pag=$pag[count($pag)-1];
  return ($p==$pag)?'class="act"':'';
}?>
    <nav class="nav2" id="menuTop">
      <ul id="menu" class="menu">
        <? foreach($menus as $menu){?>
            <? if ($menu['url']=='awards.php' && $params->estado_votaciones) { ?>
              <li><a href="<?=$menu['url']?>#menu" <?php echo setAct($menu['url'])?>><?=$menu['nombre']?></a></li>
            <? } elseif ($menu['url']=='nominations.php' && $params->estado_nominaciones) { ?>
              <li><a href="<?=$menu['url']?>#menu" <?php echo setAct($menu['url'])?>><?=$menu['nombre']?></a></li>
            <? } elseif ($menu['url']=='winners.php' && $params->estado_ganadores) { ?>
              <li><a href="<?=$menu['url']?>#menu" <?php echo setAct($menu['url'])?>><?=$menu['nombre']?></a></li>
            <? } else { ?>
              <li><a href="<?=$menu['url']?>#menu" <?php echo setAct($menu['url'])?>><?=$menu['nombre']?></a></li>
            <? } ?>
      <? }?>
      </ul>
    </nav>
