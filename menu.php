<?
function setMenu($p){
  return substr(strrchr($_SERVER['PHP_SELF'],'/'),1,-4)==$p?'active':'';
}
function setCat($a,$r){
  $pag=substr(strrchr($_SERVER['PHP_SELF'],'/'),1,-4);
  $set=explode(',',$a);
  return (in_array($pag,$set))?$r:'';
}
?>
<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link bg-success" style="background-color: #2c667d !important; text-align: center !important;">
      <span class="brand-text font-weight-light" style="color: #FFF !important;"><img src="../img/logo.jpg" style="width: 100%;" /></span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <?
      $secciones=$sec->getSecciones();
      $categorias=$sec->getCategorias();
      ?>
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
         <? foreach($categorias as $idCat=>$cat){?>
        <li class="nav-item has-treeview <?=$idCat==$_GET['cat']?'menu-open':''?>">
            <a href="#" class="nav-link  <?=$idCat==$_GET['cat']?'active':''?>">
              <i class="nav-icon <?=$cat->classIco?>"></i>
              <p>
                <?=$cat->nombre?>
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <? foreach($secciones->$idCat as $idSec=>$secc){
                $url=empty($secc->url)?'listado.php?cat='.$idCat.'&obj='.$idSec:$secc->url;
                ?>
              <li class="nav-item">
                <a href="<?=$url?>" class="nav-link <?=($idSec==$_GET['obj'] && $idCat==$_GET['cat'])?'active':''?>">
                  <i class="<?=$secc->classIco?> nav-icon"></i>
                  <p><?=$secc->nombre?></p>
                </a>
              </li>
              <? } ?>
            </ul>
          </li>
          <? } ?>
          <li class="nav-header" style="text-align: center;">----------------------------------------------</li>
          <li class="nav-item">
            <a href="changePass.php" class="nav-link">
              <i class="nav-icon fas fa-key text-warning"></i>
              <p class="text">Cambiar Contrase&ntilde;a</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="loguot.php" class="nav-link">
              <i class="nav-icon fa fa-power-off text-danger"></i>
              <p class="text">Salir</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>