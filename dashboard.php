<?php
  if (!empty($_GET['merchantId'])) {
    $_POST=$_GET;
    $_POST['state_pol']=$_GET['polTransactionState'];
    $_POST['transaction_date']=$_GET['processingDate'];
    $_POST['response_message_pol']=$_GET['message'];
    $_POST['transaction_id']=$_GET['transactionId'];
    $_POST['value']=$_GET['TX_VALUE'];
    //$_POST['billing_country']=$_GET[''];
    $_POST['payment_method_name']=$_GET['lapPaymentMethod'];
    $_POST['payment_method_id']=$_GET['polPaymentMethodType'];
    //$_POST['billing_city']=$_GET[''];
    $_POST['reference_sale']=$_GET['referenceCode'];
    include('confirmacion_pago.php');
    header('Location: dashboard.php');
    exit;
  }

  include('includes/conexion.php');
  include('includes/funciones.php');
  include('lib/geoip.inc');

  session_start();

  $abir_bd=geoip_open('lib/GeoIP.dat',GEOIP_STANDARD);
  $pais_cod=geoip_country_code_by_addr($abir_bd, $_SERVER['REMOTE_ADDR']);
  $pais_name=geoip_country_name_by_addr($abir_bd, $_SERVER['REMOTE_ADDR']);
  geoip_close($abir_bd);
  if($pais_cod=='CO'){
    $valorTicket=$con->query("select valorTicket_colombia from parametros")->fetch_object()->valorTicket_colombia;
    $valorMembresia=$con->query("select valorMembresia_colombia from parametros")->fetch_object()->valorMembresia_colombia;
    $moneda='COP';
  }else{
    $valorTicket=$con->query("select valorTicket_general from parametros")->fetch_object()->valorTicket_general;
    $valorMembresia=$con->query("select valorMembresia_general from parametros")->fetch_object()->valorMembresia_general;
    $moneda='USD';
  }


  if(!empty($_GET['token'])){
    $rs=$con->query("SELECT * FROM usuarios WHERE token='".$_GET['token']."'");
    if($rs->num_rows>0){
      $rw=$rs->fetch_object();
      if($rw->eliminado==1){
        echo 'Su usuario ha sido eliminado.';
        exit;
      }
      $con->query("update usuarios set
      activo=1,
      pais_login_cod='$pais_cod',
      pais_login_nombre='$pais_name'
      WHERE id='".$rw->id."'");

    $membresia=$con->query("SELECT * FROM usuarios_pagos WHERE id_usuario='".$rw->id."' AND nombre='Membresía / Membership' LIMIT 1 ")->num_rows;
      
      $_SESSION=array(
      	'login'=>1,
      	'id'=>$rw->id,
        'activo'=>$rw->activo,
      	'nombre'=>$rw->nombre,
      	'apellidos'=>$rw->apellido,
      	'documento'=>$rw->dni,
      	'correo'=>$rw->email,
      	'pais'=>$rw->pais,
      	'estado'=>$rw->estado,
        'pais_regis_cod'=>$rw->pais_cod,
        'estado_regis_cod'=>$rw->estado_cod,
        'ciudad'=>$rw->ciudad,
      	'telefono'=>$rw->telefono,
      	'foto'=>$rw->foto,
        'pais_cod'=>$pais_cod,
        'valorTicket'=>$valorTicket,
      'valorMembresia'=>$valorMembresia,
      'membresia'=>$membresia,
        'moneda'=>$moneda
      );
      //updateTicket('');
    }else{
      header('location: index.php');
      exit;
    }
  }
if ($_SESSION['login']!='1') {
  header('location: index.php');
  exit();
}
include('includes/idioma.php');
$lg=new idioma($_GET['lg']);
$lg->seccion(7);  
  $rs=$con->query("SELECT * FROM ticket WHERE id_usuario='".$_SESSION['id']."' AND tipo=1  AND estado=1 LIMIT 1");
  if ($rs->num_rows>0 || $_GET['transactionState']==4) {
    $btnCompra='';
    $btnTicket='<a class="btn btn-primary btn-block my-2 btn-sm text-center" href="ticket.php#vtick" >
            <i class="fa fa-fw fa-ticket"></i>Ticket Lalexpo</a>';
  } else {
	  	if ($_SESSION['membresia']==0) { 
    		$btnCompra='<a id="popup" class="btn btn-primary btn-sm btn-block text-warning" href="#comprarTicket" onclick="showForm(\'popupMembresia\');"><i class="fa fa-fw fa-lg fa-certificate"></i>'.$lg->general->ad_escara.' </a>';
   			$btnTicket='<a id="popup" class="btn btn-primary btn-sm btn-block text-center" href="#comprarTicket" onclick="showForm(\'popupMembresia\');"><i class="fa fa-fw fa-ticket"></i></i>'.$lg->general->comprar_ticket.'</a>';
	  	}else{
    		$btnCompra='<a class="btn btn-primary btn-sm btn-block text-warning" href="#comprarTicket" onclick="showForm(\'comprarTicket\');"> <i class="fa fa-fw fa-lg fa-certificate"></i></i>'.$lg->general->ad_escara.' </a>';
   			$btnTicket='<a class="btn btn-primary btn-sm btn-block text-center" href="#comprarTicket" onclick="showForm(\'comprarTicket\');"><i class="fa fa-fw fa-ticket"></i>'.$lg->general->comprar_ticket.'</a>';
		}
  }
  include('includes/header.php');
     ?>
    <link rel="stylesheet" href="css/theme.css" />
    <script>
    _lg=<?=json_encode($lg->general)?>;
    _lgSec=<?=json_encode($lg->seccion)?>;
    </script>
    <style>
    .content{
      font-size:1rem;
    }
    .fotoPend {
      display: block;
      background: #fdbb00;
      color: #000;
      font-size: 0.7em;
      opacity: 0.9;
      text-align: center;
      position: relative;
      top: -132px;
    }
    </style>
    <div class="content">
  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <center>
            <h1 class="display-4"><?=$lg->seccion->t_welcome?></h1>
          </center>
        </div>
      </div>
    </div>
  </div>
  <div class="py-6 p-0">
    <div class="container">
      <div class="row">
        <div class="col-lg-2">
          <img id="fotoPerfil" class="d-block rounded-circle img-fluid px-0 mx-3 mx-auto" src="/thumb_600_w/<?php echo $_SESSION['foto']; ?>" width="85%">
          <div class="fotoPend"><?=$lg->general->t_pendingapproval?></div>
          <div class="row"></div>
          <div class="row">
            <div class="col-md-12 my-3">
              <h1 id="nombUsuario" class="text-center"></h1>
            </div>
          </div>
          <div class="row"></div>
          <div class="row">
            <div class="col-md-12"></div>
          </div>
          <?=$btnTicket?>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center btnact" href="#">
            <i class="fa fa-fw fa-user"></i><?=$lg->general->t_perfil?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="banners.php#menu"> Banner</a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="myorders.php#menu"> <?=$lg->general->t_mispedidos?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="payments.php#menu"> <?=$lg->general->t_pays?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="misTickets.php#menu"> <?=$lg->general->t_mistickets?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="cambiarPass.php#menu"> <?=$lg->general->t_chgpass?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="misReservas.php#menu"> <?=$lg->general->t_reservas?></a>
        </div>
        <div class="bg-light px-5 ml-auto col-md-6">
          <div class="row">
            <div class="col-md-12">
              <h3 class="my-2"><?=$lg->seccion->info_contac?></h3>
            </div>
          </div>
          <form id="frmInfoContacto" class="my-3 p-0">
            <div class="form-group form-row">
              <label for="nombreUsuario" class="col-sm-3 col-form-label"><?=$lg->seccion->t_nom?></label>
              <div class="col-sm-9">
                  <input type="text" id="nombreUsuario" readonly="" style="background:#CCC" class="form-control" value="<?php echo $_SESSION['nombre']; ?>" />
              </div>
            </div>
            <div class="form-group form-row">
              <label for="apellidosUsuario" class="col-sm-3 col-form-label"><?=$lg->seccion->t_apes?></label>
              <div class="col-sm-9 ">
                  <input type="text" id="apellidosUsuario" readonly="" style="background:#CCC" class="form-control" value="<?php echo $_SESSION['apellidos']; ?>" />
              </div>
            </div>
            <div class="form-group form-row">
              <label for="documento" class="col-sm-3 col-form-label"><?=$lg->seccion->t_n_cedu?></label>
              <div class="col-sm-9 ">
                <input type="text" class="form-control" readonly="" style="background:#CCC" id="documento" placeholder="N°" value="<?php echo $_SESSION['documento']; ?>"> </div>
            </div>
            <div class="form-group form-row">
              <label for="correo" class="col-sm-3 col-form-label">Email</label>
              <div class="col-sm-9 ">
                <input type="email" class="form-control" readonly="" style="background:#CCC" id="correo" value="<?php echo $_SESSION['correo']; ?>" />
              </div>
            </div>
            <div class="form-group form-row">
              <label for="pais" class="col-sm-3 col-form-label"><?=$lg->seccion->t_pais?></label>
              <div class="col-sm-9 ">
                <select class="form-control bfh-countries" id="paises" data-country="<?=$_SESSION['pais_regis_cod']?>" onchange="$('#estado').bfhstates({country: this.value})"></select>
              </div>
            </div>
            <div class="form-group form-row">
              <label for="estadoUsuario" class="col-sm-3 col-form-label"><?=$lg->seccion->t_estado?></label>
              <div class="col-sm-9">
                <select class="form-control bfh-states" id="estado" data-country="paises" data-state="<?=$_SESSION['estado_regis_cod']?>"></select>
                <!--<input type="text" class="form-control" id="estadoUsuario" placeholder="<?=$lg->seccion->t_estado?>" value="<?php echo $_SESSION['estado']; ?>"> -->
              </div>
            </div>
            <div class="form-group form-row">
              <label for="ciudad" class="col-sm-3 col-form-label" ><?=$lg->seccion->t_city?></label>
              <div class="col-sm-9 ">
                <input type="text" class="form-control" placeholder="<?=$lg->seccion->t_city?>" name="ciudad" id="ciudad" value="<?php echo $_SESSION['ciudad']; ?>"> </div>
            </div>
            <div class="form-group form-row">
              <label for="telefonoUsuario" class="col-sm-3 col-form-label"><?=$lg->seccion->t_telef?></label>
              <div class="col-sm-9 ">
                <input type="text" class="form-control" id="telefonoUsuario" placeholder="#" value="<?php echo $_SESSION['telefono']; ?>">
              </div>
            </div>
            <div class="form-group form-row">
              <label for="passUsuario" class="col-sm-3 col-form-label"><?=$lg->seccion->t_nue_pass?></label>
              <div class="col-sm-9 ">
                <input type="password" class="form-control" id="passUsuario" placeholder="******" autocomplete="off" >
              </div>
            </div>
          </form>
        </div>
        <div class="col-p-3 bg-light ml-auto offset-md-1 col-sm-3 h-25 col-md-3">

          <p class="actionbar lead text-center my-3">
            <b><?=$lg->seccion->t_dire_acc?></b>
          </p>
          <?=$btnCompra?>
          <a class="btn btn-block my-2 btn-sm btn-primary text-warning" href="location.php#menu">
            <i class="fa fa-fw fa-bed fa-lg"></i><?=$lg->seccion->t_reserv_habi?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-warning" href="sponsor.php#menu">
            <i class="fa fa-fw fa-lg fa-certificate"></i><?=$lg->seccion->t_convert_spon?></a>
            <a class="btn btn-primary btn-block my-2 btn-sm text-warning" href="cart.php#menu">
            <i class="fa fa-fw fa-cart-arrow-down fa-lg"></i> Tienda</a>
        </div>
      </div>
      <div class="row my-3">
        <div id="redes" class="col-lg-2">
          <div class="row"></div>
          <div class="row">
            <div class="col-md-11 my-3">
              <h1 class="text-center">Social</h1>
            </div>
          </div>
          <div class="row p-0">
            <div class="col-md-11 my-1">
              <h5>
                <i class="fa fa-fw pull-right fa-pencil" onclick="editarRS(1);"></i>
                <i class="fa fa-fw pull-left fa-globe"></i><span id="soc1"></span></h5>
            </div>
          </div>
          <div class="row">
            <div class="col-md-11 my-2">
              <h5>
                <i class="fa fa-fw pull-right fa-pencil" onclick="editarRS(2);"></i>
                <i class="fa fa-fw pull-left fa-facebook-square"></i><span id="soc2"></span></h5>
            </div>
          </div>
          <div class="row">
            <div class="col-md-11 my-2">
              <h5>
                <i class="fa fa-fw pull-right fa-pencil" onclick="editarRS(3);"></i>
                <i class="fa fa-fw pull-left fa-twitter-square"></i>
                <span id="soc3"></span></h5>
            </div>
          </div>
          <div class="row">
            <div class="col-md-11 my-2">
              <h5>
                <i class="fa fa-fw pull-right fa-pencil" onclick="editarRS(4);"></i>
                <i class="fa fa-fw pull-left fa-instagram"></i><span id="soc4"></span>
                </h5>
            </div>
          </div>
          <div class="row">
            <div class="col-md-11 my-2">
              <h5>
                <i class="fa fa-fw pull-right fa-pencil" onclick="editarRS(5);"></i>
                <i class="fa fa-fw pull-left fa-skype"></i><span id="soc5"></span></h5>
            </div>
          </div>
          <div class="row">
            <div class="col-md-11 my-2">
              <h5>
                <i class="fa fa-fw pull-right fa-pencil" onclick="editarRS(6);"></i>
                <i class="fa fa-fw pull-left fa-youtube-play"></i><span id="soc6"></span></h5>
            </div>
          </div>
        </div>
        <div class="bg-light px-5 col-md-6 ml-auto">
          <div class="row">
              <div class="col-md-12">
                <h4 class="my-4"><?=$lg->seccion->t_info_esca?>
                  <br> </h4>
                    <p><?=$lg->seccion->t_des_02?>
                  <br> </p>
              </div>
          </div>
          <h4 class="my-2">
            <i class="fa fa-fw pull-right fa-info-circle"></i><?=$lg->seccion->t_name_esca?>
            <br> </h4>
          <input type="text" class="form-control px-1 my-3" id="nomEsc" placeholder="Nombre" onkeyup="$('#nombEscaparela').html($(this).val())">
          <h4 class="my-2">
            <i class="fa fa-fw pull-right fa-info-circle"></i>Studio
            <br> </h4>
          <input type="text" class="form-control px-1 my-3" id="studio" placeholder="Studio" onkeyup="$('#studioEscaparela').html($(this).val())">
          <h4 class="my-2">
            <i class="fa fa-fw pull-right fa-info-circle"></i><?=$lg->seccion->t_foto?></h4>
          <div class="row">
            <div class="col-md-6"><p class="small"><?=$lg->seccion->t_lafoto?></p></div>
            <div class="col-md-6">
              <form method="POST" enctype="multipart/form-data" id="uploadFoto">
              <img class="img-fluid d-block rounded-circle mx-auto" src="https://designshack.net/tutorialexamples/profile-layout-content-tabs/images/avatar.png">
              <input type="hidden" name="imgact" id="imgact" />
              <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['id']; ?>">
                <input name="file" id="foto" type="file" size="35" style="display: none;" onchange="cargaFoto()" /> <label class="btn btn-primary btn-block my-2 btn-sm text-center" for="foto">
                <i class="fa fa-fw fa-cloud-upload"></i><?=$lg->seccion->t_subit?></label>
              </form>
            </div>
          </div>
        </div>
        <div class="ml-auto col-md-3">
          <img class="d-block d-flex flex-column align-self-center" src="https://trello-attachments.s3.amazonaws.com/5b327423cd026a49ae5ee66d/5b4037ede5d2398bf2374818/25cba658014bd5a34104c72f556b38f0/escarapela_lalexpo_2019.jpg" width="300 380 460 400" height="400">
          <div id="nombEscaparela"></div>
          <div id="studioEscaparela"></div>
        </div>
      </div>
      <div class="row my-3">
        <div class="col-lg-2"></div>
        <div class="bg-light col-md-6 ml-auto px-3">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12">
                <h4 class="my-4"><?=$lg->seccion->t_activi?>
                  <br /> </h4>
                    <p><?=$lg->seccion->t_descrip_01?>
                  <br /> </p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
            </div>
          </div>
          <h4 class="my-4">
            <i class="fa fa-fw pull-right fa-info-circle"></i><?=$lg->seccion->t_obj?>
            <br> </h4>
          <input type="text" id="objetivo" class="form-control my-3 form-control-lg px-0">
          <h4 class="my-4">
            <i class="fa fa-fw pull-right fa-info-circle"></i><?=$lg->seccion->t_des_prof?>
            <br> </h4>
          <input type="text" class="form-control my-3 form-control-lg px-0" id="descProfesional">
        </div>
        <div class="col-md-3 ml-auto"></div>
      </div>
      <div class="row my-3">
        <div class="col-lg-2"></div>
        <div class="bg-light col-md-6 ml-auto px-3">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12">
                <a class="btn btn-primary btn-block my-2 btn-sm" href="#" onclick="guardarPerfil()">
                  <i class="fa fa-fw fa-floppy-o"></i><?=$lg->seccion->t_guard?></a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 ml-auto"></div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    try {
    $("#nombUsuario").html($("#nombreUsuario").val()+" "+$("#apellidosUsuario").val());
    $("#nombEscaparela").html($("#nomEsc").val());
  } finally { return true; }
});cargarPerfil();
</script>
<?php include('includes/footer.php')?>
<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>