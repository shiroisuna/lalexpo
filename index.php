<?php
include('includes/conexion.php');
include('includes/idioma.php');
$lg=new idioma($_GET['lg']);
$lg->seccion(2);
include('includes/header.php');
?>
<link rel="stylesheet" href="css/home.css" />
    <div class="content">
      <div class="bkg" style="display: block;padding-top: 0 !important;">
        <div class="txt01"><?=$lg->seccion->presen_patrocin?><span>
        <?
        $rs=$con->query("select * from banners_principal where banner='1' limit 1");
        if ($rs->num_rows==1) {
          $rw=$rs->fetch_object();
          $ext=substr(strrchr($rw->archivo_es, "."), 1);
          $arch=($_GET['lg']=='es')?$rw->archivo_es:$rw->archivo_en; ?>
            <a href="<?=$rw->link?>" target="_blank"><img src="/images/banners/<?=$arch?>" style="width: 100%" /></a>
       <? } ?>
        </span></div>
        <div class="txt01" style="margin-bottom: 80px;display: block;"><?=$lg->seccion->community_sponsor ?><span>
        <?
        $rs=$con->query("select * from banners_principal where banner='2' limit 1");
        if ($rs->num_rows==1) {
          $rw=$rs->fetch_object();
          $ext=substr(strrchr($rw->archivo_es, "."), 1);
          $arch=($_GET['lg']=='es')?$rw->archivo_es:$rw->archivo_en; ?>
            <a href="<?=$rw->link?>" target="_blank"><img src="/images/banners/<?=$arch?>" style="width: 100%" /></a>
       <? } ?>
        </span></div>
      </div>
      <?php echo cargarBloques(1, $lg->idioma); ?>
      <img class="bloquemov" src="images/banners/section01m_<?=$lg->idioma?>.jpg" style="width: 100%;display: none;" onclick="showForm('preRegistrer')" />
      <section class="section02">
        <header>
          <h1> <?=$lg->seccion->sponsor_category_1 ?></h1>
        </header>
        <div class="gale">
          <?php echo cargarLogosPatrocinadores(1); ?>
        </div>
      </section>
      <?php echo cargarBloques(2, $lg->idioma); ?>
      <img src="img/bloee.<?=$_GET['lg']=='es'?'jpg':'png'?>" style="width: 100%;display: none;" id="imgBlocAlt" />

      <section class="section02">
        <header>
          <h1><?=$lg->seccion->sponsor_category_2 ?></h1>
        </header>
        <div class="gale">
          <?php echo cargarLogosPatrocinadores(2); ?>
        </div>
      </section>
      <section class="section05">
        <div>
          <span><?=$lg->general->newsletter?></span>
        </div>
        <div>
          <input type="text" class="inpBusc01" id="inpNews01" placeholder="<?=$lg->general->newsletter_hint ?>" />
        </div>
        <div>
          <a class="botLink submit" onclick="regisNews('inpNews01')"><?=$lg->general->btn_submit ?></a>
        </div>
      </section>
      <section class="section02">
        <header>
          <h1><?=$lg->seccion->sponsor_category_3 ?></h1>
        </header>
        <div class="gale">
          <?php echo cargarLogosPatrocinadores(3); ?>
        </div>
      </section>
      <section class="section14">
          <div id="3" class="flexslider">
            <ul class="slides">
          <?
              $rs=$con->query("select * from banners_principal where banner='3'");
              while($rw=$rs->fetch_object()){
                $ext=substr(strrchr($rw->archivo_es, "."), 1);
                $arch=($_GET['lg']=='es')?$rw->archivo_es:$rw->archivo_en; ?>
                 <li>
                  <? if($ext=='mp4'){ ?>
                      <video autoplay="" muted="" loop="" id="myVideo" style="width: 100%;">
                        <source src="/images/banners/<?=$arch?>" type="video/mp4" />
                      </video>
                  <? } elseif(in_array($ext,array('png','gif','jpg'))) { ?>
                      <a href="<?=$rw->link?>" target="_blank"><img src="/images/banners/<?=$arch?>"  style="width: 100%;" /></a>
                  <? } ?>
                </li>
          <?
                }    ?>
            </ul>
          </div>
      </section>
      <section class="section02">
        <header>
          <h1><?=$lg->seccion->sponsor_category_4 ?></h1>
        </header>
        <div class="gale">
          <?php echo cargarLogosPatrocinadores(4); ?>
        </div>
      </section>
      <section class="section11">
          <div id="4" class="flexslider">
            <ul class="slides">
          <?
              $rs=$con->query("select * from banners_principal where banner='4'");
              while($rw=$rs->fetch_object()){
                $ext=substr(strrchr($rw->archivo_es, "."), 1);
                $arch=($_GET['lg']=='es')?$rw->archivo_es:$rw->archivo_en; ?>
                 <li>
                  <? if($ext=='mp4'){ ?>
                      <video autoplay="" muted="" loop="" id="myVideo" style="width: 100%;">
                        <source src="/images/banners/<?=$arch?>" type="video/mp4" />
                      </video>
                  <? } elseif(in_array($ext,array('png','gif','jpg'))) { ?>
                      <a href="<?=$rw->link?>" target="_blank"><img src="/images/banners/<?=$arch?>"  style="width: 100%;" /></a>
                  <? } ?>
                </li>
          <?
                }    ?>
            </ul>
          </div>
      </section>
      <section class="section02">
        <header>
          <h1><?=$lg->seccion->sponsor_category_5 ?></h1>
        </header>
        <div class="gale">
          <?php echo cargarLogosPatrocinadores(5); ?>
        </div>
      </section>
      <section class="section13">
        <header>
          <h1><?=$lg->seccion->media_partner ?></h1>
        </header>
        <div id="medspo" class="cont01">
          <div class="dv02"><?php echo cargarLogosPatrocinadores(44); ?></div>
          <div class="dv01"><?=$lg->seccion->media_sponsors ?> </div>
          <div class="dv02"><?=$lg->seccion->media_sponsors_text ?></div>
          <div class="dv03">
            <div id="logosPatrocinios" class="flexslider">
              <ul class="slides">
              <?php echo cargarLogosPatrocinadores(43); ?>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <script type="text/javascript">
$(window).load(function() {
  $('#3').flexslider({ animation: "slide", controlNav: false, directionNav: false, video: true });
  $('#4').flexslider({ animation: "slide", controlNav: false, directionNav: false, video: true });
});
      </script>
    <?php include('includes/footer.php')?>