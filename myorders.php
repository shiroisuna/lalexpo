<?php
include('includes/conexion.php');
include('includes/idioma.php');
$lg=new idioma($_GET['lg']);
$lg->seccion(11);
include('includes/header.php');
if ($_SESSION['login']!='1') {
  header('Location: index.php');
  exit();
}
$rs=$con->query("SELECT * FROM ticket WHERE id_usuario='".$_SESSION['id']."' AND tipo=1  AND estado=1 LIMIT 1");
if($rs->num_rows>0){
  $btnCompra='';
  $btnTicket='<a class="btn btn-primary btn-block my-2 btn-sm text-center" href="ticket.php#vtick" >
  <i class="fa fa-fw fa-ticket"></i>Ticket Lalexpo</a>';
}else{
	if ($_SESSION['membresia']==0) { 
    	$btnCompra='<a class="btn btn-primary btn-sm btn-block text-warning" href="#comprarTicket" onclick="showForm(\'popupMembresia\');"> <i class="fa fa-fw fa-cart-plus fa-lg"></i>'.$lg->general->ad_escara.' </a>';
   		$btnTicket='<a class="btn btn-primary btn-sm btn-block text-center" href="#comprarTicket" onclick="showForm(\'popupMembresia\');"><i class="fa fa-fw fa-cart-plus"></i>'.$lg->general->comprar_ticket.'</a>';
	}else{
    	$btnCompra='<a class="btn btn-primary btn-sm btn-block text-warning" href="#comprarTicket" onclick="showForm(\'comprarTicket\');"> <i class="fa fa-fw fa-cart-plus fa-lg"></i>'.$lg->general->ad_escara.' </a>';
   		$btnTicket='<a class="btn btn-primary btn-sm btn-block text-center" href="#comprarTicket" onclick="showForm(\'comprarTicket\');"><i class="fa fa-fw fa-cart-plus"></i>'.$lg->general->comprar_ticket.'</a>';
	}

}
?>
<link rel="stylesheet" href="css/theme.css" />
<script>
_lg=<?=json_encode($lg->general)?>;
_lgSec=<?=json_encode($lg->seccion)?>;
</script>
<style>
.content{
  font-size:1rem;
}
</style>
<div class="content" style="padding: 25px;">
  <div class="py-6 p-0">
    <div class="container">
      <div class="row">
        <div class="col-lg-2">
          <img id="fotoPerfil" class="d-block rounded-circle img-fluid px-0 mx-3 mx-auto" src="/thumb_600_w/<?php echo $_SESSION['foto']; ?>" width="85%">
          <div class="row"></div>
          <div class="row">
            <div class="col-md-12 my-3">
              <h1 id="nombUsuario" class="text-center"><?php echo $_SESSION['nombre'] . ' ' . $_SESSION['apellidos']; ?></h1>
            </div>
          </div>
          <div class="row"></div>
          <div class="row">
            <div class="col-md-12"></div>
          </div>
          <?=$btnTicket?>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="dashboard.php">
            <i class="fa fa-fw fa-user"></i><?=$lg->general->t_perfil?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="banners.php#menu"> Banner</a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center btnact" href="payments.php#menu"> <?=$lg->general->t_mispedidos?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="payments.php#menu"> <?=$lg->general->t_pays?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="misTickets.php#menu"> <?=$lg->general->t_mistickets?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="cambiarPass.php#menu"> <?=$lg->general->t_chgpass?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="misReservas.php#menu"> <?=$lg->general->t_reservas?></a>
        </div>
        <div class="bg-light ml-auto col-md-6 px-2">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12">
                <h1 contenteditable="true" class="my-3"><?=$lg->general->t_mispedidos?></h1>
              </div>
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th><?=$lg->seccion->tt_fecha?></th>
                    <th><?=$lg->seccion->tt_total?></th>
                    <th><?=$lg->seccion->tt_pagarp?></th>
                  </tr>
                </thead>
                <tbody id="tbPagos">
                  <tr>
                    <td colspan="5" align="center"> <?=$lg->seccion->t_nopedidos?>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="row"> </div>
        </div>
        <div class="col-p-3 bg-light ml-auto offset-md-1 col-sm-3 h-25 col-md-3">
          <p class="actionbar lead text-center my-3">
            <b><?=$lg->general->t_accdire?></b>
          </p>
          <?=$btnCompra?>
          <a class="btn btn-block my-2 btn-sm btn-primary text-warning" href="location.php#menu">
            <i class="fa fa-fw fa-bed fa-lg"></i><?=$lg->general->reserv_hab?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-warning" href="sponsor.php#menu">
            <i class="fa fa-fw fa-lg fa-certificate"></i><?=$lg->general->t_convspon?></a>
        </div>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="usuario" value="<?php echo $_SESSION['usuario']; ?>">
<script type="text/javascript">$(document).ready(function(){ misPedidos(); });</script>
<?php include('includes/footer.php')?>
<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>