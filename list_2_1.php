<? $categoria=array(2=>"Modelo", ""); ?>
<form  method="post" action="<?=$_SERVER["REQUEST_URI"]?>" >
  <div class="col-10">    
    <input type="text" class="form-control" style="display: inline-block !important; width: 90% !important; " id="filtro" name="filtro" placeholder="cedula, nombre, apellido, email" value="<?=$_POST['filtro']?>" onclick="$(this).select()" />
    <button type="submit" class="btn btn-info" style="float:right;">Buscar</button>
  </div>
</form>
<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th style="width: 50px"></th>
    <th>Nombre</th>
    <th>DNI</th>
    <th>Email</th>
    <th>Origen</th>
    <th>Compañia</th>
    <th>Estudio</th>
    <th>Categoria</th>
    <th>Expositor</th>
    <th>Visible</th>
    <th></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><?=$rw->id?>.</td>
    <td <?php if ($rw->estado_foto!=1 && !empty($rw->foto)) echo'style="background:#ffee9d;"'; ?>><? if(!empty($rw->foto)){?><img src="../<?=$rw->foto?>" width="50" style="max-width: 50px;max-height: 50px;" /><? } ?></td>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>"><?=$rw->nombre?> <?=$rw->apellido?></a></td>
    <td><?=$rw->dni?></td>
    <td><?=$rw->email?></td>
    <td><?=$rw->estado?> - <?=$rw->pais?></td>
    <td><?=$rw->company?></td>
    <td><?=$rw->estudio?></td>
    <td><?=$rw->categoria?></td>
    <td><?=($rw->expositor>0)?'Si':'No'?></td>
    <td><?=($rw->habilitado=='1')?'Si':'No'?></td>
    <td>
      <a href="javascript:;" onclick="msg.text('¿Desea realmente eliminar este asistente?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a>
      <a href="javascript:;" onclick="document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&env=<?=$rw->id?>'" title="Enviar codigo activación"><i class="far fa-paper-plane"></i></a>
    </td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="4">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>