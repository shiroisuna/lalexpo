/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.32-MariaDB : Database - lalexpo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`lalexpo` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `lalexpo`;

/*Table structure for table `banners_principal` */

DROP TABLE IF EXISTS `banners_principal`;

CREATE TABLE `banners_principal` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `archivo_en` varchar(150) DEFAULT NULL,
  `archivo_es` varchar(150) DEFAULT NULL,
  `fecha_alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `activo` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `banners_principal` */

insert  into `banners_principal`(`id`,`archivo_en`,`archivo_es`,`fecha_alta`,`activo`) values 
(6,'0.3717500015317182131.mp4','0.3717690015317182132.mp4','2018-07-16 02:16:53',1),
(8,'0.4182390015317182311.mp4','0.4182550015317182312.mp4','2018-07-16 02:17:11',1),
(9,'0.8747820015317185601.mp4','0.8748070015317185602.mp4','2018-07-16 02:22:40',1),
(10,'0.4025800015317185861.png','0.4025950015317185862.png','2018-07-16 02:23:06',1);

/*Table structure for table `categorias` */

DROP TABLE IF EXISTS `categorias`;

CREATE TABLE `categorias` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `categorias` */

/*Table structure for table `nomencladores` */

DROP TABLE IF EXISTS `nomencladores`;

CREATE TABLE `nomencladores` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `habilitado` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `nomencladores` */

/*Table structure for table `suscripciones` */

DROP TABLE IF EXISTS `suscripciones`;

CREATE TABLE `suscripciones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `suscripciones` */

insert  into `suscripciones`(`id`,`email`,`fecha`) values 
(1,'asd@asd.com','2018-07-15 03:08:12');

/*Table structure for table `traduccion` */

DROP TABLE IF EXISTS `traduccion`;

CREATE TABLE `traduccion` (
  `id` varchar(50) NOT NULL,
  `id_seccion` int(5) unsigned DEFAULT NULL,
  `en` varchar(150) DEFAULT NULL,
  `es` varchar(150) DEFAULT NULL,
  `obs` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seccion` (`id_seccion`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `traduccion` */

insert  into `traduccion`(`id`,`id_seccion`,`en`,`es`,`obs`) values 
('btn_login',1,'Log In','Entrar','boton de entrar en el encabezado'),
('btn_logout',1,'Log out','Salir','boton logout'),
('btn_register',1,'Register','Registro','boton del regisitro'),
('home_galeria',1,'Gallery','Galería','.'),
('menu_asisten',1,'Who is coming','Asistentes','.'),
('menu_home',1,'Home','Inicio','.'),
('menu_premios',1,'Awards','Premios','.'),
('menu_program',1,'Schedule','Programación','.'),
('menu_sponsor',1,'Be our sponsor','Sea patrocinador','.'),
('presen_patrocin',2,'Presenting Sponsor','Patrocinador Presentador','.');

/*Table structure for table `traduccion_seccion` */

DROP TABLE IF EXISTS `traduccion_seccion`;

CREATE TABLE `traduccion_seccion` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `traduccion_seccion` */

insert  into `traduccion_seccion`(`id`,`nombre`) values 
(1,'Global'),
(2,'Home');

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `activo` int(1) DEFAULT '0',
  `token` varchar(200) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `genero` varchar(50) DEFAULT NULL,
  `foto` varbinary(150) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `pass` varchar(30) DEFAULT NULL,
  `dni` varchar(50) DEFAULT NULL,
  `pais` varchar(50) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `lengua` varbinary(30) DEFAULT NULL,
  `telefono` varbinary(25) DEFAULT NULL,
  `id_categoria` int(5) unsigned DEFAULT NULL,
  `fecha_alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `anio` int(4) unsigned DEFAULT NULL,
  `company` varchar(70) DEFAULT NULL,
  `website` varchar(80) DEFAULT NULL,
  `models` varchar(10) DEFAULT NULL,
  `face` varchar(60) DEFAULT NULL,
  `twitter` varchar(60) DEFAULT NULL,
  `instagram` varchar(60) DEFAULT NULL,
  `skype` varchar(60) DEFAULT NULL,
  `youtube` varchar(60) DEFAULT NULL,
  `objetivo` varbinary(100) DEFAULT NULL,
  `descrip_profesional` varbinary(150) DEFAULT NULL,
  `nickname` varbinary(50) DEFAULT NULL,
  `sitios_adicionales` varbinary(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `usuarios` */

insert  into `usuarios`(`id`,`activo`,`token`,`nombre`,`apellido`,`genero`,`foto`,`email`,`pass`,`dni`,`pais`,`estado`,`lengua`,`telefono`,`id_categoria`,`fecha_alta`,`anio`,`company`,`website`,`models`,`face`,`twitter`,`instagram`,`skype`,`youtube`,`objetivo`,`descrip_profesional`,`nickname`,`sitios_adicionales`) values 
(1,1,NULL,'Lautaro','Lescano',NULL,NULL,'cristian_lescano@hotmail.es','1234','34521314','arg','bsas','español','43672675',2,'2018-07-14 02:20:59',2019,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `usuarios_back` */

DROP TABLE IF EXISTS `usuarios_back`;

CREATE TABLE `usuarios_back` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varbinary(30) NOT NULL,
  `pass` varbinary(30) NOT NULL,
  `email` varbinary(70) NOT NULL,
  `nombre` varbinary(70) DEFAULT NULL,
  `tipo` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `usuarios_back` */

insert  into `usuarios_back`(`id`,`user`,`pass`,`email`,`nombre`,`tipo`) values 
(1,'admin','admin','cristian_lescano@hotmail.es','Cristian333',1);

/*Table structure for table `usuarios_sitios` */

DROP TABLE IF EXISTS `usuarios_sitios`;

CREATE TABLE `usuarios_sitios` (
  `id_usuarios` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_sitios` int(6) unsigned NOT NULL,
  PRIMARY KEY (`id_usuarios`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `usuarios_sitios` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
