/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.6.24 : Database - lalexpo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`lalexpo` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `lalexpo`;

/*Table structure for table `banners_principal` */

DROP TABLE IF EXISTS `banners_principal`;

CREATE TABLE `banners_principal` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `banner` varchar(20) DEFAULT NULL,
  `archivo_en` varchar(150) DEFAULT NULL,
  `archivo_es` varchar(150) DEFAULT NULL,
  `fecha_alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `activo` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `banners_principal` */

insert  into `banners_principal`(`id`,`banner`,`archivo_en`,`archivo_es`,`fecha_alta`,`activo`) values 
(6,'principal','0.3717500015317182131.mp4','0.3717690015317182132.mp4','2018-07-17 22:55:26',1),
(8,'principal','0.4182390015317182311.mp4','0.4182550015317182312.mp4','2018-07-17 22:55:32',1),
(9,'principal','0.8747820015317185601.mp4','0.8748070015317185602.mp4','2018-07-17 22:55:40',1),
(11,'3','0.0876090015318892821.jpg','0.0876760015318892822.jpg','2018-07-18 01:07:20',1),
(12,'4','0.0894890015318913311.jpg','0.0895140015318913312.jpg','2018-07-18 01:07:24',1),
(15,'1','0.3385940015318940991.png','0.3386370015318940992.png','2018-07-18 01:08:19',1),
(16,'2','0.9026120015318941281.png','0.9026340015318941282.png','2018-07-18 01:08:48',1);

/*Table structure for table `categorias` */

DROP TABLE IF EXISTS `categorias`;

CREATE TABLE `categorias` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `categorias` */

insert  into `categorias`(`id`,`nombre`) values 
(2,'Modelo'),
(3,'Studio'),
(4,'Webmaster'),
(5,'Other');

/*Table structure for table `faq` */

DROP TABLE IF EXISTS `faq`;

CREATE TABLE `faq` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `id_seccion` int(5) unsigned DEFAULT NULL,
  `preg_es` varchar(250) DEFAULT NULL,
  `preg_en` varchar(250) DEFAULT NULL,
  `resp_es` varchar(250) DEFAULT NULL,
  `resp_en` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `faq` */

/*Table structure for table `faq_seccion` */

DROP TABLE IF EXISTS `faq_seccion`;

CREATE TABLE `faq_seccion` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `habilitado` int(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `faq_seccion` */

insert  into `faq_seccion`(`id`,`nombre_es`,`nombre_en`,`habilitado`) values 
(1,'Inicio','Home',1),
(2,'cat2','cat two',1);

/*Table structure for table `galeria` */

DROP TABLE IF EXISTS `galeria`;

CREATE TABLE `galeria` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_album` int(6) NOT NULL,
  `archivo` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `galeria` */

insert  into `galeria`(`id`,`id_album`,`archivo`) values 
(6,1,'0.763519001531772230.jpg'),
(7,1,'0.647924001531772245.jpg'),
(8,1,'0.354857001531772276.jpg');

/*Table structure for table `galeria_album` */

DROP TABLE IF EXISTS `galeria_album`;

CREATE TABLE `galeria_album` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `habilitado` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `galeria_album` */

insert  into `galeria_album`(`id`,`nombre_es`,`nombre_en`,`habilitado`) values 
(1,'Evento','Event',1),
(2,'2222','22222',1);

/*Table structure for table `nomencladores` */

DROP TABLE IF EXISTS `nomencladores`;

CREATE TABLE `nomencladores` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `habilitado` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `nomencladores` */

/*Table structure for table `patrocinador_categoria_paquete` */

DROP TABLE IF EXISTS `patrocinador_categoria_paquete`;

CREATE TABLE `patrocinador_categoria_paquete` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `patrocinador_categoria_paquete` */

insert  into `patrocinador_categoria_paquete`(`id`,`nombre_es`,`nombre_en`,`estado`) values 
(1,'Paquetes Premium de Patrocinios','Sponsorship premium packs',1),
(2,'Patrocinio de Alimentos y Bebidas','Sponsor food and drinks',1),
(3,'Paquetes y servicios adicionales','Aditional packs &amp; services',1),
(4,'Paquetes de hospitalidad','Hospitality packs',1),
(5,'Actividades y Eventos','Activities &amp; Events',1),
(6,'Premios','Awards',1);

/*Table structure for table `patrocinador_paquete` */

DROP TABLE IF EXISTS `patrocinador_paquete`;

CREATE TABLE `patrocinador_paquete` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_categoria` int(11) DEFAULT NULL,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `descrip_es` text,
  `descrip_en` text,
  `precio` decimal(18,2) DEFAULT '0.00',
  `cantidad` int(11) DEFAULT '0',
  `estado` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_categoria` (`id_categoria`),
  CONSTRAINT `categoria` FOREIGN KEY (`id_categoria`) REFERENCES `patrocinador_categoria_paquete` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `patrocinador_paquete` */

insert  into `patrocinador_paquete`(`id`,`id_categoria`,`nombre_es`,`nombre_en`,`descrip_es`,`descrip_en`,`precio`,`cantidad`,`estado`) values 
(1,1,'Patrocinador <span style=\"color:#ffcc33\">Crown</span>','<span style=\"color:#ffcc33\">Crown</span> sponsor','Registro de hotel: prioridad <br />\r\nTraslado al aeropuerto <br />\r\nSalón de lujo en el piso de la exposición: grande <br />\r\nBoletos incluidos: 10 <br />\r\nEntrada de afiliado GRATUITA: 20 <br />\r\nBoleto adicional: $ 75 <br />\r\nColocación del logotipo Página de inicio: Principal <br />\r\nColocación del logotipo Publicidad: Principal <br />\r\nBanner grande <br />\r\nPosición de la bandera: 6 <br />\r\nBanderas verticales rígidas: 4 <br />\r\nSala de seminarios: 1 hora <br />\r\nConozca la tabla de mercado <br />\r\nInserciones de exhibición y bolsa: 4 <br />\r\nCompañías participantes de precontacto <br />\r\nBoletín informativo para todos los participantes <br />\r\nPanel de entrevistas: Destacado <br />\r\nTraductores: 2 <br />\r\nExposición social <br />\r\nMostrar guía: Anuncio de página completa <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking','Hotel registration : Priority<br />\r\n              Airport transfer <br />\r\n              Luxury Lounge in show floor : Large <br />\r\n              Included tickets : 10 <br />\r\n              FREE Affiliate entrance ticket : 20 <br />\r\n              Extra ticket : $75 <br />\r\n              Logo placement Homepage : Main <br />\r\n              Logo placement Advertising : Main <br />\r\n              Large Banner<br />\r\n              Flag position : 6 <br />\r\n              Rigid stand-up-banners : 4 <br />\r\n              Seminar Lounge : 1 Hour <br />\r\n              Meet Market Table <br />\r\n              Show and bag inserts : 4 <br />\r\n              Precontact Participant Companys <br />\r\n              Newsletter to Participants All <br />\r\n              Interviews panel : Prominent <br />\r\n              Translators : 2 <br />\r\n              Social Exposure <br />\r\n              Show Guide : Full Page Ad <br />\r\n              VIP Zone Access at Parties <br />\r\n              Pre Show Dinner Networking',14999.00,5,1),
(2,1,'Patrocinador <span style=\"color:#99ccff\">Diamond</span>','<span style=\"color:#99ccff\">Diamond</span> sponsor','Registro de hotel: Prioridad <br>\r\nTraslado al aeropuerto <br>\r\nLounge de lujo en el piso de la exposición: medio <br>\r\nBoletos incluidos: 8 <br>\r\nEntrada GRATUITA para afiliado: 15 <br>\r\nBoleto adicional: $ 80 <br>\r\nLogo placement Homepage: Main <br>\r\nLogo placement Publicidad: Main <br>\r\nLarge Banner <br>\r\nPosición de la bandera: 4 <br>\r\nRígido stand-up-banners: 2 <br>\r\nSala de seminarios: 30 minutos <br>\r\nMeet Market Table <br>\r\nInserciones de vitrinas y carteras: 3 <br>\r\nPrecontact Participant Companys <br>\r\nBoletín a los participantes de todo <br>\r\nPanel de entrevistas: Secundaria <br>\r\nTraductores: 1 <br>\r\nExposición social <br>\r\nMostrar guía: Anuncio de página completa <br>\r\nAcceso a la zona VIP en las fiestas <br>\r\nPre Show Dinner Networking','Hotel registration : Priority <br>\r\nAirport transfer <br>\r\nLuxury Lounge in show floor : Medium <br>\r\nIncluded tickets : 8 <br>\r\nFREE Affiliate entrance ticket : 15 <br>\r\nExtra ticket : $80 <br>\r\nLogo placement Homepage : Main <br>\r\nLogo placement Advertising : Main <br>\r\nLarge Banner<br>\r\nFlag position : 4 <br>\r\nRigid stand-up-banners : 2 <br>\r\nSeminar Lounge : 30 Mins <br>\r\nMeet Market Table <br>\r\nShow and bag inserts : 3 <br>\r\nPrecontact Participant Companys <br>\r\nNewsletter to Participants All <br>\r\nInterviews panel : Secondary <br>\r\nTranslators : 1 <br>\r\nSocial Exposure <br>\r\nShow Guide : Full Page Ad <br>\r\nVIP Zone Access at Parties <br>\r\nPre Show Dinner Networking',9999.00,5,1),
(3,1,'Patrocinador <span style=\"color:#aae2e0\">Platinum</span>','<span style=\"color:#aae2e0\">Platinum</span> sponsor','Registro de hotel: Prioridad <br>\r\nTraslado al aeropuerto <br>\r\nSalón de lujo en el piso de exhibición: pequeño<br>\r\nBoletos incluidos: 6 <br>\r\nEntrada GRATUITA para afiliado: 10 <br>\r\nBoleto extra: $ 90 <br>\r\nLogo placement Homepage: Secundaria <br>\r\nLogo placement Publicidad: Secundaria <br>\r\nLarge Banner <br>\r\nPosición de la bandera: 2<br>\r\nRígido stand-up-banners: 1 <br>\r\nMeet Market Table <br>\r\nInserciones de exhibición y bolsa: 2 <br>\r\nPrecontact Participant Companys <br>\r\nBoletín a los participantes de todo <br>\r\nPanel de entrevistas: Secundaria <br>\r\nExposición social <br>\r\nMostrar guía: Anuncio de media página <br>\r\nAcceso a la zona VIP en las fiestas <br>\r\nPre Show Dinner Networking','Hotel registration : Priority <br>\r\nAirport transfer  <br>\r\nLuxury Lounge in show floor : Small  <br>\r\nIncluded tickets : 6  <br>\r\nFREE Affiliate entrance ticket : 10  <br>\r\nExtra ticket : $90  <br>\r\nLogo placement Homepage : Secondary  <br>\r\nLogo placement Advertising : Secondary  <br>\r\nLarge Banner <br>\r\nFlag position : 2  <br>\r\nRigid stand-up-banners : 1  <br>\r\nMeet Market Table  <br>\r\nShow and bag inserts : 2  <br>\r\nPrecontact Participant Companys  <br>\r\nNewsletter to Participants All  <br>\r\nInterviews panel : Secondary  <br>\r\nSocial Exposure  <br>\r\nShow Guide : Half Page Ad  <br>\r\nVIP Zone Access at Parties  <br>\r\nPre Show Dinner Networking',4999.00,5,1),
(4,1,'Patrocinador <span style=\"color:#f58800\">Gold</span>','<span style=\"color:#f58800\">Gold</span> sponsor','Registro de hotel: prioridad <br />\r\nBoletos incluidos: 4 <br />\r\nBoleto adicional: $ 95 <br />\r\nColocación del logotipo Página de inicio: Pequeño <br />\r\nColocación del logotipo Publicidad: Pequeño <br />\r\nBanner grande <br />\r\nPosición de la bandera: 1 <br />\r\nConozca la tabla de mercado <br />\r\nInserciones de exhibición y bolsa: 1 <br />\r\nCompañías participantes de precontacto <br />\r\nExposición social <br />\r\nMostrar guía: anuncio de página trimestre <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking','Hotel registration : Priority <br />\r\n              Included tickets : 4 <br />\r\n              Extra ticket : $95 <br />\r\n              Logo placement Homepage : Small <br />\r\n              Logo placement Advertising : Small <br />\r\n              Large Banner<br />\r\n              Flag position : 1 <br />\r\n              Meet Market Table <br />\r\n              Show and bag inserts : 1 <br />\r\n              Precontact Participant Companys <br />\r\n              Social Exposure <br />\r\n              Show Guide : Quarter Page Ad <br />\r\n              VIP Zone Access at Parties <br />\r\n              Pre Show Dinner Networking',2999.00,5,1),
(5,1,'Patrocinador <span style=\"color:#d16528\">Bronze</span>','<span style=\"color:#d16528\">Bronze</span> sponsor','Registro de hotel: prioridad <br />\r\nBoletos incluidos: 1 <br />\r\nColocación del logotipo Página de inicio: Pequeño <br />\r\nColocación del logotipo Publicidad: Pequeño <br />\r\nExposición social <br />\r\nMostrar guía: anuncio de página trimestre <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking','Hotel registration : Priority <br />\r\nIncluded tickets : 1 <br />\r\nLogo placement Homepage : Small <br />\r\nLogo placement Advertising : Small <br />\r\nSocial Exposure <br />\r\nShow Guide : Quarter Page Ad <br />\r\nVIP Zone Access at Parties <br />\r\nPre Show Dinner Networking',1250.00,5,1),
(6,2,'Cena previa al evento','Pre show Dinner Networking','Dé la bienvenida a los jugadores principales con una cena en la<br /> noche antes del evento. <br />\r\nInvitar solo Actividad para Patrocinadores y VIP\'s de LALEXPO. <br />\r\nCapacidad máxima 100 asistentes. Restaurante elegido<br /> por el Patrocinador de <br />\r\nLista provista por LALEXPO <br />\r\nVolantes (14 páginas) <br />\r\n2 banners rígidos <br />\r\n1 Anfitriona en la cena que acompaña al patrocinador<br /> (el atuendo NOCTURNO será <br />\r\nproporcionado por el patrocinador). <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nBoletos incluidos: 4 <br />\r\nColocación del logotipo Página de inicio: Pequeño <br />\r\nColocación del logotipo Publicidad: Pequeño <br />\r\nExposición social <br />\r\nMostrar guía: anuncio de página trimestre <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nCena previa al evento <br />\r\n<img style=\"margin-top: 20px;\" src=\"http://lalexpo.com/uploads/paquetes/DSC_8410.jpg\" class=\"img-responsive\">','Welcome the main players with a dinner on the Evening before the Event. <br />\r\nInvite only Activity for Sponsors and VIP´s of LALEXPO. <br />\r\nMaximum capacity 100 Attendees. Restaurant to be chosen by Sponsor from <br /> \r\nLALEXPO provided list  <br />\r\nFlyers (14 Page)  <br />\r\n2 Rigid Banners  <br />\r\n1 Hostess at Dinner accompanying the Sponsor (NIGHT Outfit to be<br /> \r\nprovided by sponsor).  <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :<br /> \r\nHotel registration : Priority  <br />\r\nIncluded tickets : 4  <br />\r\nLogo placement Homepage : Small  <br />\r\nLogo placement Advertising : Small  <br />\r\nSocial Exposure  <br />\r\nShow Guide : Quarter Page Ad  <br />\r\nVIP Zone Access at Parties  <br />\r\nPre Show Dinner Networking.  <br /> \r\n<img style=\"margin-top: 20px;\" src=\"http://lalexpo.com/uploads/paquetes/DSC_8410.jpg\" class=\"img-responsive\">',3999.00,5,1),
(7,2,'Estación de enfriamiento de agua','Water cooling','Botellas de agua personalizadas con el logotipo de su empresa. <br />\r\nIncluye 1.200 botellas de agua, 400 distribuidas cada día del evento. <br />\r\nPegatinas (Logo - Marca) en botellas de agua. <br />\r\nBanner en la estación de enfriamiento de agua. <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nBoletos incluidos: 4 <br />\r\nColocación del logotipo Página de inicio: Pequeño <br />\r\nColocación del logotipo Publicidad: Pequeño <br />\r\nExposición social <br />\r\nMostrar guía: anuncio de página trimestre <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking.','Custom branded water bottles with your company logo.  <br /> \r\nIncludes 1.200 water bottles, 400 distributed each day of the event.  <br />  \r\nStickers (Logo - Brand) on water bottles.   <br /> \r\nBanner on water cooling station.  <br />  \r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />  \r\nHotel registration : Priority  <br />\r\nIncluded tickets : 4  <br />\r\nLogo placement Homepage : Small  <br />\r\nLogo placement Advertising : Small  <br />\r\nSocial Exposure  <br />\r\nShow Guide : Quarter Page Ad  <br />\r\nVIP Zone Access at Parties  <br />\r\nPre Show Dinner Networking.',2499.00,5,1),
(8,2,'Receso','Coffe breaks','Su propia estación de café para ofrecer café gratis <br /> y comida ligera para todos. <br />\r\nSe ofrece cada día del evento de 8 a.m. a 11:00 a.m. <br />\r\nPegatinas (Logotipo - Marca) en las tazas de café <br />\r\nBanner en la estación de café <br />\r\n1000 volantes (1\r\n4 páginas) <br />\r\n1 Anfitriona sirviendo el café (El atuendo debe <br /> ser provisto por usted. <br />\r\nExcelente exposición en horas de mucho tráfico. <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nTraslado al aeropuerto <br />\r\nBoletos incluidos: 6 <br />\r\nEntrada de afiliado GRATUITA: 10 <br />\r\nColocación del logotipo Página de inicio: Secundaria <br />\r\nColocación del logotipo Publicidad: secundaria <br />\r\nBoletín informativo para todos los participantes <br />\r\nPanel de entrevistas: Secundario <br />\r\nExposición social <br />\r\nMostrar guía: Anuncio de media página <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking','Your own coffee Station to offer free cofee and light food to everybody.   <br />\r\nProvided each day of the event from 8AM to 11AM.   <br />\r\nStickers (Logo - Brand) on Coffee Cups   <br />\r\nBanner on Coffee Station   <br />\r\n1000 Flyers ( 14 page )   <br />\r\n1 Hostess Serving the Coffee (Outfit should be provided by you.   <br />\r\nExcellent exposure in the high traffic hour.   <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :   <br />\r\nHotel registration : Priority   <br />\r\nAirport transfer   <br />\r\nIncluded tickets : 6   <br />\r\nFREE Affiliate entrance ticket : 10   <br />\r\nLogo placement Homepage : Secondary   <br />\r\nLogo placement Advertising : Secondary   <br />\r\nNewsletter to Participants All   <br />\r\nInterviews panel : Secondary   <br />\r\nSocial Exposure   <br />\r\nShow Guide : Half Page Ad   <br />\r\nVIP Zone Access at Parties   <br />\r\nPre Show Dinner Networking',4999.00,5,1),
(9,2,'Cócteles Happy Hour','Happy hour Cocktails','Aloje una barra libre en el programa durante un evento de networking. <br />\r\nBebidas gratis incluidas por 1-2 horas. <br />\r\nLa barra se marcará con su logotipo y material de promoción. <br />\r\nPegatinas (Logotipo - Marca) en tazas de bebidas. <br />\r\nBanner en el Bar <br />\r\n2 camareros con traje de marca. (traje debe ser proporcionado por usted) <br />\r\n1000 folletos (14 páginas) <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nTraslado al aeropuerto <br />\r\nBoletos incluidos: 6 <br />\r\nEntrada de afiliado GRATUITA: 10 <br />\r\nColocación del logotipo Página de inicio: Secundaria <br />\r\nColocación del logotipo Publicidad: secundaria <br />\r\nBoletín informativo para todos los participantes <br />\r\nPanel de entrevistas: Secundario <br />\r\nExposición social <br />\r\nMostrar guía: Anuncio de media página <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking.','Host an open bar in the show during a networking event.   <br />\r\nFree drinks included for 1-2 hours.   <br />\r\nThe bar will be branded with your logo and promotion material.   <br />\r\nStickers (Logo - Brand) on drink cups.   <br />\r\nBanner on Bar   <br />\r\n2 bartenders with branded outfit. (outfit should be provided by you)   <br />\r\n1000 Flyers ( 14 page )   <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :   <br />\r\nHotel registration : Priority   <br />\r\nAirport transfer   <br />\r\nIncluded tickets : 6   <br />\r\nFREE Affiliate entrance ticket : 10   <br />\r\nLogo placement Homepage : Secondary   <br />\r\nLogo placement Advertising : Secondary   <br />\r\nNewsletter to Participants All   <br />\r\nInterviews panel : Secondary   <br />\r\nSocial Exposure   <br />\r\nShow Guide : Half Page Ad   <br />\r\nVIP Zone Access at Parties   <br />\r\nPre Show Dinner Networking.',4999.00,5,1),
(10,2,'Almuerzo','Lunch','Anímate a almorzar uno de los días del espectáculo durante<br />\r\nel descanso del espectáculo. <br />\r\nLa opción 1 es una estación de almuerzo tipo buffet, la <br />\r\ncantidad de platos proporcionados para 1 paquete es entre<br />\r\n150 a 250 placas, dependiendo del menú elegido por el patrocinador. <br />\r\nLa opción 2 es un almuerzo de invitación solo en uno de <br />\r\nnuestros restaurantes cercanos, límite de placas entre <br />\r\n150-200 dependiendo del restaurante elegido por el patrocinador. <br />\r\nMateriales de marca incluidos.<br /> \r\nDetalles dependiendo de la opción elegida. <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nTraslado al aeropuerto <br />\r\nBoletos incluidos: 6 <br />\r\nEntrada de afiliado GRATUITA: 10 <br />\r\nColocación del logotipo Página de inicio: Secundaria <br />\r\nColocación del logotipo Publicidad: secundaria <br />\r\nBoletín informativo para todos los participantes <br />\r\nPanel de entrevistas: Secundario <br />\r\nExposición social <br />\r\nMostrar guía: Anuncio de media página <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking.','Host lunch one of the show days during the show break.   <br />\r\nOption 1 is a buffet style lunch station, the quantity <br />\r\nof the plates provided for 1 package is between 150 to<br />\r\n250 plates depending on menu chosen by sponsor.   <br />\r\nOption 2 is a invite only lunch at one of our nearby restaurants,  <br />\r\nlimit of plates between 150- 200 depending on restaurant chosen by sponsor.  <br />\r\nBranded materials included. Details depending on the option chosen.  <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :   <br />\r\nHotel registration : Priority   <br />\r\nAirport transfer   <br />\r\nIncluded tickets : 6   <br />\r\nFREE Affiliate entrance ticket : 10   <br />\r\nLogo placement Homepage : Secondary   <br />\r\nLogo placement Advertising : Secondary   <br />\r\nNewsletter to Participants All   <br />\r\nInterviews panel : Secondary   <br />\r\nSocial Exposure   <br />\r\nShow Guide : Half Page Ad   <br />\r\nVIP Zone Access at Parties   <br />\r\nPre Show Dinner Networking.',4999.00,5,1),
(11,2,'Cena','Dinner','Organice una cena solo para invitados en uno de nuestros <br />\r\nrestaurantes cercanos. <br />\r\nlímite de placas entre 150 y 250 dependiendo del restaurante<br /> \r\nelegido por el patrocinador. <br />\r\nMateriales de marca incluidos. <br />\r\nDetalles según el restaurante elegido. <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nTraslado al aeropuerto <br />\r\nBoletos incluidos: 6 <br />\r\nEntrada de afiliado GRATUITA: 10 <br />\r\nColocación del logotipo Página de inicio: Secundaria <br />\r\nColocación del logotipo Publicidad: secundaria <br />\r\nBoletín informativo para todos los participantes <br />\r\nPanel de entrevistas: Secundario <br />\r\nExposición social <br />\r\nMostrar guía: Anuncio de media página <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking.','Host a invite only dinner at one of our nearby restaurants.  <br />\r\nlimit of plates between 150- 250 depending on restaurant chosen by sponsor.  <br />\r\nBranded materials included.  <br />\r\nDetails depending on the restaurant chosen.  <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />\r\nHotel registration : Priority  <br />\r\nAirport transfer  <br />\r\nIncluded tickets : 6  <br />\r\nFREE Affiliate entrance ticket : 10  <br />\r\nLogo placement Homepage : Secondary  <br />\r\nLogo placement Advertising : Secondary  <br />\r\nNewsletter to Participants All  <br />\r\nInterviews panel : Secondary  <br />\r\nSocial Exposure  <br />\r\nShow Guide : Half Page Ad  <br />\r\nVIP Zone Access at Parties  <br />\r\nPre Show Dinner Networking.',5999.00,5,1),
(12,2,'Snack y dulces','Snack &amp; Candy','Nunca se puede ser \"Demasiado dulce\" para los delegados. <br />\r\nLa estación de golosinas Snack and Candy está llena de una<br /> \r\nvariedad de golosinas gratuitas. <br />\r\nLa estación estará abierta a todos los asistentes durante todo el día. <br />\r\n2 Banners en la estación de la estación de bocadillos <br />\r\n1000 folletos (14 páginas). <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nBoletos incluidos: 4 <br />\r\nColocación del logotipo Página de inicio: Pequeño <br />\r\nColocación del logotipo Publicidad: Pequeño <br />\r\nExposición social <br />\r\nMostrar guía: anuncio de página trimestre <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking.','You can never be \"Too Sweet\" to the delegates.   <br />\r\nBranded Snack and Candy station filled with an assortment of free treats.   <br />\r\nThe station will be open to all attendees all day long.   <br />\r\n2 Banners on Snack station Station   <br />\r\n1000 Flyers ( 14 page ).   <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :   <br />\r\nHotel registration : Priority   <br />\r\nIncluded tickets : 4   <br />\r\nLogo placement Homepage : Small   <br />\r\nLogo placement Advertising : Small   <br />\r\nSocial Exposure   <br />\r\nShow Guide : Quarter Page Ad   <br />\r\nVIP Zone Access at Parties   <br />\r\nPre Show Dinner Networking.',2499.00,5,1),
(13,3,'Anfitriones de marca','Branded Hostesses','2 Anfitriones de marca para su empresa durante los 3 días de LALEXPO. <br />\r\nLos usé para repartir volantes, dar la bienvenida a tus socios o simplemente para <br />\r\nhaz que tu empresa luzca atractiva <br />\r\n1000 folletos (14 páginas) <br />\r\nEquipo de Anfitriones (debe proporcionar diseño e ilustraciones o traer traje propio). <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nBoletos incluidos: 4 <br />\r\nColocación del logotipo Página de inicio: Pequeño <br />\r\nColocación del logotipo Publicidad: Pequeño <br />\r\nExposición social <br />\r\nMostrar guía: anuncio de página trimestre <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking.','2 Branded Hostesses for your company during the 3 Days of LALEXPO.  <br />\r\nUsed them to spread flyers around, welcome your partners or simply to  <br />\r\nmake your Company look hot.  <br />\r\n1000 Flyers (14 Page)  <br />\r\nHostesses Outfit (Must provide design and artwork or bring Outfit yourself).  <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />\r\nHotel registration : Priority  <br />\r\nIncluded tickets : 4  <br />\r\nLogo placement Homepage : Small  <br />\r\nLogo placement Advertising : Small  <br />\r\nSocial Exposure  <br />\r\nShow Guide : Quarter Page Ad  <br />\r\nVIP Zone Access at Parties  <br />\r\nPre Show Dinner Networking.',2499.00,5,1),
(14,3,'Inserción de bolsas de regalo','Gift bag insertion','Coloque un artículo (volante, obsequio, regalito, etc.) en las bolsas del espectáculo que <br />\r\nse entregan a todos los delegados. <br />\r\nEste patrocinio no incluye ningún obsequio, es solo el derecho <br />\r\npara que insertes los regalos en las bolsas de regalo. <br />','Place an item (flyer, gift, goody, etc.) in the show bags that  <br />\r\nare given to all delegates.  <br />\r\nThis Sponsorship does not include any Gifts, is only the right  <br />\r\nfor you to insert gifts into the Gift Bags. <br />',499.00,5,1),
(15,3,'Estación de carga de teléfono','Phone charging station','¿En LALEXPO con la batería del teléfono a punto de morir? <br/>\r\nGracias al patrocinador de la estación de carga de celulares, <br/>\r\nesto no le va a pasar a ninguno de los asistentes al evento! <br/>\r\nEstación de carga de celulares brandeada y pendón de la empresa. <br/>\r\nBENEFICIOS ADICIONALES EN ESTE PATROCINIO: <br/>\r\nRegistro de hotel: Prioritario <br/>\r\nTiquetes de entrada: 4 <br/>\r\nLogo al inicio de la página web: Pequeño <br/>\r\nLogo en la publicidad del evento: Pequeño <br/>\r\nExposición en redes sociales <br/>\r\nEspacio publicitario en revista del evento: Cuarto de página <br/>\r\nAcceso a las zonas VIP en las fiestas <br/>\r\nCena de negocios previa al evento<br/>','At LALEXPO with phone`s battery about to die? Thanks you the Mobile  <br />\r\nPhone Charging Sponsor this is not going to happen<br /> \r\nto anyone at the Convention !  <br />\r\nBranded Phone charging station and company banner.  <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />\r\nHotel registration : Priority  <br />\r\nIncluded tickets : 4  <br />\r\nLogo placement Homepage : Small  <br />\r\nLogo placement Advertising : Small  <br />\r\nSocial Exposure  <br />\r\nShow Guide : Quarter Page Ad  <br />\r\nVIP Zone Access at Parties  <br />\r\nPre Show Dinner Networking.',1999.00,5,1),
(16,3,'Baños Brandeados\r\n','Bathrooms Branding','Tenga el logo de su empresa en un lugar que todos los asistentes<br> \r\nsin duda verán.<br> \r\nLogo branding en espejos o puertas de los baños en forma de adhesivos. <br>\r\nBENEFICIOS ADICIONALES EN ESTE PATROCINIO: \r\nRegistro de hotel: Prioritario <br>\r\nTiquetes de entrada: 4<br>\r\nLogo al inicio de la página web: Pequeño<br>\r\nLogo en la publicidad del evento: Pequeño<br>\r\nExposición en redes sociales <br>\r\nEspacio publicitario en revista del evento: Cuarto de página <br>\r\nAcceso a las zonas VIP en las fiestas <br>\r\nCena de negocios previa al evento								</div>','Have your logo in a spot attendees will definitely see.  <br />\r\nLogo branding in Mirrors, Door stickers, Clings, etc <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP : <br />\r\nHotel registration : Priority  <br />\r\nIncluded tickets : 4  <br />\r\nLogo placement Homepage : Small  <br />\r\nLogo placement Advertising : Small  <br />\r\nSocial Exposure  <br />\r\nShow Guide : Quarter Page Ad  <br />\r\nVIP Zone Access at Parties  <br />\r\nPre Show Dinner Networking.',2499.00,5,1);

/*Table structure for table `sitios_transmision` */

DROP TABLE IF EXISTS `sitios_transmision`;

CREATE TABLE `sitios_transmision` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `habilitado` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sitios_transmision` */

/*Table structure for table `suscripciones` */

DROP TABLE IF EXISTS `suscripciones`;

CREATE TABLE `suscripciones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `suscripciones` */

insert  into `suscripciones`(`id`,`email`,`fecha`) values 
(1,'asd@asd.com','2018-07-15 03:08:12');

/*Table structure for table `traduccion` */

DROP TABLE IF EXISTS `traduccion`;

CREATE TABLE `traduccion` (
  `id` varchar(50) NOT NULL,
  `id_seccion` int(5) unsigned DEFAULT NULL,
  `en` varchar(150) DEFAULT NULL,
  `es` varchar(150) DEFAULT NULL,
  `obs` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seccion` (`id_seccion`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `traduccion` */

insert  into `traduccion`(`id`,`id_seccion`,`en`,`es`,`obs`) values 
('btn_back',1,'Back','Atras','.'),
('btn_buy',1,'Buy','Comprar','.'),
('btn_cancel',1,'Cancel','Cancelar','boton cancelar'),
('btn_login',1,'Log In','Entrar','boton de entrar en el encabezado'),
('btn_logintobuy',1,'Login to buy','Ingresar para comprar','.'),
('btn_logout',1,'Log out','Salir','boton logout'),
('btn_profile',1,'Profile','Perfil','boton perfil'),
('btn_register',1,'Sign up','Registro','boton del regisitro'),
('btn_soldout',1,'Sold out','Agotado','.'),
('btn_submit',1,'Submit','Enviar','boton enviar formulario'),
('community_sponsor',2,'Community Sponsor','Community Sponsor','.'),
('days',1,'Days','Días','.'),
('desc_login',1,'Be part of the biggest and most exciting adult business Event in Latin America','Sé parte del evento de negocios adulto más grande y emocionante  en América Latina','descripcion formulario login'),
('frm_contact_email',1,'Your email (Required)','Correo electrónico (Requerido)','Etiqueta campo correo formulario contacto'),
('frm_contact_message',1,'Do you have a comment?','Mensaje','Etiqueta campo mensaje formulario contacto'),
('frm_contact_name',1,'Name (Required)','Nombre (Requerido)','Etiquera campo nombre formulario contacto'),
('frm_contact_subject',1,'Subject','Asunto','Etiqueta campo asunto formulario contacto'),
('frm_contact_title',1,'Lets get in touch!','Contáctenos!','titulo formulario contacto'),
('home_galeria',1,'Gallery','Galería','.'),
('hours',1,'Hours','Horas','.'),
('login_email',1,'Email','Correo electrónico','etiqueta campo correo login'),
('login_forgot_password',1,'Forgot your password?','Olvidé mi contraseña','texto olvido contraseña login'),
('login_password',1,'Password','Contraseña','etiqueta campo contraseña'),
('login_signup',1,'Don\'t have account? Sign Up here','¿No tiene cuenta? Regístrese aquí','texto registro login'),
('media_partner',2,'Media Partner','Socio de medios','.'),
('media_sponsors',2,'Media Sponsors','Medios Patrocinadores','.'),
('media_sponsors_text',2,'great companies covering and promoting our event','grandes compañías que cubren y promueven nuestro evento','.'),
('menu_asisten',1,'Who is coming','Asistentes','.'),
('menu_home',1,'Home','Inicio','.'),
('menu_premios',1,'Awards','Premios','.'),
('menu_program',1,'Schedule','Programación','.'),
('menu_sponsor',1,'Be our sponsor','Patrocinios','.'),
('minutes',1,'Minutes','Minutos','.'),
('newsletter',1,'Subscribe to our newsletter','Suscribirse a nuestro boletín','titulo boletin'),
('newsletter_hint',1,'Enter a valid email address...','Ingrese un email válido','placeholder newsletter'),
('presen_patrocin',2,'Presenting Sponsor','Patrocinador Presentador','.'),
('search',1,'Search...','Buscar...','texto buscar'),
('sponsor_category_1',2,'Crown Sponsors','Patrocinadores Crown','.'),
('sponsor_category_2',2,'Diamond Sponsors','Patrocinadores Diamond','.'),
('sponsor_category_3',2,'Platinum Sponsors','Patrocinadores Patinum','.'),
('sponsor_category_4',2,'Gold Sponsors','Patrocinadores Gold','.'),
('sponsor_category_5',2,'Bronze Sponsors','Patrocinadores Bronze','.'),
('sponsor_descrip',3,'We invite you to be a part of this exciting Event and to increase your Company Exposure in front of an extremely Targeted Audience by becoming a corpo','Te invitamos a ser parte de este emocionante evento y a aumentar la exposición de tu empresa frente a un público extremadamente objetivo al convertirt','.'),
('sponsor_text1',2,' Attend the First, Biggest and most important Adult Industry Event of','Asista al Primer, Mayor y más importante Evento de la Industria de Adultos de América latina','.'),
('sponsor_text10',2,'Best networking and Parties','Mejores redes y fiestas','.'),
('sponsor_text11',2,'Sponsorship opportunity','Oportunidad de patrocinio','.'),
('sponsor_text12',2,'Promote y our brand','Promociona tu marca','.'),
('sponsor_text13',2,'Show of your company','Muestra de su empresa','.'),
('sponsor_text14',2,'Make new contacts','Hacer nuevos contactos','.'),
('sponsor_text15',2,'Meet your latam partners','Conoce a tus socios latam','.'),
('sponsor_text16',2,'Strengthen relationships','Fortalecer las relaciones','.'),
('sponsor_text17',2,'Create a new market','Crea un nuevo mercado','.'),
('sponsor_text18',2,'Sponsors','Patrocinadores','.'),
('sponsor_text19',2,'Ready for business','Listo para los negocios','.'),
('sponsor_text2',2,'Latin-America','Latinoamérica ','.'),
('sponsor_text3',2,'The Definitive B2B Adult show of Central and South America. Come on over to the ONLY show in South America. Meet a total New market and be part of the','El espectáculo Definitive B2B Adult de América Central y del Sur. Venga al ÚNICO show en Sudamérica. Conoce un mercado totalmente nuevo y sé parte de ','.'),
('sponsor_text4',2,'1,300 Industry Profesionals','1,300 Profesionales de la industria','.'),
('sponsor_text5',2,'An unexploited new Market','Un nuevo mercado sin explotar','.'),
('sponsor_text6',2,'Companies ready for business','Empresas listas para hacer negocios','.'),
('sponsor_text7',2,'Hundreds of Deals closed','Cientos de ofertas cerradas','.'),
('sponsor_text8',2,'An opportunity to expand','Una oportunidad para expandirse','.'),
('sponsor_text9',2,'Big workshops and seminars','Grandes talleres y seminarios','.'),
('sponsor_title',3,'SPONSOR US','PATROCINAR','.'),
('title_login',1,'Log in','Ingresar','titulo login'),
('txtavailable',1,'Available','Disponible','.'),
('txtprice',1,'Price','Precio','.'),
('txtreserved',1,'Reserved','Reservado','.'),
('txtsold',1,'Sold','Vendido','.');

/*Table structure for table `traduccion_seccion` */

DROP TABLE IF EXISTS `traduccion_seccion`;

CREATE TABLE `traduccion_seccion` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `traduccion_seccion` */

insert  into `traduccion_seccion`(`id`,`nombre`) values 
(1,'Global'),
(2,'Home'),
(3,'Patrocinadores'),
(4,'Asistentes'),
(5,'Programacion');

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `activo` int(1) DEFAULT '0',
  `token` varchar(200) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `genero` varchar(50) DEFAULT NULL,
  `foto` varbinary(150) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `pass` varchar(30) DEFAULT NULL,
  `dni` varchar(50) DEFAULT NULL,
  `pais` varchar(50) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `ciudad` varchar(150) DEFAULT NULL,
  `lengua` varbinary(30) DEFAULT NULL,
  `telefono` varbinary(25) DEFAULT NULL,
  `id_categoria` int(5) unsigned DEFAULT NULL,
  `fecha_alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `anio` int(4) unsigned DEFAULT NULL,
  `company` varchar(70) DEFAULT NULL,
  `website` varchar(80) DEFAULT NULL,
  `models` varchar(10) DEFAULT NULL,
  `face` varchar(60) DEFAULT NULL,
  `twitter` varchar(60) DEFAULT NULL,
  `instagram` varchar(60) DEFAULT NULL,
  `skype` varchar(60) DEFAULT NULL,
  `youtube` varchar(60) DEFAULT NULL,
  `nombre_escarapela` varchar(100) DEFAULT NULL,
  `estudio` varchar(100) DEFAULT NULL,
  `objetivo` varbinary(100) DEFAULT NULL,
  `descrip_profesional` varbinary(150) DEFAULT NULL,
  `nickname` varbinary(50) DEFAULT NULL,
  `sitios_adicionales` varbinary(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `usuarios` */

insert  into `usuarios`(`id`,`activo`,`token`,`nombre`,`apellido`,`genero`,`foto`,`email`,`pass`,`dni`,`pais`,`estado`,`ciudad`,`lengua`,`telefono`,`id_categoria`,`fecha_alta`,`anio`,`company`,`website`,`models`,`face`,`twitter`,`instagram`,`skype`,`youtube`,`nombre_escarapela`,`estudio`,`objetivo`,`descrip_profesional`,`nickname`,`sitios_adicionales`) values 
(8,0,'8d6c07685aadbc44c1a210f16a1a67ca95ccbf2a0e8229e1a3','Cristian','Lescano','1',NULL,'cristian_lescano@hotmail.es','1','','Colombia','Atlantico',NULL,'','',2,'2018-07-12 11:17:59',2019,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dfsdfsdf',''),
(9,1,'c5609fb3bf002631ab88638ac7d4c72e1361eb7d7cb25e31249dd95a2f90356e95a61ed62aac2a4da2085777b827d7181ce7','Ana','Beltran','2','fotos/9.jpg','usuario@correo.com','1234','1111111111','Colombia','Bogota D.C.','','espanol','+5719238446',2,'2018-07-15 19:54:40',2019,'compañia uno','ffff','','/nanyonline','','@nanyoficial','','','david ','aj','entretnimiento ','','nany','aaaaa,fffff,455444f,'),
(10,0,'a68c132294f0eb0365a573312e2fbbfa3f049675c9544df2f39a77381cf193cf412138d097b562aa36293c8a51e2d4e84a66','david','lalexpo','1',NULL,'lalexpoaj@gmail.com','andres','5666555','Colombia','Antioquia',NULL,'español','3103382314',2,'2018-07-14 11:07:07',2019,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','MyFreeCams,QCams,Visit-X,WebcamClub,Webcams,XCams,'),
(11,1,'561ff80ec213db2128371b4879d511799e7ae9cfc45e24562aa2c757bb61ad55db799ff691846f29c22b8be5898c2e8d6c65','Gabriel','Martinez','1','fotos/9b98d50e.jpg','torresgabriele@gmail.com','1234','2222222','Colombia','Bogota D.C.',NULL,'espanol','+5719238446',2,'2018-07-16 22:26:00',2019,'compañia dos','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'gggg','99Cams,AdultWork,AllBBWCams,BongaCams,'),
(12,1,'c48cb4559486145a2f6f91a89e44623c973037806d51ccd8822223d96ea3475bc44b7cd2e28af1877dcf666a25b5f0aadfa7','ja','sss','1',NULL,'jaestrada12@hotmail.com','andres','38484','Colombia','Antioquia',NULL,'español','3103382314',2,'2018-07-15 19:54:57',2019,'compañia tres','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'andres','99Cams,AdultWork,'),
(13,0,'21f44b66c1dd440128ca844e31c869b4b78fc938eed4613b8fc606b6039c86803028221d6839b5917cde89e82ffca9304969','Daniel','Ramirez','1',NULL,'daniel@lacasacreativa.co','FenixRock3','1082915350','Colombia','Magdalena',NULL,'Español','3188486442',3,'2018-07-16 00:47:48',2019,'La Casa Creativa','www.lacasacreativa.co','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Dan',''),
(14,1,'e94522ace924bb93bbac83f79fe4d7fba016bb4f7961280c18d356d3119262de6885fd487348ad508f12736caddcd14a6193','sharon ','torres','2','','torreslopezs8@gmail.com','12345','1144040058','Colombia','Valle del Cauca','','Español','3155771788',2,'2018-07-17 16:33:42',2019,'','','','','','','','','sharon gomez','ajstudios','relaciones comerciales','','sharon gomez','');

/*Table structure for table `usuarios_back` */

DROP TABLE IF EXISTS `usuarios_back`;

CREATE TABLE `usuarios_back` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varbinary(30) NOT NULL,
  `pass` varbinary(30) NOT NULL,
  `email` varbinary(70) NOT NULL,
  `nombre` varbinary(70) DEFAULT NULL,
  `tipo` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `usuarios_back` */

insert  into `usuarios_back`(`id`,`user`,`pass`,`email`,`nombre`,`tipo`) values 
(1,'admin','admin','cristian_lescano@hotmail.es','Cristian333',1);

/*Table structure for table `usuarios_sitios` */

DROP TABLE IF EXISTS `usuarios_sitios`;

CREATE TABLE `usuarios_sitios` (
  `id_usuarios` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_sitios` int(6) unsigned NOT NULL,
  PRIMARY KEY (`id_usuarios`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `usuarios_sitios` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
