-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 20-07-2018 a las 17:05:24
-- Versión del servidor: 5.5.59-cll-lve
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `lalexpo_lalexpo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banners_principal`
--

DROP TABLE IF EXISTS `banners_principal`;
CREATE TABLE `banners_principal` (
  `id` int(11) UNSIGNED NOT NULL,
  `banner` varchar(20) DEFAULT NULL,
  `archivo_en` varchar(150) DEFAULT NULL,
  `archivo_es` varchar(150) DEFAULT NULL,
  `fecha_alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `activo` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `banners_principal`
--

INSERT INTO `banners_principal` (`id`, `banner`, `archivo_en`, `archivo_es`, `fecha_alta`, `activo`) VALUES
(6, 'principal', '0.3717500015317182131.mp4', '0.3717690015317182132.mp4', '2018-07-17 22:55:26', 1),
(8, 'principal', '0.4182390015317182311.mp4', '0.4182550015317182312.mp4', '2018-07-17 22:55:32', 1),
(9, 'principal', '0.8747820015317185601.mp4', '0.8748070015317185602.mp4', '2018-07-17 22:55:40', 1),
(11, '3', '0.0876090015318892821.jpg', '0.0876760015318892822.jpg', '2018-07-18 01:07:20', 1),
(12, '4', '0.0894890015318913311.jpg', '0.0895140015318913312.jpg', '2018-07-18 01:07:24', 1),
(15, '1', '0.3385940015318940991.png', '0.3386370015318940992.png', '2018-07-18 01:08:19', 1),
(16, '2', '0.9026120015318941281.png', '0.9026340015318941282.png', '2018-07-18 01:08:48', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

DROP TABLE IF EXISTS `categorias`;
CREATE TABLE `categorias` (
  `id` int(5) UNSIGNED NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`) VALUES
(2, 'Modelo'),
(3, 'Studio'),
(4, 'Webmaster'),
(5, 'Other');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faq`
--

DROP TABLE IF EXISTS `faq`;
CREATE TABLE `faq` (
  `id` int(5) UNSIGNED NOT NULL,
  `id_seccion` int(5) UNSIGNED DEFAULT NULL,
  `preg_es` varchar(250) DEFAULT NULL,
  `preg_en` varchar(250) DEFAULT NULL,
  `resp_es` varchar(250) DEFAULT NULL,
  `resp_en` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faq_seccion`
--

DROP TABLE IF EXISTS `faq_seccion`;
CREATE TABLE `faq_seccion` (
  `id` int(5) UNSIGNED NOT NULL,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `habilitado` int(1) UNSIGNED DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `faq_seccion`
--

INSERT INTO `faq_seccion` (`id`, `nombre_es`, `nombre_en`, `habilitado`) VALUES
(1, 'Inicio', 'Home', 1),
(2, 'cat2', 'cat two', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria`
--

DROP TABLE IF EXISTS `galeria`;
CREATE TABLE `galeria` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_album` int(6) NOT NULL,
  `archivo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `galeria`
--

INSERT INTO `galeria` (`id`, `id_album`, `archivo`) VALUES
(6, 1, '0.763519001531772230.jpg'),
(7, 1, '0.647924001531772245.jpg'),
(8, 1, '0.354857001531772276.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria_album`
--

DROP TABLE IF EXISTS `galeria_album`;
CREATE TABLE `galeria_album` (
  `id` int(6) UNSIGNED NOT NULL,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `habilitado` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `galeria_album`
--

INSERT INTO `galeria_album` (`id`, `nombre_es`, `nombre_en`, `habilitado`) VALUES
(1, 'Evento', 'Event', 1),
(2, '2222', '22222', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel`
--

DROP TABLE IF EXISTS `hotel`;
CREATE TABLE `hotel` (
  `id` int(11) UNSIGNED NOT NULL,
  `titulo` varchar(70) DEFAULT NULL,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `imagen_en` varchar(100) DEFAULT NULL,
  `imagen_es` varchar(100) DEFAULT NULL,
  `imgvuelos_es` varchar(100) DEFAULT NULL,
  `imgvuelos_en` varchar(100) DEFAULT NULL,
  `subtitulo_es` varchar(150) DEFAULT NULL,
  `subtitulo_en` varchar(150) DEFAULT NULL,
  `descripcion_es` longtext,
  `descripcion_en` longtext,
  `adicional_es` longtext,
  `adicional_en` longtext,
  `categoria` int(1) DEFAULT NULL,
  `estado` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `hotel`
--

INSERT INTO `hotel` (`id`, `titulo`, `nombre_es`, `nombre_en`, `imagen_en`, `imagen_es`, `imgvuelos_es`, `imgvuelos_en`, `subtitulo_es`, `subtitulo_en`, `descripcion_es`, `descripcion_en`, `adicional_es`, `adicional_en`, `categoria`, `estado`) VALUES
(1, 'Hotel NH Cali Royal', '<span class=\"title-span-ctg-lalexpo\">19-21/FEB/2017 <span class=\"box-span-red-ctg-lalexpo\">Cali /Col</span>', '<span class=\"title-span-ctg-lalexpo\">19-21/FEB/2017 <span class=\"box-span-red-ctg-lalexpo\">Cali /Col</span>', '0.4781230015318978882.jpg', '0.4766570015318978881.jpg', '0.0273510015318978663.jpg', '0.0287950015318978664.jpg', 'Tarifas especiales para los asistentes y expositores del Show LALEXPO 2018', 'Special rates for attendees and exhibitors of the 2018 LALEXPO Show', 'Cali es la capital del departamento del Valle del Cauca, también conocida como la capital mundial de la salsa, la sultana del Valle o la sucursal del cielo, es la tercera ciudad de la República de Colombia. Con más de 477 años de existencia es una de las ciudades más antiguas de América.</br></br>\n Es uno de los principales centros económicos e industriales de Colombia, además de ser el principal centro urbano, cultural, económico, industrial y agrario del suroccidente del país. </br></br>\nCali es una ciudad de grandes espacios para el turismo y la recreación,  abundan en ella las mujeres hermosas, los sitios de valor histórico y espacios para la diversión diurna y nocturna que hacen de ella una meca del turismo.  De igual manera la sucursal del valle se identifica por su gastronomía tradicional, una cocina que funde las herencias española, indígena y africana, dando un sabor único. Así nacen el sancocho de gallina, el arroz atollado, la sopa de tortillas, el aborrajado, las tostadas de plátano verde con hogao y los tamales.', 'Cali is the capital of the State of Valle del Cauca, also known as the world capital of Salsa music, La Sultana del Valle or “Sucursal del cielo” (heaven’s branch), is the third  most populated City of the Republic of Colombia. With more than 477 years of existence it is one of the oldest cities in America.</br></br>\nIt is one of the main economic and industrial centers of Colombia, as well as being the main urban, cultural, economic, Industrial and Agrarian Center of the southwest of the country.</br></br>\nCali is a City of great spaces for tourism and recreation, there are plenty of beautiful women, sites of historical value and spaces for day and night fun that makes it a mecca of tourism. Similarly, it is also identified by its traditional cuisine, a cuisine that melts together the Spanish, indigenous and African heritages, creating a unique taste. This is how the Sancocho, the atollado rice, the tortilla soup, the aborrajado, the green plantain toast with Hogao and the tamales are born.', 'Está situado en una agradable zona residencial, justo al lado del centro comercial Holguines Trade Center. Los mejores restaurantes y bares de la Avenida San Joaquín quedan a pocos minutos caminando, mientras que el animado centro de Cali se encuentra a solo 20 minutos en automóvil.</br></br>\n\n• Junto al complejo Holguines Trade Center</br>\n• Cerca de los mejores centros comerciales de la ciudad: Unicentro y Jardín Plaza</br>\n• A poca distancia a pie de uno de los mejores campos de golf de Cali</br></br>\nLas 145 habitaciones del hotel son más grandes que las de la mayoría de los hoteles de la ciudad. Limpias, modernas y con una decoración en tonos suaves, las alcobas son perfectas tanto para turistas como para viajeros de negocios. Las habitaciones Suite Royal dan a la piscina, mientras que las Ejecutivas tienen vista panorámica de la ciudad.', 'The hotel is in a charming residential area, right next to the Holguines Trade Center. It’s only a short walk to the best restaurants and bars on the Avenida San Joaquin, while the bustling centre of Cali is only 20 minutes’ drive away.</br></br>\n\n• Next to the Holguines Trade Center complex</br>\n• Close to the city’s best shopping malls: Unicentro and Jardin Plaza</br>\n• Short walk to one of Cali’s best golf courses</br></br>\nThe hotel’s 145 rooms are bigger than most hotels in the city. Clean, modern and with muted décor, rooms are perfect for both tourists and those on business. Suite Royal rooms overlook the pool, while Executive Rooms have panoramic views of the city.', 5, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel_categorias_hab`
--

DROP TABLE IF EXISTS `hotel_categorias_hab`;
CREATE TABLE `hotel_categorias_hab` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `precio` int(8) UNSIGNED DEFAULT NULL,
  `adicional` int(4) UNSIGNED DEFAULT NULL,
  `disponibles` int(4) UNSIGNED DEFAULT NULL,
  `vendidos` int(4) UNSIGNED DEFAULT '0',
  `descripcion_es` longtext,
  `descripcion_en` longtext,
  `estado` int(1) UNSIGNED DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `hotel_categorias_hab`
--

INSERT INTO `hotel_categorias_hab` (`id`, `nombre_es`, `nombre_en`, `imagen`, `precio`, `adicional`, `disponibles`, `vendidos`, `descripcion_es`, `descripcion_en`, `estado`) VALUES
(1, 'test2aa', 'aaa', '0.812720001531892831.png', 10, 2, 99, 1, 'des ', 'des ', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel_habitciones`
--

DROP TABLE IF EXISTS `hotel_habitciones`;
CREATE TABLE `hotel_habitciones` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_categoria` int(11) UNSIGNED DEFAULT NULL,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `precio` int(8) UNSIGNED DEFAULT NULL,
  `personas` int(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `hotel_habitciones`
--

INSERT INTO `hotel_habitciones` (`id`, `id_categoria`, `nombre_es`, `nombre_en`, `imagen`, `precio`, `personas`) VALUES
(2, 1, 'hab test', 'test', '0.315482001531893960.png', 150, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel_reservas`
--

DROP TABLE IF EXISTS `hotel_reservas`;
CREATE TABLE `hotel_reservas` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_usuario` int(11) UNSIGNED NOT NULL,
  `id_categoria` int(11) UNSIGNED NOT NULL,
  `id_habitacion` int(11) DEFAULT NULL,
  `id_pago` int(11) UNSIGNED DEFAULT '0',
  `entrada` date DEFAULT NULL,
  `salida` date DEFAULT NULL,
  `contac_nombre` varchar(70) DEFAULT NULL,
  `contac_email` varchar(70) DEFAULT NULL,
  `contac_telef` varchar(50) DEFAULT NULL,
  `totalPagar` int(11) UNSIGNED NOT NULL,
  `personas` int(11) UNSIGNED NOT NULL,
  `estado` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `hotel_reservas`
--

INSERT INTO `hotel_reservas` (`id`, `id_usuario`, `id_categoria`, `id_habitacion`, `id_pago`, `entrada`, `salida`, `contac_nombre`, `contac_email`, `contac_telef`, `totalPagar`, `personas`, `estado`) VALUES
(1, 0, 1, 2, 2, '0000-00-00', '0000-00-00', '', '', '', 0, 0, 2),
(2, 0, 1, 2, 0, '0000-00-00', '0000-00-00', '', '', '', 0, 0, 0),
(3, 0, 1, 2, 0, '2018-07-19', '2018-07-21', 'cris', 'asd@asd.com', '456777', 300, 3, 0),
(4, 8, 1, 2, 0, '2018-07-19', '2018-07-21', 'cris', 'asd@asd.com', '456777', 300, 3, 0),
(5, 8, 1, 2, 1, '2018-07-19', '2018-07-21', 'cristian', 'cristian_lescano@hotmail.es', '437651', 300, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel_reservas_hospedados`
--

DROP TABLE IF EXISTS `hotel_reservas_hospedados`;
CREATE TABLE `hotel_reservas_hospedados` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_reserva` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(70) CHARACTER SET latin1 DEFAULT NULL,
  `pasaporte` varchar(50) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `hotel_reservas_hospedados`
--

INSERT INTO `hotel_reservas_hospedados` (`id`, `id_reserva`, `nombre`, `pasaporte`) VALUES
(1, 3, 'pablo', '123'),
(2, 3, 'julio', '456'),
(3, 3, 'pedro', '789'),
(4, 4, 'pablo', '123'),
(5, 4, 'julio', '456'),
(6, 4, 'pedro', '789'),
(7, 5, 'juan', '123'),
(8, 5, 'marcos', '456'),
(9, 5, 'victor', '789');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomencladores`
--

DROP TABLE IF EXISTS `nomencladores`;
CREATE TABLE `nomencladores` (
  `id` int(6) UNSIGNED NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `habilitado` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

DROP TABLE IF EXISTS `pagos`;
CREATE TABLE `pagos` (
  `id` int(11) UNSIGNED NOT NULL,
  `state_pol` varchar(32) DEFAULT NULL,
  `test` int(1) UNSIGNED DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  `billing_country` varchar(2) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `value` decimal(14,2) DEFAULT NULL,
  `response_message_pol` varchar(255) DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `transaction_id` varchar(36) DEFAULT NULL,
  `payment_method_name` varchar(255) DEFAULT NULL,
  `payment_method_id` int(3) UNSIGNED DEFAULT NULL,
  `billing_city` varchar(255) DEFAULT NULL,
  `reference_sale` varchar(255) DEFAULT NULL,
  `todo` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id`, `state_pol`, `test`, `transaction_date`, `billing_country`, `description`, `value`, `response_message_pol`, `currency`, `transaction_id`, `payment_method_name`, `payment_method_id`, `billing_city`, `reference_sale`, `todo`) VALUES
(1, '4', 1, '2018-07-20 02:34:40', 'CO', 'Compra LalExpo Hoteleria', '300.00', 'APPROVED', 'USD', 'd52a1675-59cb-48eb-ac29-3dae5825dbeb', 'MASTERCARD', 2, '', 'reserva_5', '{\"response_code_pol\":\"1\",\"phone\":\"437651\",\"additional_value\":\"0.00\",\"test\":\"1\",\"transaction_date\":\"2018-07-20 02:34:40\",\"cc_number\":\"************0108\",\"cc_holder\":\"APPROVED\",\"error_code_bank\":\"\",\"billing_country\":\"CO\",\"bank_referenced_name\":\"\",\"description\":\"Compra LalExpo Hoteleria\",\"administrative_fee_tax\":\"0.00\",\"value\":\"300.00\",\"administrative_fee\":\"0.00\",\"payment_method_type\":\"2\",\"office_phone\":\"\",\"email_buyer\":\"cristian_lescano@hotmail.es\",\"response_message_pol\":\"APPROVED\",\"error_message_bank\":\"\",\"shipping_city\":\"\",\"transaction_id\":\"d52a1675-59cb-48eb-ac29-3dae5825dbeb\",\"sign\":\"d5a434488a02395282d7db106fc3c425\",\"tax\":\"0.00\",\"transaction_bank_id\":\"00000000\",\"payment_method\":\"11\",\"billing_address\":\"\",\"payment_method_name\":\"MASTERCARD\",\"pse_bank\":\"\",\"state_pol\":\"4\",\"date\":\"2018.07.20 02:34:40\",\"nickname_buyer\":\"\",\"reference_pol\":\"844451038\",\"currency\":\"USD\",\"risk\":\"0.0\",\"shipping_address\":\"\",\"bank_id\":\"11\",\"payment_request_state\":\"A\",\"customer_number\":\"\",\"administrative_fee_base\":\"0.00\",\"attempts\":\"1\",\"merchant_id\":\"508029\",\"exchange_rate\":\"2874.01\",\"shipping_country\":\"CO\",\"installments_number\":\"1\",\"franchise\":\"MASTERCARD\",\"payment_method_id\":\"2\",\"extra1\":\"\",\"extra2\":\"\",\"antifraudMerchantId\":\"\",\"extra3\":\"\",\"commision_pol_currency\":\"COP\",\"nickname_seller\":\"\",\"ip\":\"172.18.49.47\",\"commision_pol\":\"238.00\",\"airline_code\":\"\",\"billing_city\":\"\",\"pse_reference1\":\"\",\"cus\":\"00000000\",\"reference_sale\":\"reserva_5\",\"authorization_code\":\"00000000\",\"pse_reference3\":\"\",\"pse_reference2\":\"\"}'),
(2, '6', 1, '2018-07-20 03:00:00', 'CO', 'Compra LalExpo Hoteleria', '300.00', 'ABANDONED_TRANSACTION', 'USD', '12778708-a100-4d3d-990b-d40ebe05f878', '', 0, '', 'reserva_1', '{\"response_code_pol\":\"19\",\"phone\":\"423544\",\"additional_value\":\"0.00\",\"test\":\"1\",\"transaction_date\":\"2018-07-20 03:00:00\",\"cc_number\":\"\",\"cc_holder\":\"\",\"error_code_bank\":\"\",\"billing_country\":\"CO\",\"bank_referenced_name\":\"\",\"description\":\"Compra LalExpo Hoteleria\",\"administrative_fee_tax\":\"0.00\",\"value\":\"300.00\",\"administrative_fee\":\"0.00\",\"payment_method_type\":\"\",\"office_phone\":\"\",\"cc_data\":\"\",\"email_buyer\":\"cristian_@mail.com\",\"response_message_pol\":\"ABANDONED_TRANSACTION\",\"error_message_bank\":\"\",\"shipping_city\":\"\",\"transaction_id\":\"12778708-a100-4d3d-990b-d40ebe05f878\",\"sign\":\"b7be07d618c42b73f8f9fa36a4fff647\",\"tax\":\"0.00\",\"payment_method\":\"\",\"billing_address\":\"\",\"pse_bank\":\"\",\"state_pol\":\"6\",\"date\":\"2018.07.20 03:00:00\",\"nickname_buyer\":\"\",\"reference_pol\":\"844451034\",\"currency\":\"USD\",\"risk\":\"\",\"shipping_address\":\"\",\"bank_id\":\"\",\"payment_request_state\":\"C\",\"customer_number\":\"\",\"administrative_fee_base\":\"0.00\",\"attempts\":\"1\",\"merchant_id\":\"508029\",\"exchange_rate\":\"0\",\"shipping_country\":\"CO\",\"franchise\":\"\",\"payment_method_id\":\"\",\"extra1\":\"\",\"extra2\":\"\",\"antifraudMerchantId\":\"\",\"extra3\":\"\",\"nickname_seller\":\"\",\"ip\":\"172.18.49.47\",\"airline_code\":\"\",\"billing_city\":\"\",\"pse_reference1\":\"\",\"reference_sale\":\"reserva_1\",\"pse_reference3\":\"\",\"pse_reference2\":\"\"}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos_error`
--

DROP TABLE IF EXISTS `pagos_error`;
CREATE TABLE `pagos_error` (
  `id` int(11) UNSIGNED NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `query` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paquetes`
--

DROP TABLE IF EXISTS `paquetes`;
CREATE TABLE `paquetes` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_categoria` int(6) UNSIGNED DEFAULT NULL,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `precio` int(8) UNSIGNED DEFAULT NULL,
  `disponibles` int(6) UNSIGNED DEFAULT NULL,
  `vendidos` int(6) UNSIGNED DEFAULT NULL,
  `descripcion_es` longtext,
  `descripcion_en` longtext,
  `archivo` varchar(150) DEFAULT NULL,
  `estado` int(1) UNSIGNED DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paquetes_categorias`
--

DROP TABLE IF EXISTS `paquetes_categorias`;
CREATE TABLE `paquetes_categorias` (
  `id` int(6) UNSIGNED NOT NULL,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `orden` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `paquetes_categorias`
--

INSERT INTO `paquetes_categorias` (`id`, `nombre_es`, `nombre_en`, `orden`) VALUES
(2, 'SPONSORSHIP PREMIUM PACKS', 'SPONSORSHIP PREMIUM PACKS', 1),
(3, '	SPONSOR FOOD AND DRINKS', '	SPONSOR FOOD AND DRINKS', 2),
(4, '	ADITIONAL PACKS AND SERVICES', '	ADITIONAL PACKS AND SERVICES', 3),
(5, 'HOSPITALITY PACKS', 'HOSPITALITY PACKS', 4),
(6, '	MAIN ADVERTISING', '	MAIN ADVERTISING', 5),
(7, 'EVENTOS Y ACTIVIDADES', 'ACTIVITIES AND EVENTS', 6),
(8, 'FIESTAS', 'PARTIES', 7),
(9, 'AWARDS', 'AWARDS', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paquetes_usuario`
--

DROP TABLE IF EXISTS `paquetes_usuario`;
CREATE TABLE `paquetes_usuario` (
  `id_usuario` int(11) UNSIGNED NOT NULL,
  `id_paquete` int(11) UNSIGNED NOT NULL,
  `id_reserva_estado` int(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametros`
--

DROP TABLE IF EXISTS `parametros`;
CREATE TABLE `parametros` (
  `payu_url` varchar(70) DEFAULT NULL,
  `payu_merchantId` varchar(10) DEFAULT NULL,
  `payu_apikey` varchar(35) DEFAULT NULL,
  `payu_accountId` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `parametros`
--

INSERT INTO `parametros` (`payu_url`, `payu_merchantId`, `payu_apikey`, `payu_accountId`) VALUES
('https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/', '508029', '4Vj8eK4rloUd272L48hsrarnUA', '512322');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patrocinador_categoria_paquete`
--

DROP TABLE IF EXISTS `patrocinador_categoria_paquete`;
CREATE TABLE `patrocinador_categoria_paquete` (
  `id` int(11) NOT NULL,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `patrocinador_categoria_paquete`
--

INSERT INTO `patrocinador_categoria_paquete` (`id`, `nombre_es`, `nombre_en`, `estado`) VALUES
(1, 'Paquetes Premium de Patrocinios', 'Sponsorship premium packs', 1),
(2, 'Patrocinio de Alimentos y Bebidas', 'Sponsor food and drinks', 1),
(3, 'Paquetes y servicios adicionales', 'Aditional packs &amp; services', 1),
(4, 'Paquetes de hospitalidad', 'Hospitality packs', 1),
(5, 'Actividades y Eventos', 'Activities &amp; Events', 1),
(6, 'Premios', 'Awards', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patrocinador_paquete`
--

DROP TABLE IF EXISTS `patrocinador_paquete`;
CREATE TABLE `patrocinador_paquete` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `descrip_es` text,
  `descrip_en` text,
  `precio` decimal(18,2) DEFAULT '0.00',
  `cantidad` int(11) DEFAULT '0',
  `estado` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `patrocinador_paquete`
--

INSERT INTO `patrocinador_paquete` (`id`, `id_categoria`, `nombre_es`, `nombre_en`, `descrip_es`, `descrip_en`, `precio`, `cantidad`, `estado`) VALUES
(1, 1, 'Patrocinador <span style=\"color:#ffcc33\">Crown</span>', '<span style=\"color:#ffcc33\">Crown</span> sponsor', 'Registro de hotel: prioridad <br />\r\nTraslado al aeropuerto <br />\r\nSalón de lujo en el piso de la exposición: grande <br />\r\nBoletos incluidos: 10 <br />\r\nEntrada de afiliado GRATUITA: 20 <br />\r\nBoleto adicional: $ 75 <br />\r\nColocación del logotipo Página de inicio: Principal <br />\r\nColocación del logotipo Publicidad: Principal <br />\r\nBanner grande <br />\r\nPosición de la bandera: 6 <br />\r\nBanderas verticales rígidas: 4 <br />\r\nSala de seminarios: 1 hora <br />\r\nConozca la tabla de mercado <br />\r\nInserciones de exhibición y bolsa: 4 <br />\r\nCompañías participantes de precontacto <br />\r\nBoletín informativo para todos los participantes <br />\r\nPanel de entrevistas: Destacado <br />\r\nTraductores: 2 <br />\r\nExposición social <br />\r\nMostrar guía: Anuncio de página completa <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking', 'Hotel registration : Priority<br />\r\n              Airport transfer <br />\r\n              Luxury Lounge in show floor : Large <br />\r\n              Included tickets : 10 <br />\r\n              FREE Affiliate entrance ticket : 20 <br />\r\n              Extra ticket : $75 <br />\r\n              Logo placement Homepage : Main <br />\r\n              Logo placement Advertising : Main <br />\r\n              Large Banner<br />\r\n              Flag position : 6 <br />\r\n              Rigid stand-up-banners : 4 <br />\r\n              Seminar Lounge : 1 Hour <br />\r\n              Meet Market Table <br />\r\n              Show and bag inserts : 4 <br />\r\n              Precontact Participant Companys <br />\r\n              Newsletter to Participants All <br />\r\n              Interviews panel : Prominent <br />\r\n              Translators : 2 <br />\r\n              Social Exposure <br />\r\n              Show Guide : Full Page Ad <br />\r\n              VIP Zone Access at Parties <br />\r\n              Pre Show Dinner Networking', '14999.00', 5, 1),
(2, 1, 'Patrocinador <span style=\"color:#99ccff\">Diamond</span>', '<span style=\"color:#99ccff\">Diamond</span> sponsor', 'Registro de hotel: Prioridad <br>\r\nTraslado al aeropuerto <br>\r\nLounge de lujo en el piso de la exposición: medio <br>\r\nBoletos incluidos: 8 <br>\r\nEntrada GRATUITA para afiliado: 15 <br>\r\nBoleto adicional: $ 80 <br>\r\nLogo placement Homepage: Main <br>\r\nLogo placement Publicidad: Main <br>\r\nLarge Banner <br>\r\nPosición de la bandera: 4 <br>\r\nRígido stand-up-banners: 2 <br>\r\nSala de seminarios: 30 minutos <br>\r\nMeet Market Table <br>\r\nInserciones de vitrinas y carteras: 3 <br>\r\nPrecontact Participant Companys <br>\r\nBoletín a los participantes de todo <br>\r\nPanel de entrevistas: Secundaria <br>\r\nTraductores: 1 <br>\r\nExposición social <br>\r\nMostrar guía: Anuncio de página completa <br>\r\nAcceso a la zona VIP en las fiestas <br>\r\nPre Show Dinner Networking', 'Hotel registration : Priority <br>\r\nAirport transfer <br>\r\nLuxury Lounge in show floor : Medium <br>\r\nIncluded tickets : 8 <br>\r\nFREE Affiliate entrance ticket : 15 <br>\r\nExtra ticket : $80 <br>\r\nLogo placement Homepage : Main <br>\r\nLogo placement Advertising : Main <br>\r\nLarge Banner<br>\r\nFlag position : 4 <br>\r\nRigid stand-up-banners : 2 <br>\r\nSeminar Lounge : 30 Mins <br>\r\nMeet Market Table <br>\r\nShow and bag inserts : 3 <br>\r\nPrecontact Participant Companys <br>\r\nNewsletter to Participants All <br>\r\nInterviews panel : Secondary <br>\r\nTranslators : 1 <br>\r\nSocial Exposure <br>\r\nShow Guide : Full Page Ad <br>\r\nVIP Zone Access at Parties <br>\r\nPre Show Dinner Networking', '9999.00', 5, 1),
(3, 1, 'Patrocinador <span style=\"color:#aae2e0\">Platinum</span>', '<span style=\"color:#aae2e0\">Platinum</span> sponsor', 'Registro de hotel: Prioridad <br>\r\nTraslado al aeropuerto <br>\r\nSalón de lujo en el piso de exhibición: pequeño<br>\r\nBoletos incluidos: 6 <br>\r\nEntrada GRATUITA para afiliado: 10 <br>\r\nBoleto extra: $ 90 <br>\r\nLogo placement Homepage: Secundaria <br>\r\nLogo placement Publicidad: Secundaria <br>\r\nLarge Banner <br>\r\nPosición de la bandera: 2<br>\r\nRígido stand-up-banners: 1 <br>\r\nMeet Market Table <br>\r\nInserciones de exhibición y bolsa: 2 <br>\r\nPrecontact Participant Companys <br>\r\nBoletín a los participantes de todo <br>\r\nPanel de entrevistas: Secundaria <br>\r\nExposición social <br>\r\nMostrar guía: Anuncio de media página <br>\r\nAcceso a la zona VIP en las fiestas <br>\r\nPre Show Dinner Networking', 'Hotel registration : Priority <br>\r\nAirport transfer  <br>\r\nLuxury Lounge in show floor : Small  <br>\r\nIncluded tickets : 6  <br>\r\nFREE Affiliate entrance ticket : 10  <br>\r\nExtra ticket : $90  <br>\r\nLogo placement Homepage : Secondary  <br>\r\nLogo placement Advertising : Secondary  <br>\r\nLarge Banner <br>\r\nFlag position : 2  <br>\r\nRigid stand-up-banners : 1  <br>\r\nMeet Market Table  <br>\r\nShow and bag inserts : 2  <br>\r\nPrecontact Participant Companys  <br>\r\nNewsletter to Participants All  <br>\r\nInterviews panel : Secondary  <br>\r\nSocial Exposure  <br>\r\nShow Guide : Half Page Ad  <br>\r\nVIP Zone Access at Parties  <br>\r\nPre Show Dinner Networking', '4999.00', 5, 1),
(4, 1, 'Patrocinador <span style=\"color:#f58800\">Gold</span>', '<span style=\"color:#f58800\">Gold</span> sponsor', 'Registro de hotel: prioridad <br />\r\nBoletos incluidos: 4 <br />\r\nBoleto adicional: $ 95 <br />\r\nColocación del logotipo Página de inicio: Pequeño <br />\r\nColocación del logotipo Publicidad: Pequeño <br />\r\nBanner grande <br />\r\nPosición de la bandera: 1 <br />\r\nConozca la tabla de mercado <br />\r\nInserciones de exhibición y bolsa: 1 <br />\r\nCompañías participantes de precontacto <br />\r\nExposición social <br />\r\nMostrar guía: anuncio de página trimestre <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking', 'Hotel registration : Priority <br />\r\n              Included tickets : 4 <br />\r\n              Extra ticket : $95 <br />\r\n              Logo placement Homepage : Small <br />\r\n              Logo placement Advertising : Small <br />\r\n              Large Banner<br />\r\n              Flag position : 1 <br />\r\n              Meet Market Table <br />\r\n              Show and bag inserts : 1 <br />\r\n              Precontact Participant Companys <br />\r\n              Social Exposure <br />\r\n              Show Guide : Quarter Page Ad <br />\r\n              VIP Zone Access at Parties <br />\r\n              Pre Show Dinner Networking', '2999.00', 5, 1),
(5, 1, 'Patrocinador <span style=\"color:#d16528\">Bronze</span>', '<span style=\"color:#d16528\">Bronze</span> sponsor', 'Registro de hotel: prioridad <br />\r\nBoletos incluidos: 1 <br />\r\nColocación del logotipo Página de inicio: Pequeño <br />\r\nColocación del logotipo Publicidad: Pequeño <br />\r\nExposición social <br />\r\nMostrar guía: anuncio de página trimestre <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking', 'Hotel registration : Priority <br />\r\nIncluded tickets : 1 <br />\r\nLogo placement Homepage : Small <br />\r\nLogo placement Advertising : Small <br />\r\nSocial Exposure <br />\r\nShow Guide : Quarter Page Ad <br />\r\nVIP Zone Access at Parties <br />\r\nPre Show Dinner Networking', '1250.00', 5, 1),
(6, 2, 'Cena previa al evento', 'Pre show Dinner Networking', 'Dé la bienvenida a los jugadores principales con una cena en la<br /> noche antes del evento. <br />\r\nInvitar solo Actividad para Patrocinadores y VIP\'s de LALEXPO. <br />\r\nCapacidad máxima 100 asistentes. Restaurante elegido<br /> por el Patrocinador de <br />\r\nLista provista por LALEXPO <br />\r\nVolantes (14 páginas) <br />\r\n2 banners rígidos <br />\r\n1 Anfitriona en la cena que acompaña al patrocinador<br /> (el atuendo NOCTURNO será <br />\r\nproporcionado por el patrocinador). <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nBoletos incluidos: 4 <br />\r\nColocación del logotipo Página de inicio: Pequeño <br />\r\nColocación del logotipo Publicidad: Pequeño <br />\r\nExposición social <br />\r\nMostrar guía: anuncio de página trimestre <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nCena previa al evento <br />\r\n<img style=\"margin-top: 20px;\" src=\"http://lalexpo.com/uploads/paquetes/DSC_8410.jpg\" class=\"img-responsive\">', 'Welcome the main players with a dinner on the Evening before the Event. <br />\r\nInvite only Activity for Sponsors and VIP´s of LALEXPO. <br />\r\nMaximum capacity 100 Attendees. Restaurant to be chosen by Sponsor from <br /> \r\nLALEXPO provided list  <br />\r\nFlyers (14 Page)  <br />\r\n2 Rigid Banners  <br />\r\n1 Hostess at Dinner accompanying the Sponsor (NIGHT Outfit to be<br /> \r\nprovided by sponsor).  <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :<br /> \r\nHotel registration : Priority  <br />\r\nIncluded tickets : 4  <br />\r\nLogo placement Homepage : Small  <br />\r\nLogo placement Advertising : Small  <br />\r\nSocial Exposure  <br />\r\nShow Guide : Quarter Page Ad  <br />\r\nVIP Zone Access at Parties  <br />\r\nPre Show Dinner Networking.  <br /> \r\n<img style=\"margin-top: 20px;\" src=\"http://lalexpo.com/uploads/paquetes/DSC_8410.jpg\" class=\"img-responsive\">', '3999.00', 5, 1),
(7, 2, 'Estación de enfriamiento de agua', 'Water cooling', 'Botellas de agua personalizadas con el logotipo de su empresa. <br />\r\nIncluye 1.200 botellas de agua, 400 distribuidas cada día del evento. <br />\r\nPegatinas (Logo - Marca) en botellas de agua. <br />\r\nBanner en la estación de enfriamiento de agua. <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nBoletos incluidos: 4 <br />\r\nColocación del logotipo Página de inicio: Pequeño <br />\r\nColocación del logotipo Publicidad: Pequeño <br />\r\nExposición social <br />\r\nMostrar guía: anuncio de página trimestre <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking.', 'Custom branded water bottles with your company logo.  <br /> \r\nIncludes 1.200 water bottles, 400 distributed each day of the event.  <br />  \r\nStickers (Logo - Brand) on water bottles.   <br /> \r\nBanner on water cooling station.  <br />  \r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />  \r\nHotel registration : Priority  <br />\r\nIncluded tickets : 4  <br />\r\nLogo placement Homepage : Small  <br />\r\nLogo placement Advertising : Small  <br />\r\nSocial Exposure  <br />\r\nShow Guide : Quarter Page Ad  <br />\r\nVIP Zone Access at Parties  <br />\r\nPre Show Dinner Networking.', '2499.00', 5, 1),
(8, 2, 'Receso', 'Coffe breaks', 'Su propia estación de café para ofrecer café gratis <br /> y comida ligera para todos. <br />\r\nSe ofrece cada día del evento de 8 a.m. a 11:00 a.m. <br />\r\nPegatinas (Logotipo - Marca) en las tazas de café <br />\r\nBanner en la estación de café <br />\r\n1000 volantes (1\r\n4 páginas) <br />\r\n1 Anfitriona sirviendo el café (El atuendo debe <br /> ser provisto por usted. <br />\r\nExcelente exposición en horas de mucho tráfico. <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nTraslado al aeropuerto <br />\r\nBoletos incluidos: 6 <br />\r\nEntrada de afiliado GRATUITA: 10 <br />\r\nColocación del logotipo Página de inicio: Secundaria <br />\r\nColocación del logotipo Publicidad: secundaria <br />\r\nBoletín informativo para todos los participantes <br />\r\nPanel de entrevistas: Secundario <br />\r\nExposición social <br />\r\nMostrar guía: Anuncio de media página <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking', 'Your own coffee Station to offer free cofee and light food to everybody.   <br />\r\nProvided each day of the event from 8AM to 11AM.   <br />\r\nStickers (Logo - Brand) on Coffee Cups   <br />\r\nBanner on Coffee Station   <br />\r\n1000 Flyers ( 14 page )   <br />\r\n1 Hostess Serving the Coffee (Outfit should be provided by you.   <br />\r\nExcellent exposure in the high traffic hour.   <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :   <br />\r\nHotel registration : Priority   <br />\r\nAirport transfer   <br />\r\nIncluded tickets : 6   <br />\r\nFREE Affiliate entrance ticket : 10   <br />\r\nLogo placement Homepage : Secondary   <br />\r\nLogo placement Advertising : Secondary   <br />\r\nNewsletter to Participants All   <br />\r\nInterviews panel : Secondary   <br />\r\nSocial Exposure   <br />\r\nShow Guide : Half Page Ad   <br />\r\nVIP Zone Access at Parties   <br />\r\nPre Show Dinner Networking', '4999.00', 5, 1),
(9, 2, 'Cócteles Happy Hour', 'Happy hour Cocktails', 'Aloje una barra libre en el programa durante un evento de networking. <br />\r\nBebidas gratis incluidas por 1-2 horas. <br />\r\nLa barra se marcará con su logotipo y material de promoción. <br />\r\nPegatinas (Logotipo - Marca) en tazas de bebidas. <br />\r\nBanner en el Bar <br />\r\n2 camareros con traje de marca. (traje debe ser proporcionado por usted) <br />\r\n1000 folletos (14 páginas) <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nTraslado al aeropuerto <br />\r\nBoletos incluidos: 6 <br />\r\nEntrada de afiliado GRATUITA: 10 <br />\r\nColocación del logotipo Página de inicio: Secundaria <br />\r\nColocación del logotipo Publicidad: secundaria <br />\r\nBoletín informativo para todos los participantes <br />\r\nPanel de entrevistas: Secundario <br />\r\nExposición social <br />\r\nMostrar guía: Anuncio de media página <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking.', 'Host an open bar in the show during a networking event.   <br />\r\nFree drinks included for 1-2 hours.   <br />\r\nThe bar will be branded with your logo and promotion material.   <br />\r\nStickers (Logo - Brand) on drink cups.   <br />\r\nBanner on Bar   <br />\r\n2 bartenders with branded outfit. (outfit should be provided by you)   <br />\r\n1000 Flyers ( 14 page )   <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :   <br />\r\nHotel registration : Priority   <br />\r\nAirport transfer   <br />\r\nIncluded tickets : 6   <br />\r\nFREE Affiliate entrance ticket : 10   <br />\r\nLogo placement Homepage : Secondary   <br />\r\nLogo placement Advertising : Secondary   <br />\r\nNewsletter to Participants All   <br />\r\nInterviews panel : Secondary   <br />\r\nSocial Exposure   <br />\r\nShow Guide : Half Page Ad   <br />\r\nVIP Zone Access at Parties   <br />\r\nPre Show Dinner Networking.', '4999.00', 5, 1),
(10, 2, 'Almuerzo', 'Lunch', 'Anímate a almorzar uno de los días del espectáculo durante<br />\r\nel descanso del espectáculo. <br />\r\nLa opción 1 es una estación de almuerzo tipo buffet, la <br />\r\ncantidad de platos proporcionados para 1 paquete es entre<br />\r\n150 a 250 placas, dependiendo del menú elegido por el patrocinador. <br />\r\nLa opción 2 es un almuerzo de invitación solo en uno de <br />\r\nnuestros restaurantes cercanos, límite de placas entre <br />\r\n150-200 dependiendo del restaurante elegido por el patrocinador. <br />\r\nMateriales de marca incluidos.<br /> \r\nDetalles dependiendo de la opción elegida. <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nTraslado al aeropuerto <br />\r\nBoletos incluidos: 6 <br />\r\nEntrada de afiliado GRATUITA: 10 <br />\r\nColocación del logotipo Página de inicio: Secundaria <br />\r\nColocación del logotipo Publicidad: secundaria <br />\r\nBoletín informativo para todos los participantes <br />\r\nPanel de entrevistas: Secundario <br />\r\nExposición social <br />\r\nMostrar guía: Anuncio de media página <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking.', 'Host lunch one of the show days during the show break.   <br />\r\nOption 1 is a buffet style lunch station, the quantity <br />\r\nof the plates provided for 1 package is between 150 to<br />\r\n250 plates depending on menu chosen by sponsor.   <br />\r\nOption 2 is a invite only lunch at one of our nearby restaurants,  <br />\r\nlimit of plates between 150- 200 depending on restaurant chosen by sponsor.  <br />\r\nBranded materials included. Details depending on the option chosen.  <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :   <br />\r\nHotel registration : Priority   <br />\r\nAirport transfer   <br />\r\nIncluded tickets : 6   <br />\r\nFREE Affiliate entrance ticket : 10   <br />\r\nLogo placement Homepage : Secondary   <br />\r\nLogo placement Advertising : Secondary   <br />\r\nNewsletter to Participants All   <br />\r\nInterviews panel : Secondary   <br />\r\nSocial Exposure   <br />\r\nShow Guide : Half Page Ad   <br />\r\nVIP Zone Access at Parties   <br />\r\nPre Show Dinner Networking.', '4999.00', 5, 1),
(11, 2, 'Cena', 'Dinner', 'Organice una cena solo para invitados en uno de nuestros <br />\r\nrestaurantes cercanos. <br />\r\nlímite de placas entre 150 y 250 dependiendo del restaurante<br /> \r\nelegido por el patrocinador. <br />\r\nMateriales de marca incluidos. <br />\r\nDetalles según el restaurante elegido. <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nTraslado al aeropuerto <br />\r\nBoletos incluidos: 6 <br />\r\nEntrada de afiliado GRATUITA: 10 <br />\r\nColocación del logotipo Página de inicio: Secundaria <br />\r\nColocación del logotipo Publicidad: secundaria <br />\r\nBoletín informativo para todos los participantes <br />\r\nPanel de entrevistas: Secundario <br />\r\nExposición social <br />\r\nMostrar guía: Anuncio de media página <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking.', 'Host a invite only dinner at one of our nearby restaurants.  <br />\r\nlimit of plates between 150- 250 depending on restaurant chosen by sponsor.  <br />\r\nBranded materials included.  <br />\r\nDetails depending on the restaurant chosen.  <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />\r\nHotel registration : Priority  <br />\r\nAirport transfer  <br />\r\nIncluded tickets : 6  <br />\r\nFREE Affiliate entrance ticket : 10  <br />\r\nLogo placement Homepage : Secondary  <br />\r\nLogo placement Advertising : Secondary  <br />\r\nNewsletter to Participants All  <br />\r\nInterviews panel : Secondary  <br />\r\nSocial Exposure  <br />\r\nShow Guide : Half Page Ad  <br />\r\nVIP Zone Access at Parties  <br />\r\nPre Show Dinner Networking.', '5999.00', 5, 1),
(12, 2, 'Snack y dulces', 'Snack &amp; Candy', 'Nunca se puede ser \"Demasiado dulce\" para los delegados. <br />\r\nLa estación de golosinas Snack and Candy está llena de una<br /> \r\nvariedad de golosinas gratuitas. <br />\r\nLa estación estará abierta a todos los asistentes durante todo el día. <br />\r\n2 Banners en la estación de la estación de bocadillos <br />\r\n1000 folletos (14 páginas). <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nBoletos incluidos: 4 <br />\r\nColocación del logotipo Página de inicio: Pequeño <br />\r\nColocación del logotipo Publicidad: Pequeño <br />\r\nExposición social <br />\r\nMostrar guía: anuncio de página trimestre <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking.', 'You can never be \"Too Sweet\" to the delegates.   <br />\r\nBranded Snack and Candy station filled with an assortment of free treats.   <br />\r\nThe station will be open to all attendees all day long.   <br />\r\n2 Banners on Snack station Station   <br />\r\n1000 Flyers ( 14 page ).   <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :   <br />\r\nHotel registration : Priority   <br />\r\nIncluded tickets : 4   <br />\r\nLogo placement Homepage : Small   <br />\r\nLogo placement Advertising : Small   <br />\r\nSocial Exposure   <br />\r\nShow Guide : Quarter Page Ad   <br />\r\nVIP Zone Access at Parties   <br />\r\nPre Show Dinner Networking.', '2499.00', 5, 1),
(13, 3, 'Anfitriones de marca', 'Branded Hostesses', '2 Anfitriones de marca para su empresa durante los 3 días de LALEXPO. <br />\r\nLos usé para repartir volantes, dar la bienvenida a tus socios o simplemente para <br />\r\nhaz que tu empresa luzca atractiva <br />\r\n1000 folletos (14 páginas) <br />\r\nEquipo de Anfitriones (debe proporcionar diseño e ilustraciones o traer traje propio). <br />\r\nBENEFICIOS ADICIONALES SOBRE ESTE PATROCINADOR: <br />\r\nRegistro de hotel: prioridad <br />\r\nBoletos incluidos: 4 <br />\r\nColocación del logotipo Página de inicio: Pequeño <br />\r\nColocación del logotipo Publicidad: Pequeño <br />\r\nExposición social <br />\r\nMostrar guía: anuncio de página trimestre <br />\r\nAcceso a la zona VIP en las fiestas <br />\r\nPre Show Dinner Networking.', '2 Branded Hostesses for your company during the 3 Days of LALEXPO.  <br />\r\nUsed them to spread flyers around, welcome your partners or simply to  <br />\r\nmake your Company look hot.  <br />\r\n1000 Flyers (14 Page)  <br />\r\nHostesses Outfit (Must provide design and artwork or bring Outfit yourself).  <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />\r\nHotel registration : Priority  <br />\r\nIncluded tickets : 4  <br />\r\nLogo placement Homepage : Small  <br />\r\nLogo placement Advertising : Small  <br />\r\nSocial Exposure  <br />\r\nShow Guide : Quarter Page Ad  <br />\r\nVIP Zone Access at Parties  <br />\r\nPre Show Dinner Networking.', '2499.00', 5, 1),
(14, 3, 'Inserción de bolsas de regalo', 'Gift bag insertion', 'Coloque un artículo (volante, obsequio, regalito, etc.) en las bolsas del espectáculo que <br />\r\nse entregan a todos los delegados. <br />\r\nEste patrocinio no incluye ningún obsequio, es solo el derecho <br />\r\npara que insertes los regalos en las bolsas de regalo. <br />', 'Place an item (flyer, gift, goody, etc.) in the show bags that  <br />\r\nare given to all delegates.  <br />\r\nThis Sponsorship does not include any Gifts, is only the right  <br />\r\nfor you to insert gifts into the Gift Bags. <br />', '499.00', 5, 1),
(15, 3, 'Estación de carga de teléfono', 'Phone charging station', '¿En LALEXPO con la batería del teléfono a punto de morir? <br/>\r\nGracias al patrocinador de la estación de carga de celulares, <br/>\r\nesto no le va a pasar a ninguno de los asistentes al evento! <br/>\r\nEstación de carga de celulares brandeada y pendón de la empresa. <br/>\r\nBENEFICIOS ADICIONALES EN ESTE PATROCINIO: <br/>\r\nRegistro de hotel: Prioritario <br/>\r\nTiquetes de entrada: 4 <br/>\r\nLogo al inicio de la página web: Pequeño <br/>\r\nLogo en la publicidad del evento: Pequeño <br/>\r\nExposición en redes sociales <br/>\r\nEspacio publicitario en revista del evento: Cuarto de página <br/>\r\nAcceso a las zonas VIP en las fiestas <br/>\r\nCena de negocios previa al evento<br/>', 'At LALEXPO with phone`s battery about to die? Thanks you the Mobile  <br />\r\nPhone Charging Sponsor this is not going to happen<br /> \r\nto anyone at the Convention !  <br />\r\nBranded Phone charging station and company banner.  <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />\r\nHotel registration : Priority  <br />\r\nIncluded tickets : 4  <br />\r\nLogo placement Homepage : Small  <br />\r\nLogo placement Advertising : Small  <br />\r\nSocial Exposure  <br />\r\nShow Guide : Quarter Page Ad  <br />\r\nVIP Zone Access at Parties  <br />\r\nPre Show Dinner Networking.', '1999.00', 5, 1),
(16, 3, 'Baños Brandeados\r\n', 'Bathrooms Branding', 'Tenga el logo de su empresa en un lugar que todos los asistentes<br> \r\nsin duda verán.<br> \r\nLogo branding en espejos o puertas de los baños en forma de adhesivos. <br>\r\nBENEFICIOS ADICIONALES EN ESTE PATROCINIO: \r\nRegistro de hotel: Prioritario <br>\r\nTiquetes de entrada: 4<br>\r\nLogo al inicio de la página web: Pequeño<br>\r\nLogo en la publicidad del evento: Pequeño<br>\r\nExposición en redes sociales <br>\r\nEspacio publicitario en revista del evento: Cuarto de página <br>\r\nAcceso a las zonas VIP en las fiestas <br>\r\nCena de negocios previa al evento								</div>', 'Have your logo in a spot attendees will definitely see.  <br />\r\nLogo branding in Mirrors, Door stickers, Clings, etc <br />\r\nADITIONAL BENEFITS ON THIS SPONSORSHIP : <br />\r\nHotel registration : Priority  <br />\r\nIncluded tickets : 4  <br />\r\nLogo placement Homepage : Small  <br />\r\nLogo placement Advertising : Small  <br />\r\nSocial Exposure  <br />\r\nShow Guide : Quarter Page Ad  <br />\r\nVIP Zone Access at Parties  <br />\r\nPre Show Dinner Networking.', '2499.00', 5, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservas_estado`
--

DROP TABLE IF EXISTS `reservas_estado`;
CREATE TABLE `reservas_estado` (
  `id` int(2) UNSIGNED NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reservas_estado`
--

INSERT INTO `reservas_estado` (`id`, `nombre`) VALUES
(1, 'Solicitado'),
(2, 'Reservado'),
(3, 'Comprado'),
(4, 'Denegado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sitios_transmision`
--

DROP TABLE IF EXISTS `sitios_transmision`;
CREATE TABLE `sitios_transmision` (
  `id` int(6) UNSIGNED NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `habilitado` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `suscripciones`
--

DROP TABLE IF EXISTS `suscripciones`;
CREATE TABLE `suscripciones` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `suscripciones`
--

INSERT INTO `suscripciones` (`id`, `email`, `fecha`) VALUES
(1, 'asd@asd.com', '2018-07-15 03:08:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traduccion`
--

DROP TABLE IF EXISTS `traduccion`;
CREATE TABLE `traduccion` (
  `id` varchar(50) NOT NULL,
  `id_seccion` int(5) UNSIGNED DEFAULT NULL,
  `en` varchar(150) DEFAULT NULL,
  `es` varchar(150) DEFAULT NULL,
  `obs` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `traduccion`
--

INSERT INTO `traduccion` (`id`, `id_seccion`, `en`, `es`, `obs`) VALUES
('btn_back', 1, 'Back', 'Atras', '.'),
('btn_buy', 1, 'Buy', 'Comprar', '.'),
('btn_cancel', 1, 'Cancel', 'Cancelar', 'boton cancelar'),
('btn_login', 1, 'Log In', 'Entrar', 'boton de entrar en el encabezado'),
('btn_logintobuy', 1, 'Login to buy', 'Ingresar para comprar', '.'),
('btn_logout', 1, 'Log out', 'Salir', 'boton logout'),
('btn_profile', 1, 'Profile', 'Perfil', 'boton perfil'),
('btn_register', 1, 'Sign up', 'Registro', 'boton del regisitro'),
('btn_soldout', 1, 'Sold out', 'Agotado', '.'),
('btn_submit', 1, 'Submit', 'Enviar', 'boton enviar formulario'),
('community_sponsor', 2, 'Community Sponsor', 'Community Sponsor', '.'),
('days', 1, 'Days', 'Días', '.'),
('desc_login', 1, 'Be part of the biggest and most exciting adult business Event in Latin America', 'Sé parte del evento de negocios adulto más grande y emocionante  en América Latina', 'descripcion formulario login'),
('frm_contact_email', 1, 'Your email (Required)', 'Correo electrónico (Requerido)', 'Etiqueta campo correo formulario contacto'),
('frm_contact_message', 1, 'Do you have a comment?', 'Mensaje', 'Etiqueta campo mensaje formulario contacto'),
('frm_contact_name', 1, 'Name (Required)', 'Nombre (Requerido)', 'Etiquera campo nombre formulario contacto'),
('frm_contact_subject', 1, 'Subject', 'Asunto', 'Etiqueta campo asunto formulario contacto'),
('frm_contact_title', 1, 'Lets get in touch!', 'Contáctenos!', 'titulo formulario contacto'),
('home_galeria', 1, 'Gallery', 'Galería', '.'),
('hours', 1, 'Hours', 'Horas', '.'),
('login_email', 1, 'Email', 'Correo electrónico', 'etiqueta campo correo login'),
('login_forgot_password', 1, 'Forgot your password?', 'Olvidé mi contraseña', 'texto olvido contraseña login'),
('login_password', 1, 'Password', 'Contraseña', 'etiqueta campo contraseña'),
('login_signup', 1, 'Don\'t have account? Sign Up here', '¿No tiene cuenta? Regístrese aquí', 'texto registro login'),
('media_partner', 2, 'Media Partner', 'Socio de medios', '.'),
('media_sponsors', 2, 'Media Sponsors', 'Medios Patrocinadores', '.'),
('media_sponsors_text', 2, 'great companies covering and promoting our event', 'grandes compañías que cubren y promueven nuestro evento', '.'),
('menu_asisten', 1, 'Who is coming', 'Asistentes', '.'),
('menu_home', 1, 'Home', 'Inicio', '.'),
('menu_premios', 1, 'Awards', 'Premios', '.'),
('menu_program', 1, 'Schedule', 'Programación', '.'),
('menu_sponsor', 1, 'Be our sponsor', 'Patrocinios', '.'),
('minutes', 1, 'Minutes', 'Minutos', '.'),
('newsletter', 1, 'Subscribe to our newsletter', 'Suscribirse a nuestro boletín', 'titulo boletin'),
('newsletter_hint', 1, 'Enter a valid email address...', 'Ingrese un email válido', 'placeholder newsletter'),
('presen_patrocin', 2, 'Presenting Sponsor', 'Patrocinador Presentador', '.'),
('search', 1, 'Search...', 'Buscar...', 'texto buscar'),
('sponsor_category_1', 2, 'Crown Sponsors', 'Patrocinadores Crown', '.'),
('sponsor_category_2', 2, 'Diamond Sponsors', 'Patrocinadores Diamond', '.'),
('sponsor_category_3', 2, 'Platinum Sponsors', 'Patrocinadores Patinum', '.'),
('sponsor_category_4', 2, 'Gold Sponsors', 'Patrocinadores Gold', '.'),
('sponsor_category_5', 2, 'Bronze Sponsors', 'Patrocinadores Bronze', '.'),
('sponsor_descrip', 3, 'We invite you to be a part of this exciting Event and to increase your Company Exposure in front of an extremely Targeted Audience by becoming a corpo', 'Te invitamos a ser parte de este emocionante evento y a aumentar la exposición de tu empresa frente a un público extremadamente objetivo al convertirt', '.'),
('sponsor_text1', 2, ' Attend the First, Biggest and most important Adult Industry Event of', 'Asista al Primer, Mayor y más importante Evento de la Industria de Adultos de América latina', '.'),
('sponsor_text10', 2, 'Best networking and Parties', 'Mejores redes y fiestas', '.'),
('sponsor_text11', 2, 'Sponsorship opportunity', 'Oportunidad de patrocinio', '.'),
('sponsor_text12', 2, 'Promote y our brand', 'Promociona tu marca', '.'),
('sponsor_text13', 2, 'Show of your company', 'Muestra de su empresa', '.'),
('sponsor_text14', 2, 'Make new contacts', 'Hacer nuevos contactos', '.'),
('sponsor_text15', 2, 'Meet your latam partners', 'Conoce a tus socios latam', '.'),
('sponsor_text16', 2, 'Strengthen relationships', 'Fortalecer las relaciones', '.'),
('sponsor_text17', 2, 'Create a new market', 'Crea un nuevo mercado', '.'),
('sponsor_text18', 2, 'Sponsors', 'Patrocinadores', '.'),
('sponsor_text19', 2, 'Ready for business', 'Listo para los negocios', '.'),
('sponsor_text2', 2, 'Latin-America', 'Latinoamérica ', '.'),
('sponsor_text3', 2, 'The Definitive B2B Adult show of Central and South America. Come on over to the ONLY show in South America. Meet a total New market and be part of the', 'El espectáculo Definitive B2B Adult de América Central y del Sur. Venga al ÚNICO show en Sudamérica. Conoce un mercado totalmente nuevo y sé parte de ', '.'),
('sponsor_text4', 2, '1,300 Industry Profesionals', '1,300 Profesionales de la industria', '.'),
('sponsor_text5', 2, 'An unexploited new Market', 'Un nuevo mercado sin explotar', '.'),
('sponsor_text6', 2, 'Companies ready for business', 'Empresas listas para hacer negocios', '.'),
('sponsor_text7', 2, 'Hundreds of Deals closed', 'Cientos de ofertas cerradas', '.'),
('sponsor_text8', 2, 'An opportunity to expand', 'Una oportunidad para expandirse', '.'),
('sponsor_text9', 2, 'Big workshops and seminars', 'Grandes talleres y seminarios', '.'),
('sponsor_title', 3, 'SPONSOR US', 'PATROCINAR', '.'),
('title_login', 1, 'Log in', 'Ingresar', 'titulo login'),
('txtavailable', 1, 'Available', 'Disponible', '.'),
('txtprice', 1, 'Price', 'Precio', '.'),
('txtreserved', 1, 'Reserved', 'Reservado', '.'),
('txtsold', 1, 'Sold', 'Vendido', '.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traduccion_seccion`
--

DROP TABLE IF EXISTS `traduccion_seccion`;
CREATE TABLE `traduccion_seccion` (
  `id` int(5) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `traduccion_seccion`
--

INSERT INTO `traduccion_seccion` (`id`, `nombre`) VALUES
(1, 'Global'),
(2, 'Home'),
(3, 'Patrocinadores'),
(4, 'Asistentes'),
(5, 'Programacion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(11) UNSIGNED NOT NULL,
  `activo` int(1) DEFAULT '0',
  `token` varchar(200) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `genero` varchar(50) DEFAULT NULL,
  `foto` varbinary(150) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `pass` varchar(30) DEFAULT NULL,
  `dni` varchar(50) DEFAULT NULL,
  `pais` varchar(50) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `ciudad` varchar(150) DEFAULT NULL,
  `lengua` varbinary(30) DEFAULT NULL,
  `telefono` varbinary(25) DEFAULT NULL,
  `id_categoria` int(5) UNSIGNED DEFAULT NULL,
  `fecha_alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `anio` int(4) UNSIGNED DEFAULT NULL,
  `company` varchar(70) DEFAULT NULL,
  `website` varchar(80) DEFAULT NULL,
  `models` varchar(10) DEFAULT NULL,
  `face` varchar(60) DEFAULT NULL,
  `twitter` varchar(60) DEFAULT NULL,
  `instagram` varchar(60) DEFAULT NULL,
  `skype` varchar(60) DEFAULT NULL,
  `youtube` varchar(60) DEFAULT NULL,
  `nombre_escarapela` varchar(100) DEFAULT NULL,
  `estudio` varchar(100) DEFAULT NULL,
  `objetivo` varbinary(100) DEFAULT NULL,
  `descrip_profesional` varbinary(150) DEFAULT NULL,
  `nickname` varbinary(50) DEFAULT NULL,
  `sitios_adicionales` varbinary(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `activo`, `token`, `nombre`, `apellido`, `genero`, `foto`, `email`, `pass`, `dni`, `pais`, `estado`, `ciudad`, `lengua`, `telefono`, `id_categoria`, `fecha_alta`, `anio`, `company`, `website`, `models`, `face`, `twitter`, `instagram`, `skype`, `youtube`, `nombre_escarapela`, `estudio`, `objetivo`, `descrip_profesional`, `nickname`, `sitios_adicionales`) VALUES
(8, 1, '8d6c07685aadbc44c1a210f16a1a67ca95ccbf2a0e8229e1a3', 'Cristian', 'Lescano', '1', NULL, 'cristian_lescano@hotmail.es', '1', '', 'Colombia', 'Atlantico', NULL, '', '', 2, '2018-07-20 00:05:37', 2019, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dfsdfsdf', ''),
(9, 1, 'c5609fb3bf002631ab88638ac7d4c72e1361eb7d7cb25e31249dd95a2f90356e95a61ed62aac2a4da2085777b827d7181ce7', 'Ana', 'Beltran', '2', 'fotos/9.jpg', 'usuario@correo.com', '1234', '1111111111', 'Colombia', 'Bogota D.C.', '', 'espanol', '+5719238446', 2, '2018-07-15 19:54:40', 2019, 'compañia uno', 'ffff', '', '/nanyonline', '', '@nanyoficial', '', '', 'david ', 'aj', 'entretnimiento ', '', 'nany', 'aaaaa,fffff,455444f,'),
(10, 0, 'a68c132294f0eb0365a573312e2fbbfa3f049675c9544df2f39a77381cf193cf412138d097b562aa36293c8a51e2d4e84a66', 'david', 'lalexpo', '1', NULL, 'lalexpoaj@gmail.com', 'andres', '5666555', 'Colombia', 'Antioquia', NULL, 'español', '3103382314', 2, '2018-07-14 11:07:07', 2019, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'MyFreeCams,QCams,Visit-X,WebcamClub,Webcams,XCams,'),
(11, 1, '561ff80ec213db2128371b4879d511799e7ae9cfc45e24562aa2c757bb61ad55db799ff691846f29c22b8be5898c2e8d6c65', 'Gabriel', 'Martinez', '1', 'fotos/9b98d50e.jpg', 'torresgabriele@gmail.com', '1234', '2222222', 'Colombia', 'Bogota D.C.', NULL, 'espanol', '+5719238446', 2, '2018-07-16 22:26:00', 2019, 'compañia dos', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gggg', '99Cams,AdultWork,AllBBWCams,BongaCams,'),
(12, 1, 'c48cb4559486145a2f6f91a89e44623c973037806d51ccd8822223d96ea3475bc44b7cd2e28af1877dcf666a25b5f0aadfa7', 'ja', 'sss', '1', NULL, 'jaestrada12@hotmail.com', 'andres', '38484', 'Colombia', 'Antioquia', NULL, 'español', '3103382314', 2, '2018-07-15 19:54:57', 2019, 'compañia tres', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'andres', '99Cams,AdultWork,'),
(13, 0, '21f44b66c1dd440128ca844e31c869b4b78fc938eed4613b8fc606b6039c86803028221d6839b5917cde89e82ffca9304969', 'Daniel', 'Ramirez', '1', NULL, 'daniel@lacasacreativa.co', 'FenixRock3', '1082915350', 'Colombia', 'Magdalena', NULL, 'Español', '3188486442', 3, '2018-07-16 00:47:48', 2019, 'La Casa Creativa', 'www.lacasacreativa.co', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dan', ''),
(14, 1, 'e94522ace924bb93bbac83f79fe4d7fba016bb4f7961280c18d356d3119262de6885fd487348ad508f12736caddcd14a6193', 'sharon ', 'torres', '2', '', 'torreslopezs8@gmail.com', '12345', '1144040058', 'Colombia', 'Valle del Cauca', '', 'Español', '3155771788', 2, '2018-07-17 16:33:42', 2019, '', '', '', '', '', '', '', '', 'sharon gomez', 'ajstudios', 'relaciones comerciales', '', 'sharon gomez', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_back`
--

DROP TABLE IF EXISTS `usuarios_back`;
CREATE TABLE `usuarios_back` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` varbinary(30) NOT NULL,
  `pass` varbinary(30) NOT NULL,
  `email` varbinary(70) NOT NULL,
  `nombre` varbinary(70) DEFAULT NULL,
  `tipo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios_back`
--

INSERT INTO `usuarios_back` (`id`, `user`, `pass`, `email`, `nombre`, `tipo`) VALUES
(1, 'admin', 'admin', 'cristian_lescano@hotmail.es', 'Cristian333', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_sitios`
--

DROP TABLE IF EXISTS `usuarios_sitios`;
CREATE TABLE `usuarios_sitios` (
  `id_usuarios` int(11) UNSIGNED NOT NULL,
  `id_sitios` int(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `banners_principal`
--
ALTER TABLE `banners_principal`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `faq_seccion`
--
ALTER TABLE `faq_seccion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `galeria_album`
--
ALTER TABLE `galeria_album`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hotel_categorias_hab`
--
ALTER TABLE `hotel_categorias_hab`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hotel_habitciones`
--
ALTER TABLE `hotel_habitciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hotel_reservas`
--
ALTER TABLE `hotel_reservas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hotel_reservas_hospedados`
--
ALTER TABLE `hotel_reservas_hospedados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nomencladores`
--
ALTER TABLE `nomencladores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pagos_error`
--
ALTER TABLE `pagos_error`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paquetes`
--
ALTER TABLE `paquetes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paquetes_categorias`
--
ALTER TABLE `paquetes_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `patrocinador_categoria_paquete`
--
ALTER TABLE `patrocinador_categoria_paquete`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `patrocinador_paquete`
--
ALTER TABLE `patrocinador_paquete`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_categoria`);

--
-- Indices de la tabla `reservas_estado`
--
ALTER TABLE `reservas_estado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sitios_transmision`
--
ALTER TABLE `sitios_transmision`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `suscripciones`
--
ALTER TABLE `suscripciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `traduccion`
--
ALTER TABLE `traduccion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seccion` (`id_seccion`,`id`);

--
-- Indices de la tabla `traduccion_seccion`
--
ALTER TABLE `traduccion_seccion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios_back`
--
ALTER TABLE `usuarios_back`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios_sitios`
--
ALTER TABLE `usuarios_sitios`
  ADD PRIMARY KEY (`id_usuarios`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `banners_principal`
--
ALTER TABLE `banners_principal`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `faq_seccion`
--
ALTER TABLE `faq_seccion`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `galeria_album`
--
ALTER TABLE `galeria_album`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `hotel`
--
ALTER TABLE `hotel`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `hotel_categorias_hab`
--
ALTER TABLE `hotel_categorias_hab`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `hotel_habitciones`
--
ALTER TABLE `hotel_habitciones`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `hotel_reservas`
--
ALTER TABLE `hotel_reservas`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `hotel_reservas_hospedados`
--
ALTER TABLE `hotel_reservas_hospedados`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `nomencladores`
--
ALTER TABLE `nomencladores`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `pagos_error`
--
ALTER TABLE `pagos_error`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `paquetes`
--
ALTER TABLE `paquetes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `paquetes_categorias`
--
ALTER TABLE `paquetes_categorias`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `patrocinador_categoria_paquete`
--
ALTER TABLE `patrocinador_categoria_paquete`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `patrocinador_paquete`
--
ALTER TABLE `patrocinador_paquete`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `reservas_estado`
--
ALTER TABLE `reservas_estado`
  MODIFY `id` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `sitios_transmision`
--
ALTER TABLE `sitios_transmision`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `suscripciones`
--
ALTER TABLE `suscripciones`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `traduccion_seccion`
--
ALTER TABLE `traduccion_seccion`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `usuarios_back`
--
ALTER TABLE `usuarios_back`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuarios_sitios`
--
ALTER TABLE `usuarios_sitios`
  MODIFY `id_usuarios` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `patrocinador_paquete`
--
ALTER TABLE `patrocinador_paquete`
  ADD CONSTRAINT `categoria` FOREIGN KEY (`id_categoria`) REFERENCES `patrocinador_categoria_paquete` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
