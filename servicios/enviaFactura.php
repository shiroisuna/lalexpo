<?php
include('../includes/conexion.php');
include('../lib/PHPMailer/PHPMailer.php');
include('../lib/PHPMailer/Exception.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
$mail = new PHPMailer(true);                              // Passing `true` enables exceptions

$fact = str_replace(array(".pdf", ".jpg"), "", $_POST['img']);
$fact = split("_", $fact);

$rw2=$con->query("SELECT lengua, nombre, apellido FROM usuarios WHERE email='".$_POST['email']."'")->fetch_object();
$text_factura=$con->query("SELECT * FROM textos_email WHERE id='envio_factura'")->fetch_object();

if(strtolower($rw2->lengua)=='español'){
    $tit=$text_factura->titulo_es;
    $mensaje=nl2br($text_factura->texto_es);
}else{
    $tit=$text_factura->titulo_en;
    $mensaje=nl2br($text_factura->texto_en);
}
$mensaje=str_replace('%nombre%', $rw2->nombre.' '.$rw2->apellido, str_replace('%numfac%', $fact[1], $mensaje));
try {
    /*//Server settings
    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'user@example.com';                 // SMTP username
    $mail->Password = 'secret';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to
*/
    //Recipients
    $mail->setFrom('info@lalexpo.com', 'LalExpo');
    $mail->addAddress($_POST['email']);               // Name is optional
    //Attachments
    $mail->addAttachment('../images/facturas/'.$_POST['img']);
    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = $tit;
    $mail->Body    = $mensaje;
    $mail->send();
    echo 1;
}catch(Exception $e){
    echo $mail->ErrorInfo;
}
?>