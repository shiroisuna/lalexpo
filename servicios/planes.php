<?php
	$page='';
	$planes = array(
		'Sponsorship premium packs'=> array(
			'<span style="color:#ffcc33">Crown</span> sponsor' => array(
				'Hotel registration : Priority<br />
              Airport transfer <br />
              Luxury Lounge in show floor : Large <br />
              Included tickets : 10 <br />
              FREE Affiliate entrance ticket : 20 <br />
              Extra ticket : $75 <br />
              Logo placement Homepage : Main <br />
              Logo placement Advertising : Main <br />
              Large Banner<br />
              Flag position : 6 <br />
              Rigid stand-up-banners : 4 <br />
              Seminar Lounge : 1 Hour <br />
              Meet Market Table <br />
              Show and bag inserts : 4 <br />
              Precontact Participant Companys <br />
              Newsletter to Participants All <br />
              Interviews panel : Prominent <br />
              Translators : 2 <br />
              Social Exposure <br />
              Show Guide : Full Page Ad <br />
              VIP Zone Access at Parties <br />
              Pre Show Dinner Networking',
				14999,
				10,
				0,
				0
			), 
			'<span style="color:#99ccff">Diamond</span> sponsor'=> array(
				'Hotel registration : Priority <br>
			Airport transfer <br>
			Luxury Lounge in show floor : Medium <br>
			Included tickets : 8 <br>
			FREE Affiliate entrance ticket : 15 <br>
			Extra ticket : $80 <br>
			Logo placement Homepage : Main <br>
			Logo placement Advertising : Main <br>
			Large Banner<br>
			Flag position : 4 <br>
			Rigid stand-up-banners : 2 <br>
			Seminar Lounge : 30 Mins <br>
			Meet Market Table <br>
			Show and bag inserts : 3 <br>
			Precontact Participant Companys <br>
			Newsletter to Participants All <br>
			Interviews panel : Secondary <br>
			Translators : 1 <br>
			Social Exposure <br>
			Show Guide : Full Page Ad <br>
			VIP Zone Access at Parties <br>
			Pre Show Dinner Networking',
				9999,
				10,
				0,
				0
			), 
			'<span style="color:#aae2e0">Platinum</span> sponsor'=> array(
				'Hotel registration : Priority <br>
              Airport transfer  <br>
              Luxury Lounge in show floor : Small  <br>
              Included tickets : 6  <br>
              FREE Affiliate entrance ticket : 10  <br>
              Extra ticket : $90  <br>
              Logo placement Homepage : Secondary  <br>
              Logo placement Advertising : Secondary  <br>
              Large Banner <br>
              Flag position : 2  <br>
              Rigid stand-up-banners : 1  <br>
              Meet Market Table  <br>
              Show and bag inserts : 2  <br>
              Precontact Participant Companys  <br>
              Newsletter to Participants All  <br>
              Interviews panel : Secondary  <br>
              Social Exposure  <br>
              Show Guide : Half Page Ad  <br>
              VIP Zone Access at Parties  <br>
              Pre Show Dinner Networking<br /><br /><br />',
				4999,
				10,
				0,
				0
			), 
			'<span style="color:#f58800">Gold</span> sponsor'=> array(
				'Hotel registration : Priority <br />
              Included tickets : 4 <br />
              Extra ticket : $95 <br />
              Logo placement Homepage : Small <br />
              Logo placement Advertising : Small <br />
              Large Banner<br />
              Flag position : 1 <br />
              Meet Market Table <br />
              Show and bag inserts : 1 <br />
              Precontact Participant Companys <br />
              Social Exposure <br />
              Show Guide : Quarter Page Ad <br />
              VIP Zone Access at Parties <br />
              Pre Show Dinner Networking<br />
              <br /><br /><br /><br /><br /><br /><br /><br />',
				2999,
				10,
				0,
				0
			), 
			'<span style="color:#d16528">Bronze</span> sponsor'=> array(
				'Hotel registration : Priority <br />
              Included tickets : 1 <br />
              Logo placement Homepage : Small <br />
              Logo placement Advertising : Small <br />
              Social Exposure <br />
              Show Guide : Quarter Page Ad <br />
              VIP Zone Access at Parties <br />
              Pre Show Dinner Networking<br />
              ',
				1250,
				10,
				0,
				0
			)
		),
		'Sponsor food and drinks'=>array(
			'Pre show Dinner Networking'=> array(
				'Welcome the main players with a dinner on the Evening before the Event. <br />
				Invite only Activity for Sponsors and VIP´s of LALEXPO. <br />
				Maximum capacity 100 Attendees. Restaurant to be chosen by Sponsor from <br /> LALEXPO provided list  <br />
Flyers (14 Page)  <br />
2 Rigid Banners  <br />
1 Hostess at Dinner accompanying the Sponsor (NIGHT Outfit to be<br /> provided by sponsor).  <br />
ADITIONAL BENEFITS ON THIS SPONSORSHIP :<br /> 
Hotel registration : Priority  <br />
Included tickets : 4  <br />
Logo placement Homepage : Small  <br />
Logo placement Advertising : Small  <br />
Social Exposure  <br />
Show Guide : Quarter Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.  <br /> 
<img style="margin-top: 20px;" src="http://lalexpo.com/uploads/paquetes/DSC_8410.jpg" class="img-responsive">',
				3999,
				10,
				0,
				0
			),
			'Water cooling'=> array(
				'Custom branded water bottles with your company logo.  <br /> 
				Includes 1.200 water bottles, 400 distributed each day of the event.  <br />  
Stickers (Logo - Brand) on water bottles.   <br /> 
Banner on water cooling station.  <br />  
ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />  
Hotel registration : Priority  <br />
Included tickets : 4  <br />
Logo placement Homepage : Small  <br />
Logo placement Advertising : Small  <br />
Social Exposure  <br />
Show Guide : Quarter Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				2499,
				10,
				0,
				0
			),
			'Coffe breaks'=> array(
				'Your own coffee Station to offer free cofee and light food to everybody.   <br />
				Provided each day of the event from 8AM to 11AM.   <br />
Stickers (Logo - Brand) on Coffee Cups   <br />
Banner on Coffee Station   <br />
1000 Flyers ( 1
4 page )   <br />
1 Hostess Serving the Coffee (Outfit should be provided by you.   <br />
Excellent exposure in the high traffic hour.   <br />
ADITIONAL BENEFITS ON THIS SPONSORSHIP :   <br />
Hotel registration : Priority   <br />
Airport transfer   <br />
Included tickets : 6   <br />
FREE Affiliate entrance ticket : 10   <br />
Logo placement Homepage : Secondary   <br />
Logo placement Advertising : Secondary   <br />
Newsletter to Participants All   <br />
Interviews panel : Secondary   <br />
Social Exposure   <br />
Show Guide : Half Page Ad   <br />
VIP Zone Access at Parties   <br />
Pre Show Dinner Networking.',
				4999,
				10,
				0,
				0
			),
			'Happy hour Cocktails'=> array(
				'Host an open bar in the show during a networking event.   <br />
				Free drinks included for 1-2 hours.   <br />
				The bar will be branded with your logo and promotion material.   <br />
Stickers (Logo - Brand) on drink cups.   <br />
Banner on Bar   <br />
2 bartenders with branded outfit. (outfit should be provided by you)   <br />
1000 Flyers ( 14 page )   <br />
ADITIONAL BENEFITS ON THIS SPONSORSHIP :   <br />
Hotel registration : Priority   <br />
Airport transfer   <br />
Included tickets : 6   <br />
FREE Affiliate entrance ticket : 10   <br />
Logo placement Homepage : Secondary   <br />
Logo placement Advertising : Secondary   <br />
Newsletter to Participants All   <br />
Interviews panel : Secondary   <br />
Social Exposure   <br />
Show Guide : Half Page Ad   <br />
VIP Zone Access at Parties   <br />
Pre Show Dinner Networking.',
				4999,
				10,
				0,
				0
			),
			'Lunch'=> array(
				'Host lunch one of the show days during the show break.   <br />
Option 1 is a buffet style lunch station, the quantity of the plates provided  <br />
for 1 package is between 150 to 250 plates depending on menu chosen by sponsor.   <br />
Option 2 is a invite only lunch at one of our nearby restaurants,  <br />
limit of plates between 150- 200 depending on restaurant chosen by sponsor.  <br />
Branded materials included. Details depending on the option chosen.  <br />
ADITIONAL BENEFITS ON THIS SPONSORSHIP :   <br />
Hotel registration : Priority   <br />
Airport transfer   <br />
Included tickets : 6   <br />
FREE Affiliate entrance ticket : 10   <br />
Logo placement Homepage : Secondary   <br />
Logo placement Advertising : Secondary   <br />
Newsletter to Participants All   <br />
Interviews panel : Secondary   <br />
Social Exposure   <br />
Show Guide : Half Page Ad   <br />
VIP Zone Access at Parties   <br />
Pre Show Dinner Networking.',
				4999,
				10,
				0,
				0
			),
			'Dinner'=> array(
				'Host a invite only dinner at one of our nearby restaurants.  <br />
				limit of plates between 150- 250 depending on restaurant chosen by sponsor.  <br />
				Branded materials included.  <br />
				Details depending on the restaurant chosen.  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
				Hotel registration : Priority  <br />
Airport transfer  <br />
Included tickets : 6  <br />
FREE Affiliate entrance ticket : 10  <br />
Logo placement Homepage : Secondary  <br />
Logo placement Advertising : Secondary  <br />
Newsletter to Participants All  <br />
Interviews panel : Secondary  <br />
Social Exposure  <br />
Show Guide : Half Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				5999,
				10,
				0,
				0
			),
			'Snack &amp; Candy'=> array(
				'You can never be "Too Sweet" to the delegates.   <br />
				Branded Snack and Candy station filled with an assortment of free treats.   <br />
				The station will be open to all attendees all day long.   <br />
2 Banners on Snack station Station   <br />
1000 Flyers ( 14 page ).   <br />
ADITIONAL BENEFITS ON THIS SPONSORSHIP :   <br />
Hotel registration : Priority   <br />
Included tickets : 4   <br />
Logo placement Homepage : Small   <br />
Logo placement Advertising : Small   <br />
Social Exposure   <br />
Show Guide : Quarter Page Ad   <br />
VIP Zone Access at Parties   <br />
Pre Show Dinner Networking.',
				2499,
				10,
				0,
				0
			)
		),
      	'Aditional packs &amp; services'=>array(
      		'Branded Hostesses'=> array(
				'2 Branded Hostesses for your company during the 3 Days of LALEXPO.  <br />
				Used them to spread flyers around, welcome your partners or simply to  <br />
				make your Company look hot.  <br />
1000 Flyers (14 Page)  <br />
Hostesses Outfit (Must provide design and artwork or bring Outfit yourself).  <br />
ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
Hotel registration : Priority  <br />
Included tickets : 4  <br />
Logo placement Homepage : Small  <br />
Logo placement Advertising : Small  <br />
Social Exposure  <br />
Show Guide : Quarter Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				2499,
				10,
				0,
				0
			),
      		'Gift bag insertion'=> array(
				'Place an item (flyer, gift, goody, etc.) in the show bags that  <br />
				are given to all delegates.  <br />
				This Sponsorship does not include any Gifts, is only the right  <br />
				for you to insert gifts into the Gift Bags. <br />',
				499,
				10,
				0,
				0
			),
      		'Phone charging station'=> array(
				'At LALEXPO with phone`s battery about to die? Thanks you the Mobile  <br />
				Phone Charging Sponsor this is not going to happen to anyone at the Convention !  <br />
				Branded Phone charging station and company banner.  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
				Hotel registration : Priority  <br />
Included tickets : 4  <br />
Logo placement Homepage : Small  <br />
Logo placement Advertising : Small  <br />
Social Exposure  <br />
Show Guide : Quarter Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				1999,
				10,
				0,
				0
			),
      		'Bathrooms Branding'=> array(
				'Have your logo in a spot attendees will definitely see.  <br />
				Logo branding in Mirrors, Door stickers, Clings, etc <br />
ADITIONAL BENEFITS ON THIS SPONSORSHIP : <br />
Hotel registration : Priority  <br />
Included tickets : 4  <br />
Logo placement Homepage : Small  <br />
Logo placement Advertising : Small  <br />
Social Exposure  <br />
Show Guide : Quarter Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				2499,
				10,
				0,
				0
			),
      		'Tweet Wall'=> array(
				'Branded Large LCD screen featuring all the official tweets and  <br />
				retweets from show organizers, sponsors and attendees. <br />',
				849,
				10,
				0,
				0
			),
      		'Signage on show Schedules'=> array(
				'Get your company name and logo on the Show Signages, the official  <br />
				show schedule are strategically placed all around the venue.  <br />
				A perfect manner to be seen all day by everyone. Also included  <br />
				logo on printed show schedule. <br />',
				899,
				10,
				0,
				0
			),
      		'Main stairs Branding'=> array(
				'Your name will appear on every step of the main stairs.  <br />
				Be the first to guide their steps!  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
Hotel registration : Priority  <br />
Included tickets : 4  <br />
Logo placement Homepage : Small  <br />
Logo placement Advertising : Small  <br />
Social Exposure  <br />
Show Guide : Quarter Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking. ',
				1999,
				10,
				0,
				0
			),
      		'Video Wall'=> array(
				'Branded High Definition Screen on Premium location.  <br />
				LALEXPO info and Your Videos featured on the screen.  <br />
				Present your company video, your products, news about your services,  <br />
				or any kind of info you would like. <br />',
				849,
				10,
				0,
				0
			),
      		'Cash machine sponsor'=> array(
				'Sponsor the cash machine where attendees can win free money!  <br />
				The cash machine will be a permanent activity between 10 am to 5 pm  <br />
				that you decide when turn on.  <br />
				It’s a perfect way to call attention to your brand and to invite  <br />
				people to your booth.  <br />
				Your company Branded Cash machine.  <br />
				Does not include the money to insert on the machine.  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
Hotel registration : Priority  <br />
Included tickets : 4  <br />
Logo placement Homepage : Small  <br />
Logo placement Advertising : Small  <br />
Social Exposure  <br />
Show Guide : Quarter Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking. ',
				2999,
				10,
				0,
				0
			),
      		'Pre contact all attendees Feature'=> array(
				'Get access to the Contact Info of all Registered Attendees.  <br />
				By default you wont be able to view Contact info of other Attendees  <br />
				before the show, hence this is a great way for you to reach out and setup meetings in advance. <br />',
				499,
				10,
				0,
				0
			)
      	),
      	'Hospitality packs'=>array(
      		'Airport transportation'=> array(
				'Transportation for all attendees from Airport to Hotel.  <br />
				The transport would be branded on the outside with Logo and Slogan of your company.  <br />
				A branded hostess would welcome all passengers at airport or hotel on your behalf.  <br />
				In past Events hundreds of attendees took advantage of this free service.  <br />
Logo  <br />
Slogan  <br />
Hostess Outfit (Must provide design and artwork or bring Outfit yourself).  <br />
ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
Hotel registration : Priority  <br />
Airport transfer  <br />
Included tickets : 6  <br />
FREE Affiliate entrance ticket : 10  <br />
Logo placement Homepage : Secondary  <br />
Logo placement Advertising : Secondary  <br />
Newsletter to Participants All  <br />
Interviews panel : Secondary  <br />
Social Exposure  <br />
Show Guide : Half Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				4399,
				10,
				0,
				0
			),
      		'Shuttle bus'=> array(
				'Although we are in a compact setting and everybody is close together we will have a shuttle service running during the event.  <br />
				Transportation for all attendees from Hotel to Restaurants, Activities, etc.  <br />
				The transport would be branded on the outside with Logo and Slogan of your company.  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
				Hotel registration : Priority  <br />
Included tickets : 4  <br />
Logo placement Homepage : Small  <br />
Logo placement Advertising : Small  <br />
Social Exposure  <br />
Show Guide : Quarter Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				2499,
				10,
				0,
				0
			),
      		'Official WiFi Sponsor'=> array(
				'Help all conference attendees get connected by being the provider of complimentary WiFi on all the convention center areas.  <br />
Company branded signage placed in different areas of the convention center with wifi instructions.  <br />
ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
•Hotel registration : Priority  <br />
Included tickets : 4  <br />
Extra ticket : $95  <br />
Logo placement Homepage : Small  <br />
Logo placement Advertising : Small  <br />
Large Banner  <br />
Flag position : 1  <br />
Meet Market Table  <br />
Show and bag inserts : 1  <br />
Precontact Participant Companys  <br />
Social Exposure  <br />
Show Guide : Quarter Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking ',
				4999,
				10,
				0,
				0
			)
      	),
      	'Main advertising'=>array(
      		'Registration &amp; Information Desk'=> array(
				'Make the first impression be the first Sponsor that everybody would see at Registration area.  <br />
				Exclusive branding in Registration desk.  <br />
				Official registration sponsor which means your logo will be featured on all registration-related materials and communications.  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
				Hotel registration : Priority  <br />
Airport transfer  <br />
Included tickets : 6  <br />
FREE Affiliate entrance ticket : 10  <br />
Logo placement Homepage : Secondary  <br />
Logo placement Advertising : Secondary  <br />
Newsletter to Participants All  <br />
Interviews panel : Secondary  <br />
Social Exposure  <br />
Show Guide : Half Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				4999,
				10,
				0,
				0
			),
      		'Cockade ribbons'=> array(
				'Every badge needs a Lanyard, therefore Permanent and maximum exposure throughout the event.  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
				Hotel registration : Priority  <br />
Airport transfer  <br />
Included tickets : 6  <br />
FREE Affiliate entrance ticket : 10  <br />
Logo placement Homepage : Secondary  <br />
Logo placement Advertising : Secondary  <br />
Newsletter to Participants All  <br />
Interviews panel : Secondary  <br />
Social Exposure  <br />
Show Guide : Half Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				4999,
				10,
				0,
				0
			),
      		'Show wristbands'=> array(
				'Exclusive company logo printed on show wristbands that will be given to all attendees.  <br />
				They will be worn during the whole show (also to gain entry to the parties and activities).  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
				Hotel registration : Priority  <br />
Included tickets : 4  <br />
Logo placement Homepage : Small  <br />
Logo placement Advertising : Small  <br />
Social Exposure  <br />
Show Guide : Quarter Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				4999,
				10,
				0,
				0
			),
      		'Show badge'=> array(
				'Every assistant must wear a badge, everybody would look at each others badge to look at names, and other information, that’s why there’s no better way to promote your brand.  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
				Hotel registration : Priority  <br />
Airport transfer  <br />
Included tickets : 6  <br />
FREE Affiliate entrance ticket : 10  <br />
Logo placement Homepage : Secondary  <br />
Logo placement Advertising : Secondary  <br />
Newsletter to Participants All  <br />
Interviews panel : Secondary  <br />
Social Exposure  <br />
Show Guide : Half Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				3999,
				10,
				0,
				0
			),
      		'Show bag'=> array(
				'Every attendee would have the right to a Gift Bag, give your company maximum exposure by branding the Bags.  <br />
				This Sponsorship include the Bags.  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
				Hotel registration : Priority  <br />
Airport transfer  <br />
Included tickets : 6  <br />
FREE Affiliate entrance ticket : 10  <br />
Logo placement Homepage : Secondary  <br />
Logo placement Advertising : Secondary  <br />
Newsletter to Participants All  <br />
Interviews panel : Secondary  <br />
Social Exposure  <br />
Show Guide : Half Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				4999,
				10,
				0,
				0
			)
      	),
      	'Activities &amp; Events'=>array(
      		'Activity (Chiva Party Bus)'=> array(
				'Colombian Chiva party bus tour. (The tour has a duration of 2-3 hours.  <br />
				Includes 6 Chiva party buses, 6 Branded hostess, 12 bottles of aguardiente (Colombian liquor 2.000 ml bottles). <br />
				Each chiva bus has capacity for 50 people).  <br />
				If you want more chiva buses, the cost is $800 USD for each extra bus, which will also include branding, Hostess and 2 bottles of aguardiente.  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
				Hotel registration : Priority  <br />
Airport transfer  <br />
Included tickets : 6  <br />
FREE Affiliate entrance ticket : 10  <br />
Logo placement Homepage : Secondary  <br />
Logo placement Advertising : Secondary  <br />
Newsletter to Participants All  <br />
Interviews panel : Secondary  <br />
Social Exposure  <br />
Show Guide : Half Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				5999,
				10,
				0,
				0
			),
      		'Horseback riding Colombian Style tour'=> array(
				'Sponsor a sightseeing horseback riding tour thru the country land and rivers.  <br />
				The tour has a duration of 1-2 hours.  <br />
				Includes branding and horses with guides wearing branded hat and shirt, Drinks and music included.  <br />
				Shuttle transportation roundtrip from show hotel.  <br />
				200 horses activity.  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
Hotel registration : Priority  <br />
Included tickets : 4  <br />
Logo placement Homepage : Small  <br />
Logo placement Advertising : Small  <br />
Social Exposure  <br />
Show Guide : Quarter Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking. ',
				5999,
				10,
				0,
				0
			),
      		'Soccer or Volleyball sexy tournament'=> array(
				'Sponsor a 2 hour Sexy soccer or Volleyball tournament.  <br />
				Sponsor 4 teams that could be either all female or Mix  <br />
				teams with minimum of 3 girls per team, all teams will  <br />
				wear your branded tshirts, females on the loosing team  <br />
				will drop a piece of clothing on each score they get,  <br />
				the first 3 finalist will get trophy’s branded your company.  <br />
				Branding and banners at the venue, free water, free beer, free appetizers,  <br />
				free whistles, cheering items and free branded caps for all attendees.  <br />
				Professional referees for the tournament.  <br />
				Venue with great view of the San Felipe castle.  <br />
				Venue capacity for 300 people.  <br />
				Shuttle transportation roundtrip from show hotel.  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP:  <br />
				Hotel registration: Priority  <br />
Airport transfer  <br />
Included tickets: 6  <br />
FREE Affiliate entrance ticket : 10  <br />
Logo placement Homepage : Secondary  <br />
Logo placement Advertising : Secondary  <br />
Newsletter to Participants All  <br />
Interviews panel : Secondary  <br />
Social Exposure  <br />
Show Guide : Half Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				6999,
				10,
				0,
				0
			),
      		'Bowling Networking'=> array(
				'Sponsor a 2-hour fun networking event with bowling, Food and drinks.  <br />
				Includes exclusive branded bowling alley and bar with hamburgers and  <br />
				beverages, shoes and socks to play for everyone. 4 branded hostess.  <br />
				Shuttle bus transportation from Convention center.  <br />
				This event capacity is for 200 people.  <br />
ADITIONAL BENEFITS ON THIS SPONSORSHIP: <br />
Hotel registration: Priority  <br />
Included tickets: 6  <br />
FREE Affiliate entrance ticket : 10  <br />
Logo placement Homepage : Secondary  <br />
Logo placement Advertising : Secondary  <br />
Newsletter to Participants All  <br />
Interviews panel : Secondary  <br />
Social Exposure  <br />
Show Guide : Half Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking.',
				5999,
				10,
				0,
				0
			),
      		'Casino Networking'=> array(
				'Sponsor a 3 hour networking party at the famous Casino.  <br />
				Includes branding in casino, drinks and food for all attendees.  <br />
				4 branded hostess. Live band entertainment. branded credits <br />
				currency for playing tables.  <br />
				Casino capacity for 250 people.  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP:  <br />
Hotel registration: Priority  <br />
Included tickets: 6  <br />
FREE Affiliate entrance ticket : 10  <br />
Logo placement Homepage : Secondary  <br />
Logo placement Advertising : Secondary  <br />
Newsletter to Participants All  <br />
Interviews panel : Secondary  <br />
Social Exposure  <br />
Show Guide : Half Page Ad  <br />
VIP Zone Access at Parties  <br />
Pre Show Dinner Networking. ',
				6999,
				10,
				0,
				0
			)
      	),
      	'Parties'=>array(
      		'Welcome party'=> array(
				'Be the Hottest Company of them all, Everybody talks about  <br />
				the LALEXPO parties, let it be your Party. Forget about boring  <br />
				parties in other events, being Epic is the best way to advertise.  <br />
				Every Detail would be discussed with each sponsor. It includes  <br />
				drinks, Staff, entertainment and promotion material.  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
Hotel registration : Priority  <br />
Airport transfer  <br />
Included tickets : 10  <br />
FREE Affiliate entrance ticket : 20  <br />
Logo placement Homepage : Main  <br />
Logo placement Advertising : Main  <br />
Newsletter to Participants All  <br />
Interviews panel : Prominent  <br />
Social Exposure  <br />
Show Guide : Full Page Ad  <br />
Pre Show Dinner Networking ',
				12999,
				10,
				0,
				0
			),
      		'Everyday party'=> array(
				'Be the Hottest Company of them all, Everybody talks about  <br />
				the LALEXPO parties, let it be your Party. Forget about  <br />
				boring parties in other events, being Epic is the best way  <br />
				to advertise. Every Detail would be discussed with each sponsor.  <br />
				It includes drinks, Staff, entertainment and promotion material.  <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
				Hotel registration : Priority  <br />
Airport transfer  <br />
Included tickets : 10  <br />
FREE Affiliate entrance ticket : 20  <br />
Logo placement Homepage : Main  <br />
Logo placement Advertising : Main  <br />
Newsletter to Participants All  <br />
Interviews panel : Prominent  <br />
Social Exposure  <br />
Show Guide : Full Page Ad  <br />
Pre Show Dinner Networking',
				15999,
				10,
				0,
				0
			),
      		'Private N-Style VIP afterparty'=> array(
				'What happens in this party, stays in this party.  <br />
				Invite only exclusive party after the show closing party. <br />
				Private Villa, Drinks and entertainment.  <br />
				Limmited to 100 attendees.  <br />
				Every Detail would be discussed with each sponsor. <br />
				ADITIONAL BENEFITS ON THIS SPONSORSHIP :  <br />
Hotel registration : Priority  <br />
Airport transfer  <br />
Included tickets : 10  <br />
FREE Affiliate entrance ticket : 20  <br />
Logo placement Homepage : Main  <br />
Logo placement Advertising : Main  <br />
Newsletter to Participants All  <br />
Interviews panel : Prominent  <br />
Social Exposure  <br />
Show Guide : Full Page Ad  <br />
Pre Show Dinner Networking ',
				0,
				10,
				0,
				0
			)
      	),
      	'Awards'=>array(
      		'Presenting sponsor'=> array(
				'Red carpet interview  <br />
On screen video presentation  <br />
On screen promo Categories   <br />
Prominent logo on our website  <br />
Social Media announcements  <br />
Dedicated newsletter  <br />
Banners and ads in the Ballroom  <br />
Awards Wristband <br />
Tickets sponsor  <br />
Appetizers and Soft drinks sponsor  <br />
15 VIP seats  <br />
Show Guide LALEXPO (Full page Ad)  <br />
Video wall (Entrance Event)  <br />
Exclusive Transport to Show  <br />
Pre-Show Sponsors Dinner  <br />
 <br />
200 Advantage Votes  <br />
 <br />
Note: If you already are a lalexpo sponsor with right to Show guide Ad and <br />
or Pre Show Dinner Networking. Those items on this package wont apply, meaning you wont get it twice.',
				10000,
				10,
				0,
				0
			),
      		'Main sponsorship'=> array(
				'Red carpet interview  <br />
On screen video presentation  <br />
Logo on our website  <br />
Social Media announcements  <br />
Dedicated newsletter  <br />
Banners and ads in the Ballroom  <br />
8 VIP seats  <br />
Appetizers and soft drinks  <br />
Show Guide LALEXPO (Half page ad)  <br />
Video wall (Entrance Event)  <br />
Pre-Show Sponsors Dinner  <br />
 <br />
150 Advantage Votes',
				5000,
				10,
				0,
				0
			),
      		'Sponsorship'=> array(
				'Red carpet interview  <br />
Logo on our website  <br />
Social Media announcements  <br />
Logo on awards newsletters  <br />
6 VIP seats  <br />
Appetizers and soft drinks  <br />
Show Guide LALEXPO (Half page ad)  <br />
Video wall (Entrance Event)  <br />
Pre-Show Sponsors Dinner  <br />
 <br />
100 Advantage Votes',
				2999,
				10,
				0,
				0
			)
      	)
	);
	$c=1;
	foreach($planes as $category=>$packs){
        $page.='<section>
          <header>'.$category.'</header>
          <ul>';
        $cant=count($packs);
        $p=1;
        foreach($packs as $pack=>$detalle){
            //$add=$c==$cant?' class="sinBg"':'';
          $page.='<li id="sp'.$c.''.$p.'" onclick="verPlan('.$c.''.$p.')">
            <div class="tabl">
              <span class="he01 cell"><img src="img/logo02.jpg" /></span>
              <span class="he02 cell">'.$pack.'</span>
              <span class="he03 cell"><img src="img/fledo01.jpg" /></span>
            </div>';
            $vend=rand(1, 5);
            $rese=rand(1, 5);
            $disp=($detalle[2]-$vend-$rese);
            	$page.='<div id="sponsorPack'.$c.''.$p.'" class="tabl descrip" style="display: none;" >
              '.$detalle[0].'
              <div>
                <table cellpadding="0" cellspacing="0">
                  <tr>
                    <td>'.$lg->general->txtprice.'</td><td>$ '.$detalle[1].'</td>
                  </tr>
                  <tr>
                    <td>'.$lg->general->txtavailable.'</td><td>'.$disp.'</td>
                  </tr>
                  <tr>
                    <td>'.$lg->general->txtsold.'</td><td>'.$vend.'</td>
                  </tr>
                  <tr>
                    <td>'.$lg->general->txtreserved.'</td><td>'.$rese.'</td>
                  </tr>
                </table>';
                if ($disp>0) {
	                if ($_SESSION['login']) {
	                  $page.='<a href="#">Buy</a>';
	                } else {
	                  $page.='<a href="#" onclick="showForm(\'preLogin\')">Log in to buy</a>';
	                }
	            } else {
	            	$page.='<span class="btndes">Sold out</a>';
	            }
              $page.='</div>
            </div>';
              $p++;
          $page.='</li>';          
      	}
      	$c++;
        $page.='</ul>
      </section>';
    }
    echo $page;
?>