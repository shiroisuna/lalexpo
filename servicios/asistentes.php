<?php
	include('../includes/conexion.php');
	$asistentes=array();
	if (!empty($_GET['filtro'])) $filtro=" AND (nombre LIKE '%".$_GET['filtro']."%' OR apellido LIKE '%".$_GET['filtro']."%' OR company LIKE '%".$_GET['filtro']."%' OR email LIKE '%".$_GET['filtro']."%' OR objetivo LIKE '%".$_GET['filtro']."%')";
	$rs=$con->query("SELECT id, nombre, apellido, foto, estado_foto, company, (SELECT nombre FROM categorias WHERE id=id_categoria) categoria, descrip_profesional, objetivo, email, website, face, twitter, instagram, skype, youtube FROM usuarios WHERE activo=1 and habilitado=1 and eliminado=0 ".$filtro);
  	while($rw=$rs->fetch_object()){
  		if (!is_readable("../".str_replace("thumb_600_w/","",$rw->foto)) || empty($rw->foto) || trim($rw->foto)=="" || $rw->estado_foto!=1) $rw->foto="img/blank-profile-picture.png";
    	array_push($asistentes, $rw);
    }
	echo json_encode($asistentes);
?>