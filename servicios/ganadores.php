<?php
$page='';

$rs=$con->query("SELECT id, nombre_es, nombre_en, descrip_es, descrip_en FROM premios_categorias WHERE estado=1  ORDER BY orden");
while($rw=$rs->fetch_object()){
	if ($lg->idioma=='es') {
		$nombre=$rw->nombre_es;
		$descri=$rw->detalle_es;
		$txbtn1='Votar';
		$txbtn2='Inicie sesión';
	} else {
		$nombre=$rw->nombre_en;
		$descri=$rw->detalle_en;
		$txbtn1='Vote';
		$txbtn2='Login to vote';
	}

	$rs2=$con->query("SELECT pn.id, CONCAT(us.nombre, ' ', us.apellido) nominado, us.foto, pc.nombre_es, pc.nombre_en, pn.estado, COUNT(pv.id_nominado) votos FROM premios_votos pv
  INNER JOIN premios_nominados pn ON pn.id=pv.id_nominado
  INNER JOIN usuarios us ON us.id=pn.id_usuario
  INNER JOIN premios_categorias pc ON pc.id=pn.id_categoria
  WHERE pn.estado=1 AND pv.estado=1 AND pn.id_categoria='".$rw->id."'
  GROUP BY pv.id_nominado, pn.id_categoria
  ORDER BY votos DESC
  LIMIT 1");
	$nomcat='';
	if ($rs2 && $rs2->num_rows>0) {	
		$nomcat='<div id="lsnom'.$rw->id.'" class="lsnom">';	
		while($rw2=$rs2->fetch_object()){

			 $dest=' class="votado" ';
			 $nomcat.='<label '.$dest.'>'.$rw2->nominado.'</label><br/>';
			
		}
		$nomcat.='</div>';
	}
	$page.='<div class="fichaCat"><span class="tituloCat" >'.$nombre.'</span>'.$nomcat.'</div>';
}
echo '<section>'.$page.'</section>';
?>