<?php
$page='';
if ($lg->idioma=='es') setlocale (LC_ALL, "es_ES.UTF-8");
$rs=$con->query("SELECT id, fecha FROM programacion WHERE estado=1 GROUP BY fecha ORDER BY fecha");
while($rw=$rs->fetch_object()){
	$titulo=strftime("%A %d %b ", strtotime($rw->fecha));
	$page.='<section>
          <header><img src="img/calen.png" style="vertical-align: text-bottom;padding-right: 15px;" />'.$titulo.'</header>
          <ul>';
	$rs2=$con->query("SELECT id, fecha, TIME_FORMAT(inicio, '%h:%i %p') inicio, TIME_FORMAT(final, '%h:%i %p') final, titulo_es, titulo_en, detalle_es, detalle_en, ubicacion_es, ubicacion_en FROM programacion WHERE estado=1 AND fecha='".$rw->fecha."'");
	while($rw2=$rs2->fetch_object()){
		if ($lg->idioma=='es') {
			$nombre=$rw2->titulo_es;
			$descri=$rw2->detalle_es;
			$ubicacion=$rw2->ubicacion_es;
		} else {
			$nombre=$rw2->titulo_en;
			$descri=$rw2->detalle_en;
			$ubicacion=$rw2->ubicacion_en;
		}
		$page.='<li id="sp'.$rw->id.''.$rw2->id.'" onclick="verInfo('.$rw->id.''.$rw2->id.')">
            <div class="tabl">
              <span class="he01 cell"></span>
              <span class="he02 cell"><span style="color:#b42127">'.$rw2->inicio.' - '.$rw2->final.'</span> | '.$nombre.'</span>
              <span class="he03 cell"><img src="img/fledo01.jpg" /></span>
            </div>
            <div id="sponsorPack'.$rw->id.''.$rw2->id.'" class="tabl descrip" style="display: none;" >
              <p class="lalexpo-black-color">'.nl2br(str_replace(array("<br>", "<br/>", "<br />"), "", $descri)).'</p>
              <p><span style="color: #b42127;"><i class="fa fa-clock-o" aria-hidden="true"></i></span> <span class="lalexpo-red-color">'.$rw2->inicio.' - '.$rw2->final.'<span></span></span></p>
              <p><span style="color: #b42127;"><i class="fa fa-thumb-tack" aria-hidden="true"></i></span> <span class="lalexpo-red-color">'.$ubicacion.'</span></p>
            </div></li>';
	}
	$page.='</ul></section>';
}
echo $page;
?>