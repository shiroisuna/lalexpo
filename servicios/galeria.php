<?php
	include('includes/conexion.php');
	$page='';
  $ext='';
  $alb="";
  $limite="  LIMIT 0, 3";
  $p=(!empty($_GET['p'])?($_GET['p']-1):0);
	$albums=array();
  if (!empty($_GET['a'])) {
    $alb=" AND id=".$_GET['a'];
    $limite=" LIMIT ".($p*9).", 9";
  }

	$rs=$con->query("SELECT id, nombre_es, nombre_en FROM galeria_album WHERE habilitado=1  ".$alb." ORDER BY orden");
  	while($rw=$rs->fetch_object()){
  		if ($_GET['lg']=='es') {
  			$titulo=$rw->nombre_es;
        $btnver='Ver album';
        $btnvolver='Volver';
        $btnante='Anterior';
        $btnsigu='Siguiente';
  		} else {
			  $titulo=$rw->nombre_en;
        $btnvolver='Back';
        $btnante='Previous';
        $btnsigu='Next';
        $btnver='View album';
  		}
      //<header>'.$titulo.'</header>
  		$page.='<section><header>'.$titulo.'</header>';
        $fotos='<div class="tabl">';
  		$rs2=$con->query("SELECT archivo, enlace FROM galeria WHERE id_album='".$rw->id."' ".$limite);
      $numreg=$con->query("SELECT archivo, enlace FROM galeria WHERE id_album='".$rw->id."' ")->num_rows;
  		while($rw2=$rs2->fetch_object()){
        if (!empty($rw2->archivo) && is_readable('images/galeria/'.$rw2->archivo)) {
          $ext=substr(strrchr($rw2->archivo, "."), 1);
          if($ext=='mp4'){
            $fotos.='<div class="mingal" onclick="$.colorbox({html: \'<video autoplay controls><source src=../images/galeria/'.$rw2->archivo.' type=video/mp4 /></video>\', width:\'100%\', height:\'100%\'});" ><img class="btnsoc" src="img/compfb.gif" style="cursor:pointer;" title="Compartir en Facebook" alt="Compartir en Facebook" onclick="popup(\'https://www.facebook.com/sharer/sharer.php?u=http://lalexpo.com\',626,436);" /><img class="btnsoc" alt="Tweet" src="img/twitter.png" onclick="popup(\'https://twitter.com/intent/tweet?source=http%3A%2F%2Flalexpo.com%2Fes%2Fgallery.php&text=6th+Edition+Lalexpo+2019:%20http%3A%2F%2Flalexpo.com%2Fes%2Fgallery.php\',626,436);" /><video muted="" style="position: relative; top: -23px;width:350px; max-height:300px;"><source src="../images/galeria/'.$rw2->archivo.'" type="video/mp4" /></video></div>';
          }else{
            //  onclick="$.colorbox({href: \'/thumb_950_w/images/galeria/'.$rw2->archivo.'\', scalePhotos:true, innerWidth:\'90%\',rel:\'gal\'});" 
  		      $fotos.='<a class="lnkgal" href="/images/galeria/'.$rw2->archivo.'">
            <div class="mingal" style="background-image: url(/thumb_350_w/images/galeria/'.$rw2->archivo.');"><img class="btnsoc" src="img/compfb.gif" style="cursor:pointer;" title="Compartir en Facebook" alt="Compartir en Facebook" onclick="popup(\'https://www.facebook.com/sharer/sharer.php?u=http://lalexpo.com/images/galeria/'.$rw2->archivo.'\',626,436);" /><img class="btnsoc" alt="Tweet" src="img/twitter.png" onclick="popup(\'https://twitter.com/intent/tweet?source=http%3A%2F%2Flalexpo.com%2Fes%2Fgallery.php&text=6th+Edition+Lalexpo+2019:%20http%3A%2F%2Flalexpo.com%2Fes%2Fgallery.php\',626,436);" /></div></a>';
          }
        } else {
          $fotos.='<div class="mingal" onclick="$.colorbox({html: \'<iframe width=90% height=90% src=https://www.youtube.com/embed/'.$rw2->enlace.'></iframe>\', width:\'100%\', height:\'100%\'});" ><iframe width="350" height="292" src="https://www.youtube.com/embed/'.$rw2->enlace.'"></iframe></div>';
        }
  		}
		  $page.='
        '.$fotos.'        
        </div>';

      if (!empty($alb)) {
        $np=0;
        $page.='<div class="paginacion"><ul>';
        $paginas=ceil($numreg/9);
        if ($paginas>1) {

            if ($p>1) {
              $page.='<li style="width: 100px;" onclick="document.location=\'gallery.php?lg='.$_GET['lg'].'&a='.$rw->id.'&p='.($p).'\'">'.$btnante.'</li>';
              $pg=$p;
            } else {
              $pg=0;
            }

          for ($pg; $pg<$paginas; $pg++) {

            if ($pg<($p+10)) {
              $page.='<li';
              if ($pg==$p) $page.=' class="active" ';
              $page.=' onclick="document.location=\'gallery.php?lg='.$_GET['lg'].'&a='.$rw->id.'&p='.($pg+1).'\'">'.($pg+1).'</li>';
            }
            $np=$pg;
          }

            if ($paginas>10) $page.='<li style="width: 100px;" onclick="document.location=\'gallery.php?lg='.$_GET['lg'].'&a='.$rw->id.'&p='.($np+2).'\'">'.$btnsigu.'</li>';
        }
         
        $page.='</ul></div>';
      
        $page.='<div class="btnvermasg" onclick="document.location=\'gallery.php#menu\'">'.$btnvolver.'</div>';
      } else {
        $page.='<div class="btnvermasg" onclick="document.location=\'../gallery.php?lg='.$_GET['lg'].'&a='.$rw->id.'\'">'.$btnver.'</div>';
      }
      $page.='  </section>';
    }

    echo $page;
?>