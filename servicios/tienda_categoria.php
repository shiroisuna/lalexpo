<?php
$page='';

$rs=$con->query("SELECT id, nombre_es, nombre_en FROM productos_categorias WHERE estado=1 AND id='".$_GET['id']."' ");
while($rw=$rs->fetch_object()){
	if ($lg->idioma=='es') {
		$nombre=$rw->nombre_es;
		$descri=$rw->detalle_es;
		$txbtn1='Agregar al carrito';
		$txbtn2='Inicie sesión';
		$txbtn3='Agotado';
	} else {
		$nombre=$rw->nombre_en;
		$descri=$rw->detalle_en;
		$txbtn1='Add to cart';
		$txbtn2='Login';
		$txbtn3='Out of stock';
	}

	$rs2=$con->query("SELECT * FROM productos WHERE id_categoria='".$rw->id."' AND activo=1 ORDER BY id ");
	$nomcat='';
	if ($rs2 && $rs2->num_rows>0) {	
		while($rw2=$rs2->fetch_object()){

			if ($lg->idioma=='es') {
				$nombre_prod=$rw2->nombre_es;
				$descip_prod=$rw2->descripcion_es;
			} else {
				$nombre_prod=$rw2->nombre_en;
				$descip_prod=$rw2->descripcion_en;
			}

			if (!is_readable('images/productos/'.$rw2->archivo)) {
				$imagen_prod='../img/default.gif';
			} else {
				$imagen_prod='../images/productos/'.$rw2->archivo;
			}
			$disponible=$rw2->disponible-$rw2->vendido;
			if ($_SESSION['login']) {
				if ($disponible>0) {
					$boton='<form method="post" action="store.php" ><input type="hidden" name="addcart" value="1" /><input type="hidden" name="codprod" value="'.$rw2->id.'" /><input type="hidden" name="nomprod" value="'.$nombre_prod.'" /><input type="hidden" name="cantidad" value="1" /><input type="hidden" name="valor" value="'.$rw2->valor.'" /><input type="submit" class="boton" value="'.$txbtn1.'" /></form>';
				} else {
					$boton='<a href="javascript:;" class="boton" >'.$txbtn3.'</a>';
				}
				
			} else {				
				$boton='<a href="javascript:;" class="boton" onclick="showForm(\'preLogin\');$(\'body,html\').animate({scrollTop: 100}, 800);" >'.$txbtn2.'</a>';
			}
			
			$page.='<div class="ficha"  onclick="document.location=\'../product.php?lg='.$lg->idioma.'&id='.$rw2->id.'\'">
			<img src="'.$imagen_prod.'" />
			<span class="titulo" >'.$nombre_prod.'</span>
			<span class="descip" >'.substr($descip_prod, 0, 40).'</span>
			<span class="precio" >COP $ '.number_format($rw2->valor, 2, ".", ",").'</span>
			'.$boton.'</div>';
		}
		
	}

	
}
echo '<section>'.$page.'</section>';
?>