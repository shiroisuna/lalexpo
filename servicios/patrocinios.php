<?php
	$page='';
	$paquetes=array();
	$rs=$con->query("SELECT id, nombre_es, nombre_en, estado FROM patrocinador_categoria_paquete WHERE estado=1 AND id!=8
");
  	while($rw=$rs->fetch_object()){
  	 flush();
  		if ($lg->idioma=='es') {
  			$titulo=$rw->nombre_es;
  		} else {
			  $titulo=$rw->nombre_en;
  		}
  		$page.='<section>
          <header>'.$titulo.'</header>
          <ul>';
    		$rs2=$con->query("SELECT id, id_categoria, imagen, nombre_es, nombre_en, descrip_es, descrip_en, precio, cantidad, (cantidad-vendido) disponible, vendido, 0 reservado, estado FROM patrocinador_paquete WHERE estado=1 AND id_categoria='".$rw->id."'");
    		while($rw2=$rs2->fetch_object()){
    		  flush();
          if ($lg->idioma=='es') {
            $nombre=$rw2->nombre_es;
            $descri=$rw2->descrip_es;
          } else {
            $nombre=$rw2->nombre_en;
            $descri=$rw2->descrip_en;
          }
          $imagen='';
          if (!empty($rw2->imagen) && is_readable("images/paquetes/".$rw2->imagen)) {
            $imagen='<img src="images/paquetes/'.$rw2->imagen.'" title="" />';
          } else {
            $imagen='<img src="img/logo02.jpg" />';
          }
                $vendido='';
            if ($rw2->disponible<1) {
                if ($lg->idioma=='es') {
                    $vendido='<div class="vendido"></div>';
                } else {
                    $vendido='<div class="soldout"></div>';
                }
            }
          $page.='<li id="sp'.$rw->id.''.$rw2->id.'">
            <div class="tabl" onclick="verPlan('.$rw->id.''.$rw2->id.')" style="cursor:pointer">
              <span class="he01 cell">'.$imagen.'</span>
              <span class="he02 cell">'.$nombre.'</span>
              <span class="he03 cell"><img src="img/fledo01.jpg" /></span>
              '.$vendido.'
            </div>
            <div id="sponsorPack'.$rw->id.''.$rw2->id.'" class="tabl descrip" style="display: none;" >
              <span class="detalle">'.nl2br(str_replace(array("<br>", "<br/>", "<br />"), "", $descri)).'</span>
              <div>
                <table cellpadding="0" cellspacing="0">
                  <tr>
                    <td>'.$lg->general->txtprice.'</td><td>$ '.$rw2->precio.'</td>
                  </tr>
                  <tr>
                    <td>'.$lg->general->txtavailable.'</td><td>'.$rw2->disponible.'</td>
                  </tr>
                  <tr>
                    <td>'.$lg->general->txtsold.'</td><td>'.$rw2->vendido.'</td>
                  </tr>
                  <tr>
                    <td>'.$lg->general->txtreserved.'</td><td>'.$rw2->reservado.'</td>
                  </tr>
                </table>';
                if ($rw2->disponible>0) {
                  if ($_SESSION['login']) {
                    $page.='<a href="#" onclick="compraPatrocinio('.$rw2->id.')">'.$lg->general->btn_buy.'</a>';
                  } else {
                    $page.='<a href="#" onclick="showForm(\'preLogin\')">'.$lg->general->btn_logintobuy.'</a>';
                  }
                } else {
                  $page.='<span class="btndes">'.$lg->general->btn_soldout.'</span>';
                }
              $page.='</div>
            </div>';
    		}
		  $page.='</ul></section>';
    }
    echo $page;
?>