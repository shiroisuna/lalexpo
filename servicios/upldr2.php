<?php
	session_start();
	include('../includes/conexion.php');
	$archivo=hash('crc32', $_SESSION['id'].date("his"));
	if ((($_FILES["file"]["type"] == "image/gif")
	|| ($_FILES["file"]["type"] == "image/png")
	|| ($_FILES["file"]["type"] == "image/jpeg")
	|| ($_FILES["file"]["type"] == "image/pjpeg"))
	) {
	  if ($_FILES["file"]["error"] > 0) {
		$resp['codigo']=0;
		$resp['mensaje']="Codigo de Error: " . $_FILES["file"]["error"] . "";
	  } else { 
		if (file_exists("../images/patrocinadores/".$_FILES["file"]["name"]))
		  {
		  	$resp['codigo']=0;
		  	$resp['mensaje']=$_FILES["file"]["name"] . " ya existe. ";
		  }
		else
		  {
			$filename = $_FILES["file"]["tmp_name"];
			$percent = 100;
			list($width, $height) = getimagesize($filename);
			$newwidth = $width; 
			$newheight = $height;
			$thumb = imagecreatetruecolor($newwidth, $newheight); 
			if ($_FILES["file"]["type"] == "image/jpeg" || $_FILES["file"]["type"] == "image/jpg") $source = imagecreatefromjpeg($filename);
			if ($_FILES["file"]["type"] == "image/gif") $source = imagecreatefromgif($filename);
			if ($_FILES["file"]["type"] == "image/png") $source = imagecreatefrompng($filename);
			imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
			imagejpeg($thumb, "../images/patrocinadores/".$archivo.".jpg", 70);
			$resp['codigo']=1;
			$resp['mensaje']='images/patrocinadores/'.$archivo.'.jpg';
			// @unlink("../".$_POST['imgact']);
			// $con->query("UPDATE usuarios SET foto='images/patrocinadores/".$archivo.".jpg' WHERE id='".$_POST['usuario']."'");
			// $_SESSION['foto']='images/patrocinadores/'.$archivo.'.jpg';
		  }
		}
	} else {
		$resp['codigo']=0;
		$resp['mensaje']="Tipo de archivo inv&aacute;lido!! Error: ".$_FILES["file"]["error"]." Tipo: ".$_FILES["file"]["type"]." Tamaño: ".$_FILES["file"]["size"];
	}
	echo json_encode($resp);
?>