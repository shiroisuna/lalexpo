<?php
session_start();
include('../includes/conexion.php');
include('../includes/funciones.php');
include('../lib/geoip.inc');
$abir_bd=geoip_open('../lib/GeoIP.dat',GEOIP_STANDARD);
$pais_cod=geoip_country_code_by_addr($abir_bd, $_SERVER['REMOTE_ADDR']);
$pais_name=geoip_country_name_by_addr($abir_bd, $_SERVER['REMOTE_ADDR']);
geoip_close($abir_bd);
if($pais_cod=='CO'){
  $valorTicket=$con->query("select valorTicket_colombia from parametros")->fetch_object()->valorTicket_colombia;
  $moneda='COP';
}else{
  $valorTicket=$con->query("select valorTicket_general from parametros")->fetch_object()->valorTicket_general;
  $moneda='USD';
}
if ($_POST['actualizar']==1) {
	$r=new stdClass;
	//ciudad='".$_POST['ciudad']."',
		 // nickname='".$_POST['nickname']."',
		 // sitios_adicionales='".$_POST['sitios']."',
	$password="";
	if (!empty($_POST['passUsuario'])) $password=" pass=MD5('".$_POST['passUsuario']."'), ";
	$query = "UPDATE usuarios SET
		 nombre='".$_POST['nombreUsuario']."',
		 apellido='".$_POST['apellidosUsuario']."',
		 dni='".$_POST['documento']."',
		 foto='".$_POST['foto']."',
		 ".$password."
		 email='".$_POST['correo']."',
		 pais='".$_POST['pais']."',
		 estado='".$_POST['estado']."',
     pais_cod='".$_POST['pais_cod']."',
		 estado_cod='".$_POST['estado_cod']."',
		 ciudad='".$_POST['ciudad']."',
		 telefono='".$_POST['telefonoUsuario']."',
		 nombre_escarapela='".$_POST['nombreEscarapela']."',
		 estudio='".$_POST['studioEscarapela']."',
		 objetivo='".$_POST['objetivo']."',
		 descrip_profesional='".$_POST['descripcionProfesional']."',
		 website='".$_POST['soc1']."',
		 face='".$_POST['soc2']."',
		 twitter='".$_POST['soc3']."',
		 instagram='".$_POST['soc4']."',
		 skype='".$_POST['soc5']."',
		 youtube='".$_POST['soc6']."'
	   WHERE id='".$_POST['id']."' ";
	$con->query($query);
    if(empty($con->error)){
	 $r->estado=1;
	 $r->msg="Perfil actualizado exitosamente.";
   $rw=$con->query("select * from usuarios where id='".$_POST['id']."'")->fetch_object();
   $_SESSION=array(
    	'login'=>1,
    	'id'=>$rw->id,
      'activo'=>$rw->activo,
    	'nombre'=>$rw->nombre,
    	'apellidos'=>$rw->apellido,
    	'documento'=>$rw->dni,
    	'correo'=>$rw->email,
    	'pais'=>$rw->pais,
    	'estado'=>$rw->estado,
      'ciudad'=>$rw->ciudad,
    	'telefono'=>$rw->telefono,
      'pais_regis_cod'=>$rw->pais_cod,
      'estado_regis_cod'=>$rw->estado_cod,
    	'foto'=>$rw->foto,
      'pais_cod'=>$pais_cod,
      'valorTicket'=>$valorTicket,
      'moneda'=>$moneda,
    );
    } else {
	 $r->estado=0;
	 $r->msg="Error al actualizar información del perfil. ".$con->error;
    }
    if (!empty($_POST['foto'])) $_SESSION['foto']=$_POST['foto'];
    echo json_encode($r);
}
if ($_GET['id']) {
	$rs=$con->query("SELECT nombre,  apellido,  genero,  foto, estado_foto,  email,  dni,  pais,  estado, ciudad, lengua,  telefono,  id_categoria,  fecha_alta,  anio,  company,  website,  models,  face,  twitter,  instagram,  skype,  youtube, nombre_escarapela, estudio, objetivo,  descrip_profesional,  nickname,  sitios_adicionales FROM usuarios WHERE id='".$_GET['id']."'");
  	if($rs->num_rows>0){
    	$rw=$rs->fetch_object();
    	echo json_encode($rw);
    }
}
?>