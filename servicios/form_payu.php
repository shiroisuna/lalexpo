<?
include('../includes/conexion.php');
if(empty($_GET['hash'])){
  $emailComprador=$_POST['emailComprador'];
  $nombreComprador=$_POST['nombreComprador'];
  $telefonoComprador=$_POST['telefonoComprador'];
  $valor=$_POST['valor'];
  $descripcion=$_POST['descripcion'];
  $idProducto=$_POST['idProducto'];#Este es interno para nosotros para saber si es compra de ticket, hotel, paquete, etc
  $idUsuario=$_POST['idUsuario'];
  $datoReferencia=$_POST['datoReferencia'];
  $payu_moneda=$_POST['moneda'];
}else{
  $rs=$con->query("SELECT
  up.*,
  u.nombre usu_nombre,
  u.apellido usu_apellido,
  u.email,
  u.telefono,
  p.transaction_date,
  up.producto
  FROM usuarios_pagos up
  INNER JOIN usuarios u ON u.id=up.id_usuario
  LEFT JOIN pagos p ON p.id=up.id_pago where up.hash='".$_GET['hash']."'");
  if($rs->num_rows==0){
    echo 'No se encontro el vinculo de pago';
    exit;
  }
  $datos=$rs->fetch_object();
  if($datos->estado==1){
    echo 'Este link de pago fue abonado anteriormente el '.$datos->transaction_date;
    exit;
  }
  $emailComprador=$datos->email;
  $nombreComprador=$datos->usu_nombre.' '.$datos->usu_apellido;
  $telefonoComprador=$datos->telefono;
  $valor=$datos->valor;
  $descripcion=$datos->nombre;
  $idProducto=$datos->producto;#Este es interno para nosotros para saber si es compra de ticket, hotel, paquete, etc
  $idUsuario=$datos->id_usuario;
  $datoReferencia=$datos->id.'_'.$datos->opcional;
  $payu_moneda=$datos->moneda;
}
$dd=$con->query("select * from parametros")->fetch_object();
if($dd->test==1){
  $payu_modoPrueba=1;
  $payu_url='https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/';
  $payu_merchantId='508029';//id de la cuenta/comercio
  $payu_apikey="4Vj8eK4rloUd272L48hsrarnUA";
  $payu_accountId='512321';//id del pais
  $payu_iva=21;
}else{
  $payu_modoPrueba=0;
  $payu_url=$dd->payu_url;
  $payu_merchantId=$dd->payu_merchantId;
  $payu_apikey=$dd->payu_apikey;
  $payu_accountId=$dd->payu_accountId;
  $payu_iva=$dd->payu_iva;
}
$iva=ceil($valor*$payu_iva/100);

$referencia='lalexpo|'.$idUsuario.'|'.$idProducto.'|'.date('Ymd_His').'|'.$datoReferencia;
$preHash=$payu_apikey."~".$payu_merchantId."~".$referencia."~".($valor+$iva)."~".$payu_moneda;
$hash=md5($preHash);


?>
<form method="post" target="_self" id="frmPayu" action="<?=$payu_url?>">
  <input name="tax"           type="hidden"  value="<?=$iva?>" /><!--iva-->
  <input name="taxReturnBase" type="hidden"  value="<?=$valor?>" /><!--en base a que va el iva-->
  <input name="currency"      type="hidden"  value="<?=$payu_moneda?>" /><!--moneda-->
  <input name="signature"     type="hidden"  value="<?=$hash?>" />
  <input name="test"          type="hidden"  value="<?=$payu_modoPrueba?>" /><!--si es testeo-->
  <input name="buyerEmail"    type="hidden"  value="<?=$emailComprador?>" /><!--email de comprador-->
  <input name="buyerFullName"  type="hidden"  value="<?=$nombreComprador?>" />
  <input name="shippingAddress"   type="hidden"  value="" />
  <input name="shippingCity"   type="hidden"  value="" />
  <input name="shippingCountry"   type="hidden"  value="" />
  <input name="telephone"  type="hidden"  value="<?=$telefonoComprador?>" />
  <input name="referenceCode" type="hidden" value="<?=$referencia?>" />
  <input name="merchantId"    type="hidden"  value="<?=$payu_merchantId?>" /><!--cambiar id de comercio-->
  <input name="accountId"     type="hidden"  value="<?=$payu_accountId?>" /><!--cambiar codigo de pais-->
  <input name="description"   type="hidden"   value="<?=$descripcion?>"  />
  <input name="amount" type="hidden"  value="<?=($valor+$iva)?>" />
  <input name="refVenta" type="hidden" value="<?=$referencia?>" />
  <input name="usuarioId"    type="hidden"  value="<?=$payu_merchantId?>" /><!--cambiar id de comercio-->
  <input name="cuentaId"     type="hidden"  value="<?=$payu_accountId?>" /><!--cambiar codigo de pais-->
  <input name="descripcion"   type="hidden"  value="<?=$descripcion?>"  />
  <input name="valor" type="hidden"  value="<?=($valor+$iva)?>" />
  <input name="responseUrl"    type="hidden"  value="http://localhost:81/dashboard.php" />
  <input name="confirmationUrl"    type="hidden"  value="http://localhost:81/confirmacion_pago.php" />
</form>
<script>
document.getElementById('frmPayu').submit()
</script>