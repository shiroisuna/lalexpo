function setTipoHab(idTipo,nomb){
  $('.sechotel').hide()
  $('#elejir_habiltacion').show()
  $('#tipo_hab').html(nomb)
  $('#habitaciones').html('<option value="0">'+lg.js_cargan+'...</option>')
  $.ajax({
    url:'location.php',type:'post',data:'getHabitaciones=1&id='+idTipo,dataType:'json',
    success:function(d){
      var i,t='',c=0,nom
      for(i in d){
        __list=d
        nom=(lenAct=='es')?d[i].nombre_es:d[i].nombre_en
        t+='<option value="'+c+'">'+nom+'</option>'
        c++
      }
      $('#habitaciones').html(t)
    },error:function(d){
      msg.text(d.responseText+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+lg.t_acept+'</a>').load();
    }
  })
}
$(document).ready(function(){
  from=$("#entrada").datepicker({
    dateFormat:'dd/mm/yy',
    defaultDate: '2018-02-01',
    defaultDate: "+1",
  }).on( "change", function() {
    to.datepicker( "option", "minDate", getDate( this ) );
  });
  to=$("#salida").datepicker({
    dateFormat:'dd/mm/yy',
    defaultDate: "+1",
  }).on( "change", function() {
    from.datepicker( "option", "maxDate", getDate( this ) );
  });
  $('#elejir_habiltacion2 .datepicker').datepicker({dateFormat:'dd/mm/yy'})
  var a=new Date(2019,01,01)
  $("#entrada,#salida,#elejir_habiltacion2 .datepicker").datepicker( "option", "defaultDate", a );
  function getDate( element ) {
    var date;
    try {
      date = $.datepicker.parseDate( dateFormat, element.value );
    }catch( error ) {
      date = null;
    }
    return date;
  }
})

function setNroHospe(v){

}
function validaDatos1(){
  if($('#entrada').val()==''){
    msg.text(lg.js_indique+' Check-In<br /><a class="botLink btnAviso" onclick="msg.close()">'+lg.t_acept+'</a>').load()
    return false
  }
  if($('#salida').val()==''){
    msg.text(lg.js_indique+' Check-Out<br /><a class="botLink btnAviso" onclick="msg.close()">'+lg.t_acept+'</a>').load()
    return false
  }
  var dat=__list[$('#habitaciones').val()]


  var desde=$('#entrada').val().split('/')
  var hasta=$('#salida').val().split('/')
  desde=desde[2]+'-'+desde[1]+'-'+desde[0]
  hasta=hasta[2]+'-'+hasta[1]+'-'+hasta[0]
  var fecha1 = moment(desde);
  var fecha2 = moment(hasta);
  var cantDias=(fecha2.diff(fecha1, 'days')/*+1*/);
  if(cantDias<1){
    msg.text(lg.js_fe_invalid+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+lg.t_acept+'</a>').load()
    return false
  }

  var precioFinal=(cantDias*parseInt(dat.precio))
  $('#totalPagar').val(precioFinal)

  $('.precio_total').val(precioFinal)//este es el del campo que va a payu

  var cant=parseInt(dat.personas),i,t=''
  for(i=1;i<=cant;i++){
    t+='<div class="col-3"><b>'+lg.txt_nom+'</b>'
    t+='    <input type="text" id="hospe_nomb_'+i+'" name="hospe_nomb_'+i+'" class="form-control datos_hospedados" maxlength="70" />'
    t+='  </div><div class="col-3"><b>'+lg.js_pasap+'</b>'
    t+='    <input type="text" id="hospe_pasaporte_'+i+'" name="hospe_pasaporte_'+i+'" class="form-control datos_hospedados" maxlength="50" />'
    t+='  </div><br style="clear:both" />'
  }
  $('#infoHosp').html(t)
  $('#elejir_habiltacion').hide()
  $('#elejir_habiltacion2').show()
}
function irPayu(){
  if($('#contac_nombre').val()==''){
    msg.text(lg.js_ind_nom_cont+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+lg.t_acept+'</a>').load()
    return false
  }
  if($('#contac_email').val()==''){
    msg.text(lg.js_ind_email+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+lg.t_acept+'</a>').load()
    return false
  }
  var emailReg=/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
  if(!emailReg.test($('#contac_email').val())){
    msg.text(lg.js_ind_email_val+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+lg.t_acept+'</a>').load()
    return false
  }
  $('#mail_comprador').val($('#contac_email').val())
  if($('#contac_telef').val()==''){
    msg.text(lg.js_ind_telef+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+lg.t_acept+'</a>').load()
    return false
  }
  var pasa=true
  $('.datos_hospedados').each(function(a,b){
    if($(b).val()==''){
      msg.text(lg.js_hosp_incomp+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+lg.t_acept+'</a>').load()
      pasa=false
    }
  })
  $('#nombre_cliente').val($('#contac_nombre').val())
  $('#telefono_cliente').val($('#contac_telef').val())
  $('.descrip_payu').val('Compra LalExpo Hoteleria')

  var dat=__list[$('#habitaciones').val()]
  var i,t='',cant=parseInt(dat.personas)
  for(i=1;i<=cant;i++){
    t+='&hospe_nomb_'+i+'='+$('#hospe_nomb_'+i).val()
    t+='&hospe_pasaporte_'+i+'='+$('#hospe_pasaporte_'+i).val()
  }
  msg.text(lg.t_generando).load()
  $.ajax({
    url:'location.php',type:'post',dataType:'json',
    data:'id_habitacion='+dat.id
    +'&entrada='+$('#entrada').val()
    +'&salida='+$('#salida').val()
    +'&contac_nombre='+$('#contac_nombre').val()
    +'&contac_email='+$('#contac_email').val()
    +'&contac_telef='+$('#contac_telef').val()
    +'&totalPagar='+$('#totalPagar').val()
    +'&personas='+cant
    +'&precio='+$('.precio_total').val()
    +'&id_categoria='+dat.id_categoria
    +'&setReserva=1'

    +'&partida='+$('#partida').val()
    +'&llegada='+$('#llegada').val()
    +'&horario_partida='+$('#horario_partida_1').val()+':'+$('#horario_partida_2').val()+':00'
    +'&horario_llegada='+$('#horario_llegada_1').val()+':'+$('#horario_llegada_2').val()+':00'
    +'&nomb_aerolinea_partida='+$('#nomb_aerolinea_partida').val()
    +'&nomb_aerolinea_llegada='+$('#nomb_aerolinea_llegada').val()
    +'&nomb_nrovuelo_partida='+$('#nomb_nrovuelo_partida').val()
    +'&nomb_nrovuelo_llegada='+$('#nomb_nrovuelo_llegada').val()
    +'&recogeAero='+$('#recogeAero').val()
    +t,
    success:function(d){
      if(d.estado==1){
        _ob={
          emailComprador:$('#contac_email').val(),
          nombreComprador:$('#contac_nombre').val(),
          telefonoComprador:$('#contac_telef').val(),
          valor:$('#totalPagar').val(),
          descripcion:lg.txt_reserv_hab,
          idProducto:'1',
          idUsuario:_user,
          datoReferencia:d.id_reserva,
          moneda:'USD'
        }
        msg.text(lg.js_genr_dat+'<br /><a class="botLink btnAviso" onclick="goPayu(_ob)">'+lg.t_conf_pago+'</a>').load()
      }else{
        msg.text(d.msg+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+lg.t_acept+'</a>').load()
      }
    },error:function(d){
      msg.text(d.responseText+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+lg.t_acept+'</a>').load()
    }
  })
}
function chgRecoje(v){
  $('#dvDatosItinerario').css('display',(v=='1'?'block':'none'))
}