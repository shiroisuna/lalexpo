$(document).ready(function(){
    var sitios=9;
    var cont;
    try {
      const eminp = document.getElementById('email2');
      eminp.onpaste = function(e) { e.preventDefault(); }
      $('#menu').slicknav({
        prependTo:'#menuTop'
      });
    } catch(error) {}
    $("#aviso").dialog({
        autoOpen:false,modal:true,resizable:false,
        title:'Mensaje',closeOnEscape:false,open:function(){
            $(this).parent().find('a.ui-dialog-titlebar-close').hide()
        }
    })
    $("#nombre").keyup(function() {
      $("#nombUsuario").html($("#nombreUsuario").val()+" "+$("#apellidosUsuario").val());
      $("#nombEscaparela").html($("#nomEsc").val());
    });
    $("#apellidos").keyup(function() {
      $("#nombUsuario").html($("#nombreUsuario").val()+" "+$("#apellidosUsuario").val());
      $("#nombEscaparela").html($("#nomEsc").val());
    });
    $("#nomEsc").change(function() {
        $("#nombEscaparela").html($("#nomEsc").val());
    });
    $("[name=estado]").on("change", function(){
        console.log("estado : "+$("[name=estado]").val());
        //cargarCiudades();
    });
    $("#top").click(function() {
      $('body,html').animate({scrollTop: 0}, 800);
    });
    $('.section10').on('inview', function(event, isInView) {
      if (isInView) {
        $('.section10 h1').addClass('animated fadeIn')
        $('.section10 .derr div').addClass('animated bounceInRight')
        $('.section10 .log img').addClass('animated fadeIn')
      } else {
        $('.section10 h1').removeClass('animated fadeIn')
        $('.section10 .derr div').removeClass('animated bounceInRight')
        $('.section10 .log img').removeClass('animated fadeIn')
      }
    });
    $('.section01 .pre').on('inview', function(event, isInView) {
      if (isInView) {
        $('.izq01').addClass('animated bounceInRight')
        $('.section01 a').addClass('animated fadeIn')
      } else {
        $('.izq01').removeClass('animated bounceInRight')
        $('.section01 a').removeClass('animated fadeIn')
      }
    });
    $('.section03').on('inview', function(event, isInView) {
      if (isInView) {
        $('.section03 p').addClass('animated bounceInLeft')
        $('.section03 a').addClass('animated fadeIn')
      } else {
        $('.section03 p').removeClass('animated bounceInLeft')
        $('.section03 a').removeClass('animated fadeIn')
      }
    });
    $('.bkg .txt01').on('inview', function(event, isInView) {
      if (isInView) {
        $('.bkg .txt01 span img').addClass('animated fadeIn')
      } else {
        $('.bkg .txt01 span img').removeClass('animated fadeIn')
      }
    });
    $('#principal').flexslider({ animation: "fade", controlNav: false, directionNav: false, video: true });
    $('#logosPatrocinios').flexslider({ animation: "fade", controlNav: false, directionNav: false, video: true,slideshowSpeed:3000 });
    $('.menu2 div').hover(function(){
      $('.menu2 div').removeClass('act')
    	$(this).addClass('act')
    },function(){
    	//$(this).removeClass('act')
    })
    $('.section02 a,.section04 a').hover(function(){
    	$(this).addClass('pulse animated')
    },function(){
      $(this).removeClass('pulse animated')
    	//$(this).removeClass('act')
    })
    var end = new Date(2019,1,17,14);//new Date('02/17/2019 02:00 PM');
    var _minute =  60000;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var timer;
    function showRemaining() {
        var now = new Date();
        var distance = end - now;
        if (distance < 0) {
            clearInterval(timer);
            return;
        }
        var days = Math.floor(distance / _day);
        var hours = Math.floor((distance % _day) / _hour);
        var minutes = Math.floor((distance % _hour) / _minute);
        $('#dias').html(days)
        $('#horas').html(hours)
        $('#minutos').html(minutes)
    }
    timer = setInterval(showRemaining, 1000);
  })
function showForm(f){
	window.scrollTo(0,0)
	$('#login').show()
	$('#'+f).css('opacity','0').css('display','block').animate({opacity:1})
}

/*Funcion añadida para apertura popup*/
function showPopup(p){
	window.scrollTo(0,0)
	$('#login').show()
	$('#'+p).css('opacity','0').css('display','block').animate({opacity:1})  	
}
$("#popup").click(function() {
      $('body,html').animate({scrollTop: 0}, 800);
    });

var msg={
    width:function(t){
        $("#aviso").dialog({width:t});return this
    },
    height:function(t){
        $("#aviso").dialog({height:t});return this
    },
    title:function(t){
        $("#aviso").dialog({title:t});return this
    },
    load:function(){
        $("#aviso").dialog("option", "position", { my: "top", at: "center", of: window } );
        $("#aviso").dialog({buttons:{},modal: true})
        $("#aviso").dialog('open')
        return this
    },
    text:function(t){
        $('#avisoTxt').html(t);return this
    },
    close:function(){
        $('#aviso').dialog('close');
    },
    confirm:function(f,f2){
        $("#aviso").dialog({
            title:'Confirmación',
            buttons:{
                "Aceptar":f,
                "Cancelar":function(){msg.close();if(typeof f2=='string'){eval('funsCancel.'+f2+'()')}}
            }
        })
        return this
    },
    aceptar:function(f){
        $("#aviso").dialog({
            buttons:{
                "Aceptar":function(){
                    $('#aviso').dialog('close');
                    if(typeof f=='string'){
                        eval('funsCancel.'+f+'()')
                    }
                    if(typeof f=='function'){
                        f()
                    }
                }
            }
        })
		$('.ui-dialog :button').each(function(){
			if($(this).find('.ui-button-text').html()=='Aceptar'){
				$(this).focus()
			}
		})
        return this
    }
}
function closeForm(){
  	$('.login').hide()
  	$('.preLogin').animate({opacity:0},400,function(){
    $('.preLogin').hide()
  })
}
/*Funcion añadida para cierre popup*/
function closePopup(){
	$('.popup').hide()
  	$('.prePopup').animate({opacity:0},400,function(){
    $('.prePopup').hide()
  })
}
var slideIndex = 1;
showSlides(slideIndex);
function plusSlides(n,esAuto) {
  if(TIMERSLIDE)
      clearTimeout(TIMERSLIDE)
  showSlides(slideIndex += n);
}
function currentSlide(n) {
  showSlides(slideIndex = n);
}
function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  TIMERSLIDE=setTimeout(function(){plusSlides(1,true)},5000)
  //if(dots[slideIndex-1]){
  try {
    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " active";
  } finally { return true; }
  //}
}
function login(){
  var demas='<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>'
  var idioma=$("#txtbtnlog").val()
  if(typeof realBotones =='undefined')
  realBotones=$('#preLogin .login_botones').html()
  $('#preLogin .login_botones').html('<a class="botLink">'+idioma.split("-")[1]+'</a>')
  $('#log_leyenda').html('')
  $.ajax({
    url:'servicios/login.php?log=1&_lengua='+_lengua,data:$('#preLogin').serialize(),type:'post',
    success:function(d){
      if(d==1){
        $('#preLogin .login_botones').html('<a class="botLink">'+idioma.split("-")[0]+'</a>')
        document.location.href='es/dashboard.php';
      }else{
        $('#preLogin .login_botones').html(realBotones)
         msg.text(d+demas).load()
      }
    },error:function(){
      $('#log_leyenda').html('Hubo un error al intentar ingresar, por favor, intente nuevamente.')
    }
  })
  return false
}
function contactarF(leng){
  var exito,errortxt,selePais,seleCiu,seleCat,neceTerms
  //if(idioma=='en'){
    exito='Your registration was sent successfully.'
    errortxt='An error occurred while sending the record.'
    selePais='Select a country.'
    seleCiu='Select a city.'
    seleCat='Select a category.'
    neceTerms='You need to accept the terms and conditions.'
    var msg=(leng=='es')?'Tu mensaje ha sido enviado exitosamente.':'Your message has been successfully sent.'
  /*}else{
    exito='Tu registro fue enviado con exito.'
    errortxt='Ocurrio un error al enviar el registro.'
    selePais='Seleccione un pais.'
    seleCiu='Seleccione una ciudad.'
    seleCat='Seleccione una categoria.'
    neceTerms='Necesita aceptar los terminos y condiciones.'
  }*/
  $('#btnEnvFoFoot').val('Sending...').removeAttr('onclick')
  $.ajax({
    url:'/servicios/mail.php',type:'post',
    data:'env=1&'+$('#contactarF').serialize(),
    success:function(d){
      if(d=='1'){
        $('#contactarF').html('<div class="emailExito">'+msg+'</div>')
      }else{
        $('#contactarF').html('<div class="emailExito">'+errortxt+'</div>')
      }
    },error:function(d){
      $('#contactarF').html('<div class="emailExito">Error: '+d.responseText+'</div>')
    }
  })
  return false
}
function cargaFoto() {
  $("#imgact").val($("#fotoPerfil").attr("src").replace("img/blank-profile-picture.png",""));
  var form = $('#uploadFoto')[0];
  var data = new FormData(form);
  $.ajax({
    url: "/servicios/upldr.php",
    type: "POST",
    data: data,
    contentType: false,
    cache: false,
    processData:false,
    enctype: 'multipart/form-data',
    success: function(resp)
    {
      resp=JSON.parse(resp);
      if (resp.codigo){
        $("#fotoPerfil").attr("src", resp.mensaje);
        msg.text(_lg.t_fotoact+' <br /><a class="botLink btnAviso" onclick="msg.close()">'+_lg.t_acep+'</a>').load();
      }
    }
  });
}
function editarRS(idRed) {
  $("#frmEdRS").remove();
  var cont = $("#soc"+idRed).text();
  $("#soc"+idRed).html('<div id="frmEdRS"><input type="text" id="txtRS" class="form-control" value="'+cont+'" /><a href="#soc'+idRed+'" onclick="$(\'#soc'+idRed+'\').text($(\'#txtRS\').val());guardarPerfil()" class="btn btn-primary btn-sm" >'+_len.js_guarr+'</a></div>');
}
function registro(){
  var demas='<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>'

  if($('#nombre').val()==''){
    msg.text(_len.js_ingname+demas).load()
    return false
  }
  if($('#apellido').val()==''){
    msg.text(_len.js_ingape+demas).load()
    return false
  }
  if($('#dni').val()==''){
    msg.text(_len.js_inddoc+demas).load()
    return false
  }
  if($('#email1').val()==''){
    msg.text(_len.js_indmaill+demas).load()
    return false
  }
  if($('#email1').val()!=$('#email2').val()){
    msg.text(_len.mailnomatch+demas).load()
    return false
  }
  if($('#pass1').val()==''){
    msg.text(_len.jsenterpass+demas).load()
    return false
  }
  if($('#pass1').val()!=$('#pass2').val()){
    msg.text(_len.js_passnocoin+demas).load()
    return false
  }
  var pais=$('#pais option:selected').text()
  var estado=$('#estado option:selected').text()
  if(pais==''){
    msg.text(_len.js_selpais+demas).load()
    return false
  }
  if(estado==''){
    msg.text(_len.js_selestad+demas).load()
    return false
  }
  if($('#ciudad').val()==''){
    msg.text(_len.js_selecity+demas).load()
    return false
  }
  if($('#telefono').val()==''){
    msg.text(_len.js_ingsteleff+demas).load()
    return false
  }
  if($('#categoria').val()=='1'){
    msg.text(_len.js_seleccat+demas).load()
    return false
  }
  if (!$( "#terminos" ).prop('checked')) {
    msg.text(txtreg.register_selterms+demas).load()
    return false
  }
  _primerosDatos=$('#preRegistrer').serialize()+'&pais='+pais+'&estado='+estado
  $.ajax({
    url: "/servicios/registro.php",type:'post',dataType:'json',
    data:'categoria='+$("#categoria").val()+'&registro=1&email='+$('#email1').val()+'&'+_primerosDatos,
    success:function(d){
      if(d.estado==1){
        d.html=d.html.replace('register_model', txtreg.register_model)
        .replace('register_nick', txtreg.register_nick)
        .replace('register_pages', txtreg.register_pages)
        .replace('register_studio', txtreg.register_studio)
        .replace('register_company_name', txtreg.register_company_name)
        .replace('register_cmodels', txtreg.register_cmodels)
        .replace('register_webmaster', txtreg.register_webmaster)
        .replace('register_website', txtreg.register_website)
        .replace('register_sitepromoting', txtreg.register_sitepromoting)
        .replace('register_other', txtreg.register_other)
        .replace('register_segment', txtreg.register_segment)
        .replace('register_submit', txtreg.register_submit);
        $("#frmReg").html(d.html)
        if($("#categoria").val()!=5){
          cargarSitios()
        } else {
          cargarActividades()
        }
      }else{
        msg.text(_len.emailyaregist+demas).load()
      }
    },
    error:function(d){
      msg.text(d.responseText+demas).load()
    }
  })
}
function regfinal(){
  var sitios='';
  var demas='<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>';
  $("#sitios > input").each(function () {
    if($(this).attr("checked")) sitios+=$(this).val()+","
  })
  $.ajax({
    url:"servicios/registro.php",type:'post',dataType:'json',
    data:'registro=2&sitios='+sitios+'&'+_primerosDatos+'&'+$('#preRegistrer').serialize(),
    success:function(d){
      if (d.estado==1){
        d.html=d.html.replace('register_complete', txtreg.register_complete)
        .replace('register_complete_msg', txtreg.register_complete_msg);
        $("#frmReg").html(d.html)
      }else{
        msg.text(d.msg+demas).load()
      }
    },error:function(d){
      msg.text(d.responseText+demas).load()
    }
  })
}
function cargarSitios() {
  let estado=false;
  $.ajax({
    url: "/servicios/sitios.php",
    type:'get',
    dataType: 'json',
    beforeSend: function() {
      $("#sitios").html('');
    },
    success: function(resp) {
      for(s=0; s<resp.sitio.length; s++) {
        $("#sitios").append('<label class="col-sm-10" for="pag'+s+'">'+resp.sitio[s]+'</label><input class="col-sm-2" type="checkbox" id="pag'+s+'" value="'+resp.sitio[s]+'"> ');
      }
      estado=true;
    }
  });
  return estado;
}
function cargarCiudades() {
  $.ajax({
    url: "/servicios/ciudades.json",
    type:'get',
    dataType: 'json',
    success: function(resp) {
      var opciones='<option></option>';
      for(d=0; d<resp.length; d++) {
        if (resp[d].departamento==$('#estado option:selected').text()) {
          for (c=0; c<resp[d].ciudades.length; c++) {
            opciones+='<option>'+resp[d].ciudades[c]+'</option>';
          }
        }
      }
      $("#ciudad").html(opciones);
    }
  });
}
function cargarActividades() {
  $.ajax({
    url: "/servicios/actividades.php",
    type:'get',
    dataType: 'json',
    beforeSend: function() {
      $("#actividad").html('<option></option>');
    },
    success: function(resp) {
      for(a=0; a<resp.actividad.length; a++) {
        $("#actividad").append('<option>'+resp.actividad[a]+'</option> ');
      }
    }
  });
}
function agregarSitio() {
  if ($("#nuevoSitio").val().trim()!="") {
    sitios++;
    $("#sitios").append('<label class="col-sm-10" for="pag'+sitios+'">'+$("#nuevoSitio").val()+'</label><input class="col-sm-2" type="checkbox" id="pag9" value="'+$("#nuevoSitio").val()+'" checked >');
    $("#nuevoSitio").val('');
  } else {
    $("#nuevoSitio").focus();
  }
}
function cargarPerfil() {
  let sitios, est;
  $.ajax({
    data: {"id":$("#usuario").val()},
    url: "/servicios/perfil.php",
    type:'get',
    dataType: 'json',
    beforeSend: function() {
    },
    success: function(u) {
      if(u.id_categoria!='5'){
        est=cargarSitios();
      }else{
        cargarActividades();
      }
      $("#nickname").val(u.nickname);
      $("#soc1").text(u.website);
      $("#soc2").text(u.face);
      $("#soc3").text(u.twitter);
      $("#soc4").text(u.instagram);
      $("#soc5").text(u.skype);
      $("#soc6").text(u.youtube);
      $("#nomEsc").val(u.nombre_escarapela);
      $("#nombEscaparela").html(u.nombre_escarapela);
      $("#studioEscaparela").html(u.estudio);
      $("#studio").val(u.estudio);
      $("#objetivo").val(u.objetivo);
      $("#descProfesional").val(u.descrip_profesional);
      if (u.estado_foto=="1") $(".fotoPend").hide();
      sitios = u.sitios_adicionales.split(",");
      let sites='';
      $.each(sitios, function(index, value){
        if (value!="") {
          if ($("[value='+value+']").length>0) {
            sites+='$("[value='+value+']").attr("checked", true);';
          } else {
            sitios++;
            sites+='$("#sitios").append(\'<label class="col-sm-10" for="pag'+sitios+'">'+value+'</label><input class="col-sm-2" type="checkbox" id="pag9" value="'+value+'" checked >\');';
          }
        }
      });
      setTimeout(sites, 800);
    }
  });
}
function guardarPerfil() {
  let data = new Object();
  data.actualizar=1;
  data.id=$("#usuario").val();
  $("#frmInfoContacto > div > div").children().each(function () {
    if ($(this)[0].id!="") data[$(this)[0].id]=$(this).val();
    console.log($(this)[0].id+" => "+$(this).val());
  });
  data.foto=$("#fotoPerfil").attr("src").replace("img/blank-profile-picture.png","");
  data.nombreEscarapela=$("#nomEsc").val();
  data.studioEscarapela=$("#studio").val();
  data.objetivo=$("#objetivo").val();
  data.descripcionProfesional=$("#descProfesional").val();
  data.pais=$('#paises option:selected').text()
  data.pais_cod=$('#paises').val()
  data.estado=$('#estado option:selected').text()
  data.estado_cod=$('#estado').val()
  data.nickname=$("#nickname").val();
  data.sitios="";
  $("#sitios > input").each(function () {
    if ($(this).attr("checked")) data.sitios+=$(this).val()+",";
  });
  $("#redes > div > div > h5 > span").each(function () {
    if ($(this)[0].id!="") data[$(this)[0].id]=$(this).text();
    console.log($(this)[0].id+" => "+$(this).text());
  });
  console.log(JSON.stringify(data));
  $.ajax({
    url: "/servicios/perfil.php",
    data: data,
    type:'post',
    dataType: 'json',
    success: function(resp) {
      if(resp.estado=='1'){
        msg.text(_len.perf_updateexit+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load();
      }else{
        msg.text(resp.msg+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load();
      }
    }
  });
}
function pagosPendientes() {
  let data = {"usuario":$("#usuario").val()};
  $.ajax({
    url: "/servicios/pagos.php",
    data: data,
    type:'post',
    dataType: 'json',
    success: function(d) {
      $("#tbPagos").html('');
      var t='',i,c=1
      _obP=[]
      for(i in d){
        t+='<tr>'
        t+='<td>'+c+'</td>'
        t+='<td>'+d[i].nombre+'</td>'
        t+='<td>$ '+d[i].valor+' '+d[i].moneda+'</td>'
        if(d[i].estado!='1'){
          var add=''
          if(typeof d[i].hash=='undefined' || d[i].hash==null || d[i].hash==''){
            add=' href="#" data-toggle="modal" onclick="pagarPendiente('+i+')"'
          }else{
            add=' href="/servicios/form_payu.php?hash='+d[i].hash+'"'
          }
          t+='<td><a class="btn btn-primary btn-sm btn-block text-warning" '+add+'> '+_lgSec.tt_pagar+' </a> </td>';
        }else{
          t+='<td><a class="btn btn-default btn-sm btn-block" href="javascript:void(0);" > '+_lgSec.tt_pagado+' </a> </td>';
        }
        t+='</tr>'
        c++
        var ref=d[i].id
        if(d[i].producto==2){
          ref=d[i].id+'_'+d[i].cant_tickets
        }
        if(d[i].producto==3){
          ref=d[i].id+'_'+d[i].cant_tickets+'_'+d[i].opcional
        }
        _obP[i]={
          emailComprador:d[i].usu_email,
          nombreComprador:d[i].usu_nombre+' '+d[i].usu_apellido,
          telefonoComprador:d[i].usu_telef,
          valor:d[i].valor,
          descripcion:d[i].nombre,
          idProducto:d[i].producto,
          idUsuario:d[i].id_usuario,
          datoReferencia:ref,
          moneda:d[i].moneda
        }
      }
      $("#tbPagos").html(t);
    }
  });
}

function misPedidos() {
  let data = {"usuario":$("#usuario").val()};
  $.ajax({
    url: "/servicios/pedidos.php",
    data: data,
    type:'post',
    dataType: 'json',
    success: function(d) {
      $("#tbPagos").html('');
      var t='',i,c=1
      _obP=[]
      for(i in d){
        t+='<tr>'
        t+='<td>'+d[i].id+'</td>'
        t+='<td>'+d[i].fecha+'</td>'
        t+='<td>$ '+d[i].valor+'</td><td>'
        if(d[i].estado!='1'){
          var add=''
          if(typeof d[i].hash=='undefined' || d[i].hash==null || d[i].hash==''){
            add=' href="#" data-toggle="modal" onclick="pagarPendiente('+i+')"'
          }else{
            add=' href="/servicios/form_payu.php?hash='+d[i].hash+'"'
          }
          t+='<td><a class="btn btn-primary btn-sm btn-block text-warning" '+add+'> '+_lgSec.tt_pagarp+' </a> ';
        }else{
          t+='<td><a class="btn btn-default btn-sm btn-block" href="javascript:void(0);" > '+_lgSec.tt_ppagado+' </a> ';
        }
        t+='<a class="btn btn-primary btn-sm btn-block text-warning" onclick="document.location=\'../order.php?ped='+d[i].id+'\'"> '+_lgSec.tb_verpedido+' </a></td></tr>'
        c++
        var ref=d[i].id
        _obP[i]={
          emailComprador:d[i].usu_email,
          nombreComprador:d[i].usu_nombre+' '+d[i].usu_apellido,
          telefonoComprador:d[i].usu_telef,
          valor:d[i].valor,
          descripcion:d[i].nombre,
          idProducto:d[i].producto,
          idUsuario:d[i].id_usuario,
          datoReferencia:ref,
          moneda:d[i].moneda
        }
      }
      $("#tbPagos").html(t);
    }
  });
}

function listadoReservas() {
  let data = {"usuario":_user};
  $.ajax({
    url: "/servicios/reservas.php",
    data: data,
    type:'post',
    dataType: 'json',
    success: function(d) {
      $("#tbPagos").html('');
      var t='',i,c=1
      _obP=[]
      for(i in d){
        t+='<tr>'
        t+='<td>'+c+'</td>'
        t+='<td>'+d[i].categoria_es+'</td>'
        t+='<td>'+d[i].habitacion_es+'</td>'
        t+='<td>'+d[i].entrada+'</td>'
        t+='<td>'+d[i].salida+'</td>'
        t+='<td> $'+d[i].totalPagar+' USD</td>'
        if(d[i].estado!='1'){
          t+='<td><a class="btn btn-primary btn-sm btn-block text-warning" href="#" data-toggle="modal"  onclick="pagarPendiente('+i+')"> '+_len.js_pagarr+' </a> '
          t+='<a class="btn btn-primary btn-sm btn-block text-warning" href="#" data-toggle="modal"  onclick="eliRserv('+d[i].id+')" "> '+_len.js_elimm+' </a> </td>';
        }else{
          t+='<td><a class="btn btn-default btn-sm btn-block" href="javascript:;" > '+_len.js_pagado+' </a> </td>';
        }
        t+='</tr>'
        c++
        var ref=d[i].id
        _obP[i]={
          emailComprador:d[i].contac_email,
          nombreComprador:d[i].contac_nombre,
          telefonoComprador:d[i].contac_telef,
          valor:d[i].totalPagar,
          descripcion:_len.reserhab,
          idProducto:'1',
          idUsuario:_user,
          datoReferencia:ref,
          moneda:'USD'
        }
      }
      $("#tbPagos").html(t);
    }
  });
}
function eliRserv(id){
  msg.text(_len.deseaelimped).load().confirm(function(){
    msg.text(_len.elimiped).load()
    $.ajax({
      url:'/servicios/eliminaReserva.php',
      data:'elim='+id,type:'post',
      success:function(d){
        if(d==1){
          msg.text(_len.pedlimexit+'<br /><a class="botLink btnAviso" onclick="window.location.href=\'misReservas.php\'">'+_len.t_acep+'</a>').load()
         }else{
          msg.text(d+'<br><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load()
         }
      }
    })
  })
}
function pagarPendiente(id){
  msg.text(_len.tt_pagocon+' <img src="img/banner_payu.png" style="cursor: pointer;" width="60%" onclick="goPayu(_obP['+id+'])"><br><a class="botLink btnAviso" onclick="msg.close()">'+_len.btn_cancel+'</a>').load()
}
function comprarTicket (paso) {
  $("#comprar1").hide();
  $("#comprar2").hide();
  $("#comprar3").hide();
  switch (paso) {
    case 1:
      $("#comprar1").animate({height:'show'},350);
      break;
    case 2:
      $("#comprar2").animate({height:'show'},350);
      break;
    case 3:
      $("#comprar3").animate({height:'show'},350);
      break;
  }
}
function totalTiquetes(cant){
  if(cant<1) {
    cant=1;
    $("#cantdad").val(cant);
  }
  let descto=0;
  let total=(parseInt(cant)*_v).toFixed(2);
  if (_m==='COP') {
    if (cant>=5) descto=55;
    if (cant>=10) descto=60;
    if (cant>=15) descto=65;
  }
  mtot=total-(total*descto/100);
  console.log("monto:"+total+" descto:"+descto+" total:"+mtot);
  if (descto>0) {
    $("#txTotal").html('<span style="display:block;font-size:80%;text-decoration:line-through;">'+_m+' $'+total+'</span><span style="display:block;font-size:80%;">- '+descto+'%</span><span style="display:block;">'+_m+' $'+mtot.toFixed(2)+'</span>');
  } else {
    $("#txTotal").html(_m+' $'+total);
  }
  $("#descto").val(descto);
  $("#total").val(mtot);
}
function confirmaCodigo () {
  $.ajax({
    data: {"codigo":$("#codigo").val()},
    url: "/servicios/codigo.php",
    type:'post',
    dataType: 'json',
    beforeSend: function() {
    },
    success: function(response) {
    }
  });
}
function pagar() {
  let dialogo=_len.tt_pagocon+' <img src="img/banner_payu.png" style="cursor: pointer;" width="60%" onclick="msg.close();procesarPago(4)"><br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.btn_cancel+'</a>';
  msg.text(dialogo).load();
}
function procesarPago(form) {
  goPayu({
    emailComprador:'',
    nombreComprador:'',
    telefonoComprador:'',
    valor:'',
    descripcion:'',
    idProducto:'',
    idUsuario:'',
    datoReferencia:''
  })
  let dialogo='Procesando su pago<br><i class="fa fa-spinner fa-spin" style="font-size:36px"></i>';
  msg.text(dialogo).load();
  setTimeout('$("#avisoTxt").html("Pago procesado exitosamente!");', 3000);
  setTimeout('msg.close()', 4500);
  setTimeout('closeForm()', 5000);
  if (form=='1') setTimeout('window.location="ticket.php";', 5500);
}
function verPlan(plan) {
  if ($("#sponsorPack"+plan).is(":visible")) {
    $( "#sp"+plan).children()[0].children[2].innerHTML='<img src="img/fledo01.jpg">';
    $("#sponsorPack"+plan).css("display", "none");
  } else {
    $( "#sp"+plan).children()[0].children[2].innerHTML='<img src="img/fledo02.jpg">';
    $("#sponsorPack"+plan).css({"display":"table", "min-height":"350px"});
  }
}
function regisNews(id){
  if($('#'+id).val()==''){
    msg.text(_len.porfaemail+'<br /><a class="botLink btnAviso" onclick="msg.close();$(\'#'+id+'\').focus()">'+_len.t_acep+'</a>').load()
    return false
  }
  var rg=/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
  if(!rg.test($('#'+id).val())){
    msg.text(_len.mailvalido+'<br /><a class="botLink btnAviso" onclick="msg.close();$(\'#'+id+'\').focus()">'+_len.t_acep+'</a>').load()
    return false
  }
  msg.text(_len.regismailll).load()
  $.ajax({
    url:'/servicios/registro_newsletter.php',type:'post',data:'regis=1&email='+$('#'+id).val(),
    success:function(d){
      if(d==1){
        msg.text(_len.mailregist+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load()
      }else{
        msg.text(d+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load()
      }
    },error:function(d){
      msg.text(d.responseText+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load()
    }
  })
}
function expositores() {
  $.ajax({
    url: "/servicios/expositores.php",
    type:'get',
    dataType: 'json',
    beforeSend: function() {
      $("#lstExpositores").html('');
    },
    success: function(response) {
       for (p=0; p<response.length; p++) {
          $("#lstExpositores").append('<div class="ficha"><img class="d-block rounded-circle img-fluid px-0 mx-3 mx-auto" width="128" src="'+response[p].foto+'"  />  <span class="nombexp">'+response[p].nombre+'</span> <span class="compexp">'+response[p].company+'</span><span class="descexp">'+response[p].descrip_profesional.substr(0, 90)+'</span><a href="#" class="boton" >'+_len.js_leermas+'</a></div>');
       }
    }
  });
}
function asistentes() {
  $.ajax({
    data: {"filtro":$("#buscar").val()},
    url: "/servicios/asistentes.php",
    type:'get',
    dataType: 'json',
    beforeSend: function() {
      $("#tblAsistentes").html('');
    },
    success: function(response) {
      let contacto='';
       for (p=0; p<response.length; p++) {
          if (response[p].email!=null && response[p].email!="") contacto+='<a href="mailto:'+response[p].email+'" target="_blank"><i class="fa fa-fw pull-left fa-envelope"></i></a>';
          if (response[p].face!=null && response[p].face!="") contacto+='<a href="https://www.facebook.com/'+response[p].face+'" target="_blank"><i class="fa fa-fw pull-left fa-facebook-square"></i></a>';
          if (response[p].twitter!=null && response[p].twitter!="") contacto+='<a href="https://www.twitter.com/'+response[p].twitter+'" target="_blank"><i class="fa fa-fw pull-left fa-twitter-square"></i></a>';
          if (response[p].instagram!=null && response[p].instagram!="") contacto+='<a href="https://www.instagram.com/'+response[p].instagram+'" target="_blank"><i class="fa fa-fw pull-left fa-instagram"></i></a>';
          if (response[p].skype!=null && response[p].skype!="") contacto+='<a href="https://www.skype.com/es/'+response[p].skype+'" target="_blank"><i class="fa fa-fw pull-left fa-skype"></i></a>';
          if (response[p].youtube!=null && response[p].youtube!="") contacto+='<a href="https://www.youtube.com/channel/'+response[p].youtube+'" target="_blank"><i class="fa fa-fw pull-left fa-youtube-play"></i></a>';
          $("#tblAsistentes").append('<tr><td class="txt"><img class="d-block rounded-circle img-fluid px-0 mx-3 mx-auto" width="60" src="'+response[p].foto+'"  /> </td><td class="txt">&nbsp;&nbsp;'+response[p].nombre+' '+response[p].apellido+'</td><td class="txt">'+response[p].company+'</td><td class="txt">'+response[p].objetivo+'</td><td class="txt">'+contacto+'</td><td class="txt">'+response[p].categoria+'</td></tr>');
          //<td class="txt">'+response[p].descrip_profesional+'</td
          contacto='';
       }
    }
  });
}
function social(){
  $.ajax({
    url: "/servicios/social.php",
    type:'get',
    dataType: 'json',
    beforeSend: function() {
    },
    success: function(response) {
      console.log(response);
    }
  });
}
function verInfo(preg) {
  if ($("#sponsorPack"+preg).is(":visible")) {
    $( "#sp"+preg).children()[0].children[2].innerHTML='<img src="img/fledo01.jpg">';
    $("#sponsorPack"+preg).css("display", "none");
  } else {
    $( "#sp"+preg).children()[0].children[2].innerHTML='<img src="img/fledo02.jpg">';
    $("#sponsorPack"+preg).css({"display":"table"});
  }
}
function passRecover() {
  let email=$("#pcemail").val();
  let idioma=$("#lgidoma").val();
  if($('#pcemail').val()==''){
    msg.text(_len.ing_email+' <br /><a class="botLink btnAviso" onclick="msg.close();$(\'#pcemail\').focus()">'+_len.t_acep+'</a>').load();
    return false;
  }
  $.ajax({
    data: {"email":email, "idioma":idioma},
    url: "/servicios/passwordRecover.php",
    type:'post',
    dataType: 'json',
    beforeSend: function() {
      cont=$("#frmForgPass").html();
      $("#frmForgPass").html('<center><i class="fa fa-spinner fa-spin" style="font-size:66px;"></i></center>');
    },
    success: function(response) {
      if (response.estado==1) {
        $("#frmForgPass").html('<div id="log_leyenda" class="log_descrip">'+_len.envrestpass+'</div>'+
          '<div id="btnrecpas" class="login_botones"><a href="javascript:;" class="botLink" onclick="closeForm();finPassRecoverProcess()">'+_len.btn_fina+'</a></div>'
          );
      } else {
        $("#frmForgPass").html(cont);
        $("#pcemail").val(email);
        $("#log_leyenda").html(_len.correonocoin);
        $("#pcemail").select();
      }
    }
  });
}
function finPassRecoverProcess() {
  $("#frmForgPass").html(cont);
}
function compraPatrocinio(paquete) {
  $("#paqPat").val(paquete);
  showForm('solicitarPaquete');
}
function solicitudPatrocinador(){
    closeForm();
    msg.close();
    console.log("logo:"+$("#logoPat").val()+" paq:"+$("#paqPat").val()+" url:"+$("#url_patro").val());
    $.ajax({
      data:'paquete='+$("#paqPat").val()+'&logo='+$("#logoPat").val()+'&url='+$('#url_patro').val(),
      url: "/servicios/solPat.php",
      type:"post",
      dataType:"json",
      beforeSend: function() {
        msg.text(lg.t_proccess+'<br /><center><i class="fa fa-spinner fa-spin" style="font-size:66px;"></i></center>').load();
      },
      success: function(d) {
        if(d.estado=='1'){
          $("#avisoTxt").html(lg.solcompenvext+'<br /><a class="botLink btnAviso" onclick="msg.close();">'+lg.t_accp+'</a>');
        }else{
          $("#avisoTxt").html(d.msg+'<br /><a class="botLink btnAviso" onclick="msg.close();">'+lg.t_accp+'</a>');
        }
      }
  });
}
/*
goPayu({
  emailComprador:'',
  nombreComprador:'',
  telefonoComprador:'',
  valor:'',
  descripcion:'',
  idProducto:'',
  idUsuario:'',
  datoReferencia:'',
  moneda:''
})
*/
function goPayu(ob){
  $('#payu_moneda').val(ob.moneda)
  $('#payu_emailComprador').val(ob.emailComprador)
  $('#payu_nombreComprador').val(ob.nombreComprador)
  $('#payu_telefonoComprador').val(ob.telefonoComprador)
  $('#payu_valor').val(ob.valor)
  $('#payu_descripcion').val(ob.descripcion)
  $('#payu_idProducto').val(ob.idProducto)
  $('#payu_idUsuario').val(ob.idUsuario)
  $('#payu_datoReferencia').val(ob.datoReferencia)
  $('#frm_payu').submit()
}
function cargaLogo() {
  var form = $('#solicitarPaquete')[0];
  var data = new FormData(form);
  $.ajax({
    url: "/servicios/upldr2.php",
    type: "POST",
    data: data,
    contentType: false,
    cache: false,
    processData:false,
    enctype: 'multipart/form-data',
    success: function(resp)
    {
      resp=JSON.parse(resp);
      if (resp.codigo==1){
        $("#logoPat").val(resp.mensaje);
         msg.text(lg.t_logoexito+'<br/>'+lg.t_hagaclick+'<br /><a class="botLink btnAviso" onclick="solicitudPatrocinador()">'+lg.t_conti+'</a>').load();
      } else {
        msg.text(resp.mensaje+'<br /><a class="botLink btnAviso" onclick="msg.close();">'+lg.t_accp+'</a>').load();
      }
    }
  });
}
function validarCodigoDescuento(){
  if($('.login-content #codigo').val()==''){
    msg.text(_lg.j_ingcod+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_lg.t_acepp+'</a>').load();
    return false
  }
  $.ajax({
    url:'/servicios/validaDescuento.php',type:'post',dataType:'json',
    data:'codigo='+$('.login-content #codigo').val(),
    success:function(d){
      if(d.extiste==0){
        msg.text(_lg.t_codnoval+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_lg.t_acepp+'</a>').load();
         return false
      }
      if(d.fueRedimido==1){
         msg.text(_lg.t_codredant+' <br /><a class="botLink btnAviso" onclick="msg.close()">'+_lg.t_acepp+'</a>').load();
         return false
      }
      if(d.restante>0){
        msg.text(_lg.t_descup01+' '+d.porcentaje+'% '+_lg.t_descup02+' <br /><a class="botLink btnAviso" onclick="msg.close();closeForm();window.location=\'payments.php\'">'+_lg.t_acepp+'</a>').load();
         return false
      }else{
        msg.text(_lg.t_tickcreado+' <br /><a class="botLink btnAviso" onclick="msg.close();closeForm();window.location=\'dashboard.php\'">'+_lg.t_acepp+'</a>').load();
         return false
      }
    }
  })
}
function getIdPreGoPayu(ob){
  msg.text(_lg.t_generando_cop_tik).load();
  $.ajax({
    url:'/servicios/registraCantTickets.php',type:'post',dataType:'json',
    data:'cantidad='+$('#comprar2 #cantidad').val()+'&total='+$('#total').val()+'&descto='+$('#descto').val(),
    success:function(d){
      if(d.estado==1){
        ob.datoReferencia=d.id+'_'+$('#comprar2 #cantidad').val()
        __obb=ob
        msg.text(_lg.t_geneok_dattick+'.<br /><a class="botLink btnAviso" onclick="goPayu(__obb)">'+_lg.t_cont+'</a>').load();
      }else{
        msg.text(d.msg+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_lg.t_acep+'</a>').load();
      }
    }
  })
}
function ticketsDisponibles(){
  $.ajax({
    url:'/servicios/getTickets.php',type:'post',dataType:'json',
    success:function(d){
      if(d.length==0){
        return false
      }
      var i,t='',c=1
      for(i in d){
        t+='<tr>'
        t+='<td>'+c+'</td>'
        t+='<td>'+d[i].codigo+'</td>'
        t+='<td>'+d[i].porcentaje+'%</td>'
       if (d[i].redimido==0) {
         t+='<td><a class="btn btn-primary btn-sm btn-block text-warning" href="#" data-toggle="modal" onclick="showEnviarMail(\''+d[i].codigo+'\')"> Enviar </a><a class="btn btn-primary btn-sm btn-block text-warning" href="#" data-toggle="modal" onclick="$(\'#codigo\').val(\''+d[i].codigo+'\');validarCodigoDescuento(\''+d[i].codigo+'\')"> Redimir </a></td>'
       } else {
         t+='<td></td>'
       }
         t+='</tr>'
        c++;
      }
      $('#tbTickets').html(t)
    }
  })
}
function showEnviarMail(cod){
  msg.text(_len.envcodhacia+'<br /><input type="text" style="width:80%" id="emailcod" class="form-control" /><br /><a class="botLink btnAviso" onclick="enviarMail(\''+cod+'\')">Enviar</a> <a class="botLink btnAviso" onclick="msg.close()">'+_len.btn_cancel+'</a>').load();
}
function enviarMail(cod){
  var emailcod=$('#emailcod').val()
  msg.text(_len.msg_envv).load()
  $.ajax({
    url:'/servicios/enviarMailCod.php',type:'post',dataType:'json',
    data:'enviar=1&email='+emailcod+'&cod='+cod,
    //+'&asunto='+encodeURIComponent('Ticket LalExpo')+'&mensaje='+encodeURIComponent('Se ha compartido el codigo: <br><b>'+cod+'</b><br />, por favor, ingreselo en lalexpo.com y adquiera su ticket para el evento'),
    success:function(d){
      if(d==1){
        msg.text(_len.codcread+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.btn_cerrar+'</a>').load();
      }else{
        msg.text(d+'<a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load();
      }
    }
    })
}
function changePassUser(){
  if($('#ant_pass').val()==''){
    msg.text(_len.js_ind_contr+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load();
    return false
  }
  if($('#nue_pass').val()==''){
    msg.text(_len.ind_nue_pass+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load();
    return false
  }
  if($('#rep_pass').val()==''){
    msg.text(_len.js_rep_pass+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load();
    return false
  }
  if($('#nue_pass').val()!=$('#rep_pass').val()){
    msg.text(_len.js_pass_nocoin+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load();
    return false
  }
  msg.text(_len.valid_dat).load()
  $.ajax({
    url:'/servicios/changePass.php',type:'post',
    data:'chgPass=1&nuePass='+encodeURIComponent($('#nue_pass').val())+'&antPass='+encodeURIComponent($('#ant_pass').val()),
    success:function(d){
      if(d==1){
        msg.text(_len.chg_pass+'<br /><a class="botLink btnAviso" onclick="window.location.href=\'dashboard.php\'">'+_len.t_acep+'</a>').load();
      }else{
        msg.text(d+'<a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load();
      }
    }
  })
}
function generarLinkPagoWorkshop(w){
  if($('#w_nomape').val()==''){
    msg.text('Ingrese el nombre y apellido<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load();
    return false
  }
  if($('#w_tel').val()==''){
    msg.text('Ingrese el telefono<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load();
    return false
  }
  if($('#w_doc').val()==''){
    msg.text('Ingrese el documento<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load();
    return false
  }
  if($('#w_celu').val()==''){
    msg.text('Ingrese el celular<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load();
    return false
  }
  msg.text('Generando cupon de pago...').load()
  $.ajax({
    url:'/servicios/solicitar_lugar.php',type:'post',
    data:'lg='+_lengua+'&w='+w+'&nomape='+$('#w_nomape').val()+'&tel='+$('#w_tel').val()+'&celu='+$('#w_celu').val()+'&docu='+$('#w_doc').val(),dataType:'json',
    success:function(d){
      if(d.estado=='1'){
        msg.text('<span id="avisoTxt">Se realizara el pago con  <img src="img/banner_payu.png" style="cursor: pointer;" width="60%" onclick="document.location=\''+d.link+'\'"><br><a class="botLink btnAviso" onclick="msg.close()">Cancelar</a></span>').load();
        closeForm();
        $("#solicitudDatosWork").remove();
        $(".section .botLink").text('Pagar');
        $(".section .botLink").prop('onclick', 'document.location=\'payments.php#menu\'');
      }else{
        msg.text(d.msg+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load();
      }
    },
    error:function(d){
      msg.text(d.responseText+'<br /><a class="botLink btnAviso" onclick="msg.close()">'+_len.t_acep+'</a>').load();
    }
  })
}
function compMemb(){
  $.ajax({
    url:'servicios/registraMembresia.php',type:'post',dataType:'json',
    data:'usuario='+$("#usrId").val(),
    success:function(d){
      if(d.estado==1){
        $("#memcomp1").hide();
        $("#memcomp2").show();
      }
    }
  })
}