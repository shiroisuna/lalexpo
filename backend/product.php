<?php
    session_start();
    require_once 'Controller.php';
    require_once 'controllers/ProductController.php';
    $productController = new ProductController();
    $user = isset($_SESSION['id'])?$_SESSION['id']:null;
    if(isset($_GET["id"])){
        Util::showJson($productController->find($_GET["id"]));
    }else{
        Util::showJson($productController->all());
    }

    
?>

