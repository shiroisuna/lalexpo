<?php
/**
 * Created by PhpStorm.
 * User: WiljacAular
 */
require_once 'Util.php';
require_once 'DateUtil.php';
require_once 'Connection.php';

class Controller {
    protected $conn;

    function __construct(){
        $this->conn= new ConnectionMysql();
    }

    public function getAll($table,$field=null){

            $rs = $this->conn->SelectAll($table,$field);
        return Util::toArrayObjectResult($rs);
    }

    public function getWhere($table,$condition){

        $rs=null;
        try{
            $rs = $this->conn->SelectWhere($table,$condition)->fetchAll();
            $rs = Util::toArrayObjectResult($rs);
        }catch (PDOException $e){
            error_log("(".DateUtil::getDate().") ERROR: " . $e->getMessage()."\n", 3, 'log/connection.log');
        }
        
        return $rs;
    }
    public function getWhereOne($table,$condition){
        $rs=null;
        try{
            $rs = Util::toObjectResult($this->conn->SelectWhere($table,$condition)->fetch());
        }catch (PDOException $e){
            error_log("(".DateUtil::getDate().") ERROR: " . $e->getMessage()."\n", 3, 'log/connection.log');
        }
        return $rs;
    }

    public function get($table,$id,$field=null){
        $rs = $this->conn->Select($table,$id,$field);
        return Util::toObjectResult($rs);
    }

    public function select($sql){
        return $this->conn->selectBySQL($sql);
    }
    public function selectOne($sql){
        return $this->conn->selectBySQLOne($sql);
    }

    public function Insert($set,$table){
        $u=array("");
        if(!is_array($set)){
            if(is_object($set)){
                $set = (array) $set;
                $u= $this->conn->Insert($table,$this->set($table,$set));
            }else{
                $u=array("update"=>0, "message"=>"Parametro array no recibido");
            }
        }else{
            $set = (array) $set;
            $u= $this->conn->Insert($table,$this->set($table,$set));
        }

        return $u;
    }

    public function Update($set,$table,$id){
        $u=array("");
        if(!is_array($set)){
            if(is_object($set)){
                $setArray = (array) $set;
            }else{
                return $u=array("update"=>0, "message"=>"Parametro array no recibido");
            }
        }else{
            $setArray=$set;
        }
        $u= $this->conn->Update($table,$this->set($table,$setArray),$id);
        return $u;
    }

    public function Delete($table,$id){
        $u= $this->conn->DeleteWhere($table,"id=$id");
        return $u;
    }

    public function lastInsert($table){
        $sql='select max(id) as id from '.$table;
        $st= $this->conn->selectBySQLOne($sql);
        return $this->get($table,$st[0]);
    }

    public function count($table,$condition=""){
        if($condition!=""&&strpos($condition,"where")==false)
            $condition=" where ".$condition;
        $rs=$this->selectOne("select count(*) count from ".$table.$condition);
        return $rs['count'];
    }

    public function set($table,$array){
        $types=array();
        foreach($array as $field=>$value){
            if($value!=null){
                $sql="show columns from ".$table." where Field='$field'";
                $st= $this->selectOne($sql);
                $type= explode("(",$st["Type"])[0];
                if($type=='varchar'||$type=='text'||$type=='date'||$type=='datetime'||$type=='timestamp'){
                    $value = "'".$value."'";
                }
                $types[$field]=$value;
            }
        }

        return $types;
    }
}

?> 