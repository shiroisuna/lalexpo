<?php
    session_start();
    require_once 'Controller.php';
    require_once 'controllers/CarritoController.php';
    require_once 'controllers/ProductController.php';

    $carritoController = new CarritoController();
    
    if(isset($_POST['crear'])){
       Util::showJson($carritoController->create($_POST,$_SESSION['id']));
       //header('Location: ../../store.php');
    }else if(isset($_GET['quitar'])){
        Util::showJson($carritoController->delete($_GET['id_carrito']));
    }else{
        $cars = array();
        if(isset($_SESSION['id'])){
            Util::showJson($carritoController->getCart($_SESSION['id']));
        }else{
            Util::showJson();
        }
    }
    

