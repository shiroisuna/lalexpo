<?php

class CarritoController{
    const TABLE = "carrito";
    private $controller;
    public function __construct(){
        $this->controller = new Controller();
    }

    public function create($req,$userId){
        $detalle = $this->controller->getWhereOne('det_attrib_prod',"color='".$req['color']."' and talla='".$req['talla']."'");
        $data= array(
            'id_product'=>$req['codprod'],
            'id_detalle'=>$detalle->id_detalle,
            'id_usuario'=>$userId,
            'cantidad'=>$req['cantidad'],
            'precio'=>$req['valor'],
            'iva'=>'0'
        );
        return $this->controller->Insert($data, self::TABLE);
    }

    public function getCart($user){
        $cars =  array();
        $data = $this->controller->getWhere(self::TABLE, 'id_usuario='.$user);
        $subtotal=0;
        $productoController = new ProductController();
        foreach($data as $car){
            $car->producto = $productoController->find($car->id_product);
            $subtotal += floatval($car->precio);
            array_push($cars, $car);
        }
        $iva = $subtotal * 0.19;
        return array('data'=>$data, 'subtotal'=>$subtotal, 'iva'=>$iva, 'total'=>$iva + $subtotal);
    }
    
}
