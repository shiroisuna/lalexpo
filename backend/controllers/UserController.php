<?php

class UserController{
    const TABLE = 'usuarios';
    private $controller;

    public function __construct(){
        $this->controller = new Controller();
    }

    public function getByToken($token){
        return $this->controller->getWhereOne(self::TABLE, "token='$token'");
    }
    
    public function find($id){
        return $this->controller->get(self::TABLE, $id);
    }
}