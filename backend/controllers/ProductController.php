<?php

class ProductController{
    const TABLE = "productos";
    private $controller;
    public function __construct(){
        $this->controller = new Controller();
    }
    public function all() {
        $result = array();
        foreach($this->controller->getAll(self::TABLE ) as $row){
            array_push($result, $this->completeProduct($row));
        }
        return $result;
    }
    public function find($id) {
        return $this->completeProduct($this->controller->get(self::TABLE, $id));
    }

    private function completeProduct($product){
        $product->detalle = $this->controller->getWhere("det_attrib_prod",'id_producto ='.$product->id);
        
        
        return $product;
    }
    
}
