<?php
include('includes/conexion.php');
include('includes/idioma.php');
$lg=new idioma($_GET['lg']);
$lg->seccion(10);
include('includes/header.php');
if($_SESSION['login']!='1'){
  header('Location: index.php');
  exit();
}
$rs=$con->query("SELECT * FROM ticket WHERE id_usuario='".$_SESSION['id']."' AND tipo=1  AND estado=1 LIMIT 1");
if($rs->num_rows>0){
  $btnCompra='';
  $btnTicket='<a class="btn btn-primary btn-block my-2 btn-sm text-center" href="ticket.php#vtick" >
  <i class="fa fa-fw fa-ticket"></i>Ticket Lalexpo</a>';
}else{
	if ($_SESSION['membresia']==0) { 
    	$btnCompra='<a class="btn btn-primary btn-sm btn-block text-warning" href="#comprarTicket" onclick="showForm(\'popupMembresia\');" data-toggle="modal"> <i class="fa fa-fw fa-cart-plus fa-lg"></i>'.$lg->general->ad_escara.' </a>';
   		$btnTicket='<a class="btn btn-primary btn-sm btn-block text-center" href="#comprarTicket" onclick="showForm(\'popupMembresia\');"><i class="fa fa-fw fa-cart-plus"></i>'.$lg->general->comprar_ticket.'</a>';
	}else{
		  $btnCompra='<a class="btn btn-primary btn-sm btn-block text-warning" href="#" onclick="showForm(\'comprarTicket\');" data-toggle="modal"> <i class="fa fa-fw fa-cart-plus fa-lg"></i>Adquirir escarapela </a>';
		  $btnTicket='<a class="btn btn-primary btn-block my-2 btn-sm text-center" href="#" onclick="showForm(\'comprarTicket\');">
		  <i class="fa fa-fw fa-cart-plus"></i>Comprar Ticket</a>';
	}	

}
?>
<script>
_user="<?php echo $_SESSION['id']?>";
</script>
<link rel="stylesheet" href="css/theme.css" />
<script>
_lg=<?=json_encode($lg->general)?>;
_lgSec=<?=json_encode($lg->seccion)?>;
</script>
<style>
.content{
  font-size:1rem;
}
</style>
<div class="content" style="padding: 25px;">
  <div class="py-6 p-0">
    <div class="container">
      <div class="row">
        <div class="col-lg-2">
          <img id="fotoPerfil" class="d-block rounded-circle img-fluid px-0 mx-3 mx-auto" src="/thumb_600_w/<?php echo $_SESSION['foto']; ?>" width="85%">
          <div class="row"></div>
          <div class="row">
            <div class="col-md-12 my-3">
              <h1 id="nombUsuario" class="text-center"><?php echo $_SESSION['nombre'] . ' ' . $_SESSION['apellidos']; ?></h1>
            </div>
          </div>
          <div class="row"></div>
          <div class="row">
            <div class="col-md-12"></div>
          </div>
          <?=$btnTicket?>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="dashboard.php">
            <i class="fa fa-fw fa-user"></i><?=$lg->general->t_perfil?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="banners.php#menu"> Banner</a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="myorders.php#menu"> <?=$lg->general->t_mispedidos?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="payments.php#menu"> <?=$lg->general->t_pays?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="misTickets.php#menu"> <?=$lg->general->t_mistickets?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center" href="cambiarPass.php#menu"> <?=$lg->general->t_chgpass?></a>
          <a class="btn btn-primary btn-block my-2 btn-sm text-center btnact" href="misReservas.php#menu"> <?=$lg->general->t_reservas?></a>
        </div>
        <div style="width:79%">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12">
                <h1 contenteditable="true" class="my-3"><?=$lg->general->t_reservas?></h1>
              </div>
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th><?=$lg->general->tt_cate?></th>
                    <th><?=$lg->general->t_habb?></th>
                    <th>Check-out</th>
                    <th>Check-in</th>
                    <th>Total</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="tbPagos">
                  <tr>
                    <td colspan="5" align="center"> <?=$lg->general->t_noreserv?>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="row"> </div>
        </div>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="usuario" value="<?php echo $_SESSION['usuario']; ?>" />
<script type="text/javascript">$(document).ready(function(){ listadoReservas(); });</script>
<?php include('includes/footer.php')?>
<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>