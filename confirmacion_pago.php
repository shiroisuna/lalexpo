<?
session_start();
include('includes/conexion.php');
include('includes/funciones.php');
$todo=json_encode($_POST);
$sql="
insert into pagos set
state_pol='".$_POST['state_pol']."',
test='".$_POST['test']."',
transaction_date='".$_POST['transaction_date']."',
billing_country='".$_POST['billing_country']."',
description='".$_POST['description']."',
value='".$_POST['value']."',
response_message_pol='".$_POST['response_message_pol']."',
currency='".$_POST['currency']."',
transaction_id='".$_POST['transaction_id']."',
payment_method_name='".$_POST['payment_method_name']."',
payment_method_id='".$_POST['payment_method_id']."',
billing_city='".$_POST['billing_city']."',
reference_sale='".$_POST['reference_sale']."',
todo='".$todo."'
";
$con->query($sql);
if(!empty($con->error)){
 $con->query("
    insert into pagos_error set
    query='".addslashes($sql)."'");
    exit;
}
$partes=explode('|',$_POST['reference_sale']);
/**Datos a implementar**/
$id_pago=$con->insert_id;
$id_usuario=$partes[1];
$id_producto=$partes[2];
$datoAdicional=$partes[4];
/*
producto 1:Hoteles
producto 2:Tickets
producto 3:Paquetes
producto 4:Workshop
producto 5:Membresía
*/
switch($id_producto){
  case 1:
    $estado=0;
    $id_reserva=$datoAdicional;
    if($_POST['state_pol']=='4') $estado=1;
    if($_POST['state_pol']=='6') $estado=2;
    if($_POST['state_pol']=='5') $estado=3;
    $con->query("update hotel_reservas set estado='$estado', id_pago='$id_pago' where id='$id_reserva'");
    $rw=$con->query("select id_categoria,contac_email from hotel_reservas where id='$id_reserva'")->fetch_object();
    $id_categoria=$rw->id_categoria;
    $email=$rw->contac_email;
    $con->query("update hotel_categorias_hab set disponibles=disponibles-1, vendidos=vendidos+1 where id='$id_categoria'");
    $rw2=$con->query("select lengua,nombre,apellido from usuarios where id='".$id_usuario."'")->fetch_object();
    $text_compra=$con->query("select * from textos_email where id='reserva_habitacion'")->fetch_object();
    if($rw2->lengua=='Español'){
      $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->titulo_es);
      $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->texto_es);
    }else{
      $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->titulo_en);
      $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->texto_en);
    }
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"https://lalexpo.com/test/mail2.php");
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$email."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje)));
      #curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$email."&asunto=".urlencode('Confirmacion de pago')."&mensaje=".urlencode('Muchas gracias por su reserva de habitaciones, las mismas fueron reservadas para usted.'));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      echo curl_exec ($ch);
      curl_close ($ch);
  break;
  case 2:

    $partes=explode('_',$datoAdicional);
    $id_usu_pagos=$partes[0];
    $cant_tickets=$partes[1];

    $estadoAct=$con->query("select estado from usuarios_pagos where id='$id_usu_pagos'")->fetch_object()->estado;
    if($estadoAct=='1'){
      exit;
    }
    if($_POST['state_pol']!='4'){
      $con->query("UPDATE usuarios_pagos SET estado=0, id_pago='$id_pago' WHERE id='$id_usu_pagos'");
      exit;
    }
    crearTicket('', $id_usuario);
    gestionaTickets();
  break;
  case 3:
    $estadoAct=$con->query("select estado from usuarios_pagos where id='$id_usu_pagos'")->fetch_object()->estado;
    if($estadoAct=='1'){
      exit;
    }
    if($_POST['state_pol']!='4'){
      exit;
    }
    $partes=explode('_',$datoAdicional);
    $id_usu_pagos=$partes[0];
    $cant_tickets=$partes[1];
    $id_paquete=$partes[2];
    $id_solicitud=$partes[3];
    $con->query("update patrocinador_logo SET estado=1 where id_solicitud='".$id_solicitud."'");
    $con->query("update patrocinador_paquete set cantidad=cantidad-1 where id='".$id_paquete."'");

    gestionaTickets();
  break;
  case 4:
    $partes=explode('_',$datoAdicional);
    $id_usu_pagos=$partes[0];
    $id_solicitud=$partes[1];
    $estadoAct=$con->query("select estado from usuarios_pagos where id='$id_usu_pagos'")->fetch_object()->estado;
    if($estadoAct=='1'){
      exit;
    }
    if($_POST['state_pol']!='4'){
      exit;
    }    
    $id_workshop=$con->query("SELECT id_workshop FROM workshop_solicitudes WHERE id='".$id_solicitud."'");
    $ticketNum=$con->query("SELECT codigo FROM ticket WHERE id_usuario='$id_usuario' AND tipo=2 AND evento='".$id_workshop."'")->fetch_object()->codigo;
    $con->query("UPDATE ticket SET estado='1' WHERE codigo='".$ticketNum."' AND tipo=2 AND evento='".$id_workshop."'");
    $con->query("UPDATE usuarios_pagos SET estado='1' WHERE producto='4' AND opcional='".$id_solicitud."'");
    $con->query("UPDATE workshop_solicitudes SET estado='3', id_pago='".$id_pago."' WHERE id='".$id_solicitud."'");

    $datos=$con->query("select nombre,apellido,email,lengua from usuarios where id=".$id_usuario)->fetch_object();
    $text_email=$con->query("select * from textos_email where id='workshop_pago_exitoso'")->fetch_object();
    if($datos->lengua=='Español'){
      $tit=str_replace('%nombre%',$datos->nombre.' '.$datos->apellido,$text_email->titulo_es);
      $mensaje=str_replace('%nombre%',$datos->nombre.' '.$datos->apellido,$text_email->texto_es);
    }else{
      $tit=str_replace('%nombre%',$datos->nombre.' '.$datos->apellido,$text_email->titulo_en);
      $mensaje=str_replace('%nombre%',$datos->nombre.' '.$datos->apellido,$text_email->texto_en);
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://lalexpo.com/test/mail2.php");
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$datos->email."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);
    curl_close($ch);

    $text_email=$con->query("SELECT * FROM textos_email WHERE id='workshop_registro'")->fetch_object();
    if($datos->lengua=='Español'){
      $tit=$text_email->titulo_es;
      $mensaje=str_replace('%nombre%',$datos->nombre.' '.$datos->apellido,$text_email->texto_es);
    }else{
      $tit=$text_email->titulo_en;
      $mensaje=str_replace('%nombre%',$datos->nombre.' '.$datos->apellido,$text_email->texto_en);
    }

     $mensaje=str_replace('%link%', '<br /><br /><a href="https://lalexpo.com/getTicket.php?cod='.$ticketNum.'&e='.$id_workshop.'&desc=D" target="_blank">https://lalexpo.com/getTicket.php?cod='.$ticketNum.'&e='.$id_workshop.'&desc=D</a>', $mensaje);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://lalexpo.com/test/mail2.php");
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$datos->email."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);
    curl_close($ch);


  break;
  case 5:
    $estadoAct=$con->query("select estado from usuarios_pagos where id='$id_usu_pagos'")->fetch_object()->estado;
    if($estadoAct=='1'){
      exit;
    }
    if($_POST['state_pol']!='4'){
      exit;
    }
    $partes=explode('_',$datoAdicional);
    $id_usu_pagos=$partes[0];
    $cant_tickets=$partes[1];
    $id_paquete=$partes[2];
    $id_solicitud=$partes[3];

    $con->query("INSERT INTO codigos_descuento SET
    id_usuario='".$id_usuario."',
    codigo='".generarCodigo(45)."',
    porcentaje='50',
    redimido='0'");    
    
    crearTicket('', $id_usuario);
    gestionaTickets();
    break;
  case 6:
    $id_usu_pagos=$datoAdicional;
    $estadoAct=$con->query("select estado from usuarios_pagos where id='".$id_usu_pagos."'")->fetch_object()->estado;
    if($estadoAct=='1'){
      exit;
    }
    if($_POST['state_pol']!='4'){
      exit;
    }
    $con->query("UPDATE usuarios_pagos SET id_pago='".$id_pago."', estado='1' WHERE id='".$id_usu_pagos."'");
    break;
  default:
  break;
}
function gestionaTickets(){
  global $con,$id_pago,$id_usuario,$cant_tickets,$id_usu_pagos;
  $con->query("update usuarios_pagos set estado=1, id_pago='$id_pago' where id='$id_usu_pagos'");
  $ticket=$con->query("select estado,codigo from ticket where id_usuario='".$id_usuario."'")->fetch_object();
  $rw2=$con->query("select * from usuarios where id='".$id_usuario."'")->fetch_object();
  $text_compra=$con->query("select * from textos_email where id='compra_ticket'")->fetch_object();
  $text_compra_extras=$con->query("select * from textos_email where id='compra_tickets_extras'")->fetch_object();
  if($ticket->estado==0){
    $cant_tickets--;
    $con->query("update ticket SET estado='1' where id_usuario='".$id_usuario."' AND tipo=1");
    if($rw2->lengua=='Español'){
      $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->titulo_es);
      $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->texto_es);
    }else{
      $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->titulo_en);
      $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->texto_en);
    }
    $add='<br /><br /><a href="http://'.$_SERVER['SERVER_NAME'].'/getTicket.php?cod='.$ticket->codigo.'&e=1">http://'.$_SERVER['SERVER_NAME'].'/getTicket.php?cod='.$ticket->codigo.'&e=1</a>';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://lalexpo.com/test/mail2.php");
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$rw2->email."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje).$add));
    #curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$rw2->email."&asunto=".urlencode('Se ha generado su ticket de asistencia')."&mensaje=".urlencode('Se genero su ticket para ingresar al evento, el mismo lo puede ver desde su panel de control.'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    echo curl_exec ($ch);
    curl_close ($ch);
  }
  if($cant_tickets>0){
    if($rw2->lengua=='Español'){
      $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra_extras->titulo_es);
      $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra_extras->texto_es);
    }else{
      $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra_extras->titulo_en);
      $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra_extras->texto_en);
    }
    for($i=1;$i<=$cant_tickets;$i++){
      $con->query("INSERT INTO codigos_descuento SET
      id_usuario='".$id_usuario."',
      codigo='".generarCodigo(45)."',
      porcentaje='100',
      redimido='0'");
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://lalexpo.com/test/mail2.php");
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$rw2->email."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje)));
    #curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$rw2->email."&asunto=".urlencode('Se han generado codigos de ticket')."&mensaje=".urlencode('Se generaron tickets de descuento al 100%, los mismos lo puede ver desde su panel de control.'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    echo curl_exec ($ch);
    curl_close ($ch);
  }
}
function generarCodigo($longitud) {
  return rand(123456123,999999999);
 /*$key = '';
 $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
 $max = strlen($pattern)-1;
 for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
 return $key;*/
}