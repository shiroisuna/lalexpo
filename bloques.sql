/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.5.59-cll-lve : Database - lalexpo_lalexpo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `lalexpo_lalexpo`;

/*Table structure for table `bloques` */

DROP TABLE IF EXISTS `bloques`;

CREATE TABLE `bloques` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `fondo` varchar(200) DEFAULT NULL,
  `contenido1` text,
  `contenido2` text,
  `estado` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `bloques` */

insert  into `bloques`(`id`,`nombre`,`fondo`,`contenido1`,`contenido2`,`estado`) values 
(1,'section01','0.4680710015323232681.jpg','        <div class=\"cell\">\r\n          <div class=\"max01\">\r\n            <div class=\"der01\">\r\n              Asista al Primer, Mayor y más importante Evento de la Industria de Adultos de América latina            </div>\r\n            <div class=\"der02\">\r\n              Latinoamérica             </div>\r\n            <div class=\"der03\">\r\n              El espectáculo Definitive B2B Adult de América Central y del Sur. Venga al ÚNICO show en Sudamérica. Conoce un mercado totalmente nuevo y sé parte de             </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"cell cent\">\r\n          <div class=\"max01\">\r\n            <div class=\"pre\">\r\n              <div class=\"izq01 prim animated bounceInRight\">1,300 Profesionales de la industria</div>\r\n              <div class=\"izq01 animated bounceInRight\" style=\"animation-delay: .1s\">Un nuevo mercado sin explotar</div>\r\n              <div class=\"izq01 animated bounceInRight\" style=\"animation-delay: .2s\">Empresas listas para hacer negocios</div>\r\n              <div class=\"izq01 animated bounceInRight\" style=\"animation-delay: .3s\">Cientos de ofertas cerradas</div>\r\n              <div class=\"izq01 animated bounceInRight\" style=\"animation-delay: .4s\">Una oportunidad para expandirse</div>\r\n              <div class=\"izq01 animated bounceInRight\" style=\"animation-delay: .5s\">Grandes talleres y seminarios</div>\r\n              <div class=\"izq01 animated bounceInRight\" style=\"animation-delay: .6s\">Mejores redes y fiestas</div>\r\n            </div>\r\n            <a href=\"#\" onclick=\"showForm(\'preRegistrer\')\" style=\"animation-delay: 1.4s\" class=\"animated fadeIn\">Registro</a>\r\n          </div>\r\n        </div>','        <div class=\"cell\">\r\n          <div class=\"max01\">\r\n            <div class=\"der01\">\r\n               Attend the First, Biggest and most important Adult Industry Event of            </div>\r\n            <div class=\"der02\">\r\n              Latin-America            </div>\r\n            <div class=\"der03\">\r\n              The Definitive B2B Adult show of Central and South America. Come on over to the ONLY show in South America. Meet a total New market and be part of the            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"cell cent\">\r\n          <div class=\"max01\">\r\n            <div class=\"pre\">\r\n              <div class=\"izq01 prim animated bounceInRight\">1,300 Industry Profesionals</div>\r\n              <div class=\"izq01 animated bounceInRight\" style=\"animation-delay: .1s\">An unexploited new Market</div>\r\n              <div class=\"izq01 animated bounceInRight\" style=\"animation-delay: .2s\">Companies ready for business</div>\r\n              <div class=\"izq01 animated bounceInRight\" style=\"animation-delay: .3s\">Hundreds of Deals closed</div>\r\n              <div class=\"izq01 animated bounceInRight\" style=\"animation-delay: .4s\">An opportunity to expand</div>\r\n              <div class=\"izq01 animated bounceInRight\" style=\"animation-delay: .5s\">Big workshops and seminars</div>\r\n              <div class=\"izq01 animated bounceInRight\" style=\"animation-delay: .6s\">Best networking and Parties</div>\r\n            </div>\r\n            <a href=\"#\" onclick=\"showForm(\'preRegistrer\')\" style=\"animation-delay: 1.4s\" class=\"animated fadeIn\">Sign up</a>\r\n          </div>\r\n        </div>',1),
(2,'section03','0.5302430015323263961.jpg','        <div class=\"sp\">\r\n  <header><h1>Oportunidad de patrocinio</h1></header>\r\n  <p style=\"animation-delay: .1s\" class=\"animated bounceInLeft\"><img src=\"img/fleblanc.png\"> Promociona tu marca </p>\r\n  <p style=\"animation-delay: .2s\" class=\"animated bounceInLeft\"><img src=\"img/fleblanc.png\"> Muestra de su empresa</p>\r\n  <p style=\"animation-delay: .3s\" class=\"animated bounceInLeft\"><img src=\"img/fleblanc.png\"> Hacer nuevos contactos</p>\r\n  <p style=\"animation-delay: .4s\" class=\"animated bounceInLeft\"><img src=\"img/fleblanc.png\"> Conoce a tus socios latam</p>\r\n  <p style=\"animation-delay: .5s\" class=\"animated bounceInLeft\"><img src=\"img/fleblanc.png\"> Fortalecer las relaciones</p>\r\n  <p style=\"animation-delay: .6s\" class=\"animated bounceInLeft\"><img src=\"img/fleblanc.png\"> Crea un nuevo mercado</p>\r\n  <a href=\"sponsor.php\" style=\"animation-delay: 1.4s\" class=\"animated fadeIn\">Patrocinios</a>\r\n</div>\r\n<div class=\"sp\" style=\"width: auto;padding-left: 20%;\">\r\n  <div class=\"spons\">\r\n	<div class=\"dv1\">75</div>\r\n	<div class=\"dv2\">Patrocinadores</div>\r\n  </div>\r\n  <div class=\"txtUlt\">Listo para los negocios</div>\r\n</div>      ','        <div class=\"sp\">\r\n  <header><h1>Sponsorship opportunity</h1></header>\r\n  <p style=\"animation-delay: .1s\" class=\"animated bounceInLeft\"><img src=\"img/fleblanc.png\"> Promote y our brand </p>\r\n  <p style=\"animation-delay: .2s\" class=\"animated bounceInLeft\"><img src=\"img/fleblanc.png\"> Show of your company</p>\r\n  <p style=\"animation-delay: .3s\" class=\"animated bounceInLeft\"><img src=\"img/fleblanc.png\"> Make new contacts</p>\r\n  <p style=\"animation-delay: .4s\" class=\"animated bounceInLeft\"><img src=\"img/fleblanc.png\"> Meet your latam partners</p>\r\n  <p style=\"animation-delay: .5s\" class=\"animated bounceInLeft\"><img src=\"img/fleblanc.png\"> Strengthen relationships</p>\r\n  <p style=\"animation-delay: .6s\" class=\"animated bounceInLeft\"><img src=\"img/fleblanc.png\"> Create a new market</p>\r\n  <a href=\"sponsor.php\" style=\"animation-delay: 1.4s\" class=\"animated fadeIn\">Be our sponsor</a>\r\n</div>\r\n<div class=\"sp\" style=\"width: auto;padding-left: 20%;\">\r\n  <div class=\"spons\">\r\n	<div class=\"dv1\">75</div>\r\n	<div class=\"dv2\">Sponsors</div>\r\n  </div>\r\n  <div class=\"txtUlt\">Ready for business</div>\r\n</div>      ',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
