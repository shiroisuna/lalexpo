<?
include('includes/conexion.php');
include('includes/idioma.php');
$lg=new idioma($_GET['lg']);
$lg->seccion(5);
 include('includes/header.php')?>
<style>
  .bkg{
    width: 100%;
    background-color: #2d2d2d;
    display: table;
  }
  .bkg .txt01{
    width: 90%;
    position: relative;
    display:table-cell;
    vertical-align: top;
    padding-top: 4em;
  }
  .bkg .txt03{
    text-align: center;
  }
  .bkg h1{
    font-family: AspiraBlack,Verdana;
    font-size: 8em;
    color: #d0a951;
    margin: 0px;
    padding: 0px;
    display: inline-block;
    line-height: 80%;
    text-align: center;
    letter-spacing: -0.1em;
    width: 100%;
  }
  .bkg h1 span{
    color:#FFF;
    display: block;
    text-align: right;
  }
  .bkg .txt03{
    font-family: AspiraXWide-Medium,Verdana;
    font-size: 3em;
    color: #FFF;
    display: inline-block;
    width: 80%;
    padding-left: 10%;
    padding-right: 10%;
    padding-bottom: 5%
  }
  .bkg .padd{
    text-align: center;
  }

  .section{
    background: #FFF;
    min-height: 500px;
  }
  section{
    display: block;
    max-width: 1233px;
    width: 85%;
    margin: auto;
    padding-top: 50px;
  }
  section header{
    display: block;
    width: 100%;
    text-align: center;
    color:#b42127;
    font-family: AspiraBold,Verdana;
    font-size:5em;
    vertical-align: middle;
    border-bottom: solid 2px #b42127;
    margin-bottom: 10px;

  }
  section ul{
    list-style: none;
    display: block;
    margin: auto;
    border:0px solid #969696;
    padding:0;
    background: #FFF;
    width: 85%;
    margin-top: -30px;
/*    -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    border-bottom-left-radius: 40px;
    border-bottom-right-radius: 40px;*/
    position: relative;
  }
  section li{
    width: 100%;
    background: url('img/fdo_bottom_ofer.jpg') center bottom no-repeat;
  }
  .sinBg{
    background: none !important;
  }
  .cell{
    display: table-cell;
    vertical-align: middle;
  }
  .he02{
    width: 94%;
  }
  .he03{
    max-width: 134px;
    text-align: left;
  }
  .he03 img{
    max-width: 43px;
    width: 90%;
  }
  .tabl{
    display: table;
    width: 100%;
    color:#2d2d2d;
    height: 200px;
    margin-bottom: 20px;
    padding-left: 4%;
  }
  .descrip{
    font-family: Aspira,Verdana;
    font-size: 1.4em;
    color:#2d2d2d;
    line-height: 1.2em;
    position: relative;
  }
  .descrip div{
    position: absolute;
    top: 15px;
    right: 100px;
      -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    width: 280px;
    padding:30px
  }
  .descrip table{
    width: 100%;
  }
  .descrip td{
    height: 50px;
    vertical-align: middle;
    border-bottom: 1px solid #606060;
  }
  .descrip div a{
    width: 265px;
    padding: 10px 0;
    background: #a81d26;
    color:#FFF;
    display: inline-block;
    text-align: center;
    vertical-align: middle;
    margin: auto;
    text-decoration: none;
    margin-top: 40px;
    border-radius: 25px;
  }
  /*************************************/
  .fechas{
    text-align: right;
    padding-right: 20px;
    padding-top: 40px;
  }
  .fechas a,.fechas span{
    font-size:2em;
    text-decoration:none;
    color:#8f1526;
    font-family:Aspira,Verdana;
  }
  .fechas a:hover{
    text-decoration: underline;
  }
  .gral{
    -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    width: 90%;
    margin: auto;
    position: relative;
    background: #FFF;
  }
  .gral td{
    height: 150px;
    border-bottom: 1px solid #dddddd;
  }
  .gral table{
    width: 100%;
  }
  .gral td.txt{
    color:#000;
    font-size: 2.7em;
    font-family:Aspira,Verdana;
  }
  .awe{
    color:#821126;
    font-size:2.5em;
    font-family: AwesomeSolid,Verdana;
  }
  .dvBusc2{
    text-align: left;
    padding-left: 20px;
    font-family:Aspira,Verdana;
    color:#000;
    font-size:2.2em;
    width: 90%;
    margin: auto;
    padding-bottom: 15px;
  }
  .dvBusc2 .buscWho{
  }
  .dvBusc2 .red{
    color:#821126;
    display: inline-block;
    padding-right: 20px;
    background: url(img/fle04.jpg) no-repeat right 5px;
    cursor:pointer
  }
  .txt01 div{
    display: inline-block;
  }
  .he04,.he05{
    font-family: AspiraBlack,Verdana;
    font-size: 2em;
    color:#a91d26;
    padding:0 4%
  }
  .he05{
    font-size: 1em;
    color:#2d2d2d;
    display:inline
  }
  .mingal {
    width: 350px;
    height: 300px;
    float: left;
    background-size: cover;
    border: 1px solid #fff;
    margin: 0px;
    -webkit-box-shadow: 0px 0px 6px 1px rgba(0,0,0,0.6);
    -moz-box-shadow: 0px 0px 6px 1px rgba(0,0,0,0.6);
    box-shadow: 0px 0px 6px 1px rgba(0,0,0,0.6);
    cursor: pointer;
  }
  .mingal:hover {
    opacity: 0.9;
  }
  .mingal img {

  }
  .mingal img.btnsoc {
    display: inline-block;
    position: relative;
    top: 1px;
    z-index: 1;
  }
  .btnvermasg { 
    display: block;
    width: 300px;
    margin-left: auto;
    margin-right: auto;
    background: #b42127;
    text-align: center;
    font-size: 2.5em;
    font-family: Aspira,Verdana;
    border-radius: 5px;
    color: #fff;
    padding: 8px;
    cursor: pointer;
  }
  .btnvermasg:hover { 
    background: #ca353b;
    size: 98%;
  }

  .paginacion { display: block; margin: 40px; }
  .paginacion ul { text-align: center; }
  .paginacion ul li{ 
    display: inline-block;;
    width: 30px;
    margin:4px;
    background: #b42127;
    text-align: center;
    font-size: 2.5em;
    font-family: Aspira,Verdana;
    border-radius: 5px;
    color: #fff;
    padding: 8px;
    cursor: pointer;
  }
  .paginacion ul li.active { background: #f7cf79; }
  .paginacion ul li:hover { 
    background: #ca353b;
    size: 98%;
  }

  #fotos .mingal {
    line-height: 0;
    -webkit-column-count: 5;
    -webkit-column-gap:   0px;
    -moz-column-count:    5;
    -moz-column-gap:      0px;
    column-count:         5;
    column-gap:           0px;
  }
#fotos .mingal img {
  /* Just in case there are inline attributes */
  width: 30% !important;
  height: auto !important;
}
  </style>
    <script src="js/jquery.colorbox.js"></script>
    <div class="content">
      <div class="separador"></div>
      <div class="bkg">
        <div class="txt01 padd">
          <h1><?php echo $lg->general->home_galeria; ?></h1>

        </div>
      </div>
      <div  class="section">
        <? include('servicios/galeria.php')?>
      </div>
      <br/><br/>
    <script type="text/javascript">
function popup (pagina, ancho, alto) {
  var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, width="+ ancho +", height="+ alto +", top=85, left=140";
  window.open(pagina,"",opciones);
}
$(".lnkgal").colorbox({rel:'lnkgal', scalePhotos:true, innerHeight:'80%'});
    </script>
    <? include('includes/footer.php')?>