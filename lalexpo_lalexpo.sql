-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 12-07-2018 a las 16:25:49
-- Versión del servidor: 5.5.59-cll-lve
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `lalexpo_lalexpo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banners_principal`
--

CREATE TABLE `banners_principal` (
  `id` int(11) UNSIGNED NOT NULL,
  `archivo` varbinary(150) DEFAULT NULL,
  `fecha_alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `activo` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(5) UNSIGNED NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sitios_transmision`
--

CREATE TABLE `sitios_transmision` (
  `id` int(6) UNSIGNED NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `habilitado` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traduccion`
--

CREATE TABLE `traduccion` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_seccion` int(5) UNSIGNED DEFAULT NULL,
  `en` varchar(150) DEFAULT NULL,
  `es` varchar(150) DEFAULT NULL,
  `obs` varbinary(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traduccion_seccion`
--

CREATE TABLE `traduccion_seccion` (
  `id` int(5) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) UNSIGNED NOT NULL,
  `activo` int(1) DEFAULT '0',
  `token` varchar(200) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `genero` varchar(50) DEFAULT NULL,
  `foto` varbinary(150) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `pass` varchar(30) DEFAULT NULL,
  `dni` varchar(50) DEFAULT NULL,
  `pais` varchar(50) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `lengua` varbinary(30) DEFAULT NULL,
  `telefono` varbinary(25) DEFAULT NULL,
  `id_categoria` int(5) UNSIGNED DEFAULT NULL,
  `fecha_alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `anio` int(4) UNSIGNED DEFAULT NULL,
  `company` varchar(70) DEFAULT NULL,
  `website` varchar(80) DEFAULT NULL,
  `models` varchar(10) DEFAULT NULL,
  `face` varchar(60) DEFAULT NULL,
  `twitter` varchar(60) DEFAULT NULL,
  `instagram` varchar(60) DEFAULT NULL,
  `skype` varchar(60) DEFAULT NULL,
  `youtube` varchar(60) DEFAULT NULL,
  `objetivo` varbinary(100) DEFAULT NULL,
  `descrip_profesional` varbinary(150) DEFAULT NULL,
  `nickname` varbinary(50) DEFAULT NULL,
  `sitios_adicionales` varbinary(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `activo`, `token`, `nombre`, `apellido`, `genero`, `foto`, `email`, `pass`, `dni`, `pais`, `estado`, `lengua`, `telefono`, `id_categoria`, `fecha_alta`, `anio`, `company`, `website`, `models`, `face`, `twitter`, `instagram`, `skype`, `youtube`, `objetivo`, `descrip_profesional`, `nickname`, `sitios_adicionales`) VALUES
(8, 0, '8d6c07685aadbc44c1a210f16a1a67ca95ccbf2a0e8229e1a3', 'Cristian', 'Lescano', '1', NULL, 'cristian_lescano@hotmail.es', '1', '', 'Colombia', 'Atlantico', '', '', 2, '2018-07-12 07:20:49', 2019, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0x6466736466736466, 0x756e646566696e6564);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_back`
--

CREATE TABLE `usuarios_back` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` varbinary(30) NOT NULL,
  `pass` varbinary(30) NOT NULL,
  `email` varbinary(70) NOT NULL,
  `nombre` varbinary(70) DEFAULT NULL,
  `tipo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios_back`
--

INSERT INTO `usuarios_back` (`id`, `user`, `pass`, `email`, `nombre`, `tipo`) VALUES
(1, 0x61646d696e, 0x61646d696e, '', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_sitios`
--

CREATE TABLE `usuarios_sitios` (
  `id_usuarios` int(11) UNSIGNED NOT NULL,
  `id_sitios` int(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `banners_principal`
--
ALTER TABLE `banners_principal`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sitios_transmision`
--
ALTER TABLE `sitios_transmision`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `traduccion`
--
ALTER TABLE `traduccion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seccion` (`id_seccion`,`id`);

--
-- Indices de la tabla `traduccion_seccion`
--
ALTER TABLE `traduccion_seccion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios_back`
--
ALTER TABLE `usuarios_back`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios_sitios`
--
ALTER TABLE `usuarios_sitios`
  ADD PRIMARY KEY (`id_usuarios`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `banners_principal`
--
ALTER TABLE `banners_principal`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sitios_transmision`
--
ALTER TABLE `sitios_transmision`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `traduccion`
--
ALTER TABLE `traduccion`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `traduccion_seccion`
--
ALTER TABLE `traduccion_seccion`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `usuarios_back`
--
ALTER TABLE `usuarios_back`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuarios_sitios`
--
ALTER TABLE `usuarios_sitios`
  MODIFY `id_usuarios` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
