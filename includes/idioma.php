<?php
class idioma{
  var $general,$seccion,$idioma;
  function __construct($idioma){
    global $con;
    if (empty($idioma)) $idioma='es';
    $this->idioma=$idioma;
    $this->general=new stdClass;
    $rs=$con->query("select id, ".$idioma." from traduccion where id_seccion=1");
    if ($rs) {
      while($rw=$rs->fetch_object()){
        $this->general->{$rw->id}=$rw->$idioma;
      }
    }

  }
  function seccion($sec){
    global $con;
    $this->seccion=new stdClass;
    $rs=$con->query("select id, ".$this->idioma." from traduccion where id_seccion='".$sec."'");
    if ($rs) {
      while($rw=$rs->fetch_object()){
        $this->seccion->{$rw->id}=$rw->{$this->idioma};
      }
    }
  }
}