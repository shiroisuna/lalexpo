<?
function properText($text){    
    $text = str_replace(array('á','é','í','ó','ú','ñ'), array('a','e','i','o','u','n'), $text);
    return($text); 
}

function crearTicket($DIR='', $idUsr){  
  global $con;
  $catego=$con->query("SELECT
c.nombre
FROM usuarios u
LEFT JOIN categorias c ON c.id=u.id_categoria
WHERE u.id=".$idUsr)->fetch_object()->nombre;
  $usuario=$con->query("SELECT * FROM usuarios WHERE id=".$idUsr)->fetch_object();
$hash=$usuario->token;
// $hash=substr($hash,0,40);
  $codigo=strtoupper(substr(sha1($usuario->id),0,17));
  $hash=substr($hash,0,40).'_'.$codigo.'_1_1';
	$codtick='*** '.$codigo.' ***';
	$imgcod=imagecreate(240, 25);
	$color_fondo = imagecolorallocatealpha($imgcod, 255, 255, 255, 0);
	$color_texto = imagecolorallocatealpha($imgcod, 0, 0, 0, 100);
	imagestring($imgcod, 5, 10, 5, $codtick, $color_texto);
	$codigoTicket=imagescale(imagerotate($imgcod,90, 0), 50);
	$evento = '6th Edition Lalexpo 2019';
	$fecha='18,19,20/02/2019';
	$hora='9:00 am – 5:00 pm';
	$dir1='Verde Arena Convention';
	$dir2='Center Cra 125 #23a-58';
	$dir3='';
	$dir4='';
	$informacion1='Attend the First, Biggest and most important';
	$informacion2='Adult Industry Event of Latin America';
	$informacion3='';
	$tipo=$catego;
	$nombre=$usuario->nombre;
	$apellido=$usuario->apellido;
	$documento=$usuario->dni;
	$numero_ticket=$codigo;
  ob_start();
  include($DIR."lib/QR/qrlib.php");
  QRcode::png($hash,$outfile = false, $level = QR_ECLEVEL_L, $size = 5);
  $qr=ob_get_clean();
  $qrcod=imagecreatefromstring($qr);
  $tamanio_qr=getimagesizefromstring($qr);
#	$qrcod=imagecreatefrompng('http://'.$_SERVER['SERVER_NAME'].'/qr/'.sha1($_SESSION['id']).'.png');
	// .$nombre.''.$documento.''
	$im     = imagecreatefromjpeg($DIR."img/formato_ticket.jpg");
	$negro = imagecolorallocate($im, 0, 0, 0);
	$blanco = imagecolorallocate($im, 255, 255, 255);
	imagestring($im, 5, 150, 150, $evento, $negro);
	imagestring($im, 5, 150, 210, $fecha, $negro);
	imagestring($im, 5, 150, 280, $hora, $negro);
	imagestring($im, 5, 500, 210, $dir1, $negro);
	imagestring($im, 5, 500, 230, $dir2, $negro);
	imagestring($im, 5, 500, 250, $dir3, $negro);
	imagestring($im, 5, 500, 270, $dir4, $negro);
	imagestring($im, 5, 150, 340, $informacion1, $negro);
	imagestring($im, 5, 150, 360, $informacion2, $negro);
	imagestring($im, 5, 150, 380, $informacion3, $negro);
	imagestring($im, 5, 150, 440, $tipo, $negro);
	imagestring($im, 5, 920, 250, $nombre, $blanco);
	imagestring($im, 5, 920, 310, $apellido, $blanco);
	imagestring($im, 5, 920, 375, $documento, $blanco);
	imagestring($im, 5, 920, 440, $numero_ticket, $blanco);
	imagecopymerge($im, $codigoTicket, 30, 30, 0, 0, 40, 460, 100);

  $nuevo_ancho=190;
  $nuevo_alto=190;
  $qrcod_nuevo = imagecreatetruecolor($nuevo_ancho, $nuevo_alto);
  imagecopyresized($qrcod_nuevo, $qrcod, 0, 0, 0, 0, $nuevo_ancho, $nuevo_alto, $tamanio_qr[0], $tamanio_qr[1]);


	imagecopymerge($im, $qrcod_nuevo, 910, 10, 0, 0, 190, 190, 100);
	ob_start();
	imagepng($im);
	$bin = ob_get_clean();
	$b64 = base64_encode($bin);
	$blobimg = '' . $b64;
	$rs=$con->query("select * from ticket where id_usuario='".$usuario->id."' AND tipo=1");
	  if($rs->num_rows>0){
	    $query = "update ticket SET
			 codigo='".$codigo."',
			 ticket='".$blobimg."',
			 estado=1
	     where id_usuario='".$usuario->id."' AND tipo=1";
	  }else{
	    $query = "INSERT ticket SET
			 id_usuario='".$usuario->id."',
			 codigo='".$codigo."',
			 ticket='".$blobimg."',
			 tipo=1,
			 estado=1";
	  }
	$con->query($query);
  header('Content-Type: text/html',true);
  if(!empty($con->error)){
    echo $con->error;
    exit;
  }
}

function crearTicketWorkshop($DIR='', $idUsr){
  global $con;
  $catego=$con->query("SELECT
c.nombre
FROM usuarios u
LEFT JOIN categorias c ON c.id=u.id_categoria
WHERE u.id=".$idUsr)->fetch_object()->nombre;
  $workshop=$con->query("select * from workshop where estado=1 LIMIT 1")->fetch_object();
  $usuario=$con->query("SELECT * FROM usuarios WHERE id=".$idUsr)->fetch_object();
  $numTick=1;
	if ($con->query("SELECT codigo+1 numero FROM ticket WHERE tipo=2 AND evento=".$workshop->id." ORDER BY id DESC LIMIT 1")->num_rows>0) {
	  $numTick=$con->query("SELECT codigo+1 numero FROM ticket WHERE tipo=2 AND evento=".$workshop->id." ORDER BY id DESC LIMIT 1")->fetch_object()->numero;
	}
	$hash=$con->query("SELECT token FROM usuarios WHERE id=".$idUsr)->fetch_object()->token;
	if ($usuario->lengua=='Español') {
		$titulo=properText($workshop->titulo_es);
		$descripcion=properText($workshop->descripcion_es);
	} else {
		$titulo=$workshop->titulo_en;
		$descripcion=$workshop->descripcion_en;
	}

	// $hash=substr($hash,0,40);
  	$codigo=str_pad($numTick, 17, '0', STR_PAD_LEFT);
  	$hash=substr($hash,0,40).'_'.$codigo.'_2_'.$workshop->id;
	$codtick='*** '.$codigo.' ***';
	$imgcod=imagecreate(240, 25);
	$color_fondo = imagecolorallocatealpha($imgcod, 255, 255, 255, 0);
	$color_texto = imagecolorallocatealpha($imgcod, 0, 0, 0, 100);
	imagestring($imgcod, 5, 10, 5, $codtick, $color_texto);
	$codigoTicket=imagescale(imagerotate($imgcod,90, 0), 50);
	$evento = $titulo;
	$fecha=$workshop->fecha;
	$hora=$workshop->hora_evento;
	$dir1=substr($workshop->lugar, 0, 26);
	$dir2=substr($workshop->lugar, 27, 26);
	$dir3=substr($workshop->lugar, 51, 26);
	$dir4=substr($workshop->lugar, 77, 26);;
	$informacion1=substr($descripcion, 0, 60);
	$informacion2=substr($descripcion, 61, 60);
	$informacion3=substr($descripcion, 121, 60);
	$tipo=$catego;
	$nombre=$usuario->nombre;
	$apellido=$usuario->apellido;
	$documento=$usuario->dni;
	$numero_ticket=$codigo;
  ob_start();
  include($DIR."lib/QR/qrlib.php");
  QRcode::png($hash,$outfile = false, $level = QR_ECLEVEL_L, $size = 5);
  $qr=ob_get_clean();
  $qrcod=imagecreatefromstring($qr);
  $tamanio_qr=getimagesizefromstring($qr);
#	$qrcod=imagecreatefrompng('http://'.$_SERVER['SERVER_NAME'].'/qr/'.sha1($_SESSION['id']).'.png');
	// .$nombre.''.$documento.''
	$im     = imagecreatefromjpeg($DIR."img/formato_ticket_workshop.jpg");
	$negro = imagecolorallocate($im, 0, 0, 0);
	$blanco = imagecolorallocate($im, 255, 255, 255);
	imagestring($im, 5, 150, 150, $evento, $negro);
	imagestring($im, 5, 150, 210, $fecha, $negro);
	imagestring($im, 5, 150, 280, $hora, $negro);
	imagestring($im, 5, 500, 210, $dir1, $negro);
	imagestring($im, 5, 500, 230, $dir2, $negro);
	imagestring($im, 5, 500, 250, $dir3, $negro);
	imagestring($im, 5, 500, 270, $dir4, $negro);
	imagestring($im, 5, 150, 340, $informacion1, $negro);
	imagestring($im, 5, 150, 360, $informacion2, $negro);
	imagestring($im, 5, 150, 380, $informacion3, $negro);
	imagestring($im, 5, 150, 440, $tipo, $negro);
	imagestring($im, 5, 920, 250, $nombre, $blanco);
	imagestring($im, 5, 920, 310, $apellido, $blanco);
	imagestring($im, 5, 920, 375, $documento, $blanco);
	imagestring($im, 5, 920, 440, $numero_ticket, $blanco);
	imagecopymerge($im, $codigoTicket, 30, 30, 0, 0, 40, 460, 100);

  $nuevo_ancho=140;
  $nuevo_alto=140;
  $qrcod_nuevo = imagecreatetruecolor($nuevo_ancho, $nuevo_alto);
  imagecopyresized($qrcod_nuevo, $qrcod, 0, 0, 0, 0, $nuevo_ancho, $nuevo_alto, $tamanio_qr[0], $tamanio_qr[1]);


	imagecopymerge($im, $qrcod_nuevo, 888, 30, 0, 0, 140, 140, 100);
	ob_start();
	imagepng($im);
	$bin = ob_get_clean();
	$b64 = base64_encode($bin);
	$blobimg = '' . $b64;
	$rs=$con->query("select * from ticket where id_usuario='".$idUsr."' AND tipo=2 AND evento='".$workshop->id."'");
  if($rs->num_rows>0){
    $query = "update ticket SET
		 codigo='".$codigo."',
		 ticket='".$blobimg."'
     where id_usuario='".$idUsr."' AND tipo=2 AND evento='".$workshop->id."'";
  }else{
    $query = "INSERT ticket SET
		 id_usuario='".$idUsr."',
		 codigo='".$codigo."',
		 ticket='".$blobimg."',
		 tipo=2,
		 evento='".$workshop->id."'";
  }
	$con->query($query);
  header('Content-Type: text/html',true);
  if(!empty($con->error)){
    echo $con->error;
    exit;
  }
  return $codigo;
}

function updateTicket($DIR=''){
  global $con;
  $catego=$con->query("SELECT
c.nombre
FROM usuarios u
LEFT JOIN categorias c ON c.id=u.id_categoria
WHERE u.id=".$_SESSION['id'])->fetch_object()->nombre;
$hash=$con->query("SELECT token FROM usuarios WHERE id=".$_SESSION['id'])->fetch_object()->token;
// $hash=substr($hash,0,40);
  $codigo=strtoupper(substr(sha1($_SESSION['id']),0,17));
  $hash=substr($hash,0,40).'_'.$codigo.'_1_1';
	$codtick='*** '.$codigo.' ***';
	$imgcod=imagecreate(240, 25);
	$color_fondo = imagecolorallocatealpha($imgcod, 255, 255, 255, 0);
	$color_texto = imagecolorallocatealpha($imgcod, 0, 0, 0, 100);
	imagestring($imgcod, 5, 10, 5, $codtick, $color_texto);
	$codigoTicket=imagescale(imagerotate($imgcod,90, 0), 50);
	$evento = '6th Edition Lalexpo 2019';
	$fecha='18,19,20/02/2019';
	$hora='9:00 am – 5:00 pm';
	$dir1='Verde Arena Convention';
	$dir2='Center Cra 125 #23a-58';
	$dir3='';
	$dir4='';
	$informacion1='Attend the First, Biggest and most important';
	$informacion2='Adult Industry Event of Latin America';
	$informacion3='';
	$tipo=$catego;
	$nombre=$_SESSION['nombre'];
	$apellido=$_SESSION['apellidos'];
	$documento=$_SESSION['documento'];
	$numero_ticket=$codigo;
  ob_start();
  include($DIR."lib/QR/qrlib.php");
  QRcode::png($hash,$outfile = false, $level = QR_ECLEVEL_L, $size = 5);
  $qr=ob_get_clean();
  $qrcod=imagecreatefromstring($qr);
  $tamanio_qr=getimagesizefromstring($qr);
#	$qrcod=imagecreatefrompng('http://'.$_SERVER['SERVER_NAME'].'/qr/'.sha1($_SESSION['id']).'.png');
	// .$nombre.''.$documento.''
	$im     = imagecreatefromjpeg($DIR."img/formato_ticket.jpg");
	$negro = imagecolorallocate($im, 0, 0, 0);
	$blanco = imagecolorallocate($im, 255, 255, 255);
	imagestring($im, 5, 150, 150, $evento, $negro);
	imagestring($im, 5, 150, 210, $fecha, $negro);
	imagestring($im, 5, 150, 280, $hora, $negro);
	imagestring($im, 5, 500, 210, $dir1, $negro);
	imagestring($im, 5, 500, 230, $dir2, $negro);
	imagestring($im, 5, 500, 250, $dir3, $negro);
	imagestring($im, 5, 500, 270, $dir4, $negro);
	imagestring($im, 5, 150, 340, $informacion1, $negro);
	imagestring($im, 5, 150, 360, $informacion2, $negro);
	imagestring($im, 5, 150, 380, $informacion3, $negro);
	imagestring($im, 5, 150, 440, $tipo, $negro);
	imagestring($im, 5, 920, 250, $nombre, $blanco);
	imagestring($im, 5, 920, 310, $apellido, $blanco);
	imagestring($im, 5, 920, 375, $documento, $blanco);
	imagestring($im, 5, 920, 440, $numero_ticket, $blanco);
	imagecopymerge($im, $codigoTicket, 30, 30, 0, 0, 40, 460, 100);

  $nuevo_ancho=190;
  $nuevo_alto=190;
  $qrcod_nuevo = imagecreatetruecolor($nuevo_ancho, $nuevo_alto);
  imagecopyresized($qrcod_nuevo, $qrcod, 0, 0, 0, 0, $nuevo_ancho, $nuevo_alto, $tamanio_qr[0], $tamanio_qr[1]);


	imagecopymerge($im, $qrcod_nuevo, 910, 10, 0, 0, 190, 190, 100);
	ob_start();
	imagepng($im);
	$bin = ob_get_clean();
	$b64 = base64_encode($bin);
	$blobimg = '' . $b64;
	$rs=$con->query("select * from ticket where id_usuario='".$_SESSION['id']."' AND tipo=1");
  if($rs->num_rows>0){
    $query = "update ticket SET
		 codigo='".$codigo."',
		 ticket='".$blobimg."'
     where id_usuario='".$_SESSION['id']."' AND tipo=1";
  }else{
    $query = "INSERT ticket SET
		 id_usuario='".$_SESSION['id']."',
		 codigo='".$codigo."',
		 ticket='".$blobimg."',
		 tipo=1";
  }
	$con->query($query);
  header('Content-Type: text/html',true);
  if(!empty($con->error)){
    echo $con->error;
    exit;
  }
}

function updateTicketWorkshop($DIR=''){
  global $con;
  $catego=$con->query("SELECT
c.nombre
FROM usuarios u
LEFT JOIN categorias c ON c.id=u.id_categoria
WHERE u.id=".$_SESSION['id'])->fetch_object()->nombre;
  $numTick=1;
  $workshop=$con->query("select * from workshop where estado=1 LIMIT 1")->fetch_object();
if ($con->query("SELECT codigo+1 numero FROM ticket WHERE tipo=2 AND evento=".$workshop->id." ORDER BY id DESC LIMIT 1")->num_rows>0) {
  $numTick=$con->query("SELECT codigo+1 numero FROM ticket WHERE tipo=2 AND evento=".$workshop->id." ORDER BY id DESC LIMIT 1")->fetch_object()->numero;
}
$hash=$con->query("SELECT token FROM usuarios WHERE id=".$_SESSION['id'])->fetch_object()->token;
// $hash=substr($hash,0,40);
  $codigo=str_pad($numTick, 17, '0', STR_PAD_LEFT);
  $hash=substr($hash,0,40).'_'.$codigo.'_2_'.$workshop->id;
	$codtick='*** '.$codigo.' ***';
	$imgcod=imagecreate(240, 25);
	$color_fondo = imagecolorallocatealpha($imgcod, 255, 255, 255, 0);
	$color_texto = imagecolorallocatealpha($imgcod, 0, 0, 0, 100);
	imagestring($imgcod, 5, 10, 5, $codtick, $color_texto);
	$codigoTicket=imagescale(imagerotate($imgcod,90, 0), 50);
	$evento = '2th Edition Lalexpo Workshop';
	$fecha='11/09/2018';
	$hora='9:00 am – 5:00 pm';
	$dir1='Hotel San Fernando Plaza';
	$dir2='Medellin';
	$dir3='';
	$dir4='';
	$informacion1='Lalexpo Workshop is just a sample of what you';
	$informacion2='can get to live in our annual edition of Lalexpo';
	$informacion3='';
	$tipo=$catego;
	$nombre=$_SESSION['nombre'];
	$apellido=$_SESSION['apellidos'];
	$documento=$_SESSION['documento'];
	$numero_ticket=$codigo;
  ob_start();
  include($DIR."../lib/QR/qrlib.php");
  QRcode::png($hash,$outfile = false, $level = QR_ECLEVEL_L, $size = 5);
  $qr=ob_get_clean();
  $qrcod=imagecreatefromstring($qr);
  $tamanio_qr=getimagesizefromstring($qr);
#	$qrcod=imagecreatefrompng('http://'.$_SERVER['SERVER_NAME'].'/qr/'.sha1($_SESSION['id']).'.png');
	// .$nombre.''.$documento.''
	$im     = imagecreatefromjpeg($DIR."../img/formato_ticket_workshop.jpg");
	$negro = imagecolorallocate($im, 0, 0, 0);
	$blanco = imagecolorallocate($im, 255, 255, 255);
	imagestring($im, 5, 150, 150, $evento, $negro);
	imagestring($im, 5, 150, 210, $fecha, $negro);
	imagestring($im, 5, 150, 280, $hora, $negro);
	imagestring($im, 5, 500, 210, $dir1, $negro);
	imagestring($im, 5, 500, 230, $dir2, $negro);
	imagestring($im, 5, 500, 250, $dir3, $negro);
	imagestring($im, 5, 500, 270, $dir4, $negro);
	imagestring($im, 5, 150, 340, $informacion1, $negro);
	imagestring($im, 5, 150, 360, $informacion2, $negro);
	imagestring($im, 5, 150, 380, $informacion3, $negro);
	imagestring($im, 5, 150, 440, $tipo, $negro);
	imagestring($im, 5, 920, 250, $nombre, $blanco);
	imagestring($im, 5, 920, 310, $apellido, $blanco);
	imagestring($im, 5, 920, 375, $documento, $blanco);
	imagestring($im, 5, 920, 440, $numero_ticket, $blanco);
	imagecopymerge($im, $codigoTicket, 30, 30, 0, 0, 40, 460, 100);

  $nuevo_ancho=140;
  $nuevo_alto=140;
  $qrcod_nuevo = imagecreatetruecolor($nuevo_ancho, $nuevo_alto);
  imagecopyresized($qrcod_nuevo, $qrcod, 0, 0, 0, 0, $nuevo_ancho, $nuevo_alto, $tamanio_qr[0], $tamanio_qr[1]);


	imagecopymerge($im, $qrcod_nuevo, 888, 30, 0, 0, 140, 140, 100);
	ob_start();
	imagepng($im);
	$bin = ob_get_clean();
	$b64 = base64_encode($bin);
	$blobimg = '' . $b64;
	$rs=$con->query("select * from ticket where id_usuario='".$_SESSION['id']."' AND tipo=2");
  if($rs->num_rows>0){
    $query = "update ticket SET
		 codigo='".$codigo."',
		 ticket='".$blobimg."'
     where id_usuario='".$_SESSION['id']."' AND tipo=2";
  }else{
    $query = "INSERT ticket SET
		 id_usuario='".$_SESSION['id']."',
		 codigo='".$codigo."',
		 ticket='".$blobimg."',
		 tipo=2";
  }
	$con->query($query);
  header('Content-Type: text/html',true);
  if(!empty($con->error)){
    echo $con->error;
    exit;
  }
  return $codigo;
}