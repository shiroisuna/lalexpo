<?
if(empty($_GET['env'])){
  exit;
}
include('includes/conexion.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR | E_WARNING | E_PARSE);

ob_start();?>
<html>
  <body>
    <span style="font-size:14px;font-family:Tahoma,Arial,Verdana">
      Hola _nombre_,<br /><br />
      Gracias por haberte registrado en nuestra Landing Page.
      <br /><br />
      A partir de este momento podrás ingresar a nuestra nueva página web y adquirir tu escarapela digital.
      <br /><br />
      Haz clic en el siguiente link, para ingresar a tu perfil y completar tus datos de registro:
      <br /><br />
      <a href="http://lalexpo.com/dashboard.php?token=_token_#menu" target="_blank" style="text-decoration:underline;color:#333">http://lalexpo.com/dashboard.php?token=_token_#menu</a>
      <br /><br />
      Recuerda que estos datos son importantes y serán verificados al momento de ingresar al evento.
      <br /><br />
      Atentamente,<br />
      <img src="http://lalexpo.com/img/firma_es.jpg" alt="LalExpo" style="max-width:500px" />
    </span>
  </body>
</html>
<?
$txt_es=ob_get_clean();
ob_start();?>
<html>
  <body>
    <span style="font-size:14px;font-family:Tahoma,Arial,Verdana">
      Hello _nombre_, <br />
Thank you for registered on our landing page. <br />
From this moment, you can enter our new website and obtain your digital badge.<br />
Click on the following link to access your profile and complete your registration information:
<br /><br />
<a href="http://lalexpo.com/dashboard.php?token=_token_#menu" target="_blank" style="text-decoration:underline;color:#333">http://lalexpo.com/dashboard.php?token=_token_#menu</a>
<br /><br />
Remember that these data are very important and will be verified in the registration desk in the event.
<br /><br />
Sincerely,<br />
      <img src="http://lalexpo.com/img/firma_en.jpg" alt="LalExpo" style="max-width:500px" />
    </span>
  </body>
</html>
<?
$txt_en=ob_get_clean();





$rs=$con->query("select * from usuarios where id>124");
while($rw=$rs->fetch_object()){
  if($rw->lengua=='Español'){
    $asunto='Hola '.$rw->nombre;
    $body=str_replace('_nombre_',$rw->nombre,$txt_es);
  }else{
    $asunto='Hello '.$rw->nombre;
    $body=str_replace('_nombre_',$rw->nombre,$txt_en);
  }
  $body=str_replace('_token_',$rw->token,$body);
  //$rw->email='cristian_lescano@hotmail.es';
  //echo 'A: '.$rw->nombre.' | '.$rw->email.'<br />Asunto: '.$asunto.'<br />'.$body.'<br />---------------------------------------------<br /><br /><br />';


  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";

  $headers .= 'To: '.$rw->email . "\r\n";
  $headers .= "From: Lalexpo <info@lalexpo.com>\r\n";
  $headers.="Return-Path:<info@lalexpo.com>\r\n";
  $headers .= 'X-Mailer: PHP/' . phpversion();
  if(mail($rw->email, $asunto, $body, $headers)){
    echo 'Exito: '.$rw->email.'<br />';
  }else{
    echo 'Error: '.$rw->email.'<br />';
  }
  //break;
}
