<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th>Nombre Esp.</th>
    <th>Categoria</th>
    <th>Imagen</th>
    <th>Precio</th>
    <th>Disponible</th>
    <th>Vendidos</th>
    <th>Activo</th>
    <th></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><?=$rw->id?>.</td>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>"><?=$rw->nombre_es?></a></td>
    <td><?=$rw->categoria?></a></td>
    <td><?
    if(!empty($rw->archivo)){
      $ext=strtolower(substr(strrchr($rw->archivo, "."), 1));
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 70px;">
          <source src="../images/productos/<?=$rw->archivo?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="../thumb_120_w/images/productos/<?=$rw->archivo?>" style="max-height: 70px;" />
      <? }else{?>
        <?=$rw->archivo?>
      <? }}?></td>
    <th><?=$rw->valor?></th>
    <th><?=$rw->disponible?></th>
    <th><?=(int)$rw->vendido?></th>
    <td><?=($rw->activo=='1')?'Si':'No'?></td>
    <td><a href="javascript:;" onclick="msg.text('¿Desea realmente eliminar este producto?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a><button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal"> <i class="far fa-pencil-alt"></i></button></td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="8">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>