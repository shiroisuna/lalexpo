<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th width="80">Nombre</th>
    <th width="210">Fondo</th>
    <th width="210">Contenido Español</th>
    <th>Contenido Inglés</th>
    <th>Estado</th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr style="cursor: pointer;" onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'">
    <td><?=$rw->id?>.</td>
    <td><?=$rw->nombre ?></td>
    <td>
      <? if(!empty($rw->fondo)){?>
        <img src="/thumb_200_w/images/banners/<?=$rw->fondo?>" style="max-height: 70px;max-width: 208px;" />
      <? } ?>
    </td>
    <td>
        <?=$rw->contenido1?>
    </td>
    <td>
        <?=$rw->contenido2?>
    </td>
    <td><?=($rw->estado==1)?'Activo':'Inactivo'?></td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="6">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>