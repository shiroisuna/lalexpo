<?php
include('includes/conexion.php');
include('includes/idioma.php');
include('servicios/logosPatrocinios.php');
session_start();

$rw = $con->query("select * from eventos where estado = '1'");
$rsp = $rw->fetch_object();

$fecha_evento= $rsp['fecha'];

if(!empty($_POST['setReserva'])){
  $detalle='';
  $catego_hab=$con->query("select * from hotel_categorias_hab where estado=1 and id='".$_POST['id_categoria']."'")->fetch_object();
  $info_hab=$con->query("select * from hotel_habitciones where id='".$_POST['id_habitacion']."' and id_categoria='".$_POST['id_categoria']."'")->fetch_object();
  $feEntrada=explode('/',$_POST['entrada']);
  $feSalida=explode('/',$_POST['salida']);
  $feEntrada=$feEntrada[2].'-'.$feEntrada[1].'-'.$feEntrada[0];
  $feSalida=$feSalida[2].'-'.$feSalida[1].'-'.$feSalida[0];
  $con->query("insert into hotel_reservas set
  id_usuario='".$_SESSION['id']."',
  id_categoria='".$_POST['id_categoria']."',
  id_habitacion='".$_POST['id_habitacion']."',
  entrada='".$feEntrada."',
  salida='".$feSalida."',
  contac_nombre='".$_POST['contac_nombre']."',
  contac_email='".$_POST['contac_email']."',
  contac_telef='".$_POST['contac_telef']."',
  totalPagar='".$_POST['totalPagar']."',
  personas='".$_POST['personas']."'");

  if($_GET['lg']=='es'){
    $detalle.=$catego_hab->nombre_es.'
    '.$info_hab->nombre_es.'
    '.$_POST['personas'].' personas

    Check-In: '.$_POST['entrada'].'
    Check-Out: '.$_POST['salida'].'

    Contacto:
    '.$_POST['contac_nombre'].'
    '.$_POST['contac_email'].'
    '.$_POST['contac_telef'].'

    Total: (USD) $ '.$_POST['totalPagar'].'

    Hospedados:
    ';
  } else {
    $detalle.=$catego_hab->nombre_en.'
    '.$info_hab->nombre_en.'
    '.$_POST['personas'].' people

    Check-In: '.$_POST['entrada'].'
    Check-Out: '.$_POST['salida'].'

    Contact info:
    '.$_POST['contac_nombre'].'
    '.$_POST['contac_email'].'
    '.$_POST['contac_telef'].'

    Amount: (USD) $ '.$_POST['totalPagar'].'

    Hosted:
    ';
  }

  $r=new stdClass;
  if(empty($con->error)){
    $id=$con->insert_id;
    for($i=1;$i<=$_POST['personas'];$i++){
      $con->query("insert into hotel_reservas_hospedados set
      id_reserva='".$id."',
      nombre='".$_POST['hospe_nomb_'.$i]."',
      pasaporte='".$_POST['hospe_pasaporte_'.$i]."'");
      $detalle.=$_POST['hospe_nomb_'.$i].' DNI: '.$_POST['hospe_pasaporte_'.$i].'
';
    }
    $addPartida='';
    if(!empty($_POST['partida'])){
      $pati=explode('/',$_POST['partida']);
      $partida=$pati[2].'-'.$pati[1].'-'.$pati[0].' '.$_POST['horario_partida'];
      $addPartida=" partida='".$partida."',";
    }
    $addLlegada='';
    if(!empty($_POST['llegada'])){
      $lleg=explode('/',$_POST['llegada']);
      $llegada=$lleg[2].'-'.$lleg[1].'-'.$lleg[0].' '.$_POST['horario_llegada'];
      $addLlegada=" llegada='".$llegada."',";
    }
    if($_POST['recogeAero']=='1'){
      $con->query("insert into hotel_itinerarios set
      id_reserva='".$id."',
      ".$addPartida."
      ".$addLlegada."
      llega_aerolinea='".$_POST['nomb_aerolinea_llegada']."',
      llega_vuelo='".$_POST['nomb_nrovuelo_llegada']."',
      partida_aerolinea='".$_POST['nomb_aerolinea_partida']."',
      partida_vuelo='".$_POST['nomb_nrovuelo_partida']."'");

      if($_GET['lg']=='es'){
        $detalle.='

        Aerolinea llegada: '.$_POST['nomb_aerolinea_llegada'].'
        No. vuelo llegada: '.$_POST['nomb_nrovuelo_llegada'].'
        Fecha/Hora de llegada:   '.$_POST['llegada'].' '.$_POST['horario_llegada'].'

        Aerolinea partida: '.$_POST['nomb_aerolinea_partida'].'
        No. vuelo partida: '.$_POST['nomb_nrovuelo_partida'].'
        Fecha/Hora de partida:   '.$_POST['partida'].' '.$_POST['horario_partida'].'
        ';
      } else {
        $detalle.='

        Arrival airline: '.$_POST['nomb_aerolinea_llegada'].'
        No. flight arrival: '.$_POST['nomb_nrovuelo_llegada'].'
        Arrival date:   '.$_POST['llegada'].' '.$_POST['horario_llegada'].'

        Departure airline: '.$_POST['nomb_aerolinea_partida'].'
        No. flight departure: '.$_POST['nomb_nrovuelo_partida'].'
        Departure date:   '.$_POST['partida'].' '.$_POST['horario_partida'].'
        ';
      }

    }
    $r->estado=1;
    $r->id_reserva=$id;
  }else{
    $r->estado=0;
    $r->msg=$con->error;
  }

  $text_compra=$con->query("select * from textos_email where id='detalle_reserva_hotel'")->fetch_object();
  if($rw2->lengua=='Español'){
    $tit=str_replace('%nombre%',$_SESSION['nombre'].' '.$_SESSION['apellidos'],$text_compra->titulo_es);
    $mensaje=str_replace('%nombre%',$_SESSION['nombre'].' '.$_SESSION['apellidos'],$text_compra->texto_es);
  }else{
    $tit=str_replace('%nombre%',$_SESSION['nombre'].' '.$_SESSION['apellidos'],$text_compra->titulo_en);
    $mensaje=str_replace('%nombre%',$_SESSION['nombre'].' '.$_SESSION['apellidos'],$text_compra->texto_en);
  }

  $mensaje = str_replace('%detalles%', nl2br($detalle), $mensaje);
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL,"https://lalexpo.com/test/mail2.php");
  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$_SESSION['correo']."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje)));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_exec ($ch);
  curl_close ($ch);

  echo json_encode($r);
  exit;
}
if(!empty($_POST['getHabitaciones'])){
  $rs=$con->query("SELECT * FROM hotel_habitciones WHERE id_categoria=".$_POST['id']);
  $arr=array();
  while($rw=$rs->fetch_object()){
    $arr[]=$rw;
  }
  echo json_encode($arr);
  exit;
}
$lg=new idioma($_GET['lg']);
$lg->seccion(6);
$datHotel=$con->query("select * from hotel where id=1")->fetch_object();
if($_GET['lg']=='es'){
  $img_banner='/images/hoteleria/'.$datHotel->imagen_es;
}else{
  $img_banner='/images/hoteleria/'.$datHotel->imagen_en;
}
$rs_menu=$con->query("select id,url,nombre_es,nombre_en,tipo from menu where habilitado=1 and id_seccion=1 order by orden asc");
$menus=array();
if($rs_menu)
while($rw=$rs_menu->fetch_object()){
  $url=($rw->tipo==2)?'segmento.php?id='.$rw->id:$rw->url;
  $nom=($_GET['lg']=='es')?$rw->nombre_es:$rw->nombre_en;
  $menus[]=array('url'=>$url,'nombre'=>$nom);
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?=$_GET['lg']?>" xml:lang="<?=$_GET['lg']?>" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta charset="utf-8" />
  <meta http-equiv="Content-Language" content="<?=$_GET['lg']?>"/>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <link rel="shortcut icon" href="/fav.ico" type="image/x-icon" />
  <meta content="follow, index, all" name="robots" />
  <title>LALEXPO</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
  <script src="js/jquery.nice-select.min.js"></script>
  <script src="js/inviewport.jquery.js"></script>
  <script src="js/bootstrap-formhelpers.min.js"></script>
  <script src="js/general.js"></script>
  <link rel="stylesheet" href="css/fonts.css" />
  <!--<link rel="stylesheet" href="css/slideshow.css" />-->
  <link rel="stylesheet" href="css/animate.css" />
  <link rel="stylesheet" href="css/nice-select.css" />
  <link rel="stylesheet" href="css/general.css" />
  <link rel="stylesheet" href="css/bootstrap-formhelpers.min.css"  />
  <link rel="stylesheet" href="css/flexslider.css"  />
  <link rel="stylesheet" href="css/jquery.desoslide.css"  />
  <link rel="stylesheet" href="css/demo.css"  />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="js/jquery.flexslider.js"></script>
  <script src="js/jquery.desoslide.min.js"></script>
  <script>
  _lengua='<?=$_GET['lg']?>';
  _len=<?=json_encode($lg->general)?>;
  </script>
  <script>
  $(document).ready(function(){
  $('#slideshow').desoSlide({
    thumbs: $('#slideshow_thumbs li > a'),
    auto: {
        start: true
    },
    first: 1,
    interval: 6000
})
})</script>
  <style>
  h1{
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-weight: 500;
    line-height: 1.1;
    font-size: 36px;
    color: #ffffff !important;
    border-color: #ffffff !important;
    text-shadow: 4px 4px 14px rgba(28, 28, 28, 1);
  }
  .color_bk_home_hotel_title span span {
    background-color: #EC3237 !important;
    color: #ffffff !important;
    text-shadow:none
}
  </style>
  <script>
  lenAct='<?=$_GET['lg']?>';
  lg=<?=json_encode($lg->seccion)?>
  </script>
</head>
<body itemscope itemtype="http://schema.org/WebPage">
  <div id="todo">
    <div class="topH">
      <div class="logo">
        <a href="index.php"><img src="img/logo.jpg" alt="LalExpo" /></a>
      </div>
      <div class="botones">
        <?php echo cargarLogosPatrocinadores(26); ?>
        <?php if (!isset($_SESSION['login'])) { ?>
          <a href="#" onclick="showForm('preRegistrer')"><?=$lg->general->btn_register?></a>
          <a href="#" onclick="showForm('preLogin')"><?=$lg->general->btn_login?></a>
        <?php } else { ?>
          <a href="dashboard.php" ><?=$lg->general->btn_profile?></a>
          <a href="/servicios/logout.php" ><?=$lg->general->btn_logout?></a>
        <?php } ?>
        <div class="btnsIdiomas">
          <a href="/en<?=$_SERVER['PHP_SELF']?>" title="English"><img src="img/eng.jpg" alt="English" /></a>
          <a href="/es<?=$_SERVER['PHP_SELF']?>" title="Español"><img src="img/esp.jpg" alt="Español" /></a>
        </div>
      </div>
    </div>
    <div class="mySlides fade" style="display: block;">
      <img src="<?=$img_banner?>" style="width: 100%" />
      <h1 class="color_bk_home_hotel_title title-ht-lxp" style="position: absolute; margin-left: 10%; margin-top: -80px;">
      <span class="title-span-ctg-lalexpo"><?php echo $fecha_evento; ?>
      <span class="box-span-red-ctg-lalexpo">Cali /Col</span>
      </span>
      </h1>
    </div>
<?php
function setAct($p){
  $pag=explode('/',$_SERVER['PHP_SELF']);
  $pag=$pag[count($pag)-1];
  return ($p==$pag)?'class="act"':'';
}?>
    <nav class="nav2" id="menuTop">
      <ul id="menu" class="menu">
        <? foreach($menus as $menu){?>
	     <li><a href="<?=$menu['url']?>#menu" <?php echo setAct($menu['url'])?>><?=$menu['nombre']?></a></li>
      <? }?>
      </ul>
    </nav>
<section class="sechotel">
  <?
  $estado_pag=$con->query("select habilitado from menu where id=6")->fetch_object()->habilitado;
  if($estado_pag){
  if($_GET['lg']=='es'){
    $nombre=$datHotel->nombre_es;
    $imgvuelos=$datHotel->imgvuelos_es;
    $subtit=$datHotel->subtitulo_es;
    $descripcion=$datHotel->descripcion_es;
    $adicional=$datHotel->adicional_es;
  }else{
    $nombre=$datHotel->nombre_en;
    $imgvuelos=$datHotel->imgvuelos_en;
    $subtit=$datHotel->subtitulo_en;
    $descripcion=$datHotel->descripcion_en;
    $adicional=$datHotel->adicional_en;
  }
  ?>
  <div class="txtDescript">
    <div style="float: right;">
      <img src="images/hoteleria/<?=$imgvuelos?>" style="width: 480px;cursor:pointer;" onclick="$.colorbox({href: 'images/hoteleria/<?=$imgvuelos?>', scalePhotos:true, innerWidth:'90%'});" />
    </div>
  <?=$descripcion?>
  </div>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
   <link rel="stylesheet" href="/css/location.css" />
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="/css/theme.css" />
  <link rel="stylesheet" href="/css/colorbox.css" />
  <script src="/js/moment.min.js"></script>
  <script src="/js/jquery.md5.js"></script>
  <script src="/js/jquery.colorbox.js"></script>
  <script src="/js/location.js"></script>
<script>
_user="<?php echo $_SESSION['id']?>";
function setAccion(id,nomb){
  <? if(empty($_SESSION['login'])){?>
  $('html,body').animate({scrollTop: 100}, 1000);
  showForm('preLogin')
  <? }else{?>
        setTipoHab(id,nomb)
  <? } ?>
}
</script>
  <div class="dvSubt"><?=$subtit?></div>
  <div class="nom_hotel"><?=$datHotel->titulo?></div>
    <div class="tipoHab nom_hotel" style="margin-bottom: 15px;">
    <div class="cat col-sm-15 col-md-15 col-lg-15 text-danger" style="border: 0px;
    width: 160px;
    font-size: 0.6em;">
        <i class="fa fa-bed fa-3x" style="font-size: 2em;"></i> <?=$lg->seccion->t_tipohab?>
        <div style="font-size:0.7em"><?=$lg->seccion->t_reservtuhotel?></div>
    </div>
      <? $rs=$con->query("select * from hotel_categorias_hab where estado=1");
      while($rw=$rs->fetch_object()){
        if($_GET['lg']=='es'){
          $nombre=$rw->nombre_es;
          $descri=nl2br($rw->descripcion_es);
        }else{
          $nombre=$rw->nombre_en;
          $descri=nl2br($rw->descripcion_en);
        }
        ?>
        <div class="cat">
        <div class="nom"><?=$nombre?></div>
        <div class="precio"><?=$rw->precio?> USD <?=$lg->seccion->txt_por_noche?></div>
          <a href="javascript:;" class="reservar" onclick="$.colorbox({html: '<p class=\'tex_\'><b style=\'font-size:140%;\'><?=$nombre?></b><br/><br/><span><?=$rw->precio?> USD <?=$lg->seccion->txt_por_noche?></span><br/><br/><?=$descri?></p><img src=\'images/hoteleria/<?=$rw->imagen?>\' style=\'width: 360px; float: right;\' />', width:'50%'});"><?=$lg->seccion->txt_readmore?></a>
        <? if($rw->disponibles>0){?>
          <a href="#elejir_habiltacion" class="reservar" onclick="setAccion(<?=$rw->id?>,'<?=$nombre?>');"><?=$lg->seccion->txt_habit?></a>
        <? } else {?>
          <a class="agotado"><?=$lg->seccion->txt_agotado?></a>
        <? } ?>
        </div>
      <? } ?>
  </div>
  <!-- Result 3 -->
  <style>
  #section_demo *{
   -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
  }
  .desoslide-overlay {
    bottom: 0px !important;
    top: auto !important;
  }
  #slideshow{
     flex: none;
     display: inline-block;
    width: 35%;
    margin-top: 0;
  }
  .tex_{
    font-size:2em;
    display: inline-block;
    width: 36%;
    margin-left: auto;
    margin-top: 0;
  }
  .botoness{
    flex: none;
    display: inline-block;
    margin-right: auto;
  }
  .adap{
    color:#b94a48;
    font-size:1.6em;
    padding-bottom: 10px;
  }
  #slideshow_thumbs li {
    display: inline;
    width: 120px;
  }
  </style>
  <article>
      <div class="row" id="section_demo">
          <div class="tex_ col-lg-6">
          <div class=".nom_hotel adap"><?=$datHotel->titulo?>
          <span style="color:#c09853">
          <? for($i=1;$i<=$datHotel->categoria;$i++){?>
          <i class="fa fa-star"></i>
          <? } ?>
          </span>
          </div>
          <?=$adicional?>
          </div>
          <div id="slideshow" class="col-lg-6 col-md-10"></div>
          <div class="col-lg-12 col-md-11 text-center botoness">
              <ul id="slideshow_thumbs" class="desoslide-thumbs-horizontal list-inline">
                <? $rs=$con->query("select archivo from hotel_imagenes");
                if($rs->num_rows>0){
                  while($rw=$rs->fetch_object()){?>
                    <li>
                      <a href="/images/galeria/<?=$rw->archivo?>">
                          <img src="/images/galeria/<?=$rw->archivo?>" style="max-width: 120px" />
                      </a>
                    </li>
                  <? }
                }
                ?>
              </ul>
          </div>
      </div>
  </article>
  <? }else{ ?>
<img src="/img/<?=$_GET['lg']=='es'?'proximo_hotel':'hotelcomming'?>.jpg" style="width: 100%;" />
  <? } ?>
</section>
<section id="elejir_habiltacion" style="display: none;">
  <div class="col-6 spacios" style="margin: auto;text-align: center;">
    <div class="col-6" style="margin: auto;"><?=$lg->seccion->txt_tipo_hab?> <br /><b id="tipo_hab" style="font-size:17px"></b></div>
    <div class="col-6" style="margin: auto;"><?=$lg->seccion->txt_nro_asist?>
      <select id="habitaciones" class="form-control" onchange="setNroHospe(this.value)">
      </select>
    </div>
    <br style="clear:both" />
    <div class="col-3">Check-In: <input type="text" readonly="" class="datepicker form-control" size="10" id="entrada" /></p></div>
    <div class="col-3">Check-Out: <input type="text" readonly="" class="datepicker form-control" size="10" id="salida" /></div>
    <div style="text-align: center;">
      <input type="button" class="btn btn-info" value="<?=$lg->seccion->txt_sig?>" onclick="validaDatos1()" />
    </div>
  </div>
</section>
<section id="elejir_habiltacion2"  style="display: none;">
  <div class="col-6 spacios" style="margin: auto;text-align: center; font-size:14px">
    <div class="col-6" style="margin: auto;"> <br /><b style="font-size:17px"><?=$lg->seccion->txt_dat_vuelo?></b> (<?=$lg->seccion->txt_optional?>)</div>
    <br style="clear:both" />
    <div class="col-3">
      <b><?=$lg->seccion->txt_busca_aero?></b>
      <select id="recogeAero" class="form-control" onchange="chgRecoje(this.value)">
        <option value="0">No</option>
        <option value="1"><?=$lg->seccion->txt_si?></option>
      </select>
    </div>
    <div id="dvDatosItinerario" style="display:none">
      <div class="col-3"><?=$lg->seccion->txt_fe_llegada?>: <input type="text" readonly="" class="datepicker form-control" size="10" id="llegada" /></div>
      <div class="col-3"><?=$lg->seccion->txt_fe_partida?> : <input type="text" readonly="" class="datepicker form-control" size="10" id="partida" /></p></div>
      <br style="clear:both" />
      <div class="col-3">
        <b><?=$lg->seccion->txt_hor_llegada?></b>
        <select id="horario_llegada_1" class="form-control horas">
          <? for($i=0;$i<=23;$i++){$e=$i<10?'0'.$i:$i;?>
          <option value="<?=$e?>"><?=$e?></option>
          <? } ?>
        </select>:<select id="horario_llegada_2" class="form-control horas">
          <? for($i=0;$i<=59;$i++){$e=$i<10?'0'.$i:$i;?>
          <option value="<?=$e?>"><?=$e?></option>
          <? } ?>
        </select>
      </div>
      <div class="col-3">
        <b><?=$lg->seccion->txt_hor_part?></b>
        <select id="horario_partida_1" class="form-control horas">
          <? for($i=0;$i<=23;$i++){$e=$i<10?'0'.$i:$i;?>
          <option value="<?=$e?>"><?=$e?></option>
          <? } ?>
        </select>:<select id="horario_partida_2" class="form-control horas">
          <? for($i=0;$i<=59;$i++){$e=$i<10?'0'.$i:$i;?>
          <option value="<?=$e?>"><?=$e?></option>
          <? } ?>
        </select>
      </div>
      <br style="clear:both" />
      <div class="col-3">
        <b><?=$lg->seccion->txt_nom_aero_llegada?></b>
        <input type="text" class="form-control" id="nomb_aerolinea_llegada" maxlength="100" />
      </div>
      <div class="col-3">
        <b><?=$lg->seccion->txt_nom_aero_parti?></b>
        <input type="text" class="form-control" id="nomb_aerolinea_partida" maxlength="100" />
      </div>
      <br style="clear:both" />
      <div class="col-3">
        <b><?=$lg->seccion->txt_nro_vuelo_llegada?></b>
        <input type="text" class="form-control" id="nomb_nrovuelo_llegada" maxlength="50" />
      </div>
      <div class="col-3">
        <b><?=$lg->seccion->txt_nro_vuelo_part?></b>
        <input type="text" class="form-control" id="nomb_nrovuelo_partida" maxlength="50" />
      </div>
    </div>
    <div style="text-align: center;">
    <input type="button" class="btn btn-info" value="<?=$lg->seccion->btn_volver?>" onclick="$('#elejir_habiltacion2').hide();$('#elejir_habiltacion').show()" />
      <input type="button" class="btn btn-info" value="<?=$lg->seccion->txt_sig?>" onclick="$('#elejir_habiltacion2').hide();$('#frmContact').show()" />
    </div>
  </div>
</section>
<div class="form-contacto col-6" id="frmContact" style="background: #FFF; margin: 50px auto; text-align: center; display:none">
  <div class="tit_"><?=$lg->seccion->txt_info_contact?></div>
  <div class="col-3">
    <b><?=$lg->seccion->txt_nom?></b>
    <input type="text" class="form-control" id="contac_nombre" name="contac_nombre" maxlength="70" />
  </div>
  <div class="col-3">
    <b>Email</b>
    <input type="text" class="form-control" id="contac_email" name="contac_email" maxlength="70" />
  </div>
  <div class="col-3">
    <b><?=$lg->seccion->txt_telef?></b>
    <input type="text" class="form-control" id="contac_telef" name="contac_telef" maxlength="50" />
  </div>
  <div class="col-3">
    <b><?=$lg->seccion->txt_metod_pago?></b>
    <select class="form-control">
      <option value="1">PayU</option>
    </select>
  </div>
  <div class="col-3">
    <b><?=$lg->seccion->txt_total_pagar?></b>
    <input type="text" class="form-control" style="text-align: center;" id="totalPagar" readonly="" />
  </div>
  <div class="tit_"><?=$lg->seccion->txt_info_contact_asis?></div>
  <section id="infoHosp">
  </section>
  <input type="button" class="btn btn-info" value="<?=$lg->seccion->btn_volver?>" onclick="$('#frmContact').hide();$('#elejir_habiltacion2').show()" />
  <input type="button" class="btn btn-info" value="<?=$lg->seccion->btn_guard_reserv?>" onclick="irPayu()" />
</div>
    <?php include('includes/footer.php')?>