/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.6.24 : Database - lalexpo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `lalexpo`;

/*Table structure for table `traduccion` */

DROP TABLE IF EXISTS `traduccion`;

CREATE TABLE `traduccion` (
  `id` varchar(50) NOT NULL,
  `id_seccion` int(5) unsigned DEFAULT NULL,
  `en` varchar(255) DEFAULT NULL,
  `es` varchar(255) DEFAULT NULL,
  `obs` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seccion` (`id_seccion`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `traduccion` */

insert  into `traduccion`(`id`,`id_seccion`,`en`,`es`,`obs`) values 
('btn_back',1,'Back','Atras','.'),
('btn_buy',1,'Buy','Comprar','.'),
('btn_cancel',1,'Cancel','Cancelar','boton cancelar'),
('btn_login',1,'Log In','Entrar','boton de entrar en el encabezado'),
('btn_logintobuy',1,'Login to buy','Ingresar para comprar','.'),
('btn_logout',1,'Log out','Salir','boton logout'),
('btn_pay',1,'Payment','Pagar','.'),
('btn_profile',1,'Profile','Perfil','boton perfil'),
('btn_register',1,'Sign up','Registro','boton del regisitro'),
('btn_signing',1,'Entering...','Ingresando...','.'),
('btn_soldout',1,'Sold out','Agotado','.'),
('btn_submit',1,'Submit','Enviar','boton enviar formulario'),
('btn_up',1,'Back to top','Ir al inicio','.'),
('buy_ticket_code_1',1,'If you have an invitation, purchase or access affiliate code please enter it in the next box','Si usted tiene un codigo de afiliado invitación, compra o acceso por favor ingresarlo en la siguiente casilla','.'),
('buy_ticket_code_2',1,'To confirm your attendance you must enter a code that generates the ticket and the rosette.','Para confirmar su asistencia deberá ingresar un código que genera el tiquete y la escarapela.','.'),
('buy_ticket_code_3',1,' 171/5000 Ways to get a code:  - Buy a code - Receive an affiliate code from a sponsor - Contact organizers and thus get a guest code','Formas de obtener un codigo:  - Comprar un codigo - Recibir un codigo de afiliado por parte de un patrocinador -Contactar organizadores y así conseguir un codigo invitado','.'),
('buy_ticket_confirm_code',1,'Confimr code','Confirmar codigo','.'),
('buy_ticket_descrip',1,'Please, click button below to buy or obtain the payment information for the LALEXPO code.','Por favor, pulse el siguiente botón para comprar u obtener la información de pago para el codigo de LALEXPO.','.'),
('buy_ticket_enter_code',1,'Enter code','Ingrese codigo','.'),
('buy_ticket_text_1',1,'Ticket price: COP $ 150,000 ','Tiquete acceso total: 150.000 Pesos','.'),
('buy_ticket_text_2',1,'Ticket','Tiquete de acceso','.'),
('buy_ticket_title',1,'Buy Ticket','Comprar Ticket','.'),
('community_sponsor',2,'Community Sponsor','Community Sponsor','.'),
('contact_skype',1,'Lalexpo.international','Lalexpo','.'),
('days',1,'Days','Días','.'),
('desc_login',1,'Be part of the biggest and most exciting adult business Event in Latin America','Sé parte del evento de negocios adulto más grande y emocionante  en América Latina','descripcion formulario login'),
('forgot_password_descrip',1,'Enter your email to start the recovery process','Ingrese su correo electrónico para iniciar el proceso de recuperación','.'),
('forgot_password_title',1,'Password recovery','Recuperar contraseña','.'),
('frm_contact_email',1,'Your email (Required)','Correo electrónico (Requerido)','Etiqueta campo correo formulario contacto'),
('frm_contact_message',1,'Do you have a comment?','Mensaje','Etiqueta campo mensaje formulario contacto'),
('frm_contact_name',1,'Name (Required)','Nombre (Requerido)','Etiquera campo nombre formulario contacto'),
('frm_contact_subject',1,'Subject','Asunto','Etiqueta campo asunto formulario contacto'),
('frm_contact_title',1,'Lets get in touch!','Contáctenos!','titulo formulario contacto'),
('home_galeria',1,'Gallery','Galería','.'),
('hours',1,'Hours','Horas','.'),
('loading',1,'Loading...','Cargando...','.'),
('location',1,'Location','Locacion','.'),
('login_email',1,'Email','Correo electrónico','etiqueta campo correo login'),
('login_forgot_password',1,'Forgot your password?','Olvidé mi contraseña','texto olvido contraseña login'),
('login_password',1,'Password','Contraseña','etiqueta campo contraseña'),
('login_signup',1,'Don\'t have account? Sign Up here','¿No tiene cuenta? Regístrese aquí','texto registro login'),
('media_partner',2,'Media Partner','Socio de medios','.'),
('media_sponsors',2,'Media Sponsors','Medios Patrocinadores','.'),
('media_sponsors_text',2,'great companies covering and promoting our event','grandes compañías que cubren y promueven nuestro evento','.'),
('menu_asisten',1,'Who is coming','Asistentes','.'),
('menu_home',1,'Home','Inicio','.'),
('menu_premios',1,'Awards','Premios','.'),
('menu_program',1,'Schedule','Programación','.'),
('menu_sponsor',1,'Be our sponsor','Patrocinios','.'),
('minutes',1,'Minutes','Minutos','.'),
('newsletter',1,'Subscribe to our newsletter','Suscribete a nuestro boletín','titulo boletin'),
('newsletter_hint',1,'Enter a valid email address...','Ingrese un email válido','placeholder newsletter'),
('presen_patrocin',2,'Presenting Sponsor','Presenting Sponsor','.'),
('register_category',1,'Category:','Categoria:','.'),
('register_city',1,'City:','Ciudad:','.'),
('register_country',1,'Country:','Pais:','.'),
('register_descrip',1,'Be part of the biggest and most exciting adult business Event in Latin America','Sé parte del evento de entretenimiento adulto más grande y emocionante en América Latina','.'),
('register_email',1,'Email:','Correo electrónico','.'),
('register_email2',1,'Confirm Email:','Confirme Correo electrónico:','.'),
('register_gender',1,'Gender:','Genero:','.'),
('register_id',1,'ID or Passport:','DNI / No. Pasaporte:','.'),
('register_language',1,'Language:','Idioma:','.'),
('register_lastname',1,'Lastname:','Apellidos:','.'),
('register_name',1,'Name:','Nombre:','.'),
('register_opt_category_1',1,'Model','Modelo','.'),
('register_opt_category_2',1,'Studio','Estudio','.'),
('register_opt_category_3',1,'Webmaster','Webmaster','.'),
('register_opt_category_4',1,'Other','Otro','.'),
('register_opt_gender_1',1,'Male','Masculino','.'),
('register_opt_gender_2',1,'Female','Femenino','.'),
('register_opt_gender_3',1,'Transgender','Transgénero','.'),
('register_password',1,'Password:','Contraseña:','.'),
('register_password2',1,'Confirm Password:','Confirme contraseña:','.'),
('register_phone',1,'Phone:','Telefono:','.'),
('register_state',1,'State:','Estado:','.'),
('register_title',1,'Register','Registro','.'),
('search',1,'Search...','Buscar...','texto buscar'),
('sponsor_category_1',2,'Crown Sponsors','Patrocinio Crown','.'),
('sponsor_category_2',2,'Diamond Sponsors','Patrocinio Diamond','.'),
('sponsor_category_3',2,'Platinum Sponsors','Patrocinio Platinum','.'),
('sponsor_category_4',2,'Gold Sponsors','Patrocinio Gold','.'),
('sponsor_category_5',2,'Bronze Sponsors','Patrocinio Bronze','.'),
('sponsor_descrip',3,'We invite you to be a part of this exciting Event and to increase your Company Exposure in front of an extremely Targeted Audience by becoming a corpo','Te invitamos a ser parte de este emocionante evento y a aumentar la exposición de tu empresa frente a un público extremadamente objetivo al convertirt','.'),
('sponsor_text1',2,' Attend the First, Biggest and most important Adult Industry Event of','Asista al Primer, Mayor y más importante Evento de la Industria de Adultos de América latina','.'),
('sponsor_text10',2,'Best networking and Parties','Mejores redes y fiestas','.'),
('sponsor_text11',2,'Sponsorship opportunity','Oportunidad de patrocinio','.'),
('sponsor_text12',2,'Promote y our brand','Promociona tu marca','.'),
('sponsor_text13',2,'Show of your company','Muestra de su empresa','.'),
('sponsor_text14',2,'Make new contacts','Hacer nuevos contactos','.'),
('sponsor_text15',2,'Meet your latam partners','Conoce a tus socios latam','.'),
('sponsor_text16',2,'Strengthen relationships','Fortalecer las relaciones','.'),
('sponsor_text17',2,'Create a new market','Crea un nuevo mercado','.'),
('sponsor_text18',2,'Sponsors','Patrocinadores','.'),
('sponsor_text19',2,'Ready for business','Listo para los negocios','.'),
('sponsor_text2',2,'Latin-America','Latinoamérica ','.'),
('sponsor_text3',2,'The Definitive B2B Adult show of Central and South America. Come on over to the ONLY show in South America. Meet a total New market and be part of the','El espectáculo Definitive B2B Adult de América Central y del Sur. Venga al ÚNICO show en Sudamérica. Conoce un mercado totalmente nuevo y sé parte de ','.'),
('sponsor_text4',2,'1,300 Industry Profesionals','1,300 Profesionales de la industria','.'),
('sponsor_text5',2,'An unexploited new Market','Un nuevo mercado sin explotar','.'),
('sponsor_text6',2,'Companies ready for business','Empresas listas para hacer negocios','.'),
('sponsor_text7',2,'Hundreds of Deals closed','Cientos de ofertas cerradas','.'),
('sponsor_text8',2,'An opportunity to expand','Una oportunidad para expandirse','.'),
('sponsor_text9',2,'Big workshops and seminars','Grandes talleres y seminarios','.'),
('sponsor_title',3,'SPONSOR US','PATROCINAR','.'),
('title_login',1,'Log in','Ingresar','titulo login'),
('txtavailable',1,'Available','Disponible','.'),
('txtprice',1,'Price','Precio','.'),
('txtreserved',1,'Reserved','Reservado','.'),
('txtsold',1,'Sold','Vendido','.'),
('txt_col_category',4,'Category','Categoria','.'),
('txt_col_company',4,'Company','Compañia','.'),
('txt_col_contact',4,'Contact','Contacto','.'),
('txt_col_goal',4,'Goal','Objetivo','.'),
('txt_col_name',4,'Name / Name on Badge','Nombre','.'),
('who_descrip',4,'Below is an overview of who is coming to LALEXPO. You can already start networking with them as soon as you register! Start setting up your business meetings right now!','A continuación se muestra un listado general de quién viene a LALEXPO. ¡Ya puede comenzar a trabajar en red con ellos tan pronto como se registre! ¡Comienza a programar tus reuniones de negocios ahora mismo!','.');

/*Table structure for table `traduccion_seccion` */

DROP TABLE IF EXISTS `traduccion_seccion`;

CREATE TABLE `traduccion_seccion` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `traduccion_seccion` */

insert  into `traduccion_seccion`(`id`,`nombre`) values 
(1,'Global'),
(2,'Home'),
(3,'Patrocinadores'),
(4,'Asistentes'),
(5,'Programacion');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
