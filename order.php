<?
include('includes/conexion.php');
include('includes/idioma.php');
$lg=new idioma($_GET['lg']);
$lg->seccion(7);
 include('includes/header.php');

 ?>
<style>
  .bkg{
    background-color: #2d2d2d;
    display: block;
  }
  .bkg .txt01{
    position: relative;
    vertical-align: top;
    padding-top: 4em;
  }
  .bkg .txt03{
    text-align: center;
  }
  .bkg h1{
    font-family:AspiraBlack,Verdana;
    font-size:8em;
    color:#d0a951;
    margin:0px;
    padding:0px;
    display:inline-block;
    line-height: 80%;
    text-align: center;
    letter-spacing: -0.1em;
    width: 100%;
  }
  .bkg h1 span{
    color:#FFF;
  }
  .bkg .txt03{
    font-family: AspiraLight,Verdana;
    font-size: 5em;
    color: #FFF;
    display: inline-block;
    padding: 0 9%;
  }
  .bkg .padd{
    text-align: center;
  }
  .section{
    background: #FFF;
    min-height: 500px;
  }
  section{
    display: block;
    max-width: 1233px;
    width: 85%;
    margin: auto;
    padding-top: 50px;
  }
  section header{
    background: url(img/fdo_enc_sec.jpg) left repeat-y;
    height: 108px;
    display: block;
    max-width: 1233px;
    width: 100%;
    border-radius: 25px;
    text-align: center;
    color:#FFF;
    font-family: AspiraBold,Verdana;
    font-size:5em;
    vertical-align: middle;
    padding-top: 30px;
  }
  section ul{
    list-style: none;
    display: block;
    margin: auto;
    border:0px solid #969696;
    padding:0;
    background: #FFF;
    width: 85%;
    margin-top: -30px;
    -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    border-bottom-left-radius: 40px;
    border-bottom-right-radius: 40px;
    position: relative;
  }
  section li{
    width: 100%;
    background: url('img/fdo_bottom_ofer.jpg') center bottom no-repeat;
  }
  .sinBg{
    background: none !important;
  }
  .cell{
    display: table-cell;
    vertical-align: middle;
  }
  .he02{
    width: 94%;
  }
  .he03{
    max-width: 134px;
    text-align: left;
  }
  .he03 img{
    max-width: 43px;
    width: 90%;
  }
  .tabl{
    display: table;
    width: 100%;
    color:#2d2d2d;
    height: 140px;
  }
  .descrip{
    font-family: Aspira,Verdana;
    font-size: 1.4em;
    color:#2d2d2d;
    line-height: 1.2em;
    position: relative;
  }
  .descrip div{
    position: absolute;
    top: 15px;
    right: 100px;
      -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    width: 280px;
    padding:30px
  }
  .descrip table{
    width: 100%;
  }
  .descrip td{
    height: 50px;
    vertical-align: middle;
    border-bottom: 1px solid #606060;
  }
  .descrip div a{
    width: 265px;
    padding: 10px 0;
    background: #a81d26;
    color:#FFF;
    display: inline-block;
    text-align: center;
    vertical-align: middle;
    margin: auto;
    text-decoration: none;
    margin-top: 40px;
    border-radius: 25px;
  }
  /*************************************/
  .fechas{
    text-align: right;
    padding-right: 20px;
    padding-top: 40px;
  }
  .fechas a,.fechas span{
    font-size:2em;
    text-decoration:none;
    color:#8f1526;
    font-family:Aspira,Verdana;
  }
  .fechas a:hover{
    text-decoration: underline;
  }
  .gral{
    -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    width: 90%;
    margin: auto;
    position: relative;
    background: #FFF;
  }
  .gral td{
    height: 150px;
    border-bottom: 1px solid #dddddd;
  }
  .gral table{
    width: 100%;
  }
  .gral td.txt{
    color:#000;
    font-size: 2.7em;
    font-family:Aspira,Verdana;
  }
  .awe{
    color:#821126;
    font-size:2.5em;
    font-family: AwesomeSolid,Verdana;
  }
  .dvBusc2{
    text-align: left;
    padding-left: 20px;
    font-family:Aspira,Verdana;
    color:#000;
    font-size:2.2em;
    width: 90%;
    margin: auto;
    padding-bottom: 15px;
  }
  .dvBusc2 .buscWho{
  }
  .dvBusc2 .red{
    color:#821126;
    display: inline-block;
    padding-right: 20px;
    background: url(img/fle04.jpg) no-repeat right 5px;
    cursor:pointer
  }
  .txt01 div{
    display: inline-block;
  }
  .he04,.he05{
    font-family: AspiraBlack,Verdana;
    font-size: 2em;
    color:#a91d26;
    padding:0 4%
  }
  .he05{
    font-size: 1em;
    color:#2d2d2d;
    display:inline
  }

  .fichaCat{
    display: inline-block;
    width: 251px;
    height: 167px;
    margin-right: 10px;
    background-image: url("img/cat_award.png");
    background-repeat: no-repeat;
    padding-top: 50px;
    cursor: pointer;
  }
  .tituloCat{
    display: block;
    width: 150px;
    height: 80px;
    max-height: 80px;
    overflow: hidden;
    margin-right: auto;
    margin-left: auto;
    color: #c43337;
    font-family: AspiraBlack,Verdana;
    font-size: 2.3em;
    text-align: center;
    padding-top: 10%;
  }

  .ficha {
    display: block;
    position: relative;
    width: 240px;
    height: 260px;
    float: left;
    padding:20px;
    margin:10px 20px;
    font-family:Aspira,Verdana;
    background-color: #fff;
    text-align: center;
    border-radius: 15px;
    -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
  }
  .ficha img {
    display: block;
    width: 100%;
    max-width: 180px;
    height: auto;
    max-height: 180px;
    margin-left: auto;
    margin-right: auto;
  }
  .ficha .titulo {
    display: block;
    position: absolute;
    top: 190px;
    width: 240px;
    margin-left: auto;
    margin-right: auto;
    font-size: 3em;
    font-family:AspiraBold,Verdana;
    font-weight: bold;
  }
  .ficha .precio {
    display: block;
    position: absolute;
    top: 220px;
    width: 240px;
    margin-left: auto;
    margin-right: auto;
    font-size: 2.3em;
  }
  .ficha .descrip {
    display: block;
    font-size: 1.8em;
    text-align: justify;
  }
  .ficha .boton {
    display: block;
    position: absolute;
    top: 260px;
    width: 60%;
    margin-left: 13%;
    margin-right: 13%;
    text-align: center;
    background-color: #ebc574;
    color: #333333;
    font-family:AspiraBold,Verdana;
    font-size: 2em;
    border-radius: 25px;
  }
 .boton {
    display: inline-block;
    text-align: center;
    background-color: #ebc574;
    color: #333333;
    font-family:AspiraBold,Verdana;
    font-size: 1em;
    border-radius: 25px;
    padding: 4px 10px;
  }
  .botones {
    display: block;
    width: 90%;
    text-align: center;
    margin-top: 60px;
  }
  .btncrt {
    display: inline-block;
    width: 32px;
    text-align: center;
    background-color: #ebc574;
    color: #333333;
    font-family:AspiraBold,Verdana;
    font-size: 1em;
    border-radius: 25px;
  }
  .btncrt2 {
    display: inline-block;
    width: 32px;
    text-align: center;
    background-color: #a6212f;
    color: #ffffff;
    font-family:AspiraBold,Verdana;
    font-size: 1em;
    border-radius: 25px;
  }
  .btncrt, .btncrt2, .boton {
    opacity: 0.9;
  }
  .lintot {
    font-family:AspiraBold,Verdana;
    font-size: 1.1em;
  }
  </style>
  <script src="js/jquery.colorbox.js"></script>
  <script type="text/javascript">var _obPP=[];</script>
    <div class="content">
      <div class="bkg">
      <div class="separador"></div>
      <div class="bkg" style="width:100%">

      </div>
      </div>
      <div class="section">
        <div style="margin-left: 10%; font-size: 2.4em; font-family:Aspira,Verdana; ">
            <h3>Pedido No. <?=$_SESSION['pedido']?></h3>
            <table width="90%" border="0" cellpadding="4" cellspacing="0">
              <tr bgcolor="#ebc574">
                <th align="center" width="60"><strong>Código</strong></th>
                <th align="center" width="400"><strong>Producto</strong></th>
                <th align="center" width="80"><strong>Cantidad</strong></th>
                <th align="center" width="120"><strong>Precio</strong></th>
                <th align="center" width="120"><strong>Subtotal</strong></th>
              </tr>
              <?php
                  $flpr = 0; $base=0; $items=0;
                  $rs=$con->query("SELECT vdet.*, pro.nombre_es, pro.nombre_en FROM productos_ventas_detalle vdet INNER JOIN productos pro ON pro.id=vdet.id_producto WHERE vdet.id_venta=".$_SESSION['pedido']);
                  while ($rw=$rs->fetch_object()) {
                      if ($flpr > 0) {
                        $estilo = 'bgcolor="#F0F0F0"';
                        $flpr = 0;
                      } else {
                        $estilo = 'bgcolor="#F7F7F7"';
                        $flpr = 1;
                      }
                      $nomprod=$lg->idioma=='es' ? $rw->nombre_es : $rw->nombre_en;
                      echo '<tr '.$estilo.'>
                      <td align="center">'.$rw->id_producto.'</td>
                      <td>'.$nomprod.'</td>
                      <td align="center">'.$rw->cantidad.'</td>
                      <td align="right">'.number_format($rw->valor, 2, ",", ".").'</td>
                      <td align="right">'.number_format($rw->total, 2, ",", ".").'</td>
                      </tr>';
                      $base += $rw->total;
                  }
                  
                $iva = ($base * 0.19);
                $total = $base + $iva;
              ?>
              <tr><td colspan="6"></td></tr>
              <tr><td colspan="4" align="right">Importe</td><td align="right"><?=number_format($base, 2, ",", ".")?></td></tr>
              <tr><td colspan="4" align="right">I.V.A 19%</td><td align="right"><?=number_format($iva, 2, ",", ".")?></td></tr>
              <tr class="lintot"><td colspan="4" align="right">Total</td><td align="right"><?=number_format($total, 2, ",", ".")?></td></tr>
            </table>
            <div class="botones">
              <a href="javascript:;" class="boton" onclick="document.location='store.php'" >Regresar</a>            

              <a href="javascript:;" class="boton" onclick="goPayu(_obPP)" >Pagar</a>
            </div>
        </div>
      </div>
      <script type="text/javascript">        
        var ref=<?=$_SESSION['pedido']?>;
        _obPP={
          emailComprador:"<?=$_SESSION['correo']?>",
          nombreComprador:"<?=$_SESSION['nombre'].' '.$_SESSION['apellidos']?>",
          telefonoComprador:"<?=$_SESSION['telefono']?>",
          valor:<?=$total?>,
          descripcion:"Pago Pedido No. <?=$_SESSION['pedido']?>",
          idProducto:6,
          idUsuario:<?=$_SESSION['id']?>,
          datoReferencia:ref,
          moneda:"COP"
        }
      </script>
    <? include('includes/footer.php')?>