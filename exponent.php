<?
include('includes/conexion.php');
include('includes/idioma.php');
$lg=new idioma($_GET['lg']);
$lg->seccion(11);
 include('includes/header.php')?>
  <link rel="stylesheet" href="css/theme.css" />
<style>
    .bkg{
        /*background: url(img/fdo_red01.jpg);
        background-repeat: no-repeat;
        background-size: 50% 100%;*/
        background-color: #2d2d2d;
        display: table;
    }
    .bkg .txt01{
        width: 49%;
        position: relative;
        display:table-cell;
        vertical-align: top;
        padding-top: 4em;
    }
    .bkg .txt03{
        text-align: center;
    }
  .bkg h1{
    font-family:AspiraBlack,Verdana;
    font-size:30em;
    color:#2d2d2d;
    margin:0px;
    padding:0px;
    display:inline-block;
    line-height: 63%;
    text-align: left;
    letter-spacing: -0.1em;
    width: 100%;
  }
  .bkg h1 span{
    color:#FFF;
    display: block;
    text-align: right;
  }
  .bkg .txt03{
    font-family: AspiraXWide-Medium,Verdana;
    font-size: 3em;
    color: #FFF;
    display: inline-block;
    width: 90%;
    padding-left: 10%;
    padding-bottom: 5%
  }
  .bkg .padd{
    background-color: #b42127;
    padding-left: 5%;
  }
  .section{
    background: #FFF;
    min-height: 500px;
  }
  section{
    display: block;
    max-width: 1233px;
    width: 85%;
    margin: auto;
    padding-top: 50px;
  }
  section header{
    background: url(img/fdo_enc_sec.jpg) left repeat-y;
    height: 108px;
    display: block;
    max-width: 1233px;
    width: 100%;
    border-radius: 25px;
    text-align: center;
    color:#FFF;
    font-family: AspiraBold,Verdana;
    font-size:5em;
    vertical-align: middle;
    padding-top: 30px;
  }
  section ul{
    list-style: none;
    display: block;
    margin: auto;
    border:0px solid #969696;
    padding:0;
    background: #FFF;
    width: 85%;
    margin-top: -30px;
    -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    border-bottom-left-radius: 40px;
    border-bottom-right-radius: 40px;
    position: relative;
  }
  section li{
    width: 100%;
    background: url('img/fdo_bottom_ofer.jpg') center bottom no-repeat;
  }
  .sinBg{
    background: none !important;
  }
  .cell{
    display: table-cell;
    vertical-align: middle;
  }
  .he01{
    width: 22%;
    max-width: 278px;
    text-align: center;
  }
  .he01 img{
    max-width: 167px;
    width: 90%;
  }
  .he02{
    width: 72%;
  }
  .he03{
    max-width: 134px;
    text-align: left;
  }
  .he03 img{
    max-width: 43px;
    width: 90%;
  }
  .tabl{
    display: table;
    display: table;
    width: 100%;
    font-family: AspiraBold,Verdana;
    font-size:3.9em;
    color:#2d2d2d;
    height: 140px;
  }
  .descrip{
    font-family: AspiraLight,Verdana;
    font-size: 2em;
    color:#606060;
    padding-left: 70px;
    padding-bottom: 35px;
    line-height: 1.2em;
    position: relative;
  }
  .descrip div{
    position: absolute;
    top: 15px;
    right: 100px;
      -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    width: 280px;
    padding:30px
  }
  .descrip table{
    width: 100%;
  }
  .descrip td {
    height: 50px;
    vertical-align: middle;
    border-bottom: 1px solid #606060;
  }
  .descrip div a{
    width: 265px;
    padding: 10px 0;
    background: #a81d26;
    color:#FFF;
    display: inline-block;
    text-align: center;
    vertical-align: middle;
    margin: auto;
    text-decoration: none;
    margin-top: 40px;
    border-radius: 25px;
  }
  thead tr {
    /*background: url(../img/fdo_am_01.jpg) right repeat-y #d0a951;*/
  }
  th {
    font-weight: bold;
    text-align: left;
    color: #a8212f;
    font-family: Aspira,Verdana;
    font-size: 2.8em;
    height: 50px;
    vertical-align: middle;
    border-bottom: 1px solid #606060;    
  }
  /*************************************/
  .fechas{
    text-align: right;
    padding-right: 20px;
    padding-top: 40px;
  }
  .fechas a,.fechas span{
    font-size:2em;
    text-decoration:none;
    color:#8f1526;
    font-family:Aspira,Verdana;
  }
  .fechas a:hover{
    text-decoration: underline;
  }
  .gral{
    -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    width: 90%;
    margin: auto;
    position: relative;
    background: #FFF;
  }
  .gral td{
    height: 150px;
    border-bottom: 1px solid #dddddd;
  }
  .gral table{
    width: 100%;
  }
  .gral td.txt{
    color:#000;
    font-size: 2.7em;
    font-family:Aspira,Verdana;
  }
  .awe{
    color:#821126;
    font-size:2.5em;
    font-family: AwesomeSolid,Verdana;
  }
  .dvBusc2{
    text-align: left;
    padding-left: 20px;
    font-family:Aspira,Verdana;
    color:#000;
    font-size:2.2em;
    width: 90%;
    margin: auto;
    padding-bottom: 15px;
  }
  .dvBusc2 .buscWho{
    border: 2px solid #e3bd69;
    background: #FFF;
    border-radius: 17px;
    font-size: 1.5em;
    margin-bottom: 18px;
    margin-top: 6px;
    width: 180px;
    outline: none;
    padding: 3px 15px;
  }
  .dvBusc2 .red{
    color:#821126;
    display: inline-block;
    padding-right: 20px;
    background: url(img/fle04.jpg) no-repeat right 5px;
    cursor:pointer
  }
  .txt a { color: #bb212f; }
  .txt a:hover { color: #d0a951; }
  .txt a:visited { color: #bb212f; }
  #tblExpositores {
    margin-top:60px;
    margin-left: 6%;
    margin-right: 6%;
  }
  #tblExpositores .titulo {
    display: block;
    font-size: 8em;
    font-family:AspiraBold,Verdana;
    font-weight: bold;
    text-align: center;
  }
  #tblExpositores hr {
    border: 1px solid #bb212f;
  }
  .ficha {
    display: block;
    width: 266px;
    height: 318px;
    float: left;
    padding:20px;
    margin:10px 20px;
    font-family:Aspira,Verdana;
    background-color: #ebc574;
    text-align: center;
    border-radius: 15px;
    -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
  }
  .ficha img {
    border: 4px solid #333333;
  }
  .ficha .nombexp {
    display: block;
    font-size: 3em;
    font-family:AspiraBold,Verdana;
    font-weight: bold;
  }
  .ficha .compexp {
    display: block;
    font-size: 2.3em;
  }
  .ficha .descexp {
    display: block;
    font-size: 1.8em;
    text-align: justify;
  }
  .ficha .boton {
    display: block;
    text-align: center;
    background-color: #fff;
    color: #333333;
    font-family:AspiraBold,Verdana;
    font-size: 2em;
    border-radius: 25px;
  }
  </style>
    <div class="content">
        <div class="separador"></div>
      <div class="bkg">
        <div class="txt01">
          <span class="txt03">
           <?php echo cargarBloque('speakers-descrip', $lg->idioma); ?>
          </span>
        </div>
      </div>
      <div id="tblExpositores" class="section">
          <span class="titulo"><?=$lg->general->speakers?></span>
          <hr>
          <div id="lstExpositores"></div>
      </div>
    </div>
    <script type="text/javascript">expositores();</script>
    <? include('includes/footer.php')?>