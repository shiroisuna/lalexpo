<?php
include('includes/conexion.php');
include('includes/idioma.php');
$lg=new idioma($_GET['lg']);
$lg->seccion(7);
 include('includes/header.php');
 
 ?>

<style>

.topH .botones a, .btnsIdiomas {
    display: inline-block;
    background: url(../img/fdo_am_01.jpg) right repeat-y #d0a951;
    border: solid 1px #f7cf79 !important;
    color: #FFF;
    font-size: 1.3em;
    font-family: AspiraBlack,Verdana;
    padding: 8px 20px;
    /* border: 1px; */
    margin: 0 5px;
    border-radius: 20px;
}
  .bkg{
    background-color: #2d2d2d;
    display: block;
  }
  .bkg .txt01{
    position: relative;
    vertical-align: top;
    padding-top: 4em;
  }
  .bkg .txt03{
    text-align: center;
  }
  .bkg h1{
    font-family:AspiraBlack,Verdana;
    font-size:8em;
    color:#d0a951;
    margin:0px;
    padding:0px;
    display:inline-block;
    line-height: 80%;
    text-align: center;
    letter-spacing: -0.1em;
    width: 100%;
  }
  .bkg h1 span{
    color:#FFF;
  }
  .bkg .txt03{
    font-family: AspiraLight,Verdana;
    font-size: 5em;
    color: #FFF;
    display: inline-block;
    padding: 0 9%;
  }
  .bkg .padd{
    text-align: center;
  }
  .section{
    background: #FFF;
    min-height: 500px;
  }
  section{
    display: block;
    max-width: 1233px;
    width: 85%;
    margin: auto;
    padding-top: 50px;
  }
  section header{
    background: url(img/fdo_enc_sec.jpg) left repeat-y;
    height: 108px;
    display: block;
    max-width: 1233px;
    width: 100%;
    border-radius: 25px;
    text-align: center;
    color:#FFF;
    font-family: AspiraBold,Verdana;
    font-size:5em;
    vertical-align: middle;
    padding-top: 30px;
  }
  section ul{
    list-style: none;
    display: block;
    margin: auto;
    border:0px solid #969696;
    padding:0;
    background: #FFF;
    width: 85%;
    margin-top: -30px;
    -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    border-bottom-left-radius: 40px;
    border-bottom-right-radius: 40px;
    position: relative;
  }
  section li{
    width: 100%;
    background: url('img/fdo_bottom_ofer.jpg') center bottom no-repeat;
  }
  .sinBg{
    background: none !important;
  }
  .cell{
    display: table-cell;
    vertical-align: middle;
  }
  .he02{
    width: 94%;
  }
  .he03{
    max-width: 134px;
    text-align: left;
  }
  .he03 img{
    max-width: 43px;
    width: 90%;
  }
  .tabl{
    display: table;
    width: 100%;
    color:#2d2d2d;
    height: 140px;
  }
  .descrip{
    font-family: Aspira,Verdana;
    font-size: 1.4em;
    color:#2d2d2d;
    line-height: 1.2em;
    position: relative;
  }
  .descrip div{
    position: absolute;
    top: 15px;
    right: 100px;
      -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    width: 280px;
    padding:30px
  }
  .descrip table{
    width: 100%;
  }
  .descrip td{
    height: 50px;
    vertical-align: middle;
    border-bottom: 1px solid #606060;
  }
  .descrip div a{
    width: 265px;
    padding: 10px 0;
    background: #a81d26;
    color:#FFF;
    display: inline-block;
    text-align: center;
    vertical-align: middle;
    margin: auto;
    text-decoration: none;
    margin-top: 40px;
    border-radius: 25px;
  }
  /*************************************/
  .fechas{
    text-align: right;
    padding-right: 20px;
    padding-top: 40px;
  }
  .fechas a,.fechas span{
    font-size:2em;
    text-decoration:none;
    color:#8f1526;
    font-family:Aspira,Verdana;
  }
  .fechas a:hover{
    text-decoration: underline;
  }
  .gral{
    -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    width: 90%;
    margin: auto;
    position: relative;
    background: #FFF;
  }
  .gral td{
    height: 150px;
    border-bottom: 1px solid #dddddd;
  }
  .gral table{
    width: 100%;
  }
  .gral td.txt{
    color:#000;
    font-size: 2.7em;
    font-family:Aspira,Verdana;
  }
  .awe{
    color:#821126;
    font-size:2.5em;
    font-family: AwesomeSolid,Verdana;
  }
  .dvBusc2{
    text-align: left;
    padding-left: 20px;
    font-family:Aspira,Verdana;
    color:#000;
    font-size:2.2em;
    width: 90%;
    margin: auto;
    padding-bottom: 15px;
  }
  .dvBusc2 .buscWho{
  }
  .dvBusc2 .red{
    color:#821126;
    display: inline-block;
    padding-right: 20px;
    background: url(img/fle04.jpg) no-repeat right 5px;
    cursor:pointer
  }
  .txt01 div{
    display: inline-block;
  }
  .he04,.he05{
    font-family: AspiraBlack,Verdana;
    font-size: 2em;
    color:#a91d26;
    padding:0 4%
  }
  .he05{
    font-size: 1em;
    color:#2d2d2d;
    display:inline
  }

  .fichaCat{
    display: inline-block;
    width: 251px;
    height: 167px;
    margin-right: 10px;
    background-image: url("img/cat_award.png");
    background-repeat: no-repeat;
    padding-top: 50px;
    cursor: pointer;
  }
  .tituloCat{
    display: block;
    width: 150px;
    height: 80px;
    max-height: 80px;
    overflow: hidden;
    margin-right: auto;
    margin-left: auto;
    color: #c43337;
    font-family: AspiraBlack,Verdana;
    font-size: 2.3em;
    text-align: center;
    padding-top: 10%;
  }

  .ficha {
    display: inline-block;
    position: relative;
    width: 90%;  
    padding:20px;
    margin:10px 20px;
    font-family:Aspira,Verdana;
    background-color: #fff;
    text-align: center;
    border-radius: 15px;
    -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    cursor: pointer;
  }
.ficha img {
    display: block;
    margin-top: 41px;
    width: 100%;
    max-width: 200px;
    height: auto;
    max-height: 200px;
    margin-right: auto;
}
  .ficha .titulo {
    display: block;
    position: absolute;
    top: -6px;
    right: 1px;
    width: 70%;
    text-align: left;
    font-size: 3em;
    font-family: AspiraBold,Verdana;
    font-weight: bold;
}
.ficha .precio {
    display: block;
    position: absolute;
    top: 157px;
    width: 70%;
    right: 1px;
    text-align: right;
    font-size: 2.3em;
}

.ficha .descip {
    /* display: block; */
    position: absolute;
    top: 10px;
    right: 1px;
    width: 90%;
    text-align: right;
    font-size: 2em;
    font-family: Aspira,Verdana;
    font-weight: bold;
}
.ficha .talla  {
    /* display: block; */
    position: absolute;
    top: 49px;
    right: 1px;
    width: 90%;
    text-align: right;
    font-size: 2em;
    font-family: Aspira,Verdana;
    font-weight: bold;
}

.ficha .color {
	position: absolute;
    top: 49px;
    right: 204px;
    width: 90%;
    text-align: right;
    font-size: 2em;
    font-family: Aspira,Verdana;
    font-weight: bold;
}


.ficha .boton {
    display: block;
    position: absolute;
    top: 221px;
    width: 200px;
    right: 39%;
    text-align: center;
    background-color: #ebc574;
    color: #333333;
    font-family: AspiraBold,Verdana;
    font-size: 2em;
    border-radius: 25px;
}

 .boton {
    display: inline-block;
    text-align: center;
    color: #333333;
    font-family:AspiraBold,Verdana;
    font-size: 2.4em;
    padding: 4px 10px;

  }
 .boton .act{
    background-color: #a6212f;
    color: #ffffff;
  }
  .botones {
    display: block;
    width: 90%;
    text-align: left; 
    margin-top: 30px;  
  }

  .contador .grupo span {
    display: block;
    font-size: 2em;
    letter-spacing: -0.1em;
    background: -webkit-linear-gradient(#fcd88a, #6f5416);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
}

.contador .grupo label {
    display: block;
    height: 62px;
    /*padding-top: 10px;*/
    margin-top: -30px;
    font-size: 1em;
    background: -webkit-linear-gradient(#fcd88a, #6f5416);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
}

nav {
    text-align: center;
    padding: 1em 0 0em 0;
    background: #2d2d2d;
    position: relative;
    z-index: 2;
}

.redes2 span {
	padding-top:0 !important;
	height: 43px;

}

.section01 a, .botLink {
    
    font-size: 1em;
    
}

.section10 h1 {
	font-size: 2.3em;
}

.section10 .cell span {
	font-size: 1.3em;
}

.section10 ._subtit{
	font-size: 1.7em;
}

.section10 .derr div p {
    display: inline-block;
    font-size: 1.5em;
}

footer .tit_news {
    
    font-size: 1em;
    
}

.dvImpNews input {
    color: #c39b3e;
    font-family: AspiraLight, Verdana;
    font-size: 1em;
    border: 0px;
    outline: none;
}

.dvImpNews ._c a {
    font-family: AspiraBoldIt,Verdana;
    font-size: 1em;
    color: #FFF !important;
    cursor: pointer;
}
  </style>
  <script src="js/jquery.colorbox.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <div class="content">
      <div class="bkg">
      <div class="separador"></div>
      <div class="bkg" style="width:100%">
      </div>
      </div>
      <div class="section">        
        <?php
          include('servicios/articulo.php'); 
        ?>
      </div>
    <?php include('includes/footer.php')?>