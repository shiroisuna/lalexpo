<?php
include('includes/conexion.php');
include('includes/idioma.php');
session_start();
if(!empty($_POST['setReserva'])){
  $feEntrada=explode('/',$_POST['entrada']);
  $feSalida=explode('/',$_POST['salida']);
  $feEntrada=$feEntrada[2].'-'.$feEntrada[1].'-'.$feEntrada[0];
  $feSalida=$feSalida[2].'-'.$feSalida[1].'-'.$feSalida[0];
  $con->query("insert into hotel_reservas set
  id_usuario='".$_SESSION['id']."',
  id_categoria='".$_POST['id_categoria']."',
  id_habitacion='".$_POST['id_habitacion']."',
  entrada='".$feEntrada."',
  salida='".$feSalida."',
  contac_nombre='".$_POST['contac_nombre']."',
  contac_email='".$_POST['contac_email']."',
  contac_telef='".$_POST['contac_telef']."',
  totalPagar='".$_POST['totalPagar']."',
  personas='".$_POST['personas']."'");
  $r=new stdClass;
  if(empty($con->error)){
    $id=$con->insert_id;
    for($i=1;$i<=$_POST['personas'];$i++){
      $con->query("insert into hotel_reservas_hospedados set
      id_reserva='".$id."',
      nombre='".$_POST['hospe_nomb_'.$i]."',
      pasaporte='".$_POST['hospe_pasaporte_'.$i]."'");
    }
    $addPartida='';
    if(!empty($_POST['partida'])){
      $pati=explode('/',$_POST['partida']);
      $partida=$pati[2].'-'.$pati[1].'-'.$pati[0].' '.$_POST['horario_partida'];
      $addPartida=" partida='".$partida."',";
    }
    $addLlegada='';
    if(!empty($_POST['llegada'])){
      $lleg=explode('/',$_POST['llegada']);
      $llegada=$lleg[2].'-'.$lleg[1].'-'.$lleg[0].' '.$_POST['horario_llegada'];
      $addLlegada=" llegada='".$llegada."',";
    }
    if($_POST['recogeAero']=='1'){
      $con->query("insert into hotel_itinerarios set
      id_reserva='".$id."',
      ".$addPartida."
      ".$addLlegada."
      llega_aerolinea='".$_POST['nomb_aerolinea_llegada']."',
      llega_vuelo='".$_POST['nomb_nrovuelo_llegada']."',
      partida_aerolinea='".$_POST['nomb_aerolinea_partida']."',
      partida_vuelo='".$_POST['nomb_nrovuelo_partida']."'");
    }
    $r->estado=1;
    $r->id_reserva=$id;
  }else{
    $r->estado=0;
    $r->msg=$con->error;
  }
  echo json_encode($r);
  exit;
}
if(!empty($_POST['getHabitaciones'])){
  $rs=$con->query("SELECT * FROM hotel_habitciones WHERE id_categoria=".$_POST['id']);
  $arr=array();
  while($rw=$rs->fetch_object()){
    $arr[]=$rw;
  }
  echo json_encode($arr);
  exit;
}
$lg=new idioma($_GET['lg']);
$lg->seccion(6);
$datHotel=$con->query("select * from hotel where id=1")->fetch_object();
if($_GET['lg']=='es'){
  $img_banner='/images/hoteleria/'.$datHotel->imagen_es;
}else{
  $img_banner='/images/hoteleria/'.$datHotel->imagen_en;
}
$rs_menu=$con->query("select id,url,nombre_es,nombre_en,tipo from menu where habilitado=1 order by orden asc");
$menus=array();
if($rs_menu)
while($rw=$rs_menu->fetch_object()){
  $url=($rw->tipo==2)?'segmento.php?id='.$rw->id:$rw->url;
  $nom=($_GET['lg']=='es')?$rw->nombre_es:$rw->nombre_en;
  $menus[]=array('url'=>$url,'nombre'=>$nom);
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?=$_GET['lg']?>" xml:lang="<?=$_GET['lg']?>" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta charset="utf-8" />
  <meta http-equiv="Content-Language" content="<?=$_GET['lg']?>"/>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <link rel="shortcut icon" href="favicon2.ico" type="image/x-icon" />
  <meta content="follow, index, all" name="robots" />
  <title>LALEXPO</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
  <script src="js/jquery.nice-select.min.js"></script>
  <script src="js/inviewport.jquery.js"></script>
  <script src="js/bootstrap-formhelpers.min.js"></script>
  <script src="js/general.js"></script>
  <link rel="stylesheet" href="css/fonts.css" />
  <link rel="stylesheet" href="css/slideshow.css" />
  <link rel="stylesheet" href="css/animate.css" />
  <link rel="stylesheet" href="css/nice-select.css" />
  <link rel="stylesheet" href="css/general.css" />
  <link rel="stylesheet" href="css/bootstrap-formhelpers.min.css"  />
  <link rel="stylesheet" href="css/flexslider.css"  />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="js/jquery.flexslider.js"></script>
  <style>
  h1{
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-weight: 500;
    line-height: 1.1;
    font-size: 36px;
    color: #ffffff !important;
    border-color: #ffffff !important;
    text-shadow: 4px 4px 14px rgba(28, 28, 28, 1);
  }
  .color_bk_home_hotel_title span span {
    background-color: #EC3237 !important;
    color: #ffffff !important;
    text-shadow:none
}
  </style>
</head>
<body itemscope itemtype="http://schema.org/WebPage">
  <div id="todo">
    <div class="topH">
      <div class="logo">
        <a href="index.php"><img src="img/logo.jpg" alt="LalExpo" /></a>
      </div>
      <div class="botones">
        <img src="img/registerBy.jpg" class="logotemp" />
        <?php if (!isset($_SESSION['login'])) { ?>
          <a href="#" onclick="showForm('preRegistrer')"><?=$lg->general->btn_register?></a>
          <a href="#" onclick="showForm('preLogin')"><?=$lg->general->btn_login?></a>
        <?php } else { ?>
          <a href="dashboard.php" ><?=$lg->general->btn_profile?></a>
          <a href="index.php?logout=1" ><?=$lg->general->btn_logout?></a>
        <?php } ?>
        <div class="btnsIdiomas">
          <a href="/en/" title="English"><img src="img/eng.jpg" alt="English" /></a>
          <a href="/es/" title="Español"><img src="img/esp.jpg" alt="Español" /></a>
        </div>
      </div>
    </div>
    <div class="mySlides fade" style="display: block;">
      <img src="<?=$img_banner?>" style="width: 100%" />
      <h1 class="color_bk_home_hotel_title title-ht-lxp" style="position: absolute; margin-left: 10%; margin-top: -80px;">
      <span class="title-span-ctg-lalexpo">18-20/FEB/2019
      <span class="box-span-red-ctg-lalexpo">Cali /Col</span>
      </span>
      </h1>
    </div>
<?php
function setAct($p){
  $pag=explode('/',$_SERVER['PHP_SELF']);
  $pag=$pag[count($pag)-1];
  return ($p==$pag)?'class="act"':'';
}?>
    <nav class="nav2">
      <?
      foreach($menus as $menu){?>
      <a href="<?=$menu['url']?>" <?php echo setAct($menu['url'])?>><?=$menu['nombre']?></a>
      <? }?>
    </nav>
<section class="sechotel">
  <?
  if($_GET['lg']=='es'){
    $nombre=$datHotel->nombre_es;
    $imgvuelos=$datHotel->imgvuelos_es;
    $subtit=$datHotel->subtitulo_es;
    $descripcion=$datHotel->descripcion_es;
    $adicional=$datHotel->adicional_es;
  }else{
    $nombre=$datHotel->nombre_en;
    $imgvuelos=$datHotel->imgvuelos_en;
    $subtit=$datHotel->subtitulo_en;
    $descripcion=$datHotel->descripcion_es;
    $adicional=$datHotel->adicional_en;
  }
  ?>
  <div class="txtDescript">
    <div style="float: right;">
      <img src="images/hoteleria/<?=$imgvuelos?>" style="width: 480px;" />
    </div>
  <?=$descripcion?>
  </div>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
   <link rel="stylesheet" href="/css/location.css" />
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="/css/theme.css" />
  <script src="/js/moment.min.js"></script>
  <script src="/js/jquery.md5.js"></script>
  <script src="/js/location.js"></script>
<script>
_user="<?php echo $_SESSION['id']?>";
function setAccion(id,nomb){
  <? if(empty($_SESSION['login'])){?>
  $('html,body').animate({scrollTop: 100}, 1000);
  showForm('preLogin')
  <? }else{?>
        setTipoHab(id,nomb)
  <? } ?>
}
</script>
  <div class="dvSubt"><?=$subtit?></div>
  <div class="nom_hotel"><?=$datHotel->titulo?></div>
    <div class="tipoHab">
    <div class="cat" style="border: 0px;">
        <div class="nom"><?=$nombre?></div>
        <div class="precio"><?=$rw->precio?> USD por noche</div>
        <? if($rw->disponibles>0){?>
          <a class="reservar">Reserve esta habitaci&oacute;n</a>
        <? } else {?>
          <a class="agotado">Agorado</a>
        <? } ?>
        </div>
      <? $rs=$con->query("select * from hotel_categorias_hab where estado=1");
      while($rw=$rs->fetch_object()){
        if($_GET['lg']=='es'){
          $nombre=$rw->nombre_es;
        }else{
          $nombre=$rw->nombre_en;
        }
        ?>
        <div class="cat">
        <div class="nom"><?=$nombre?></div>
        <div class="precio"><?=$rw->precio?> USD por noche</div>
        <? if($rw->disponibles>0){?>
          <a class="reservar" onclick="setAccion(<?=$rw->id?>,'<?=$nombre?>');">Reserve esta habitaci&oacute;n</a>
        <? } else {?>
          <a class="agotado">Agorado</a>
        <? } ?>
        </div>
      <? } ?>
  </div>
</section>
<section id="elejir_habiltacion" style="display: none;">
  <div class="col-6 spacios" style="margin: auto;text-align: center;">
    <div class="col-6" style="margin: auto;">Tipo de habitación: <br /><b id="tipo_hab" style="font-size:17px"></b></div>
    <div class="col-6" style="margin: auto;">Nro. de asistentes:
      <select id="habitaciones" class="form-control" onchange="setNroHospe(this.value)">
      </select>
    </div>
    <br style="clear:both" />
    <div class="col-3">Check-In de : <input type="text" readonly="" class="datepicker form-control" size="10" id="entrada" /></p></div>
    <div class="col-3">Check-Out: <input type="text" readonly="" class="datepicker form-control" size="10" id="salida" /></div>
    <div style="text-align: center;">
      <input type="button" class="btn btn-info" value="Siguiente" onclick="validaDatos1()" />
    </div>
  </div>
</section>
<section id="elejir_habiltacion2"  style="display: none;">
  <div class="col-6 spacios" style="margin: auto;text-align: center; font-size:14px">
    <div class="col-6" style="margin: auto;"> <br /><b style="font-size:17px">Datos de su vuelvo</b> (opcional)</div>
    <br style="clear:both" />
    <div class="col-3">
      <b>Me gustaria que me recojan en el aeropuerto</b>
      <select id="recogeAero" class="form-control" onchange="chgRecoje(this.value)">
        <option value="0">No</option>
        <option value="1">Si</option>
      </select>
    </div>
    <div id="dvDatosItinerario" style="display:none">
      <div class="col-3">Fecha de partida : <input type="text" readonly="" class="datepicker form-control" size="10" id="partida" /></p></div>
      <div class="col-3">Fecha de llegada: <input type="text" readonly="" class="datepicker form-control" size="10" id="llegada" /></div>
      <br style="clear:both" />
      <div class="col-3">
        <b>Horario de partida</b>
        <select id="horario_partida_1" class="form-control horas">
          <? for($i=0;$i<=23;$i++){$e=$i<10?'0'.$i:$i;?>
          <option value="<?=$e?>"><?=$e?></option>
          <? } ?>
        </select>:<select id="horario_partida_2" class="form-control horas">
          <? for($i=0;$i<=59;$i++){$e=$i<10?'0'.$i:$i;?>
          <option value="<?=$e?>"><?=$e?></option>
          <? } ?>
        </select>
      </div>
      <div class="col-3">
        <b>Horario de llegada</b>
        <select id="horario_llegada_1" class="form-control horas">
          <? for($i=0;$i<=23;$i++){$e=$i<10?'0'.$i:$i;?>
          <option value="<?=$e?>"><?=$e?></option>
          <? } ?>
        </select>:<select id="horario_llegada_2" class="form-control horas">
          <? for($i=0;$i<=59;$i++){$e=$i<10?'0'.$i:$i;?>
          <option value="<?=$e?>"><?=$e?></option>
          <? } ?>
        </select>
      </div>
      <br style="clear:both" />
      <div class="col-3">
        <b>Nombre Aerolinea (partida)</b>
        <input type="text" class="form-control" id="nomb_aerolinea_partida" maxlength="100" />
      </div>
      <div class="col-3">
        <b>Nombre Aerolinea (llegada)</b>
        <input type="text" class="form-control" id="nomb_aerolinea_llegada" maxlength="100" />
      </div>
      <br style="clear:both" />
      <div class="col-3">
        <b>Nro vuelo (partida)</b>
        <input type="text" class="form-control" id="nomb_nrovuelo_partida" maxlength="50" />
      </div>
      <div class="col-3">
        <b>Nro vuelo (llegada)</b>
        <input type="text" class="form-control" id="nomb_nrovuelo_llegada" maxlength="50" />
      </div>
    </div>
    <div style="text-align: center;">
    <input type="button" class="btn btn-info" value="Volver" onclick="$('#elejir_habiltacion2').hide();$('#elejir_habiltacion').show()" />
      <input type="button" class="btn btn-info" value="Siguiente" onclick="$('#elejir_habiltacion2').hide();$('#frmContact').show()" />
    </div>
  </div>
</section>
<div class="form-contacto col-6" id="frmContact" style="background: #FFF; margin: 50px auto; text-align: center; display:none">
  <div class="tit_">Información para contacto</div>
  <div class="col-3">
    <b>Nombre</b>
    <input type="text" class="form-control" id="contac_nombre" name="contac_nombre" maxlength="70" />
  </div>
  <div class="col-3">
    <b>Correo</b>
    <input type="text" class="form-control" id="contac_email" name="contac_email" maxlength="70" />
  </div>
  <div class="col-3">
    <b>Telefono</b>
    <input type="text" class="form-control" id="contac_telef" name="contac_telef" maxlength="50" />
  </div>
  <div class="col-3">
    <b>Metodo de pago</b>
    <select class="form-control">
      <option value="1">PayU</option>
    </select>
  </div>
  <div class="col-3">
    <b>Total a pagar</b>
    <input type="text" class="form-control" style="text-align: center;" id="totalPagar" readonly="" />
  </div>
  <div class="tit_">Información de los asistentes hospedados</div>
  <section id="infoHosp">
  </section>
  <input type="button" class="btn btn-info" value="Volver" onclick="$('#frmContact').hide();$('#elejir_habiltacion2').show()" />
  <input type="button" class="btn btn-info" value="Guarda reserva y confirmar pago" onclick="irPayu()" />
</div>
    <?php include('includes/footer.php')?>