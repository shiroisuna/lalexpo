<?
include("includes/conexion.php");
$rsProv=$con->query("SELECT * from provincias");
if(!empty($_POST["razonsocial"])){
  $id=(int)$_GET["id"];
  $cuit=(int)$_POST["cuit"];
  $razonsocial=addslashes($_POST["razonsocial"]);
  $direccion=addslashes($_POST["direccion"]);
  $localidad=addslashes($_POST["localidad"]);
  $provincia=(int)$_POST["provincia"];
  $telefono=addslashes($_POST["telefono"]);
  $celular=addslashes($_POST["celular"]);
  $mail=addslashes($_POST["mail"]);
  if(!empty($id)){
    $con->query("UPDATE empresas SET cuit='".$cuit."',
    razonsocial='".$razonsocial."',
    direccion='".$direccion."',
    localidad='".$localidad."',
    provincia='".$provincia."',
    telefono='".$telefono."',
    celular='".$celular."',
    mail='".$mail."' WHERE id=".$id);
  }else{
    $con->query("INSERT INTO empresas SET cuit='".$cuit."',
    razonsocial='".$razonsocial."',
    direccion='".$direccion."',
    localidad='".$localidad."',
    provincia='".$provincia."',
    telefono='".$telefono."',
    celular='".$celular."',
    mail='".$mail."'");
    $_GET["id"]=$con->insert_id;
  }
  if(empty($con->error)){
    $con->query("DELETE FROM empresas_forms WHERE id_empresa=".((int)$_GET["id"]));
    if(is_array($_POST['formularios'])){
      foreach($_POST['formularios'] as $k=>$v){
        $con->query("INSERT INTO empresas_forms SET
        id_empresa='".((int)$_GET["id"])."',
        id_forms='".$v."'");
      }
    }
    echo 1;
  }else{
    echo $con->error;
  }
  exit;
}
if(!empty($_GET["id"])){
  $datos=$con->query("SELECT * from empresas where id=".((int)$_GET["id"]))->fetch_object();
  $f_emp=array();
  $rs=$con->query("SELECT id_forms FROM empresas_forms WHERE id_empresa=".((int)$_GET["id"]));
  while($rw=$rs->fetch_object()){
    $f_emp[$rw->id_forms]=1;
  }
}
$form_tipo=$con->query("SELECT * from forms_tipo");
include("includes/header.php");
#include("includes/top.php");
include("includes/menu.php")?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ":"Nueva"?> Empresa</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item"><a href="empresas.php">Empresas</a></li>
              <li class="breadcrumb-item active"><?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ":"Nueva"?> Empresa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <style>
    .col-5{    display: inline-block;}
    </style>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6" style="margin: auto;">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ":"Nueva"?> Empresa</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <? if(!empty($msg)){?>
              <span style="color: green;"><?=$msg?></span>
              <?}?>
              <? if(!empty($error)){?>
              <span style="color: red;"><?=$error?></span>
              <?}?>
                <form role="form"
                id="form"
                class="form-horizontal"
                method="post"
                onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'empresas.php'})"
                action="<?=$_SERVER["REQUEST_URI"]?>">
                  <!-- text input -->
                  <div class="col-5">
                    <label>CUIT</label>
                    <input type="text" class="form-control" name="cuit" value="<?=$datos->cuit?>" />
                  </div>
                  <div class="col-5">
                    <label>Razon Social</label>
                    <input type="text" class="form-control" name="razonsocial" value="<?=$datos->razonsocial?>" required="" />
                  </div>
                  <div class="col-5">
                    <label>Direcci&oacute;n</label>
                    <input type="text" class="form-control" name="direccion" value="<?=$datos->direccion?>" />
                  </div>
                  <div class="col-5">
                    <label>Localidad</label>
                    <input type="text" class="form-control" name="localidad" value="<?=$datos->localidad?>" />
                  </div>
                  <div class="col-5">
                    <label>Provincia</label>
                    <select class="form-control" name="provincia">
                    <? while($prov=$rsProv->fetch_object()){
                      $add=$prov->id==$datos->provincia?'selected="selected"':"";
                      echo '<option value="'.$prov->id.'" '.$add.'>'.$prov->nombre."</option>";
                      }?>
                    </select>
                  </div>
                  <div class="col-5">
                    <label>Tel&eacute;fono</label>
                    <input type="text" class="form-control" name="telefono" value="<?=$datos->telefono?>" />
                  </div>
                  <div class="col-5">
                    <label>Celular</label>
                    <input type="text" class="form-control" name="celular" value="<?=$datos->celular?>" />
                  </div>
                  <div class="col-5">
                    <label>Email</label>
                    <input type="text" class="form-control" name="email" value="<?=$datos->email?>" />
                  </div>
                  <br /><br /><br />
                  <? if($form_tipo->num_rows>0){?>
                  <div class="col-5">
                    <label>Tipos de formulario:</label>
                    <? while($rw=$form_tipo->fetch_object()){?>
                    <br /><label style="font-weight:normal;cursor:pointer;"><input type="checkbox" name="formularios[]" <?=(isset($f_emp[$rw->id]))?'checked':''?> value="<?=$rw->id?>" /><?=$rw->nombre?></label>
                    <? } ?>
                  </div>
                  <? } ?>
                  <div class="card-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>
                    <a type="submit" class="btn btn-default float-right" href="empresas.php">Volver</a>
                  </div>
                </form>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include("includes/footer.php")?>