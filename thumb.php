<?
//var_dump($_GET);
$file = $_GET["file"];
$width = $_GET["width"];

// Ponemos el . antes del nombre del archivo porque estamos considerando que la ruta está a partir del archivo thumb.php
$file_info = getimagesize($file);
// Obtenemos la relación de aspecto
$ratio = $file_info[0] / $file_info[1];

// Calculamos las nuevas dimensiones
$newwidth = $width;
$newheight = round($newwidth / $ratio);

if($newwidth>$file_info[0]){
  $newwidth=$file_info[0];
  $newheight=$file_info[1];
}

// Sacamos la extensión del archivo
$ext = explode(".", $file);
$ext = strtolower($ext[count($ext) - 1]);
if ($ext == "jpeg") $ext = "jpg";

// Dependiendo de la extensión llamamos a distintas funciones
switch ($ext) {
  case "jpg":
  $img = imagecreatefromjpeg($file);
  break;
  case "png":
  $img = imagecreatefrompng($file);
  break;
  case "gif":
  $img = imagecreatefromgif($file);
  break;
}
// Creamos la miniatura
$thumb = imagecreatetruecolor($newwidth, $newheight);
if($ext=='png' || $ext=='gif'){
  imagesavealpha($thumb, true);
  $alpha = imagecolorallocatealpha($thumb, 0, 0, 0, 127);
  imagefill($thumb, 0, 0, $alpha);
}
// La redimensionamos
  imagecopyresampled($thumb, $img, 0, 0, 0, 0, $newwidth, $newheight, $file_info[0], $file_info[1]);
// La mostramos como jpg

/*header('HTTP/1.1 304 Not Modified', true);*/
$dias_to_cache = 800*60*60*24;
$ts = gmdate("D, d M Y H:i:s", time() + $dias_to_cache) . " GMT";
header("Expires: $ts", true);
header("Pragma: cache", true);
header("Cache-Control: max-age=$dias_to_cache", true);
/*var_dump($_GET);
var_dump($thumb);*/
switch ($ext) {
  case "jpg":
  header("Content-type: image/jpeg");
  $img = imagecreatefromjpeg($file);
  imagejpeg($thumb, null, 100);
  break;
  case "png":
  header("Content-type: image/png");
  imagepng($thumb, null, 9);
  break;
  case "gif":
  header("Content-type: image/gif");
  $img = imagecreatefromgif($file);
  imagegif($thumb);
  break;
}
imagedestroy($thumb);
