<?
$sec=new secciones();
$cat=new stdClass;
$cat->id=1;
$cat->nombre='Administrativo';
$cat->classIco='nav-icon fa fa-th';
$sec->addCategoria($cat);
$cat=new stdClass;
$cat->id=2;
$cat->nombre='Contenido';
$cat->classIco='fas fa-server';
$sec->addCategoria($cat);
$cat=new stdClass;
$cat->id=3;
$cat->nombre='Extras';
$cat->classIco='fas fa-chart-bar';
$sec->addCategoria($cat);
/*$cat=new stdClass;
$cat->id=4;
$cat->nombre='Paquetes';
$cat->classIco='fas fa-chart-bar';
$sec->addCategoria($cat);*/
$cat=new stdClass;
$cat->id=5;
$cat->nombre='Hoteleria';
$cat->classIco='far fa-building';
$sec->addCategoria($cat);
$obj=new stdClass;
$obj->id=1;
$obj->categoria=1;
$obj->nombre='Traducciones';
$obj->nuevo='Nueva traduccion';
$obj->classIco='fas fa-globe-americas';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM traduccion';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=2;
$obj->categoria=1;
$obj->nombre='Usuarios Front';
$obj->nuevo='Nuevo usuario';
$obj->classIco='fas fa-user-friends';
$obj->selectList='SELECT * FROM usuarios';
$obj->enConstruccion=1;
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=3;
$obj->categoria=1;
$obj->nombre='Usuarios Back';
$obj->nuevo='Nuevo usuario';
$obj->classIco='fas fa-user-tie';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM usuarios_back';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=10;
$obj->categoria=2;
$obj->nombre='Menu';
$obj->nuevo='Nuevo menu';
$obj->classIco='fas fa-tv';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM menu';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=4;
$obj->categoria=2;
$obj->nombre='Banners';
$obj->nuevo='Nuevo banner';
$obj->classIco='far fa-images';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM banners_principal';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=12;
$obj->categoria=2;
$obj->nombre='Bloques';
$obj->nuevo='Nuevo Bloque';
$obj->classIco='fas fa-clone';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM bloques ';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=1;
$obj->categoria=2;
$obj->nombre='Asistentes';
$obj->nuevo=null;
$obj->classIco='fa fa-address-card';
$obj->selectList='SELECT us.id, us.foto, us.dni, us.nombre, us.apellido, cat.nombre AS categoria, us.email, us.estado, us.pais, us.company, us.estudio, COALESCE(ex.id, 0) AS expositor, us.habilitado
  FROM usuarios AS us 
  INNER JOIN categorias AS cat ON cat.id=us.id_categoria
  LEFT JOIN expositores AS ex ON ex.id_usuario=us.id';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=2;
$obj->categoria=2;
$obj->nombre='Galería';
$obj->nuevo='Subir imagen';
$obj->classIco='far fa-file-image';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS g.*,ga.nombre_es FROM galeria g
 inner join galeria_album ga on ga.id=g.id_album';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=5;
$obj->categoria=2;
$obj->nombre='Galería Albums';
$obj->nuevo='Nuevo Album';
$obj->classIco='far fa-file-image';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * from galeria_album';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=3;
$obj->categoria=2;
$obj->nombre='FAQ';
$obj->nuevo='Nueva faq';
$obj->classIco='fas fa-list';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS f.*,fs.nombre_es FROM faq f
inner join faq_seccion fs on fs.id=f.id_seccion';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=6;
$obj->categoria=2;
$obj->nombre='FAQ Secciones';
$obj->nuevo='Nueva seccion faq';
$obj->classIco='fas fa-list';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM faq_seccion';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=7;
$obj->categoria=2;
$obj->nombre='Categorias de Patrocinios';
$obj->nuevo='Nueva categoria';
$obj->classIco='fas fa-certificate';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM patrocinador_categoria_paquete';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=8;
$obj->categoria=2;
$obj->nombre='Paquetes de Patrocinios';
$obj->nuevo='Nuevo paquete';
$obj->classIco='fas fa-box-open';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM vw_paquetes_patrocinios';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=9;
$obj->categoria=2;
$obj->nombre='Solucitides de Patrocinios';
$obj->nuevo=null;
$obj->classIco='fas fa-receipt';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM vw_solicitudes_patrocinios ORDER BY fecha DESC';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=11;
$obj->categoria=2;
$obj->nombre='Logos de Patrocinadores';
$obj->nuevo=null;
$obj->classIco='fas fa-bookmark';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM vw_logos_patrocinios ';
$sec->addSeccion($obj);
//Extras
$obj=new stdClass;
$obj->id=1;
$obj->categoria=3;
$obj->nombre='Suscripciones';
$obj->nuevo='Exportar lista';
$obj->classIco='far fa-paper-plane';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM suscripciones';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=2;
$obj->categoria=3;
$obj->nombre='Nomencladores';
$obj->nuevo='Nuevo nomenclador';
$obj->classIco='fas fa-tv';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM nomencladores';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=3;
$obj->categoria=3;
$obj->nombre='Configuración PayU';
$obj->url='contenido.php?cat=3&obj=3&id=1';
$obj->nuevo='Exportar';
$obj->classIco='far fa-list-alt';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=4;
$obj->categoria=3;
$obj->nombre='Banners enlaces';
$obj->nuevo='Nuevo banner enlace';
$obj->classIco='far fa-share-square';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM banners';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=5;
$obj->categoria=3;
$obj->nombre='Códigos de descuentos';
$obj->nuevo='Nuevo código';
$obj->classIco='far fa-credit-card';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS c.*,
u.nombre asig_nombre,
u.apellido asig_apellido,
u2.nombre red_nombre,
u2.apellido red_apellido
 FROM codigos_descuento c
LEFT JOIN usuarios u ON u.id=c.id_usuario
LEFT JOIN usuarios u2 ON u2.id=c.id_usuario_redimido
';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=6;
$obj->categoria=3;
$obj->nombre='Pagos de usuarios';
$obj->nuevo='Nueva pedido de pago';
$obj->classIco='fas fa-dollar-sign';
$obj->selectList='SELECT up.*,u.nombre usu_nom,u.apellido usu_ape,
p.state_pol            AS state_pol,
  p.transaction_date     AS transaction_date,
  p.billing_city         AS billing_city,
  p.billing_country      AS billing_country,
  p.value                AS VALUE,
  p.response_message_pol AS response_message_pol,
  p.reference_sale       AS reference_sale,
  p.todo                 AS todo
FROM usuarios_pagos up
LEFT JOIN usuarios u ON up.id_usuario=u.id
LEFT JOIN pagos p ON p.id=up.id_pago
order by up.id desc';
$sec->addSeccion($obj);
//Paquetes
$obj=new stdClass;
$obj->id=1;
$obj->categoria=4;
$obj->nombre='Categorias';
$obj->nuevo='Nuevo categoria';
$obj->classIco='fas fa-tv';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM paquetes_categorias';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=2;
$obj->categoria=4;
$obj->nombre='Paquetes';
$obj->nuevo='Nuevo paquete';
$obj->classIco='fas fa-tv';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM paquetes';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=3;
$obj->categoria=4;
$obj->nombre='Reservas';
$obj->nuevo='Nueva reserva';
$obj->classIco='fas fa-tv';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM paquetes_usuario';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=4;
$obj->categoria=4;
$obj->nombre='Reservar paquete';
$obj->nuevo='Nueva reserva';
$obj->classIco='fas fa-tv';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM paquetes_usuario';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=5;
$obj->categoria=4;
$obj->nombre='Reservas';
#$obj->nuevo='Exportar';
$obj->classIco='fas fa-tv';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM paquetes_usuario';
$obj->enConstruccion=1;
$sec->addSeccion($obj);
//Hoteleria
$obj=new stdClass;
$obj->id=1;
$obj->categoria=5;
$obj->nombre='Reservas';
#$obj->nuevo='Exportar';
$obj->classIco='far fa-calendar-alt';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM pagos_hotel order by id desc';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=2;
$obj->categoria=5;
$obj->nombre='Hotel';
$obj->url='contenido.php?cat=5&obj=2&id=1';
$obj->nuevo='Exportar';
$obj->classIco='far fa-building';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM paquetes_usuario';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=3;
$obj->categoria=5;
$obj->nombre='Categorias';
$obj->nuevo='Nueva categoria';
$obj->classIco='fas fa-cubes';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM hotel_categorias_hab';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=4;
$obj->categoria=5;
$obj->nombre='Habitaciones';
$obj->nuevo='Nueva habitación';
$obj->classIco='fas fa-bed';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS h.*,hc.nombre_es categoria FROM hotel_habitciones h
inner join hotel_categorias_hab hc on hc.id=h.id_categoria  ';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=5;
$obj->categoria=5;
$obj->nombre='Itinerario de Pasajeros';
$obj->nuevo='Exportar';
$obj->classIco='far fa-clock';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS
hi.id,
hr.contac_nombre,
hr.contac_email,
hr.contac_telef,
hi.llegada,
hi.partida,
hi.llega_aerolinea,
hi.partida_aerolinea,
hi.llega_vuelo,
hi.id_reserva,
hi.partida_vuelo
 FROM hotel_itinerarios hi
INNER JOIN hotel_reservas hr ON hr.id=hi.id_reserva
ORDER BY llegada ASC';
$sec->addSeccion($obj);
class secciones{
  var $secciones,$categorias;
  function __construct(){
    $this->secciones=new stdClass;
    $this->categorias=new stdClass;
  }
  function addSeccion($ob){
    if(!isset($this->secciones->{$ob->categoria}))
    $this->secciones->{$ob->categoria}=new stdClass;
    $this->secciones->
    {$ob->categoria}->
    {$ob->id}=$ob;
  }
  function addCategoria($ob){
    $this->categorias->{$ob->id}=$ob;
  }
  function getSecciones(){
    return $this->secciones;
  }
  function getCategorias(){
    return $this->categorias;
  }
  function getSeccion($cat,$obj){
    return $this->secciones->{$cat}->{$obj};
  }
}