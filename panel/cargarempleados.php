<?
include('includes/conexion.php');
if(!empty($_POST['id_obra']) && empty($_GET['id'])){
  $_GET['id']=(int)$_POST['id_obra'];
}
$datosObra=$con->query('SELECT o.*, e.razonsocial empresa_nombre FROM obras o
inner join empresas e on e.id=o.empresa
where o.id='.((int)$_GET['id']))->fetch_object();
if(!empty($_POST['camb'])){
  $con->query("DELETE FROM obras_empleados WHERE id_obra='".$_POST['id_obra']."' AND id_empleado=".$_POST['id']);
  if($_POST['acc']=='add'){
    $con->query("insert into obras_empleados SET id_obra='".$_POST['id_obra']."', id_empleado=".$_POST['id']);
  }
  exit;
}
$limit=8;
$desde=((int)$_GET['p'])*$limit;
$rs=$con->query('SELECT e.id,e.nombre,e.cuil,e.externo,
(SELECT COUNT(oe.id) FROM obras_empleados oe WHERE oe.id_empleado=e.id AND oe.id_obra='.((int)$_GET['id']).') esta
 FROM empleados e
INNER JOIN empresas empre ON empre.id=e.id_empresa
where e.id_empresa='.$datosObra->empresa.'
 limit '.$desde.','.$limit);
$total=$con->query('SELECT COUNT(id) total FROM empleados where id_empresa='.$datosObra->empresa)->fetch_object()->total;
include('includes/header.php');
include('includes/top.php');
include('includes/menu.php')?>
  <!-- Content Wrapper. Contains page content -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<style>
.material-switch > input[type="checkbox"] {
    display: none;
}
.material-switch > label {
    cursor: pointer;
    height: 0px;
    position: relative;
    width: 40px;
}
.material-switch > label::before {
    background: rgb(0, 0, 0);
    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
    border-radius: 8px;
    content: '';
    height: 16px;
    margin-top: -8px;
    position:absolute;
    opacity: 0.3;
    transition: all 0.4s ease-in-out;
    width: 40px;
}
.material-switch > label::after {
    background: rgb(255, 255, 255);
    border-radius: 16px;
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
    content: '';
    height: 24px;
    left: -4px;
    margin-top: -8px;
    position: absolute;
    top: -4px;
    transition: all 0.3s ease-in-out;
    width: 24px;
}
.material-switch > input[type="checkbox"]:checked + label::before {
    background: inherit;
    opacity: 0.5;
}
.material-switch > input[type="checkbox"]:checked + label::after {
    background: inherit;
    left: 20px;
}
</style>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Obra: <?=$datosObra->nombre?></h1>
            <h8>Empresa: <?=$datosObra->empresa_nombre?></h8>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item"><a href="obras.php">Obras</a></li>
              <li class="breadcrumb-item active">Cargar empleados</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
<script>
function camb(o,i){
  <? if($_SESSION['tipo']==1){?>
  msg.text('Estos cambios no se guardaran por que es una cuenta administrador').load().aceptar()
  return false;
  <? } ?>
  var acc=($(o).is(':checked')?'add':'elim')
  $.ajax({
    url:'cargarempleados.php',type:'post',
    data:'camb=1&acc='+acc+'&id='+i+'&id_obra=<?=(int)$_GET['id']?>',
    success:function(d){
    }
  })
}
</script>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Carga de empleados</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <tbody><tr>
                    <th style="width: 10px">#</th>
                    <th>Nombre</th>
                    <th>CUIL</th>
                    <th style="width: 90px">Externo</th>
                    <th style="width: 90px">Asignado</th>
                  </tr>
                  <?
                  if($rs->num_rows>0){
                  while($rw=$rs->fetch_object()){?>
                  <tr>
                    <td><?=$rw->id?>.</td>
                    <td><a href="newempleado.php?id=<?=$rw->id?>"><?=$rw->nombre?></a></td>
                    <td><?=$rw->cuil?></td>
                    <td><?=($rw->externo==1)?'Si':'No'?></td>
                    <td>
                    <div class="material-switch pull-right">
                            <input id="chk_<?=$rw->id?>" name="someSwitchOption001[]" <?=($rw->esta!='0'?'checked':'')?> type="checkbox" onchange="camb(this,'<?=$rw->id?>')"/>
                            <label for="chk_<?=$rw->id?>" class="label-primary"></label>
                        </div>
                    </td>
                  </tr>
                  <? }}else{ ?>
                  <tr>
                    <td colspan="5">No se encontraron datos.</td>
                  </tr>
                  <? } ?>
                </tbody></table>
              </div>
              <script>
              //$('input[type=checkbox').iCheck('check');
              </script>
              <!-- /.card-body -->
              <? $paginas=ceil($total/$limit)-1;
              if($paginas>0){?>
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <?
                  $act=(int)$_GET['p'];
                  if($_GET['p']>0){?>
                  <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.($act-1)?>">Anterior</a></li>
                  <? }
                  for($i=($act-5);$i<$act;$i++){
                    if($i<0) continue;?>
                    <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.$i?>"><?=$i?></a></li>
                  <? }?>
                  <li class="page-item active"><a class="page-link" href="javascript:;"><?=$act?></a></li>
                  <?for($i=$act+1;$i<=$i+5;$i++){
                    if($i>$paginas) break;?>
                    <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.$i?>"><?=$i?></a></li>
                  <? }?>
                  <? if($paginas>$act){?>
                  <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.($act+1)?>">Siguiente</a></li>
                  <? } ?>
                </ul>
              </div>
              <? } ?>
            </div>
            <div style="clear: both; text-align:center">
              <a class="btn btn-info" href="obras.php">Volver</a>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include('includes/footer.php')?>