/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.1.33-community : Database - planillas
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`planillas` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `planillas`;

/*Table structure for table `accidentes` */

DROP TABLE IF EXISTS `accidentes`;

CREATE TABLE `accidentes` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `id_obra` int(6) unsigned NOT NULL,
  `fecha` date NOT NULL,
  `prop_aspt` smallint(5) unsigned NOT NULL,
  `prop_acpt` smallint(5) unsigned NOT NULL,
  `prop_acumulados` smallint(5) unsigned NOT NULL,
  `prop_dias_perdidos` smallint(5) unsigned NOT NULL,
  `prop_acto_inseg` smallint(5) unsigned NOT NULL,
  `prop_cond_inseg` smallint(5) unsigned NOT NULL,
  `nop_aspt` smallint(5) unsigned NOT NULL,
  `nop_acpt` smallint(5) unsigned NOT NULL,
  `nop_acumulados` smallint(5) unsigned NOT NULL,
  `nop_dias_perdidos` smallint(5) unsigned NOT NULL,
  `nop_acto_inseg` smallint(5) unsigned NOT NULL,
  `nop_cond_inseg` smallint(5) unsigned NOT NULL,
  `caidas` smallint(3) unsigned NOT NULL,
  `golcontra` smallint(3) unsigned NOT NULL,
  `golpor` smallint(3) unsigned NOT NULL,
  `objcort` smallint(3) unsigned NOT NULL,
  `calientes` smallint(3) unsigned NOT NULL,
  `atrap` smallint(3) unsigned NOT NULL,
  `partojos` smallint(3) unsigned NOT NULL,
  `itinere` smallint(3) unsigned NOT NULL,
  `otro01` smallint(3) unsigned NOT NULL,
  `manos` smallint(3) unsigned NOT NULL,
  `dedos` smallint(3) unsigned NOT NULL,
  `cara` smallint(3) unsigned NOT NULL,
  `cabeza` smallint(3) unsigned NOT NULL,
  `super` smallint(3) unsigned NOT NULL,
  `infe` smallint(3) unsigned NOT NULL,
  `pie` smallint(3) unsigned NOT NULL,
  `otro02` smallint(3) unsigned NOT NULL,
  `contu` smallint(3) unsigned NOT NULL,
  `hecort` smallint(3) unsigned NOT NULL,
  `ematoma` smallint(3) unsigned NOT NULL,
  `quemadura` smallint(3) unsigned NOT NULL,
  `fractu` smallint(3) unsigned NOT NULL,
  `otro03` smallint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `accidentes` */

insert  into `accidentes`(`id`,`id_obra`,`fecha`,`prop_aspt`,`prop_acpt`,`prop_acumulados`,`prop_dias_perdidos`,`prop_acto_inseg`,`prop_cond_inseg`,`nop_aspt`,`nop_acpt`,`nop_acumulados`,`nop_dias_perdidos`,`nop_acto_inseg`,`nop_cond_inseg`,`caidas`,`golcontra`,`golpor`,`objcort`,`calientes`,`atrap`,`partojos`,`itinere`,`otro01`,`manos`,`dedos`,`cara`,`cabeza`,`super`,`infe`,`pie`,`otro02`,`contu`,`hecort`,`ematoma`,`quemadura`,`fractu`,`otro03`) values (1,1,'2018-06-01',1,1,2,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(2,2,'2018-06-01',1,1,2,0,2,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,2,0,0,0,0,0,2,0,0,0);

/*Table structure for table `empleados` */

DROP TABLE IF EXISTS `empleados`;

CREATE TABLE `empleados` (
  `id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `id_empresa` int(5) unsigned NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `localidad` varchar(50) DEFAULT NULL,
  `provincia` tinyint(2) unsigned DEFAULT NULL,
  `telefono` varbinary(30) DEFAULT NULL,
  `celular` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `cuil` bigint(11) unsigned DEFAULT NULL,
  `dni` bigint(8) unsigned DEFAULT NULL,
  `externo` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `empleados` */

insert  into `empleados`(`id`,`id_empresa`,`nombre`,`direccion`,`localidad`,`provincia`,`telefono`,`celular`,`email`,`cuil`,`dni`,`externo`) values (1,1,'Empleado 1','Santa Fe','Santa Fe',20,'','','',30223334448,22333444,0),(2,1,'Empleado 2','Sauce VIejo','Santa Fe',20,'','','',2721666555,21666555,0),(3,2,'Ornela','Mitre 5960','SANTA FE ',20,'4601515','','ornelaconforti3@gmail.com',20244445556,24444555,0),(4,2,'Gabriel','Santa Fe','SANTA FE',20,'','','',27233339934,23339993,0);

/*Table structure for table `empresas` */

DROP TABLE IF EXISTS `empresas`;

CREATE TABLE `empresas` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `cuit` bigint(11) unsigned DEFAULT NULL,
  `razonsocial` varchar(70) DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `localidad` varchar(50) DEFAULT NULL,
  `provincia` tinyint(2) unsigned DEFAULT NULL,
  `telefono` varchar(30) DEFAULT NULL,
  `celular` varbinary(30) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `empresas` */

insert  into `empresas`(`id`,`cuit`,`razonsocial`,`direccion`,`localidad`,`provincia`,`telefono`,`celular`,`mail`) values (1,2147483647,'Nueva SRL','Santa Fe','Santa Fe',20,'','',''),(2,2147483647,'Cocyar SA','Mitre 5960','SANTA FE ',20,'4601515','','');

/*Table structure for table `empresas_forms` */

DROP TABLE IF EXISTS `empresas_forms`;

CREATE TABLE `empresas_forms` (
  `id_empresa` int(5) unsigned NOT NULL,
  `id_forms` int(5) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `empresas_forms` */

insert  into `empresas_forms`(`id_empresa`,`id_forms`) values (1,9),(1,10),(2,9);

/*Table structure for table `forms` */

DROP TABLE IF EXISTS `forms`;

CREATE TABLE `forms` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT,
  `id_tipoforms` smallint(4) unsigned NOT NULL,
  `version` smallint(4) unsigned NOT NULL DEFAULT '1',
  `version_fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nombre_form` varchar(50) DEFAULT NULL,
  `nombre_si` varchar(10) DEFAULT 'Si',
  `nombre_no` varchar(10) DEFAULT 'No',
  `tiene_noaplica` tinyint(1) unsigned DEFAULT '1',
  `add1` varchar(50) DEFAULT 'Observaciones',
  `add2` varchar(50) DEFAULT NULL,
  `puede_muchos` tinyint(1) unsigned DEFAULT NULL,
  `tiene_nro_modelo` varchar(1) DEFAULT NULL,
  `encabezado1` varchar(70) DEFAULT NULL,
  `encabezado2` varchar(70) DEFAULT NULL,
  `muestra_accionestomar` int(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `forms` */

insert  into `forms`(`id`,`id_tipoforms`,`version`,`version_fecha`,`nombre_form`,`nombre_si`,`nombre_no`,`tiene_noaplica`,`add1`,`add2`,`puede_muchos`,`tiene_nro_modelo`,`encabezado1`,`encabezado2`,`muestra_accionestomar`) values (1,9,1,'2018-07-12 17:40:57','INSPECCIÓN DE MARTILLO ELÉCTRICO','Si','No',1,'Observaciones',NULL,NULL,NULL,NULL,NULL,1),(2,9,1,'2018-07-12 17:12:47','test02','Si','No',1,'Observaciones',NULL,NULL,NULL,NULL,NULL,1);

/*Table structure for table `forms_criterios` */

DROP TABLE IF EXISTS `forms_criterios`;

CREATE TABLE `forms_criterios` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `id_forms` mediumint(5) unsigned NOT NULL,
  `descripcion` varchar(120) NOT NULL,
  `orden` smallint(3) unsigned NOT NULL DEFAULT '1',
  `esDivision` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `forms_criterios` */

insert  into `forms_criterios`(`id`,`id_forms`,`descripcion`,`orden`,`esDivision`) values (4,0,'El cable eléctrico de conexión está en buenas condiciones.			\n',1,0),(5,1,'El cable eléctrico de conexión está en buenas condiciones.			',1,0),(6,1,'El mandril está en buen estado.			\n',2,0),(7,1,'Cuenta con llave para abrir y cerrar el mandril (en buen estado)			\n',3,0),(8,1,'El swich de encendido funciona correctamente.			\n',4,0),(9,1,'El enchufe se encuentra en buen estado con su cable a tierra.			\n',5,0),(10,1,'Cuenta con manilla de agarre.			\n',6,0),(11,1,'El cableado de conexión se encuentra sin cortes y en buen estado.			',7,0),(12,1,'test01',0,1),(13,1,'test02',5,1),(14,2,'ghjhk',1,0);

/*Table structure for table `forms_datos` */

DROP TABLE IF EXISTS `forms_datos`;

CREATE TABLE `forms_datos` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `id_empresa` int(5) unsigned NOT NULL,
  `id_forms` mediumint(5) unsigned NOT NULL,
  `fecha` datetime NOT NULL,
  `fecha_mod` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `usuario_mod` smallint(6) unsigned DEFAULT NULL,
  `version` smallint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `forms_datos` */

insert  into `forms_datos`(`id`,`id_empresa`,`id_forms`,`fecha`,`fecha_mod`,`usuario_mod`,`version`) values (1,2,1,'2018-06-01 00:00:00',NULL,NULL,1),(2,1,1,'2018-05-01 00:00:00',NULL,NULL,1),(3,1,1,'2018-05-01 00:00:00',NULL,NULL,1);

/*Table structure for table `forms_datos_criterios` */

DROP TABLE IF EXISTS `forms_datos_criterios`;

CREATE TABLE `forms_datos_criterios` (
  `id_criterio` int(8) unsigned NOT NULL,
  `id_forms_datos` int(6) unsigned NOT NULL,
  `valor` tinyint(1) unsigned NOT NULL,
  `add1` varchar(50) DEFAULT NULL,
  `add2` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `forms_datos_criterios` */

insert  into `forms_datos_criterios`(`id_criterio`,`id_forms_datos`,`valor`,`add1`,`add2`) values (5,1,1,'aaaaaaaaaaa',NULL),(6,1,2,'',NULL),(7,1,1,'',NULL),(8,1,3,'',NULL),(9,1,1,'',NULL),(10,1,1,'',NULL),(11,1,1,'',NULL),(5,2,2,'',NULL),(6,2,2,'',NULL),(7,2,3,'',NULL),(8,2,1,'',NULL),(9,2,1,'',NULL),(10,2,1,'',NULL),(11,2,1,'',NULL),(5,3,2,'',NULL),(6,3,2,'',NULL),(7,3,3,'',NULL),(8,3,1,'',NULL),(9,3,1,'',NULL),(10,3,1,'',NULL),(11,3,1,'',NULL);

/*Table structure for table `forms_tipo` */

DROP TABLE IF EXISTS `forms_tipo`;

CREATE TABLE `forms_tipo` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `forms_tipo` */

insert  into `forms_tipo`(`id`,`nombre`) values (9,'Inpección de herramientas manueles'),(10,'aaaa');

/*Table structure for table `horas` */

DROP TABLE IF EXISTS `horas`;

CREATE TABLE `horas` (
  `id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `id_obra` int(6) unsigned NOT NULL,
  `id_empleado` int(12) unsigned NOT NULL,
  `fecha` date NOT NULL,
  `hhe` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `hhst` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `indice` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `indice` (`indice`),
  KEY `busqueda` (`id_obra`,`fecha`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `horas` */

insert  into `horas`(`id`,`id_obra`,`id_empleado`,`fecha`,`hhe`,`hhst`,`indice`) values (1,1,1,'2018-06-06',1,2,'1_1_2018-06-06'),(2,1,2,'2018-06-06',2,1,'2_1_2018-06-06'),(3,2,3,'2018-06-06',1,1,'3_2_2018-06-06');

/*Table structure for table `obras` */

DROP TABLE IF EXISTS `obras`;

CREATE TABLE `obras` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `nro_obra` varchar(30) DEFAULT NULL,
  `empresa` int(5) unsigned NOT NULL,
  `nombre` varchar(70) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `obras` */

insert  into `obras`(`id`,`nro_obra`,`empresa`,`nombre`,`fecha_inicio`) values (1,'1',1,'Primera OBra',NULL),(2,'1',2,'Puerto Santa Fe',NULL),(3,'2',2,'Puerto 2',NULL);

/*Table structure for table `obras_empleados` */

DROP TABLE IF EXISTS `obras_empleados`;

CREATE TABLE `obras_empleados` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_obra` int(6) unsigned NOT NULL,
  `id_empleado` int(12) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `obras_empleados` */

insert  into `obras_empleados`(`id`,`id_obra`,`id_empleado`) values (1,1,1),(2,1,2),(5,2,3),(6,3,3),(7,3,4);

/*Table structure for table `provincias` */

DROP TABLE IF EXISTS `provincias`;

CREATE TABLE `provincias` (
  `id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(19) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `provincias` */

insert  into `provincias`(`id`,`nombre`) values (1,'Buenos Aires'),(2,'Catamarca'),(3,'Chaco'),(4,'Chubut'),(5,'Córdoba'),(6,'Corrientes'),(7,'Entre Ríos'),(8,'Formosa'),(9,'Jujuy'),(10,'La Pampa'),(11,'La Rioja'),(12,'Mendoza'),(13,'Misiones'),(14,'Neuquén'),(15,'Río Negro'),(16,'Salta'),(17,'San Juan'),(18,'San Luis'),(19,'Santa Cruz'),(20,'Santa Fe'),(21,'Santiago del Estero'),(22,'Tierra del Fuego'),(23,'Tucumán');

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `empresa` int(5) DEFAULT NULL,
  `usuario` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `email` varchar(70) NOT NULL,
  `tipo` tinyint(1) NOT NULL DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `usuarios` */

insert  into `usuarios`(`id`,`empresa`,`usuario`,`password`,`email`,`tipo`) values (1,0,'admin','admin','á´d',1),(5,1,'Supervisor ','Supervisor','encargado1@mail.com',2),(6,2,'ornela','ornela','ornelaconforti3@gmail.com',2);

/* Procedure structure for procedure `deleteForms` */

/*!50003 DROP PROCEDURE IF EXISTS  `deleteForms` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteForms`(
	in _id int(4)
    )
BEGIN
	DECLARE exit handler for sqlexception
BEGIN
	SELECT '0' resultado;
ROLLBACK;
END;
    
	START TRANSACTION;
	
	DELETE FROM forms_datos_criterios WHERE id_forms_datos IN(
	SELECT fd.id FROM forms_datos fd
	where fd.id_forms =_id);
	DELETE FROM forms_datos WHERE id_forms =_id;
	
	DELETE FROM forms_criterios WHERE id_forms=_id;
	DELETE FROM forms WHERE id=_id;
	
	select '1' resultado;
	COMMIT;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `deleteTipoForms` */

/*!50003 DROP PROCEDURE IF EXISTS  `deleteTipoForms` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteTipoForms`(
	in _id int(4)
    )
BEGIN
	DECLARE exit handler for sqlexception
BEGIN
	SELECT '0' resultado;
ROLLBACK;
END;
    
	START TRANSACTION;
	
	DELETE FROM forms_datos_criterios WHERE id_forms_datos IN(
	SELECT fd.id FROM forms_datos fd
	INNER JOIN forms f ON f.id=fd.id_forms
	WHERE f.id_tipoforms=_id);
	DELETE FROM forms_datos WHERE id_forms IN (
	SELECT id FROM forms WHERE id_tipoforms=_id);
	
	DELETE FROM forms_criterios WHERE id_forms IN (
	SELECT id FROM forms WHERE id_tipoforms=_id);
	DELETE FROM forms WHERE id_tipoforms=_id;
	DELETE FROM forms_tipo WHERE id=_id;
	
	select '1' resultado;
	COMMIT;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `obRepAnual` */

/*!50003 DROP PROCEDURE IF EXISTS  `obRepAnual` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `obRepAnual`(
    IN _anio varchar(4),
    IN _obra INT(6)
    )
BEGIN
	SELECT c.*,a.*,a.hhe+a.hhst hhe_total, ac.*,
	prop_aspt+nop_aspt aspt, prop_acpt+nop_acpt acpt, prop_aspt+nop_aspt+prop_acpt+nop_acpt accidentes,
	nop_acto_inseg+prop_acto_inseg acto_inseg, nop_cond_inseg+prop_cond_inseg cond_inseg,
	prop_dias_perdidos+nop_dias_perdidos dias_perdidos,
	b.dias_labo
	FROM
	(SELECT SUM(h1.hhe) hhe,SUM(h1.hhst) hhst, MONTH(h1.fecha) mes, YEAR(h1.fecha) anio,h1.id_obra FROM horas h1 WHERE
	h1.id_obra=_obra
	AND YEAR(h1.fecha)=_anio
	GROUP BY mes) a
	LEFT JOIN
	(
	SELECT COUNT(DISTINCT(h2.id_empleado)) nrotrab, MONTH(h2.fecha) mes, YEAR(h2.fecha) anio
	FROM horas h2 WHERE h2.id_obra=_obra AND YEAR(h2.fecha)=_anio
	GROUP BY mes) c ON c.mes=a.mes
	LEFT JOIN accidentes ac ON ac.id_obra=a.id_obra
	AND MONTH(ac.fecha)=a.mes AND YEAR(ac.fecha)=a.anio
	LEFT JOIN (SELECT COUNT(DISTINCT fecha) dias_labo, YEAR(fecha) anio, MONTH(fecha) mes,id_obra FROM horas
	GROUP BY anio,mes,id_obra) b ON b.id_obra=a.id_obra
	AND b.mes=a.mes AND b.anio=a.anio;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `obTotXobraXmes` */

/*!50003 DROP PROCEDURE IF EXISTS  `obTotXobraXmes` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `obTotXobraXmes`(
    IN _obra int(6),
    IN _mes VARCHAR(2),
    IN _anio VARCHAR(4)
    )
BEGIN
     SELECT *,
	hhe_propio+hhe_nopropio hhe_total,
	hhst_propio+hhst_nopropio hhst_total,
	hs_total_propio+hs_total_nopropio hs_total,
	dias_propio+dias_nopropio dias_total
	FROM (
SELECT
COALESCE(SUM(h.hhe),0) hhe_propio,
COALESCE(SUM(h.hhst),0) hhst_propio,
COALESCE(SUM(h.hhe),0) +COALESCE(SUM(h.hhst),0) hs_total_propio
FROM horas h
INNER JOIN empleados e ON e.id=h.id_empleado
WHERE h.id_obra=_obra
AND e.externo=0
AND MONTH(h.fecha)=_mes
AND YEAR(h.fecha)=_anio
) a,
(
SELECT
COALESCE(SUM(h.hhe),0) hhe_nopropio,
COALESCE(SUM(h.hhst),0) hhst_nopropio,
COALESCE(SUM(h.hhe),0) +COALESCE(SUM(h.hhst),0) hs_total_nopropio
FROM horas h
INNER JOIN empleados e ON e.id=h.id_empleado
WHERE h.id_obra=_obra
AND e.externo=1
AND MONTH(h.fecha)=_mes
AND YEAR(h.fecha)=_anio
 ) b,
 (
SELECT COUNT( DISTINCT h.fecha) dias_propio FROM horas h
INNER JOIN empleados e ON e.id=h.id_empleado
WHERE e.externo=0
AND MONTH(h.fecha)=_mes
AND YEAR(h.fecha)=_anio
AND  h.id_obra=_obra
) c,
(
SELECT COUNT( DISTINCT h.fecha) dias_nopropio FROM horas h
INNER JOIN empleados e ON e.id=h.id_empleado
WHERE   e.externo=1
AND MONTH(h.fecha)=_mes
AND YEAR(h.fecha)=_anio
AND  h.id_obra=_obra
) d;
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
