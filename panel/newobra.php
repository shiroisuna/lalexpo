<?
include("includes/conexion.php");
$rsEmpre=$con->query("SELECT id,razonsocial from empresas");
if(!empty($_POST["nombre"])){
  $id=(int)$_GET["id"];
  if($_SESSION['tipo']==2){
    $empresa=$_SESSION['empresa'];
  }else{
    $empresa=(int)$_POST["empresa"];
  }
  $nombre=addslashes($_POST["nombre"]);
  $nro_obra=addslashes($_POST["nro_obra"]);
  if(!empty($id)){
    $sql="UPDATE obras SET empresa='".$empresa."',
    nro_obra='".$nro_obra."',
    nombre='".$nombre."'
    WHERE id=".$id;
  }else{
    $sql="INSERT INTO obras SET empresa='".$empresa."',
    nro_obra='".$nro_obra."',
    nombre='".$nombre."'";
    $_GET["id"]=$con->insert_id;
  }
  #echo $sql;
  $con->query($sql);
  if(empty($con->error)){
    echo 1;
  }else{
    echo $con->error;
  }
  exit;
}
if(!empty($_GET["id"])){
  $datos=$con->query("SELECT o.*,e.razonsocial empresa_nombre FROM obras o
INNER JOIN empresas e ON e.id=o.empresa where o.id=".((int)$_GET["id"]))->fetch_object();
}
$rsEmpre=$con->query("SELECT id,razonsocial from empresas");
include("includes/header.php");
include("includes/top.php");
include("includes/menu.php")?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ":"Nuevo"?> Obra</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item"><a href="usuariobrasos.php">Obras</a></li>
              <li class="breadcrumb-item active"><?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ":"Nueva"?> Obra</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
<style>
    .col-5,.col-3{float: left;}
    </style>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6" style="margin: auto;">
            <div class="card">
              <div class="card-header reves">
                <h3 class="card-title"><?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ":"Nueva"?> Obra</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <? if(!empty($msg)){?>
              <span style="color: green;"><?=$msg?></span>
              <?}?>
              <? if(!empty($error)){?>
              <span style="color: red;"><?=$error?></span>
              <?}?>
                <form role="form"
                id="form"
                class="form-horizontal"
                onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'obras.php'})"
                method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
                  <!-- text input -->
                  <? if($_SESSION['tipo']==1 && empty($_GET["id"])){?>
                  <div class="col-5">
                    <label>Empresa</label>
                    <select class="form-control" name="empresa">
                    <? while($empre=$rsEmpre->fetch_object()){
                      $add=$empre->id==$datos->empresa?'selected="selected"':"";
                      echo '<option value="'.$empre->id.'" '.$add.'>'.$empre->razonsocial."</option>";
                      }?>
                    </select>
                  </div>
                  <? }else{ ?>
                  <div class="col-5">
                    <label>Empresa</label>
                    <input type="text" class="form-control" value="<?=$datos->empresa_nombre?>" disabled="disabled" />
                    <input type="hidden" name="empresa" value="<?=$datos->empresa?>" />
                  </div>
                  <? } ?>
                  <br style="clear: both;" />
                  <div class="col-3">
                    <label>Nro de obra</label>
                    <input type="text" class="form-control" name="nro_obra" value="<?=$datos->nro_obra?>" required="" />
                  </div>
                  <div class="col-5">
                    <label>Denominaci&oacute;n</label>
                    <input type="text" class="form-control" name="nombre" value="<?=$datos->nombre?>" required="" />
                  </div>
                  <div class="card-footer" style="clear: both;">
                  <? if($_SESSION['tipo']!=1){?>
                    <button type="submit" class="btn btn-info">Guardar</button>
                    <? } ?>
                    <a type="submit" class="btn btn-default float-right" href="obras.php">Volver</a>
                  </div>
                </form>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include("includes/footer.php")?>