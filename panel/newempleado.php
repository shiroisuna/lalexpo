<?
include("includes/conexion.php");
$rsEmpre=$con->query("SELECT id,razonsocial from empresas");
$rsProv=$con->query("SELECT * from provincias");
if(!empty($_POST["nombre"])){
  $id=(int)$_GET["id"];
  if($_SESSION['tipo']==2){
    $empresa=$_SESSION['empresa'];
  }else{
    $empresa=(int)$_POST["id_empresa"];
  }
  $nombre=addslashes($_POST["nombre"]);
  $direccion=addslashes($_POST["direccion"]);
  $localidad=addslashes($_POST["localidad"]);
  $provincia=(int)$_POST["provincia"];
  $telefono=addslashes($_POST["telefono"]);
  $celular=addslashes($_POST["celular"]);
  $email=addslashes($_POST["email"]);
  $cuil=addslashes($_POST["cuil"]);
  $dni=addslashes($_POST["dni"]);
  $externo=(int)$_POST["externo"];
  if(!empty($id)){
    $res=$con->query("UPDATE empleados SET id_empresa='".$empresa."',
    nombre='".$nombre."',
    direccion='".$direccion."',
    localidad='".$localidad."',
    provincia='".$provincia."',
    telefono='".$telefono."',
    celular='".$celular."',
    email='".$email."',
    cuil='".$cuil."',
    dni='".$dni."',
    externo='".$externo."'
    WHERE id=".$id);
  }else{
    $con->query("INSERT INTO empleados SET id_empresa='".$empresa."',
    nombre='".$nombre."',
    direccion='".$direccion."',
    localidad='".$localidad."',
    provincia='".$provincia."',
    telefono='".$telefono."',
    celular='".$celular."',
    email='".$email."',
    cuil='".$cuil."',
    dni='".$dni."',
    externo='".$externo."'");
    $_GET["id"]=$con->insert_id;
  }
  if(empty($con->error)){
    echo 1;
  }else{
    echo $con->error;
  }
  exit;
}
if(!empty($_GET["id"])){
  $datos=$con->query("SELECT e.*,empr.razonsocial empresa_nombre FROM empleados e
INNER JOIN empresas empr ON empr.id=e.id_empresa where e.id=".((int)$_GET["id"]))->fetch_object();
}
include("includes/header.php");
#include("includes/top.php");
include("includes/menu.php")?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ":"Nuevo"?> Empleado</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item"><a href="empleados.php">Empleados</a></li>
              <li class="breadcrumb-item active"><?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ":"Nuevo"?> Empleado</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <style>
    .col-5,.col-3{float: left;}
    </style>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-8" style="margin: auto;">
            <div class="card">
              <div class="card-header reves">
                <h3 class="card-title"><?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ":"Nuevo"?> Empleado</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <? if(!empty($msg)){?>
              <span style="color: green;"><?=$msg?></span>
              <?}?>
              <? if(!empty($error)){?>
              <span style="color: red;"><?=$error?></span>
              <?}?>
                <form role="form"
                id="form"
                onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'empleados.php'})"
                class="form-horizontal"
                method="post">
                  <!-- text input -->
                  <? if($_SESSION['tipo']==1 && empty($_GET["id"])){?>
                  <div class="col-5">
                    <label>Empresa</label>
                    <select class="form-control" name="id_empresa">
                    <? while($empre=$rsEmpre->fetch_object()){
                      $add=$empre->id==$datos->id_empresa?'selected="selected"':"";
                      echo '<option value="'.$empre->id.'" '.$add.'>'.$empre->razonsocial."</option>";
                      }?>
                    </select>
                  </div>
                  <? }else{ ?>
                  <div class="col-5">
                    <label>Empresa</label>
                    <input type="text" class="form-control" value="<?=$datos->empresa_nombre?>" disabled="disabled" />
                    <input type="hidden" name="id_empresa" value="<?=$datos->id_empresa?>" />
                  </div>
                  <? } ?>
                  <br style="clear: both;" />
                  <div class="col-5">
                    <label>Apellido y nombre</label>
                    <input type="text" class="form-control" name="nombre" value="<?=$datos->nombre?>" required="" />
                  </div>
                  <div class="col-3">
                    <label>Externo</label>
                    <select class="form-control" name="externo">
                      <option value="0" <?=($datos->externo=='0')?'selected="selected"':''?>>No</option>
                      <option value="1" <?=($datos->externo=='1')?'selected="selected"':''?>>Si</option>
                    </select>
                  </div>
                  <br style="clear: both;" />
                  <div class="col-5">
                    <label>DNI</label>
                    <input type="text" class="form-control" name="dni" value="<?=$datos->dni?>" required="" />
                  </div>
                  <div class="col-5">
                    <label>CUIL</label>
                    <input type="text" class="form-control" name="cuil" value="<?=$datos->cuil?>" required="" />
                  </div>
                  <div class="col-5">
                    <label>Direcci&oacute;n</label>
                    <input type="text" class="form-control" name="direccion" value="<?=$datos->direccion?>" />
                  </div>
                  <div class="col-5">
                    <label>Localidad</label>
                    <input type="text" class="form-control" name="localidad" value="<?=$datos->localidad?>" />
                  </div>
                  <div class="col-5">
                    <label>Provincia</label>
                    <select class="form-control" name="provincia">
                    <? while($prov=$rsProv->fetch_object()){
                      $add=$prov->id==$datos->provincia?'selected="selected"':"";
                      echo '<option value="'.$prov->id.'" '.$add.'>'.$prov->nombre."</option>";
                      }?>
                    </select>
                  </div>
                  <div class="col-5">
                    <label>Tel&eacute;fono</label>
                    <input type="text" class="form-control" name="telefono" value="<?=$datos->telefono?>" />
                  </div>
                  <div class="col-5">
                    <label>Celular</label>
                    <input type="text" class="form-control" name="celular" value="<?=$datos->celular?>" />
                  </div>
                  <div class="col-5">
                    <label>Email</label>
                    <input type="text" class="form-control" name="email" value="<?=$datos->email?>" />
                  </div>
                  <div class="card-footer" style="clear: both;">
                  <? if($_SESSION['tipo']!=1){?>
                    <button type="submit" class="btn btn-info">Guardar</button>
                    <? } ?>
                    <a type="submit" class="btn btn-default float-right" href="empleados.php">Volver</a>
                  </div>
                </form>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include("includes/footer.php")?>