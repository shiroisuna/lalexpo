<?
include('includes/conexion.php');
if(!empty($_POST['get'])){
  $sql="select * from forms_datos where id_empresa='".$_SESSION['empresa']."' AND
  id_forms='".$_GET['id']."' AND
  fecha='".$_POST['periodo']."'";
  $rs=$con->query($sql);
  $r=new stdClass;
  $r->datos=$rs->fetch_object();
  $rs=$con->query("select * from forms_datos_criterios where id_forms_datos='".$r->datos->id."'");
  $arr=array();
  while($rw=$rs->fetch_object()){
    $arr[]=$rw;
  }
  $r->datos_crit=$arr;
  echo json_encode($r);
  exit;
}
if(!empty($_POST['guardar'])){
  $rs=$con->query("select id from forms_datos where id_empresa='".$_SESSION['empresa']."' AND
  id_forms='".$_GET['id']."' AND
  fecha='".$_POST['periodo']."'");
  if($rs->nums_rows==0){
    $con->query("INSERT INTO forms_datos SET
    id_empresa='".$_SESSION['empresa']."',
    id_forms='".$_GET['id']."',
    fecha='".$_POST['periodo']."',
    version=1");
    $id_datos=$con->insert_id;
  }else{
    $id_datos=$rs->fetch_object()->id;
    $con->query("update forms_datos SET
    id_empresa='".$_SESSION['empresa']."',
    id_forms='".$_GET['id']."',
    fecha='".$_POST['periodo']."',
    version=version+1
    where id=".$id_datos);
  }
  if(empty($con->error)){
    $con->query("delete from forms_datos_criterios where id_forms_datos=".$id_datos);
    foreach($_POST as $k=> $dat){
      $id_criterio=explode('_',$k);
     # var_dump($id_criterio);
      if($id_criterio[0]!='criterio') continue;
      $id_criterio=$id_criterio[1];
      echo $sql="INSERT INTO forms_datos_criterios SET
      id_criterio='".$id_criterio."',
      id_forms_datos='".$id_datos."',
      valor='".$_POST['criterio_'.$id_criterio]."',
      add1='".$_POST['obs_'.$id_criterio]."'
      ";
      $con->query($sql);
    }
    echo 1;
  }else{
    echo $con->error;
  }
  exit;
}
$datos_tipo=$con->query('SELECT id,nombre  from forms_tipo where id='.(int)$_GET['tipo'])->fetch_object();
$datos=$con->query('SELECT * from forms where id='.(int)$_GET['id'])->fetch_object();
$rs_criterios=$con->query("SELECT id,descripcion,esDivision FROM forms_criterios WHERE id_forms=".((int)$_GET['id'])." ORDER BY orden ASC");
#$total=$con->query('SELECT COUNT(id) total FROM obras')->fetch_object()->total;
$meses=array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
include('includes/header.php');
include('includes/top.php');
include('includes/menu.php')?>
<link rel="stylesheet" href="dist/css/adminlte.min.css" />
<link rel="stylesheet" href="css/01.css" />
<link rel="stylesheet" href="css/forms.css?<?=rand(0,99999)?>" />
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?=$datos->nombre_form?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="getFormularios.php?tipo=<?=$datos_tipo->id?>"><?=$datos_tipo->nombre?></a></li>
              <li class="breadcrumb-item active"><?=$datos->nombre_form?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <style>
    td{
      vertical-align: middle !important;
    }
    </style>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
                <!-- daterange picker -->
              <div class="card-body">
                <form id="formCampos">
                <div style="padding-bottom:10px" class="no-print" id="periodo">
                <b>Mes:
                  <select id="mes" onchange="getDatos()">
                    <?
                    $mes=date('m');
                    for($i=0;$i<=11;$i++){
                      $id=($i+1);
                      $add=($id==$mes)?'selected="selected"':'';
                      echo '<option value="'.$i.'" '.$add.'>'.$meses[$i].'</option>';
                    }?>
                  </select>/
                  <select id="anio" onchange="getDatos()">
                    <?
                     for($i=date('Y');$i>=2016;$i--){
                      echo '<option value="'.$i.'">'.$i.'</option>';
                    }?>
                  </select>
                </b>
                </div>
                <table class="table table-bordered"  id="tbTodo">
                  <thead class="only-print">
                    <tr>
                      <td colspan="10" style="padding: 0 !important;">
                        <table  style="width: 100%;">
                          <tr>
                            <th style="width: 50%;font-size: 22px;padding: 20px 12px" id="dts_nomForm"><?=$datos->nombre_form?></th>
                            <th style="font-size: 22px; text-align: center;padding: 2px 12px;vertical-align: middle;"><?=$_SESSION['empresa_nombre']?></th>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2" class="txtEnc" style="    padding: 2px 12px;">
                        NRO. DE OBRA:
                      </td>
                      <td colspan="8" class="txtEnc" style="    padding: 2px 12px;">
                        DENOMINACI&Oacute;N: <span id="nomObraPrint"></span>
                      </td>
                    </tr>
                    <tr>
                      <td class="txtEnc" colspan="10" style="    padding: 2px 12px;">
                        Mes:  <span id="periodoPrint"></span>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 10px" class="reves">&nbsp;</td>
                      <th class="tits borBottom reves" style="width: 65%">Criterios a evaluar</th>
                      <th class="tits dats reves" style="width:50px;text-align:center">Si</th>
                      <th class="tits dats reves" style="width:50px;text-align:center">No</th>
                      <th class="tits dats reves" style="width:50px;text-align:center">N/A</th>
                      <th class="tits dats reves" style="text-align:center">Observaciones</th>
                    </tr>
                  </thead>
                  <style>
                  #thContent input, input[type=text]{
                    width: 40px;
                    font-size: 12px;
                    height: 20px;
                    text-align: left;
                    border-radius: 3px;
                    border: 1px solid #777;
                    padding: 0 5px;
                  }
                  .tits{
                      padding: 3px 5px !important
                  }
                  .dats{
                    text-align: right;
                  }
                  .grafi{
                    display: inline-block;
                    border: 1px solid #CCC;
                    border-radius: 5px;
                  }
                  .tbTipos{
                    margin: auto;
                    width: 80%;
                    font-size: 12px;
                    margin-bottom: 5px;
                  }
                  .tbTipos td{
                    padding:2px;
                    text-align: center;
                  }
                  .tbTipos .tit{
                    font-size: 13px;
                    font-weight: bold;
                  }
                  .tbTipos input{
                    text-align: center !important;
                  }
                  .foot01 td{
                    text-align:right;
<<<<<<< HEAD
                    font-size:14px;
=======
                    font-size:12px;
>>>>>>> e67293abddb0034b4d8980badacb381efe389974
                    font-weight: bold;
                    padding: 5px 15px;
                  }
                  .center{
                    text-align:center !important
                  }
<<<<<<< HEAD
                  </style>
                  <script>
                  $(document).ready(function(){
                    chgCrit()
                    getDatos()
                  })
                  function chgCrit(){
                    var c=0
                    $('.criterios').each(function(a,b){
                    	if($(this).is(':checked') && $(this).val()=='1') c++
                    })
                    $('#items_afirmativos').html(c)
                    var calif=c*100/totalCrit
                    $('#calificacion_uni').html(calif.toFixed(0)+'%')
=======
                  #tbContent td{
                    font-size:12px
                  }
                  .dvResu {
                    border: 1px solid;
                    height: 30px;
                    width: 30px;
                    margin: auto;
                    text-align: center;
                    font-size: 19px;
                    font-weight: bold;
                  }
                  #tbResu td{
                    padding: 0px 5px !important;
                  }
                  </style>
                  <script>
                  $(document).ready(function(){
                    //chgCrit()
                    getDatos()
                  })
                  function chgCrit(){
                    var c=0,itemsEva=0
                    $('.criterios').each(function(a,b){
                    	if($(this).is(':checked') && $(this).val()=='1') c++
                    })
                    $('.criterios').each(function(a,b){
                    	if($(this).is(':checked') && $(this).val()!='3') itemsEva++
                    })
                    $('#items_afirmativos').html(c)
                    $('#itemsEvaluados').html(itemsEva)
                    var calif=c*100/itemsEva
                    cali=calif.toFixed(0)
                    $('#calificacion_uni').html(cali+'%')
                    $('.dvResu').html('').css('background','#CCC')
                    if(cali>=80 && cali<=89){
                      $('#resu1').html('X').css('background','#FFF')
                    }
                    if(cali>=90 && cali<=94){
                      $('#resu2').html('X').css('background','#FFF')
                    }
                    if(cali>=95 && cali<=99){
                      $('#resu3').html('X').css('background','#FFF')
                    }
                    if(cali==100){
                      $('#resu4').html('X').css('background','#FFF')
                    }
>>>>>>> e67293abddb0034b4d8980badacb381efe389974
                  }
                  function getDatos(){
                    $('.obss').val('')
                    $('.criterios[value=1]').prop('checked', true)
                    $('#periodoPrint').html($('#mes option:selected').text()+' '+$('#anio option:selected').text())
<<<<<<< HEAD
                    chgCrit()
=======
                    //chgCrit()
>>>>>>> e67293abddb0034b4d8980badacb381efe389974
                    $.ajax({
                      url:'<?=$_SERVER['REQUEST_URI']?>',type:'post',dataType:'json',
                      data:'get=1&periodo='+$('#anio').val()+'-'+$('#mes').val()+'-01',
                      success:function(d){
                        for(i in d.datos_crit){
                          var $radios = $('input:radio[name=criterio_'+d.datos_crit[i].id_criterio+']')
                          $radios.filter('[value='+d.datos_crit[i].valor+']').prop('checked', true)
                          $('#obs_'+d.datos_crit[i].id_criterio).val(d.datos_crit[i].add1)
                        }
                        chgCrit()
                      },error:function(d){
                        msg.text(d.responseText).load().aceptar()
                      }
                    })
                  }
                  function guardar(){
                    $.ajax({
                      url:'<?=$_SERVER['REQUEST_URI']?>',type:'post',dataType:'json',
                      data:'guardar=1&'+$('#formCampos').serialize()+'&periodo='+$('#anio').val()+'-'+$('#mes').val()+'-01',
                      success:function(d){
                        console.log(d)
                      },error:function(d){
                        msg.text(d.responseText).load().aceptar()
                      }
                    })
                  }
                  </script>
                  <tbody id="tbContent">
                    <? $c=1;while($rw=$rs_criterios->fetch_object()){
                      if($rw->esDivision=='1'){?>
                      <tr class="trCriterios" style="background:#d6ebf4">
                        <td colspan="6" style="font-weight:bold;font-size: 12px"><?=$rw->descripcion?></td>
                      </tr>
                      <? }else{?>
                    <tr class="trCriterios _act">
                      <td style="width:10px"><?=$c?></td>
                      <td><?=$rw->descripcion?></td>
                      <td style="text-align:center">
                        <input type="radio" checked="" class="criterios" value="1" name="criterio_<?=$rw->id?>" onclick="chgCrit()"  />
                      </td>
                      <td style="text-align:center">
                        <input type="radio" value="2" class="criterios" name="criterio_<?=$rw->id?>" onclick="chgCrit()" />
                      </td>
                      <td style="text-align:center">
                        <input type="radio" value="3" class="criterios" name="criterio_<?=$rw->id?>" onclick="chgCrit()" />
                      </td>
                      <td style="text-align:center">
                        <input type="text" style="width:100%" maxlength="50" class="obss" name="obs_<?=$rw->id?>" id="obs_<?=$rw->id?>" />
                      </td>
                    </tr>
                    <? $c++;}} ?>
                    <script>totalCrit=<?=$c-1?></script>
<<<<<<< HEAD
                  </tbody>
                  <tfoot>
=======
>>>>>>> e67293abddb0034b4d8980badacb381efe389974
                    <tr class="foot01">
                      <td colspan="2">TOTAL DE ÍTEMS AFIRMATIVOS</td>
                      <td colspan="3" class="center" id="items_afirmativos"></td>
                      <td rowspan="3" class="center"></td>
                    </tr>
                    <tr class="foot01">
                      <td colspan="2">TOTAL DE ÍTEMS EVALUADOS</td>
<<<<<<< HEAD
                      <td colspan="3" class="center"><?=$c?></td>
=======
                      <td colspan="3" class="center" id="itemsEvaluados"><?=$c-1?></td>
>>>>>>> e67293abddb0034b4d8980badacb381efe389974
                    </tr>
                    <tr class="foot01">
                      <td colspan="2" class="reves">CALIFICACIÓN PARCIAL UNITARIA</td>
                      <td colspan="3" class="center" id="calificacion_uni"></td>
                    </tr>
                    <tr>
<<<<<<< HEAD
=======
                      <td colspan="6">
                        <table style="width: 100%; max-width: 500px;margin:auto" id="tbResu">
                          <tr>
                            <th class="tits dats reves" style="width: 35%;text-align:center" colspan="2">CRITERIO DE VALORACIÓN</th>
                            <th class="tits dats reves" style="text-align:center" colspan="4">RESULTADOS DE VALORACIÓN</th>
                          </tr>
                          <tr>
                            <td>Mejorable</td>
                            <td>80-89</td>
                            <td rowspan="2">Mejorable</td>
                            <td rowspan="2">Aceptable</td>
                            <td rowspan="2">Bueno</td>
                            <td rowspan="2">Excelente</td>
                          </tr>
                          <tr>
                            <td>Aceptable</td>
                            <td>90-94</td>
                          </tr>
                          <tr>
                            <td>Bueno</td>
                            <td>95-99</td>
                            <td rowspan="2">
                              <div class="dvResu" id="resu1"></div>
                            </td>
                            <td rowspan="2">
                              <div class="dvResu" id="resu2"></div>
                            </td>
                            <td rowspan="2">
                              <div class="dvResu" id="resu3"></div>
                            </td>
                            <td rowspan="2">
                              <div class="dvResu" id="resu4"></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Excelente</td>
                            <td>90-94</td>
                          </tr>
                          <tr>
                            <td colspan="6">NOTA:  CUALQUIER VALOR INFERIOR A 79 IMPLICA UNA CALIFICACIÓN "DEFICIENTE"</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="6">
                        <table style="width: 100%;">
                          <tr>
                            <th class="tits dats reves" style="width: 40%;text-align:center">ACCIONES A TOMAR PARA CORREGIR LAS DEFICIENCIAS DETECTADAS</th>
                            <th class="tits dats reves" style="width:30%;text-align:center">PLAZO DE EJECUCIÓN</th>
                            <th class="tits dats reves" style="width:30%;text-align:center">RESPONSABLE</th>
                          </tr>
                          <? for($i=1;$i<=3;$i++){?>
                          <tr><td>&nbsp;</td><td></td><td></td></tr>
                          <? } ?>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr>
>>>>>>> e67293abddb0034b4d8980badacb381efe389974
                     <td colspan="10" style="padding-top: 25px;">
                      <table style="width: 100%; max-width: 700px;margin:auto">
                        <tr>
                          <td style="text-align: center;    padding: 2px 12px;width: 50%;" class="txtEnc reves">RESPONSABLE DE HYS</td>
                          <td style="text-align: center;    padding: 2px 12px;" class="txtEnc reves">JEFE DE OBRA</td>
                        </tr>
                        <tr>
                          <td class="txtEnc" style="padding: 2px 12px !important;">Aclaraci&oacute;n</td>
                          <td class="txtEnc" style="padding: 2px 12px !important;">Aclaraci&oacute;n</td>
                        </tr>
                        <tr>
                          <td class="txtEnc" style="height: 120px;    padding: 2px 12px;">Firma</td>
                          <td class="txtEnc" style="height: 120px;    padding: 2px 12px;">Firma</td>
                        </tr>
                      </table>
                     </td>
                    </tr>
                  </tfoot>
                </table>
                  <div class="card-footer no-print" id="btnGuard" style="text-align: center !important;">
                    <button type="button" class="btn btn-info" onclick="window.print()">Imprimir</button>
                    <button type="button" class="btn btn-info" onclick="guardar()">Guardar</button>
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include('includes/footer.php')?>