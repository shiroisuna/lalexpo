<?
include('includes/conexion.php');
$limit=8;
$desde=((int)$_GET['p'])*$limit;
$rsFormsXTipo=$con->query('SELECT id,nombre_form  from forms where id_tipoforms='.(int)$_GET['tipo']);
$datos=$con->query('SELECT id,nombre  from forms_tipo where id='.(int)$_GET['tipo'])->fetch_object();
#$total=$con->query('SELECT COUNT(id) total FROM obras')->fetch_object()->total;
include('includes/header.php');
include('includes/top.php');
include('includes/menu.php')?>
<link rel="stylesheet" href="dist/css/adminlte.min.css" />
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="nav-icon fas fa-user-secret"></i> <?=$datos->nombre?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item active"><?=$datos->nombre?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <style>
    td{
      vertical-align: middle !important;
    }
    </style>
    <section class="content">
    <? while($rw=$rsFormsXTipo->fetch_object()){?>
        <div class="col-12 col-sm-6 col-md-3" style="display:inline-block;width:auto;max-width:none;cursor:pointer"
        onclick="document.location='<?='completeForm.php?tipo='.((int)$_GET['tipo']).'&id='.$rw->id?>'">
          <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fa fa-clipboard"></i></span>
            <div class="info-box-content">
              <span class="info-box-text"><?=$rw->nombre_form?></span>
              <span class="info-box-number">
                Cargar datos
                <small>%</small>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
          <? } ?>
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
                <!-- daterange picker -->
              <div class="card-body">
                <form id="formCampos">
                <div style="padding-bottom:10px" class="no-print" id="periodo">
                <b>Mes:
                  <select id="mes" onchange="getDatos()">
                    <?
                    $mes=date('m');
                    for($i=0;$i<=11;$i++){
                      $id=($i+1);
                      $add=($id==$mes)?'selected="selected"':'';
                      echo '<option value="'.$i.'" '.$add.'>'.$meses[$i].'</option>';
                    }?>
                  </select>/
                  <select id="anio" onchange="getDatos()">
                    <?
                     for($i=date('Y');$i>=2016;$i--){
                      echo '<option value="'.$i.'">'.$i.'</option>';
                    }?>
                  </select>
                </b>
                </div>
                <table class="table table-bordered"  id="tbTodo">
                  <thead class="only-print">
                    <tr>
                      <td colspan="10" style="padding: 0 !important;">
                        <table  style="width: 100%;">
                          <tr>
                            <th style="width: 50%;font-size: 22px;padding: 20px 12px" id="dts_nomForm">CONTROL DE INSPECCIONES</th>
                            <th style="font-size: 22px; text-align: center;padding: 2px 12px;vertical-align: middle;"><?=$_SESSION['empresa_nombre']?></th>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2" class="txtEnc" style="    padding: 2px 12px;">
                        NRO. DE OBRA:
                      </td>
                      <td colspan="8" class="txtEnc" style="    padding: 2px 12px;">
                        DENOMINACI&Oacute;N: <span id="nomObraPrint"></span>
                      </td>
                    </tr>
                    <tr>
                      <td class="txtEnc" colspan="10" style="    padding: 2px 12px;">
                        Mes:  <span id="periodoPrint"></span>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 10px" class="reves">&nbsp;</td>
                      <th class="tits borBottom reves" style="width: 65%">Criterios a evaluar</th>
                      <th class="tits dats reves" style="width:50px;text-align:center">Si</th>
                      <th class="tits dats reves" style="width:50px;text-align:center">No</th>
                      <th class="tits dats reves" style="width:50px;text-align:center">N/A</th>
                      <th class="tits dats reves" style="text-align:center">Observaciones</th>
                    </tr>
                  </thead>
                  <style>
                  #thContent input, input[type=text]{
                    width: 40px;
                    font-size: 12px;
                    height: 20px;
                    text-align: left;
                    border-radius: 3px;
                    border: 1px solid #777;
                    padding: 0 5px;
                  }
                  .tits{
                      padding: 3px 5px !important
                  }
                  .dats{
                    text-align: right;
                  }
                  .grafi{
                    display: inline-block;
                    border: 1px solid #CCC;
                    border-radius: 5px;
                  }
                  .tbTipos{
                    margin: auto;
                    width: 80%;
                    font-size: 12px;
                    margin-bottom: 5px;
                  }
                  .tbTipos td{
                    padding:2px;
                    text-align: center;
                  }
                  .tbTipos .tit{
                    font-size: 13px;
                    font-weight: bold;
                  }
                  .tbTipos input{
                    text-align: center !important;
                  }
                  .foot01 td{
                    text-align:right;
                    font-size:12px;
                    font-weight: bold;
                    padding: 5px 15px;
                  }
                  .center{
                    text-align:center !important
                  }
                  #tbContent td{
                    font-size:12px
                  }
                  .dvResu {
                    border: 1px solid;
                    height: 30px;
                    width: 30px;
                    margin: auto;
                    text-align: center;
                    font-size: 19px;
                    font-weight: bold;
                  }
                  #tbResu td{
                    padding: 0px 5px !important;
                  }
                  </style>
                  <script>
                  $(document).ready(function(){
                    //chgCrit()
                    getDatos()
                  })
                  function chgCrit(){
                    var c=0,itemsEva=0
                    })
                    $('.criterios').each(function(a,b){
                    	if($(this).is(':checked') && $(this).val()!='3') itemsEva++
                    })
                    $('.criterios').each(function(a,b){
                    	if($(this).is(':checked') && $(this).val()=='1') c++
                    $('#items_afirmativos').html(c)
                    $('#itemsEvaluados').html(itemsEva)
                    var calif=c*100/itemsEva
                    cali=calif.toFixed(0)
                    $('#calificacion_uni').html(cali+'%')
                    $('.dvResu').html('').css('background','#CCC')
                    if(cali>=80 && cali<=89){
                      $('#resu1').html('X').css('background','#FFF')
                    }
                    if(cali>=90 && cali<=94){
                      $('#resu2').html('X').css('background','#FFF')
                    }
                    if(cali>=95 && cali<=99){
                      $('#resu3').html('X').css('background','#FFF')
                    }
                    if(cali==100){
                      $('#resu4').html('X').css('background','#FFF')
                    }
                  }
                  function getDatos(){
                    $('.obss').val('')
                    $('.criterios[value=1]').prop('checked', true)
                    $('#periodoPrint').html($('#mes option:selected').text()+' '+$('#anio option:selected').text())
                    //chgCrit()
                    $.ajax({
                      url:'<?=$_SERVER['REQUEST_URI']?>',type:'post',dataType:'json',
                      data:'get=1&periodo='+$('#anio').val()+'-'+$('#mes').val()+'-01',
                      success:function(d){
                        for(i in d.datos_crit){
                          var $radios = $('input:radio[name=criterio_'+d.datos_crit[i].id_criterio+']')
                          $radios.filter('[value='+d.datos_crit[i].valor+']').prop('checked', true)
                          $('#obs_'+d.datos_crit[i].id_criterio).val(d.datos_crit[i].add1)
                        }
                        chgCrit()
                      },error:function(d){
                        msg.text(d.responseText).load().aceptar()
                      }
                    })
                  }
                  function guardar(){
                    $.ajax({
                      url:'<?=$_SERVER['REQUEST_URI']?>',type:'post',dataType:'json',
                      data:'guardar=1&'+$('#formCampos').serialize()+'&periodo='+$('#anio').val()+'-'+$('#mes').val()+'-01',
                      success:function(d){
                        console.log(d)
                      },error:function(d){
                        msg.text(d.responseText).load().aceptar()
                      }
                    })
                  }
                  </script>
                  <tbody id="tbContent">
                    <? $c=1;while($rw=$rs_criterios->fetch_object()){
                      if($rw->esDivision=='1'){?>
                      <tr class="trCriterios" style="background:#d6ebf4">
                        <td colspan="6" style="font-weight:bold;font-size: 12px"><?=$rw->descripcion?></td>
                      </tr>
                      <? }else{?>
                    <tr class="trCriterios _act">
                      <td style="width:10px"><?=$c?></td>
                      <td><?=$rw->descripcion?></td>
                      <td style="text-align:center">
                        <input type="radio" checked="" class="criterios" value="1" name="criterio_<?=$rw->id?>" onclick="chgCrit()"  />
                      </td>
                      <td style="text-align:center">
                        <input type="radio" value="2" class="criterios" name="criterio_<?=$rw->id?>" onclick="chgCrit()" />
                      </td>
                      <td style="text-align:center">
                        <input type="radio" value="3" class="criterios" name="criterio_<?=$rw->id?>" onclick="chgCrit()" />
                      </td>
                      <td style="text-align:center">
                        <input type="text" style="width:100%" maxlength="50" class="obss" name="obs_<?=$rw->id?>" id="obs_<?=$rw->id?>" />
                      </td>
                    </tr>
                    <? $c++;}} ?>
                    <script>totalCrit=<?=$c-1?></script>
                    <tr class="foot01">
                      <td colspan="2">TOTAL DE ÍTEMS AFIRMATIVOS</td>
                      <td colspan="3" class="center" id="items_afirmativos"></td>
                      <td rowspan="3" class="center"></td>
                    </tr>
                    <tr class="foot01">
                      <td colspan="2">TOTAL DE ÍTEMS EVALUADOS</td>
                      <td colspan="3" class="center" id="itemsEvaluados"><?=$c-1?></td>
                    </tr>
                    <tr class="foot01">
                      <td colspan="2" class="reves">CALIFICACIÓN PARCIAL UNITARIA</td>
                      <td colspan="3" class="center" id="calificacion_uni"></td>
                    </tr>
                    <tr>
                      <td colspan="6">
                        <table style="width: 100%; max-width: 500px;margin:auto" id="tbResu">
                          <tr>
                            <th class="tits dats reves" style="width: 35%;text-align:center" colspan="2">CRITERIO DE VALORACIÓN</th>
                            <th class="tits dats reves" style="text-align:center" colspan="4">RESULTADOS DE VALORACIÓN</th>
                          </tr>
                          <tr>
                            <td>Mejorable</td>
                            <td>80-89</td>
                            <td rowspan="2">Mejorable</td>
                            <td rowspan="2">Aceptable</td>
                            <td rowspan="2">Bueno</td>
                            <td rowspan="2">Excelente</td>
                          </tr>
                          <tr>
                            <td>Aceptable</td>
                            <td>90-94</td>
                          </tr>
                          <tr>
                            <td>Bueno</td>
                            <td>95-99</td>
                            <td rowspan="2">
                              <div class="dvResu" id="resu1"></div>
                            </td>
                            <td rowspan="2">
                              <div class="dvResu" id="resu2"></div>
                            </td>
                            <td rowspan="2">
                              <div class="dvResu" id="resu3"></div>
                            </td>
                            <td rowspan="2">
                              <div class="dvResu" id="resu4"></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Excelente</td>
                            <td>90-94</td>
                          </tr>
                          <tr>
                            <td colspan="6">NOTA:  CUALQUIER VALOR INFERIOR A 79 IMPLICA UNA CALIFICACIÓN "DEFICIENTE"</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="6">
                        <table style="width: 100%;">
                          <tr>
                            <th class="tits dats reves" style="width: 40%;text-align:center">ACCIONES A TOMAR PARA CORREGIR LAS DEFICIENCIAS DETECTADAS</th>
                            <th class="tits dats reves" style="width:30%;text-align:center">PLAZO DE EJECUCIÓN</th>
                            <th class="tits dats reves" style="width:30%;text-align:center">RESPONSABLE</th>
                          </tr>
                          <? for($i=1;$i<=3;$i++){?>
                          <tr><td>&nbsp;</td><td></td><td></td></tr>
                          <? } ?>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                     <td colspan="10" style="padding-top: 25px;">
                      <table style="width: 100%; max-width: 700px;margin:auto">
                        <tr>
                          <td style="text-align: center;    padding: 2px 12px;width: 50%;" class="txtEnc reves">RESPONSABLE DE HYS</td>
                          <td style="text-align: center;    padding: 2px 12px;" class="txtEnc reves">JEFE DE OBRA</td>
                        </tr>
                        <tr>
                          <td class="txtEnc" style="padding: 2px 12px !important;">Aclaraci&oacute;n</td>
                          <td class="txtEnc" style="padding: 2px 12px !important;">Aclaraci&oacute;n</td>
                        </tr>
                        <tr>
                          <td class="txtEnc" style="height: 120px;    padding: 2px 12px;">Firma</td>
                          <td class="txtEnc" style="height: 120px;    padding: 2px 12px;">Firma</td>
                        </tr>
                      </table>
                     </td>
                    </tr>
                  </tfoot>
                </table>
                  <div class="card-footer no-print" id="btnGuard" style="text-align: center !important;">
                    <button type="button" class="btn btn-info" onclick="window.print()">Imprimir</button>
                    <button type="button" class="btn btn-info" onclick="guardar()">Guardar</button>
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include('includes/footer.php')?>