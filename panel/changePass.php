<?
include("../includes/conexion.php");
if(!empty($_POST["passviejo"])){
  if($_SESSION['password']!=$_POST["passviejo"]){
    echo '<span style="color:red">La contrase&ntilde;a introducida es incorrecta</span>';
    exit;
  }
  if($_POST['pass1']!=$_POST['pass2']){
    echo '<span style="color:red">Las nuevas contrase&ntilde;as no coinciden</span>';
    exit;
  }
  $sql="UPDATE usuarios_back SET password='".$_POST['pass1']."' where id=".$_SESSION['id'];
  $con->query($sql);
  if(empty($con->error)){
    echo 1;
  }else{
    echo $con->error;
  }
  exit;
}
include("includes/header.php");
include("includes/top.php");
include("includes/menu.php")?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="nav-icon fas fa-key text-warning"></i> Cambiar Contrase&ntilde;a</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item active">Cambiar Contrase&ntilde;a</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
<style>
    .col-5,.col-3{float: left;}
    </style>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6" style="margin: auto;">
            <div class="card">
              <div class="card-header reves">
                <h3 class="card-title">Cambiar Contrase&ntilde;a</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <? if(!empty($msg)){?>
              <span style="color: green;"><?=$msg?></span>
              <?}?>
              <? if(!empty($error)){?>
              <span style="color: red;"><?=$error?></span>
              <?}?>
                <form role="form"
                id="form"
                class="form-horizontal"
                onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'changePass.php'})"
                method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
                  <!-- text input -->
                  <div class="col-5">
                    <label>Contrase&ntilde;a actual</label>
                    <input type="password" class="form-control" name="passviejo" autocomplete="off" required="" />
                  </div>
                  <br  style="clear: both;" />
                  <div class="col-5">
                    <label>Nueva contrase&ntilde;a</label>
                    <input type="password" class="form-control" name="pass1" autocomplete="off" required="" />
                  </div>
                  <div class="col-5">
                    <label>Repita la contrase&ntilde;a</label>
                    <input type="password" class="form-control" name="pass2" autocomplete="off" required="" />
                  </div>
                  <div class="card-footer" style="clear: both;">
                    <button type="submit" class="btn btn-info">Guardar</button>
                  </div>
                </form>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include("includes/footer.php")?>