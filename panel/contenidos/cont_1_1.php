<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'})"
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Key</label>
      <input type="text" class="form-control" name="id" <?=(!empty($datos->id))?'readonly style="background:#CCC"':''?> value="<?=$datos->id?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Seccion</label>
      <select name="seccion" id="seccion" class="form-control" onchange="chgCat(this.value)">
        <? while($rw=$cat_tradu->fetch_object()){
          $add='';
          if(empty($datos->id) && $_SESSION['ult_cat_tradu']==$rw->id){
            $add='selected';
          }
          if($datos->id_seccion==$rw->id){
            $add='selected';
          }
          ?>
        <option value="<?=$rw->id?>" <?=$add?>><?=$rw->id?> - <?=$rw->nombre?></option>
        <? } ?>
        <option value="-1" style="color:blue;font-weight:bold;">Nuevo</option>
      </select>
    </div>
    <div class="col-5">
      <label>Español</label>
      <input type="text" class="form-control" name="es" value="<?=$datos->es?>" required="" maxlength="600"  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Ingles</label>
      <input type="text" class="form-control" name="en" value="<?=$datos->en?>" required="" maxlength="600"  autocomplete="off" />
    </div>
    <div class="col-10" style="display:inline-block;text-align:left">
      <label>Observaciones</label>
      <input type="text" class="form-control" name="obs" value="<?=$datos->obs?>" maxlength="200"  autocomplete="off" />
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>
  <script>
  function chgCat(v){
    if(v=='-1'){
      msg.text('<b>Nombre:</b><br /><input type="text" class="form-control" id="nombre_cat" />').load().aceptar(function(){
        if($('#nombre_cat').val()==''){
          chgCat($('#seccion').val())
          return false
        }
        var nombre=$('#nombre_cat').val()
        msg.text('Creando seccion...<br /><img src="img/loading.gif" />').load()
        $.ajax({
          url:'contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>',type:'post',
          data:'addSec=1&nombre='+nombre,dataType:'json',
          success:function(d){
            if(d.estado=='1'){
              var i,t='',add
              for(i in d.lista){
                add=(d.lista[i].id==d.id)?'selected':''
                t+='<option value="'+d.lista[i].id+'" '+add+'>'+d.lista[i].id+' - '+d.lista[i].nombre+'</option>'
              }
              t+='<option value="-1" style="color:blue;font-weight:bold;">Nuevo</option>'
              $('#seccion').html(t)
              msg.close()
            }else{
              msg.text(d.msg).load().aceptar()
            }
          },error:function(d){
            msg.text(d.responseText).load().aceptar()
          }
        })
      })
    }
  }
  </script>