<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'})"
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <div class="col-5">
      <label >Fecha</label>
      <input type="date" class="form-control" name="fecha" id="fecha" required="" value="<?=$datos->fecha?>" />
    </div>
    <div class="col-5">
      <label >Inicio</label>
      <input type="time" class="form-control" style="display: inline-block; width: 120px;margin-right: 10px;" name="inicio" id="inicio" required="" value="<?=$datos->inicio?>" />
      <label >Final</label>
      <input type="time" class="form-control" style="display: inline-block; width: 120px;" name="final" id="final" required="" value="<?=$datos->final?>" />
    </div>
    <div class="col-5">
      <label >Titulo español</label>
      <input type="text" class="form-control" name="titulo_es" id="titulo_es" required="" value="<?=$datos->titulo_es?>" />
    </div>
    <div class="col-5">
      <label>Titulo inglés</label>
      <input type="text" class="form-control" name="titulo_en" id="titulo_en" required="" value="<?=$datos->titulo_en?>" />
    </div>
    <div class="col-5">
      <label>Contenido Español</label>
      <textarea class="form-control" name="detalle_es" id="detalle_es" ><?=$datos->detalle_es?></textarea>
    </div>
    <div class="col-5">
      <label>Contenido Inglés</label>
      <textarea class="form-control"  name="detalle_en" id="detalle_en" ><?=$datos->detalle_en?></textarea>
    </div>
    <div class="col-5">
      <label>Ubicación español</label>
      <input type="text" class="form-control" name="ubicacion_es" id="ubicacion_es" required="" value="<?=$datos->ubicacion_es?>" />
    </div>
    <div class="col-5">
      <label>Ubicación inglés</label>
      <input type="text" class="form-control" name="ubicacion_en" id="ubicacion_en" required="" value="<?=$datos->ubicacion_en?>" />
    </div>
    <div class="col-5">
      <label>Estado</label>
      <select class="form-control" name="estado" id="estado">
        <option value="1" <?=$datos->estado==1?'selected':''?>>Activo</option>
        <option value="0" <?=$datos->estado==0?'selected':''?>>Inactivo</option>
      </select>
    </div>
    <div class="card-footer">
      <input type="hidden" name="id" value="<?=$datos->id?>">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>
<script src="js/jquery.form.js"></script>
<script src="dist/ckeditor/ckeditor.js"></script>
<script>
  CKEDITOR.editorConfig = function( config ) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;
  };
  CKEDITOR.inline( 'detalle_es' );
  CKEDITOR.inline( 'detalle_en' );
</script>