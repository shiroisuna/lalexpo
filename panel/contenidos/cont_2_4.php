<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="  "
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label style="float:left;">Banner/Bloque</label>
      <input type="text" class="form-control" name="banner" id="banner" required="" value="<?=$datos->banner?>" />
    </div>
    <div class="col-5">
      <label style="float:left;">Link Url</label>
      <input type="text" class="form-control" name="link" id="link" required="" value="<?=$datos->link?>" />
    </div>
    <div class="col-5">
      <label>Archivo Español</label>      
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg,.mp4" name="archivo_es" id="archivo_es"  />
      <img src="../images/banners/<?=$datos->archivo_es?>" width="400">
    </div>
    <div class="col-5">
      <label>Archivo Ingles</label>      
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg,.mp4" name="archivo_en" id="archivo_en"  />
      <img src="../images/banners/<?=$datos->archivo_en?>" width="400">
    </div>
    <div class="col-5">
      <label>Activo</label>
      <select class="form-control" name="activo" id="activo">
        <option value="1" <?=$datos->activo==1?'selected':''?>>Si</option>
        <option value="0" <?=$datos->activo==0?'selected':''?>>No</option>
      </select>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>

    <input type="hidden" name="id" value="<?=$datos->id?>">
    <input type="hidden" name="env_banner" value="1" />
  </form>
<script src="js/jquery.form.js"></script>
  <script>
function inArray(needle, haystack) {
  var length = haystack.length;
  for(var i = 0; i < length; i++) {
      if(haystack[i] == needle) return true;
  }
  return false;
}
(function(){
  $('form').ajaxForm({
    beforeSend: function() {
       if($('#banner').val()==''){
        msg.text('Ingrese el nombre o numero del banner al que pertenece la imagen').load().aceptar()
        return false
      }     
      // if($('#archivo_es').val()==''){
      //   msg.text('Seleccione un archivo').load().aceptar()
      //   return false
      // }
      // if($('#archivo_en').val()==''){
      //   msg.text('Seleccione un archivo').load().aceptar()
      //   return false
      // }
      if($('#archivo_es').val()!=''){
        var ext=$('#archivo_es').val().split('.').pop()
        if(!inArray(ext,['png','gif','jpg','mp4'])){
          msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
          return false
        }
      }
      if($('#archivo_en').val()!=''){
        var ext=$('#archivo_en').val().split('.').pop()
        if(!inArray(ext,['png','gif','jpg','mp4'])){
          msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
          return false
        }
      }
      msg.text('Subiendo archivos...<br /><b id="procentaje">0%</b>').load()
    },
    uploadProgress: function(event, position, total, percentComplete) {
      $('#procentaje').html(percentComplete+'%')
    },
    success: function() {
      msg.text('Procesando archivos...<br /><img src="img/loading.gif" />').load()
    },
    complete: function(d) {
      if(d.responseText==1){
        msg.text('Los banners han sido guardado exitosamente').load().aceptar(function(){
          document.location.href='listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'
        })
      }else{
        msg.text(d.responseText.load().aceptar())
      }
    }
  })
})();
  </script>