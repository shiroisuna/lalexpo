<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="
  "
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-12" style="margin: auto;">
      <div class="dvTit">Configuración de PayU</div>
    </div>
    <div class="col-5">
      <label>PayU URL</label>
      <input type="text" class="form-control" spellcheck="false" name="payu_url" maxlength="70" id="payu_url" value="<?=$datos->payu_url?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5"></div>
    <br style="clear: both;" />
    <div class="col-5">
      <label>Id de la cuenta/comercio</label>
      <input type="text" class="form-control" spellcheck="false" name="payu_merchantId" maxlength="150" id="payu_merchantId" value='<?=$datos->payu_merchantId?>' required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Api key</label>
      <input type="text" class="form-control" spellcheck="false" name="payu_apikey" maxlength="150" id="payu_apikey" value='<?=$datos->payu_apikey?>' required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Id del país</label>
      <input type="text" class="form-control" spellcheck="false" name="payu_accountId" maxlength="150" id="payu_accountId" value='<?=$datos->payu_accountId?>' required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Modo de prueba</label>
      <select name="test" class="form-control">
        <option value="1" <?=$datos->test=='1'?'selected':''?>>Activo</option>
        <option value="0" <?=$datos->test=='0'?'selected':''?>>Inactivo</option>
      </select>
    </div>
    <div class="col-5">
      <label>IVA %</label>
      <input type="number" class="form-control" size="2" style="width: 100px;" spellcheck="false" name="payu_iva" min="0" max="99" maxlength="2" id="payu_iva" value='<?=$datos->payu_iva?>' required=""  autocomplete="off" />
    </div>
    <div class="col-5"></div>
    <div class="col-12" style="margin: auto; margin-top: 20px;">
      <div class="dvTit">Precio ticket</div>
    </div>
    <div class="col-5">
      <label>Precio Colombia (COP)</label>
      <input type="number" class="form-control" spellcheck="false" name="valorTicket_colombia" maxlength="10" min="1" max="99999999" placeholder="COP" id="valorTicket_colombia" value='<?=$datos->valorTicket_colombia?>' required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Precio General (USD)</label>
      <input type="number" class="form-control" spellcheck="false" name="valorTicket_general" maxlength="10" min="1" max="99999999" placeholder="USD" id="valorTicket_general" value='<?=$datos->valorTicket_general?>' required=""  autocomplete="off" />
    </div>
    <div class="col-12" style="margin: auto; margin-top: 20px;">
      <div class="dvTit">Precio membresia</div>
    </div>
    <div class="col-5">
      <label>Precio Colombia (COP)</label>
      <input type="number" class="form-control" spellcheck="false" name="valorMembresia_colombia" maxlength="10" min="1" max="99999999" placeholder="COP" id="valorMembresia_colombia" value='<?=$datos->valorMembresia_colombia?>' required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Precio General (USD)</label>
      <input type="number" class="form-control" spellcheck="false" name="valorMembresia_general" maxlength="10" min="1" max="99999999" placeholder="USD" id="valorMembresia_general" value='<?=$datos->valorMembresia_general?>' required=""  autocomplete="off" />
    </div>
    <div class="col-12" style="margin: auto; margin-top: 20px;">
      <div class="dvTit">Imagenes de fondo</div>
    </div>
    <div class="col-5">
      <label>Fondo contacto</label>
      <? if(!empty($datos->fdo_contacto)){
      $ext=substr(strrchr($datos->fdo_contacto, "."), 1);
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 70px;">
          <source src="../images/banners/<?=$datos->fdo_contacto?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="../images/banners/<?=$datos->fdo_contacto?>" style="max-height: 70px;" />
      <? }else{?>
        <?=$datos->fdo_contacto?>
      <? }
      }?>
      <input type="hidden" name="img_ant_fdo_contacto" value="<?=$datos->fdo_contacto?>" />
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg" name="fdo_contacto" id="fdo_contacto"  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Fondo Footer</label>
      <? if(!empty($datos->fdo_footer)){
      $ext=substr(strrchr($datos->fdo_footer, "."), 1);
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 70px;">
          <source src="../images/banners/<?=$datos->fdo_footer?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="../images/banners/<?=$datos->fdo_footer?>" style="max-height: 70px;" />
      <? }else{?>
        <?=$datos->fdo_footer?>
      <? }
      }?>
      <input type="hidden" name="img_ant_fdo_footer" value="<?=$datos->fdo_footer?>" />
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg" name="fdo_footer" id="fdo_footer"  autocomplete="off" />
    </div>

    <div class="col-12" style="margin: auto; margin-top: 20px;">
      <div class="dvTit">Premios</div>
    </div>
    <div class="col-5">
      <label>Estado Votaciones</label>
      <select name="estado_votaciones" class="form-control">
        <option value="1" <?=$datos->estado_votaciones=='1'?'selected':''?>>Abierto</option>
        <option value="0" <?=$datos->estado_votaciones=='0'?'selected':''?>>Cerrado</option>
      </select>
    </div>
    <div class="col-5">
      <label>Estado Nominaciones</label>
      <select name="estado_nominaciones" class="form-control">
        <option value="1" <?=$datos->estado_nominaciones=='1'?'selected':''?>>Abierto</option>
        <option value="0" <?=$datos->estado_nominaciones=='0'?'selected':''?>>Cerrado</option>
      </select>
    </div>
    <div class="col-5">
      <label>Estado Ganadores</label>
      <select name="estado_ganadores" class="form-control">
        <option value="1" <?=$datos->estado_ganadores=='1'?'selected':''?>>Activado</option>
        <option value="0" <?=$datos->estado_ganadores=='0'?'selected':''?>>Desacitvado</option>
      </select>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
    </div>
    <input type="hidden" name="env_datos" value="1" />
  </form>
  <style>
  .dvTit{
    border-bottom: 1px solid #CCC; font-size: 18px; padding-bottom: 10px; margin-bottom: 10px;font-weight: bold;color:#777
  }
  </style>
<script src="js/jquery.form.js"></script>
  <script>
function inArray(needle, haystack) {
  var length = haystack.length;
  for(var i = 0; i < length; i++) {
      if(haystack[i] == needle) return true;
  }
  return false;
}
(function(){
  $('form').ajaxForm({
    beforeSend: function() {
      if($('#fdo_contacto').val()!=''){
        var ext=$('#fdo_contacto').val().split('.').pop()
        if(!inArray(ext,['png','gif','jpg','mp4'])){
          msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
          return false
        }
      }
      if($('#fdo_footer').val()!=''){
        var ext=$('#fdo_footer').val().split('.').pop()
        if(!inArray(ext,['png','gif','jpg','mp4'])){
          msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
          return false
        }
      }
      msg.text('Subiendo...<br /><b id="procentaje">0%</b>').load()
    },
    uploadProgress: function(event, position, total, percentComplete) {
      $('#procentaje').html(percentComplete+'%')
    },
    success: function() {
      msg.text('Procesando...<br /><img src="img/loading.gif" />').load()
    },
    complete: function(d) {
      if(d.responseText==1){
        msg.text('Los datos ha sido guardados exitosamente').load().aceptar(function(){
          document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$_GET['id']?>'
        })
      }else{
        msg.text(d.responseText).load().aceptar()
      }
    }
  })
})();
  </script>