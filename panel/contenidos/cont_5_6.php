<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="
  "
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Archivo</label>
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg" name="archivo" id="archivo" value="<?=$datos->archivo?>" required=""  autocomplete="off" />
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
    <input type="hidden" name="env_imagen" value="1" />
  </form>
<script src="js/jquery.form.js"></script>
  <script>
function inArray(needle, haystack) {
  var length = haystack.length;
  for(var i = 0; i < length; i++) {
      if(haystack[i] == needle) return true;
  }
  return false;
}
(function(){
  $('form').ajaxForm({
    beforeSend: function() {
      if($('#archivo').val()==''){
        msg.text('Seleccione un archivo').load().aceptar()
        return false
      }
      var ext=$('#archivo').val().split('.').pop()
      if(!inArray(ext,['png','gif','jpg','mp4'])){
        msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
        return false
      }
      msg.text('Subiendo archivo...<br /><b id="procentaje">0%</b>').load()
    },
    uploadProgress: function(event, position, total, percentComplete) {
      $('#procentaje').html(percentComplete+'%')
    },
    success: function() {
      msg.text('Procesando archivo...<br /><img src="img/loading.gif" />').load()
    },
    complete: function(d) {
      if(d.responseText==1){
        msg.text('La imagen han sido guardado exitosamente').load().aceptar(function(){
          document.location.href='listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'
        })
      }else{
        msg.text(d.responseText).load().aceptar()
      }
    }
  })
})();
  </script>