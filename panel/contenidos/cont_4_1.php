<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'})"
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Nombre Español</label>
      <input type="text" name="nombre_es" class="form-control" value="<?=$datos->nombre_es?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Nombre Ingles</label>
      <input type="text" name="nombre_en" class="form-control" value="<?=$datos->nombre_en?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Orden</label>
      <input type="number" name="orden" max="999" min="0" maxlength="3" class="form-control" value="<?=$datos->orden?>" required=""  autocomplete="off" />
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>