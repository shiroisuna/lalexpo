<?php
  while ($rw=$rs_detalle->fetch_object()) {
    $detalped .= '<tr><td align="center" style=" border-bottom: solid 1px #888888; background-color: #F4F4F4;">'.$rw->id_producto.'</td><td style=" border-bottom: solid 1px #888888;">'.$rw->nombre_es.'</td>
            <td align="center" style=" border-bottom: solid 1px #888888; background-color: #F4F4F4;">'.$rw->cantidad.'</td><td align="right" style=" border-bottom: solid 1px #888;"><span class="moneda">COP $</span><span class="monto">'.number_format($rw->valor, 2, ",", ".").'</span></td><td align="right" style=" border-bottom: solid 1px #888; background-color: #F4F4F4;"><span class="moneda">COP $</span><span class="monto">'.number_format($rw->total, 2, ",", ".").'</span></td></tr>';
    $base+=$rw->total;
  }
  $iva = ($base * 0.19);
  $total = $base + $iva;  

  echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
      <meta http-equiv="Content-Type" content="text/html;" />
      <title>Pedido N&deg; '.$_GET['id'].'</title>
      <style type="text/css" media="all">
      * {font-family: Tahoma, Geneva, sans-serif; font-size: 12px;}
      </style>
      </head>
      <body>
      <table width="700" border="0" cellspacing="0" cellpadding="2" style="border: solid 1px #000;margin-left:auto;margin-right:auto;">
        <tr>
        <td colspan="3"><div style="border: solid 1px #000000;">
        <table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr>  
          <td width="30%" valign="top" ><img src="../img/logo.jpg" height="70" align="left" /></td>
          <td width="40%" valign="top"><div style="border-left: dotted 1px #999999; border-right: dotted 1px #999999; padding-left: 5px; padding-right: 5px; height: 70px;">'.$datos->nombre.' '.$datos->apellido.'<br>'.$datos->dni.'<br />'.$datos->ciudad.' - '.$datos->pais.'<br />'.$datos->telefono.'</div></td>
          <td width="30%" align="right" valign="top" ><span style="font-size: 8; font-weight: bold; padding-right: 9px;">Pedido N&deg; '.$datos->id.'</span><br />Fecha: '.$datos->fecha.'<br />Estatus: '.($datos->estado==1?'Pagado':'Pendiente').'</td></tr></table></div>
        </td>
        </tr>
        <tr>
        <td colspan="3">
          <br /><br />
        </td>
        </tr>
        <tr>
        <td colspan="3" height="300" valign="top">
          <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr bgcolor="#CCCCCC">
            <td width="40" align="center" style=" border-top: solid 1px #888888; border-bottom: solid 1px #888888;" ><strong>C&oacute;digo</strong></th>
            <td width="280" align="center" style=" border-top: solid 1px #888888; border-bottom: solid 1px #888888;" ><strong>Producto</strong></th>
            <td width="50" align="center" style=" border-top: solid 1px #888888; border-bottom: solid 1px #888888;" ><strong>Cantidad</strong></th>
            <td width="80" align="center" style=" border-top: solid 1px #888888; border-bottom: solid 1px #888888;" ><strong>Precio</strong></th>
            <td width="80" align="center" style=" border-top: solid 1px #888888; border-bottom: solid 1px #888888;" ><strong>Subtotal</strong></th>
            </tr>
            '.$detalped.'
          </table>        
        </td>
        </tr>
        <tr>
        <td valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="40%" valign="top"><table width="100%" border="0" align="right" cellpadding="2" cellspacing="2" style="border-top: solid 1px #003366;">
        <tr>
        <td >Base Imponible</td>
        <td align="right">COP $&nbsp;&nbsp;'.number_format($base, 2, ",", ".").'</td>
        </tr>
        <tr>
        <td>I.V.A (19%)</td>
        <td align="right">COP $&nbsp;&nbsp;'.number_format($iva, 2, ",", ".").'</td>
        </tr>
        <tr>
        <td style=" font-weight: bold;">Total a Pagar</td>
        <td align="right" style="font-weight: bold;">COP $&nbsp;&nbsp;'.number_format($total, 2, ",", ".").'</td>
        </tr>
      </table></td>
        </tr>
        <tr>
        <td colspan="3" ><div style="border: solid 1px #000000; padding: 5px; font-size: 10px;"></div></td>
        </tr>
      </table>
      
      </body>
      </html>';
      ?>
    <div class="card-footer">
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>