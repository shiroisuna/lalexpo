<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
<form role="form"
  id="form"
  onsubmit=""
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
<?php if (!empty($_GET['id'])) { ?>
    <div class="col-5">
      <label>ID</label>
      <input type="text" name="id" class="form-control" value='<?=$datos->id?>' disabled />
    </div>
    <div class="col-5">
      <label>Usuario</label>
      <p class="form-control"><?=$datos->usuario?></p>
    </div>
    <div class="col-5">
      <label>Categoria</label>
      <p class="form-control"><?=$datos->categoria?></p>
    </div>
    <div class="col-5">
      <label>Paquete</label>
      <p class="form-control"><?=$datos->paquete?></p>
    </div>
  <?php } else { ?>
    <div class="col-5">
      <label>Usuario</label>
      <select id="id_usuario" name="id_usuario" class="form-control">
        <option></option>
        <? while($rw=$rs_usuarios->fetch_object()){ ?>
            <option value="<?=$rw->id?>"><?=$rw->nombre?></option>
        <?php } ?>
      </select>
    </div>
    <div class="col-5">
      <label>Paquete</label>
      <select id="id_paquete" name="id_paquete" class="form-control">
        <? while($rw=$rs_paquetes->fetch_object()){ ?>
            <option value="<?=$rw->id?>"><?=$rw->nombre_es?></option>
        <?php } ?>
      </select>
    </div>
  <?php } ?>
    <div class="col-5">
      <label>Logo</label>
      <p class="form-control">
        <img src="../<?=$datos->logo?>" height="120" />
        <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg,.mp4" name="logo" id="logo"  />
      </p>
    </div>
    <div class="col-5">
      <label>Posicion</label>
      <input class="form-control" type="number" name="posicion" id="posicion" value="<?=$datos->posicion?>" />
    </div>
    <div class="col-5">
      <label>URL</label>
      <input class="form-control" type="url" name="url" id="url" value="<?=$datos->url?>" />
    </div>
    <div class="col-5">
      <label>Estado</label>
      <select class="form-control" name="estado" id="estado">
        <option value="1" <?=$datos->estado==0?'selected':''?>>Inactivo</option>
        <option value="2" <?=$datos->estado==1?'selected':''?>>Activo</option>
      </select>
    </div>
    <div class="card-footer">
      <? if (!empty($_GET['id'])) { ?>
      <input type="hidden" name="logoact" value="../<?=$datos->logo?>">
      <input type="hidden" name="id_usuario" value="<?=$datos->id_usuario?>">
      <input type="hidden" name="id_solicitud" value="<?=$datos->id_solicitud?>">
      <? } ?>
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>
  <script src="js/jquery.form.js"></script>
  <script>
function inArray(needle, haystack) {
  var length = haystack.length;
  for(var i = 0; i < length; i++) {
      if(haystack[i] == needle) return true;
  }
  return false;
}
(function(){
    $('form').ajaxForm({
        beforeSend: function() {
            if($('#logo').val()!=''){
                var ext=$('#logo').val().split('.').pop()
                if(!inArray(ext,['png','gif','jpg','mp4'])){
                    msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
                    return false
                }
            }
            msg.text('Subiendo archivos...<br /><b id="procentaje">0%</b>').load()
        },
        uploadProgress: function(event, position, total, percentComplete) {
            $('#procentaje').html(percentComplete+'%')
        },
        success: function() {
            msg.text('Procesando archivos...<br /><img src="img/loading.gif" />').load()
        },
        complete: function(d) {
            console.log(d.responseText);
            if(d.responseText==1){
                msg.text('Los datos han sido guardados exitosamente').load().aceptar(function(){
                    document.location.href='listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'
                })
            }else{
                msg.text(d.responseText.substr(0, 255)).load().aceptar()
            }
        }
    })
})();
  </script>