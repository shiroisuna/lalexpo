<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="  "
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label style="float:left;">Nombre</label>
      <input type="text" class="form-control" name="nombre" id="nombre" required="" value="<?=$datos->nombre?>" />
    </div>
    <div class="col-5">
      <label>Fondo</label>
      <img src="../images/banners/<?=$datos->fondo?>" height="80">
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg,.mp4" name="fondo" id="fondo"  />
    </div>
    <div class="col-5">
      <label>Contenido Español</label>
      <textarea class="form-control" name="contenido1" id="contenido1" ><?=$datos->contenido1?></textarea>
    </div>
    <div class="col-5">
      <label>Contenido Inglés</label>
      <textarea class="form-control" name="contenido2" id="contenido2" ><?=$datos->contenido2?></textarea>
    </div>
    <div class="col-5">
      <label>Estado</label>
      <select class="form-control" name="estado" id="estado">
        <option value="1" <?=$datos->estado==1?'selected':''?>>Activo</option>
        <option value="0" <?=$datos->estado==0?'selected':''?>>Inactivo</option>
      </select>
    </div>
    <div class="card-footer">
      <input type="hidden" name="id" value="<?=$datos->id?>">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>
<script src="js/jquery.form.js"></script>
<script src="dist/ckeditor/ckeditor.js"></script>
<script>
  CKEDITOR.editorConfig = function( config ) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;
  };
  CKEDITOR.inline( 'contenido1' );
  CKEDITOR.inline( 'contenido2' );
</script>
  <script>
    function inArray(needle, haystack) {
  var length = haystack.length;
  for(var i = 0; i < length; i++) {
      if(haystack[i] == needle) return true;
  }
  return false;
}
(function(){
  $('form').ajaxForm({
    beforeSend: function() {
       if($('#nombre').val()==''){
        msg.text('Ingrese el nombre del bloque').load().aceptar()
        return false
      }     
      if($('#fondo').val()!=''){
        var ext=$('#fondo').val().split('.').pop()
        if(!inArray(ext,['png','gif','jpg','mp4'])){
          msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
          return false
        }
      }
      msg.text('Subiendo archivos...<br /><b id="procentaje">0%</b>').load()
    },
    uploadProgress: function(event, position, total, percentComplete) {
      $('#procentaje').html(percentComplete+'%')
    },
    success: function() {
      msg.text('Procesando archivos...<br /><img src="img/loading.gif" />').load()
    },
    complete: function(d) {
      if(d.responseText==1){
        msg.text('Los datos han sido guardados exitosamente').load().aceptar(function(){
          document.location.href='listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'
        })
      }else{
        msg.text(d.responseText).load().aceptar()
      }
    }
  })
})();
  </script>