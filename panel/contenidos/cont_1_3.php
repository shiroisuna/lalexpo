<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'})"
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Usuario</label>
      <input type="text" class="form-control" name="usuario" value="<?=$datos->usuario?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Contrase&ntilde;a</label>
      <input type="password" class="form-control" name="password" value="<?=$datos->password?>" required="" />
    </div>
    <div class="col-5">
      <label>Email</label>
      <input type="text" class="form-control" name="email" value="<?=$datos->email?>" />
    </div>
    <div class="col-5">
      <label>Nombre</label>
      <input type="text" class="form-control" name="nombre" value="<?=$datos->nombre?>" required="" />
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>