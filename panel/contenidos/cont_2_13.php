<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="
  "
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Titulo Español</label>
      <input type="text" class="form-control" maxlength="70" name="titulo_es" id="titulo_es" value="<?=$datos->titulo_es?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Titulo Ingles</label>
      <input type="text" class="form-control" maxlength="70" name="titulo_en" id="titulo_en" value="<?=$datos->titulo_en?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Lugar</label>
      <input type="text" class="form-control" maxlength="70" name="lugar" id="lugar" value="<?=$datos->lugar?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Fecha</label>
      <input type="text" class="form-control" maxlength="70" name="fecha" id="fecha" value="<?=$datos->fecha?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Hora registro</label>
      <input type="text" class="form-control" maxlength="70" name="hora_registro" id="hora_registro" value="<?=$datos->hora_registro?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Hora evento</label>
      <input type="text" class="form-control" maxlength="70" name="hora_evento" id="hora_evento" value="<?=$datos->hora_evento?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Precio</label>
      <input type="text" class="form-control" maxlength="70" name="precio" id="precio" value="<?=$datos->precio?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Moneda</label>
      <select class="form-control" name="moneda" id="moneda">
        <option value="USD" <?=$datos->moneda=='USD'?'selected':''?>>USD - Dólar Americano</option>
        <option value="COP" <?=$datos->moneda=='COP'?'selected':''?>>COP - Peso Colombiano</option>
        <option value="MXN" <?=$datos->moneda=='MXN'?'selected':''?>>MXN - Peso Mexicano</option>
        <option value="PEN" <?=$datos->moneda=='PEN'?'selected':''?>>PEN - Nuevo Sol Peruano</option>
        <option value="ARS" <?=$datos->moneda=='ARS'?'selected':''?>>ARS - Peso Argentino</option>
        <option value="BRL" <?=$datos->moneda=='BRL'?'selected':''?>>BRL - Real Brasileño</option>
        <option value="CLP" <?=$datos->moneda=='CLP'?'selected':''?>>CLP - Peso Chileno</option>
      </select>
    </div>
    <div class="col-5">
      <label>Imagen Español</label>
      <? if(!empty($datos->imagen_es)){
      $ext=substr(strrchr($datos->imagen_es, "."), 1);
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 70px;">
          <source src="../images/banners/<?=$datos->imagen_es?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="/thumb_200_w/images/banners/<?=$datos->imagen_es?>" style="max-height: 70px;" />
      <? }else{?>
        <?=$datos->imagen_es?>
      <? }
      }?>
      <input type="hidden" name="img_ant_es" value="<?=$datos->imagen_es?>" />
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg" name="imagen_es" id="imagen_es"  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Imagen Inglés</label>
      <? if(!empty($datos->imagen_en)){
      $ext=substr(strrchr($datos->imagen_en, "."), 1);
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 70px;">
          <source src="../images/banners/<?=$datos->imagen_en?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="/thumb_200_w/images/banners/<?=$datos->imagen_en?>" style="max-height: 70px;" />
      <? }else{?>
        <?=$datos->imagen_en?>
      <? }
      }?>
      <input type="hidden" name="img_ant_en" value="<?=$datos->imagen_en?>" />
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg" name="imagen_en" id="imagen_en"  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Descripción Español</label>
      <textarea class="form-control" style="height:150px" name="descripcion_es" id="descripcion_es" required=""><?=$datos->descripcion_es?></textarea>
    </div>
    <div class="col-5">
      <label>Descripción Ingles</label>
      <textarea class="form-control" style="height:150px" name="descripcion_en" id="descripcion_en" required=""><?=$datos->descripcion_en?></textarea>
    </div>
    <div class="col-5">
      <label>Itinerario Español</label>
      <textarea class="form-control" style="height:150px" name="itinerario_es" id="itinerario_es" required=""><?=$datos->itinerario_es?></textarea>
    </div>
    <div class="col-5">
      <label>Itinerario Ingles</label>
      <textarea class="form-control" style="height:150px" name="itinerario_en" id="itinerario_en" required=""><?=$datos->itinerario_en?></textarea>
    </div>
    <div class="col-5">
      <label>Cupo/Nro Tickets</label>
      <input type="number" class="form-control" name="tickets" id="tickets" value="<?=$datos->tickets?>" required=""  autocomplete="off" min="0" />
    </div>
    <div class="col-5">
      <label>Estado</label>
      <select class="form-control" name="estado" id="estado">
        <option value="0" <?=$datos->estado=='0'?'selected':''?>>Cerrado</option>
        <option value="1" <?=$datos->estado=='1'?'selected':''?>>Abierto</option>
      </select>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
    <input type="hidden" name="env_datos" value="1" />
  </form>
<script src="js/jquery.form.js"></script>
<script src="dist/ckeditor/ckeditor.js"></script>
<script>
  CKEDITOR.editorConfig = function( config ) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;
  };
  CKEDITOR.inline( 'descripcion_es' );
  CKEDITOR.inline( 'descripcion_en' );
  CKEDITOR.inline( 'itinerario_es' );
  CKEDITOR.inline( 'itinerario_en' );
  
function inArray(needle, haystack) {
  var length = haystack.length;
  for(var i = 0; i < length; i++) {
      if(haystack[i] == needle) return true;
  }
  return false;
}
(function(){
  $('form').ajaxForm({
    beforeSend: function() {
    if($('#imagen_es').val()!=''){
      var ext=$('#imagen_es').val().split('.').pop()
      if(!inArray(ext,['png','gif','jpg','mp4'])){
        msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
        return false
      }
    }
    if($('#imagen_en').val()!=''){
      var ext=$('#imagen_en').val().split('.').pop()
      if(!inArray(ext,['png','gif','jpg','mp4'])){
        msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
        return false
      }
    }
    msg.text('Subiendo datos...<br /><b id="procentaje">0%</b>').load()
    },
    uploadProgress: function(event, position, total, percentComplete) {
      $('#procentaje').html(percentComplete+'%')
    },
    success: function() {
      msg.text('Procesando datos...<br /><img src="img/loading.gif" />').load()
    },
    complete: function(d) {
      if(d.responseText==1){
        msg.text('Los datos han sido guardados exitosamente').load().aceptar(function(){
          document.location.href='listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'
        })
      }else{
        msg.text(d.responseText).load().aceptar()
      }
    }
  })
})();
  </script>