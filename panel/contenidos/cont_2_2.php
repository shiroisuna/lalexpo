<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="
  "
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Archivo imagen/video</label><br/>
      <?
      if (!empty($datos->archivo)) {
      $ext=strtolower(substr(strrchr($datos->archivo, "."), 1));
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 100px;">
          <source src="../images/galeria/<?=$datos->archivo?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="../thumb_120_w/images/galeria/<?=$datos->archivo?>" style="max-height: 100px;" />
      <? } }?>
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg" name="archivo" id="archivo" value="<?=$datos->archivo?>"  />
    </div>
    <div class="col-5">
      <label>Enlace video (youtube)</label><br/>
      <? if (!empty($datos->enlace)) { ?>
        <iframe width="200" height="120" src="https://www.youtube.com/embed/<?=$rw->enlace?>"></iframe>
      <? } ?>
      <input type="text" class="form-control" name="enlace" id="enlace" value="<?=$datos->enlace?>" />
    </div>
    <div class="col-5">
      <label>Album</label>
      <select class="form-control" name="album" id="album">
        <? while($rw=$rs_albums->fetch_object()){?>
        <option value="<?=$rw->id?>" <?=($datos->id_album==$rw->id)?'selected':'' ?> ><?=$rw->nombre_es?></option>
        <? } ?>
      </select>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
    <input type="hidden" name="id" value="<?=$datos->id?>" />
    <input type="hidden" name="env_imagen" value="1" />
  </form>
<script src="js/jquery.form.js"></script>
  <script>
function inArray(needle, haystack) {
  var length = haystack.length;
  for(var i = 0; i < length; i++) {
      if(haystack[i] == needle) return true;
  }
  return false;
}
(function(){
  $('form').ajaxForm({
    beforeSend: function() {
      if($('#archivo').val()=='' && $('#enlace').val()==''){
        msg.text('Seleccione un archivo o ingrese un enlace de video').load().aceptar()
        return false
      }
      if($('#archivo').val()!=''){
        var ext=$('#archivo').val().split('.').pop()
        if(!inArray(ext,['png','gif','jpg','mp4','avi','mpeg'])){
          msg.text('Solo se permiten las extenciones jpg,gif,png,mp4,avi,mpeg').load().aceptar()
          return false
        }
      }
      msg.text('Subiendo archivo...<br /><b id="procentaje">0%</b>').load()
    },
    uploadProgress: function(event, position, total, percentComplete) {
      $('#procentaje').html(percentComplete+'%')
    },
    success: function() {
      msg.text('Procesando archivo...<br /><img src="img/loading.gif" />').load()
    },
    complete: function(d) {
      if(d.responseText==1){
        msg.text('La imagen/video ha sido guardado exitosamente').load().aceptar(function(){
          document.location.href='listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'
        })
      }else{
        msg.text(d.responseText).load().aceptar()
      }
    }
  })
})();
  </script>