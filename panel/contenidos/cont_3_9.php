<? @session_start(); if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
<style type="text/css">
  .busart {
    display: none;
    width: 440px;
    max-height: 250px;
    overflow-y: auto;
    position: absolute;
    background: #e2e2e2;
    background: -moz-linear-gradient(top, #e2e2e2 0%, #eeeeee 100%);
    background: -webkit-linear-gradient(top, #e2e2e2 0%,#eeeeee 100%);
    background: linear-gradient(to bottom, #e2e2e2 0%,#eeeeee 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#eeeeee',GradientType=0 );
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
  }
  .busart p {
    display: block;
    padding: 10px;
    margin: 0;
    text-align: left;
    cursor: pointer;
  }
  .busart p span { float: right; }
  .busart p:hover {
    background: #17a2b8;
    color: #fff;
  }
</style>
  <script src="js/jquery.print.js"></script>
<? if(!empty($_GET['id'])){
  $facnum = "No. " . str_pad($datos->num_fac, 6, '0', STR_PAD_LEFT);
  $factura = imagecreatefrompng("../img/factura.png");
  $negro = imagecolorallocate($factura, 0, 0, 0);
  $blanco = imagecolorallocate($factura, 255, 255, 255);
  if ($datos->moneda==1) {
    $moneda='USD $ ';
  } else {
    $moneda='COP $ ';
  }
  imagestring($factura, 3, 70, 140, $datos->nombre, $negro);
  imagestring($factura, 3, 70, 156, $datos->direccion, $negro);
  imagestring($factura, 3, 650, 156, $facnum, $negro);
  imagestring($factura, 3, 70, 173, $datos->ciudad, $negro);
  imagestring($factura, 3, 430, 173, $datos->nit, $negro);
  imagestring($factura, 3, 70, 191, $datos->pais, $negro);
  imagestring($factura, 3, 70, 207, $datos->telefono, $negro);
  imagestring($factura, 3, 408, 223, $datos->email, $negro);
  imagestring($factura, 3, 20, 243, $datos->descrip, $negro);
  imagestring($factura, 3, 620, 174, strftime("%d/%m/%Y", strtotime($datos->fec_emi)), $negro);
  imagestring($factura, 3, 620, 190, strftime("%d/%m/%Y", strtotime($datos->fec_ven)), $negro);
  imagestring($factura, 3, 620, 207, 'Contado', $negro);
  imagestring($factura, 3, 371, 640, number_format(($datos->mon_bto-$datos->mon_des), 2, ",", "."), $negro);
  imagestring($factura, 3, 460, 640, number_format($datos->tasa_imp, 0, ",", "."), $negro);
  imagestring($factura, 3, 514, 640, number_format($datos->mon_iva, 2, ",", "."), $negro);
  $fila=296;
  $longitud=17;
  $slin=0;
  while ($linea=$rs_lineas->fetch_object()) {
//    imagestring($factura, 3, 10, $fila, $linea->cod_con, $negro);

    imagestring($factura, 3, 70, $fila, substr($linea->concepto, 0, 45), $negro);

    imagestring($factura, 3, 390, $fila, 'UNI', $negro);
    imagestring($factura, 3, 390, $fila, str_pad(number_format($linea->ren_prec, 2, ",", "."), $longitud, ' ', STR_PAD_LEFT), $negro);
    imagestring($factura, 3, 540, $fila, $linea->cantidad, $negro);
    imagestring($factura, 3, 673, $fila, str_pad(number_format($linea->ren_neto, 2, ",", "."), $longitud, ' ', STR_PAD_LEFT), $negro);

    $pos=46;
    while (strlen(substr($linea->concepto, $pos))>45) {
      $slin+=20;
      imagestring($factura, 3, 70, ($fila+$slin), substr($linea->concepto, $pos, 45), $negro);
      $pos+=45;
    }

    $fila+=20+$slin;
    $slin=0;

  }
  imagestring($factura, 3, 673, 625, str_pad(number_format($datos->mon_bto, 2, ",", "."), $longitud, ' ', STR_PAD_LEFT), $negro);
  imagestring($factura, 3, 650, 642, number_format($datos->porc_des, 0, ",", "."). "% ", $negro);
  imagestring($factura, 3, 673, 642,  str_pad(number_format($datos->mon_des, 2, ",", "."), $longitud, ' ', STR_PAD_LEFT), $negro);
  imagestring($factura, 3, 673, 658,  str_pad(number_format(($datos->mon_bto-$datos->mon_des), 2, ",", "."), $longitud, ' ', STR_PAD_LEFT), $negro);
  imagestring($factura, 3, 673, 674,  str_pad(number_format($datos->mon_iva, 2, ",", "."), $longitud, ' ', STR_PAD_LEFT), $negro);
  imagestring($factura, 3, 673, 742,  str_pad(number_format($datos->mon_net, 2, ",", "."), $longitud, ' ', STR_PAD_LEFT), $negro);
  imagestring($factura, 3, 665, 742, $moneda, $negro);
  ob_start();
  imagepng($factura);
  $bin = ob_get_clean();
  file_put_contents($_SERVER["DOCUMENT_ROOT"].'/images/facturas/factura_'.$_GET['id'].'.png', $bin);
  $b64 = base64_encode($bin);
  //$contmail='<img src="data:image/png;base64,'.$b64 .'" />';
  $contmail='<img src="'.$_SERVER["DOCUMENT_ROOT"].'/images/facturas/factura_'.$_GET['id'].'.png" />';
  /***********************************/
  include('../lib/TCPDF/tcpdf.php');
  $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('LALEXPO');
  $pdf->SetTitle('Factura');
  $pdf->SetFont('dejavusans', '', 10);
  $pdf->AddPage();
  $pdf->writeHTML($contmail, true, false, true, false, '');
  $pdf->lastPage();
  $pdf->Output($_SERVER["DOCUMENT_ROOT"].'/images/facturas/Factura_'.$_GET['id'].'.pdf','F');
  /***********************************/
  @unlink($_SERVER["DOCUMENT_ROOT"].'/images/facturas/factura_'.$_GET['id'].'.png');
  echo '<img id="factura" src="data:image/png;base64,'.$b64 .'" /><br/><br/>';
//$('#factura').print() ?>
  <div class="card-footer">
    <a class="btn btn-info" href="javascript:;" onclick="window.print();" >Imprimir</a> 
    <a class="btn btn-info" href="javascript:;" onclick="msg.text('Ingrese el correo electronico<br/><input type=email id=emailto />').load().confirm(function(){enviarFactura()})" >Enviar</a> 
    <a class="btn btn-info" target="_blank" href="../images/facturas/Factura_<?=$_GET['id']?>.pdf" >Descargar PDF</a>
    <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>

  </div>
 <script>
   function enviarFactura() {
   $.ajax({
     data:"email="+$("#emailto").val()+'&img=Factura_<?=$_GET['id']?>.pdf',
     url:"../servicios/enviaFactura.php",
     type:'post',
     success:function(d) {
       if(d==1){
        msg.text('Factura enviada exitosamente.').load().aceptar()
       }else{
        msg.text(d).load().aceptar()
       }
     }
   });
   }
 </script>
  <?
  } else { ?>
  <form
  id="form"
  onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'})"
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <div class="col-5">
      <label>Código Cliente</label>
      <input type="text" name="cod_cli" id="cod_cli" class="form-control" value="<?=$datos->cod_cli?>" required="" spellcheck="false" autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Nombre</label>
      <input type="text" name="nombre" id="nombre" class="form-control" value="<?=$datos->nombre?>" required="" spellcheck="false" autocomplete="off" />
    </div>
    <div class="col-5">
      <label>N.I.T</label>
      <input type="text" name="nit" id="nit" class="form-control" value="<?=$datos->nit?>" required="" spellcheck="false" autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Email</label>
      <input type="text" name="email" id="email" class="form-control" value="<?=$datos->email?>" required="" spellcheck="false" autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Direccion</label>
      <input type="text" name="direccion" id="direccion" class="form-control"  value="<?=$datos->direccion?>" required="" spellcheck="false" autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Ciudad</label>
      <input type="text" name="ciudad" id="ciudad" class="form-control"  value="<?=$datos->ciudad?>" required="" spellcheck="false" autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Pais</label>
      <input type="text" name="pais" id="pais" class="form-control"  value="<?=$datos->pais?>" required="" spellcheck="false" autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Telefono</label>
      <input type="text" name="telefono" id="telefono" class="form-control" value="<?=$datos->telefono?>" required="" spellcheck="false" autocomplete="off" />
    </div>
    <div class="col-10 offset-1">
      <label style="float: left;">Descripción</label>
      <input type="text" name="descripcion" id="descripcion" class="form-control"  value="<?=$datos->descrip?>" required="" spellcheck="false" autocomplete="off" />
    </div>
    <div class="col-10 offset-1">
      <label>Detalle</label>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Concepto</th>
            <th width="50">Cantidad</th>
            <th width="120">Precio</th>
            <th width="120">Subtotal</th>
            <th width="40"></th>
          </tr>
        </thead>
        <tbody id="detalle">
        </tbody>
        <tfoot>
          <tr>
            <td>
              <input type="text" class="form-control" id="articulo" onkeyup="buscarItem()" />
              <div class="busart" id="articulos"></div>
            </td>
            <td><input type="number" class="form-control" id="cantidad" min="1" onchange="subtotal()" /> </td>
            <td><input type="number" class="form-control" id="precio" min="1" value="0" onchange="subtotal()" /></td>
            <td><p id="subtotal" class="form-control-plaintext"></p> </td>
            <td><i class="fas fa-plus-circle fa-2x" style="color:green;cursor: pointer;" onclick="agregarItem()"></i></td>
          </tr>
          <tr>
            <td colspan="5" ></td>
          </tr>
          <tr>
            <td colspan="3" align="right">Importe</td>
            <td align="right" id="mbase"><?=$datos->mon_base?></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="3" align="right">Descuento <input type="number" class="form-control" style="display: inline-block; width: 60px;" id="pdcto" name="pdcto" min="0" value="0" onchange="totalizar()" /> %</td>
            <td align="right" id="mdcto" style="color:red;"></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="3" align="right">I.V.A <input type="number" class="form-control" style="display: inline-block; width: 60px;" id="piva" name="piva" min="0" value="19" onchange="totalizar()" />%</td>
            <td align="right" id="miva"></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="3" align="right">Total</td>
            <td align="right" id="mtotal"></td>
            <td></td>
          </tr>
        </tfoot>
      </table>
    </div>
    <div class="card-footer">
      <input type="hidden" name="base" id="base" />
      <input type="hidden" name="iva" id="iva" />
      <input type="hidden" name="descuento" id="descuento" />
      <input type="hidden" name="total" id="total" />
      <input type="hidden" name="lineas" id="lineas" />
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>
<script>
  var lineas = new Array();
  var base=0;
  var descuento=0;
  var iva=0;
  var total=0;
  function subtotal() {
    $("#subtotal").html(parseFloat($("#cantidad").val())*parseFloat($("#precio").val()));
  }
  function agregarItem() {
    let linea = new Object();
    linea.concepto=$("#articulo").val();
    linea.cantidad=$("#cantidad").val();
    linea.precio=$("#precio").val();
    linea.subtotal=parseFloat(linea.cantidad)*parseFloat(linea.precio);
    lineas.push(linea);
    let ind = lineas.indexOf(linea);
    $("#lineas").val(JSON.stringify(lineas));
    $("#articulo").val(''); $("#cantidad").val(''); $("#precio").val('');$("#subtotal").html('');
    totalizar();
  }
  function quitarItem(ind) {
    lineas.splice(ind,1);
    $("#lineas").val(JSON.stringify(lineas));
    totalizar();
  }
  function totalizar() {
    base=0;
    $("#detalle").html('');
    lineas.forEach(actualizar);
  }
  function actualizar(linea, ind) {
    $("#detalle").append('<tr id="lind'+ind+'"><td align="left">'+linea.concepto+'</td><td>'+linea.cantidad+'</td><td>'+linea.precio+'</td><td>'+linea.subtotal+'</td><td><i class="fas fa-minus-circle fa-2x" style="color:red;cursor: pointer;" onclick="quitarItem('+ind+')"></td></tr>');
    base+=parseFloat(linea.subtotal);
    descuento=base*parseFloat($("#pdcto").val())/100;
    iva=(base-descuento)*parseFloat($("#piva").val())/100;
    total=(base-descuento)+iva;
    $("#mbase").html('$ '+base);
    $("#mdcto").html('- $ '+descuento.toFixed(2));
    $("#miva").html('$ '+iva.toFixed(2));
    $("#mtotal").html('$ '+total.toFixed(2));
    $("#base").val(base);
    $("#dcto").val(descuento.toFixed(2));
    $("#iva").val(iva.toFixed(2));
    $("#total").val(total.toFixed(2));
  }
  function buscarItem() {
    $("#cantidad").val(''); $("#precio").val('');$("#subtotal").html('');
    $.ajax({
      data:{"articulo":$("#articulo").val()},
      url:"../includes/articulos.php",
      type:'post',
      dataType:'json',
      success:function(art) {
        $("#articulos").html('');
        for (i=0; i<art.length; i++) {
          $("#articulos").append('<p onclick="selItem(\''+art[i].nombre_es+'\', '+art[i].precio+')">'+art[i].nombre_es+' <span>'+art[i].precio+'</span></p>');
        }
        $("#articulos").show();
      }
    });
  }
  function selItem(conc, precio) {
    $("#articulo").val(conc);
    $("#cantidad").val(1);
    $("#precio").val(precio);
    $("#subtotal").html(precio);
    $("#cantidad").focus();
    $("#articulos").hide();
  }
  <? if(!empty($_SESSION['factura'])) { ?>
    $("#cod_cli").val('<?=$_SESSION['factura']['nit']?>');
    $("#nombre").val('<?=$_SESSION['factura']['nombre']?>');
    $("#nit").val('<?=$_SESSION['factura']['nit']?>');
    $("#email").val('<?=$_SESSION['factura']['email']?>');
    $("#direccion").val('');
    $("#telefono").val('<?=$_SESSION['factura']['telefono']?>');
    $("#descripcion").val('Compra de Paquete de patrocinio');
    $("#articulo").val('<?=$_SESSION['factura']['concepto']?>');
    $("#cantidad").val(1);
    $("#precio").val('<?=$_SESSION['factura']['precio']?>');
    agregarItem();
  <?}?>
</script>
<? }  ?>