<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'})"
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Pregunta Español</label>
      <input type="text" name="preg_es" class="form-control" value="<?=$datos->preg_es?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Pregunta Ingles</label>
      <input type="text" name="preg_en" class="form-control" value="<?=$datos->preg_en?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Respuesta Español</label>
      <textarea name="resp_es" class="form-control"  required=""><?=$datos->resp_es?></textarea>
    </div>
    <div class="col-5">
      <label>Respuesta Ingles</label>
      <textarea name="resp_en" class="form-control"  required=""><?=$datos->resp_en?></textarea>
    </div>
    <div class="col-5">
      <label>Faq seccion</label>
      <select class="form-control" name="seccion" id="seccion">
        <? while($rw=$rs_seccion->fetch_object()){
          $add=$rw->id==$datos->id_seccion?'selected':'';
          ?>
        <option value="<?=$rw->id?>" <?=$add?>><?=$rw->nombre_es?></option>
        <? } ?>
      </select>
    </div>
    <div class="col-5">
      <label>Estado</label>
      <select class="form-control" name="estado" id="estado">
        <option value="1" <?=$datos->estado==1?'selected':''?>>Activo</option>
        <option value="0" <?=$datos->estado==0?'selected':''?>>Inactivo</option>
      </select>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>
  <script src="dist/ckeditor/ckeditor.js"></script>
<script>
  CKEDITOR.editorConfig = function( config ) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;
  };
  CKEDITOR.inline( 'resp_es' );
  CKEDITOR.inline( 'resp_en' );
</script>