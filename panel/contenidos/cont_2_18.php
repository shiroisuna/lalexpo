<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'})"
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Nominar a:</label>
      <select id="usuario" name="usuario" class="form-control">
        <option value="0">Seleccione</option>
        <? foreach($usuarioss as $usu){
          $add=$usu->id==$datos->id_usuario?'selected':'';?>
        <option value="<?=$usu->id?>" <?=$add?>><?=$usu->nombre.' '.$usu->apellido?></option>
        <? } ?>
      </select>
    </div>
    <div class="col-5">
      <label>Categoria</label>
      <select class="form-control" name="categoria" id="categoria">
        <? while($rw=$rs_categorias->fetch_object()){
          $add=$rw->id==$datos->id_categoria?'selected':'';
          ?>
        <option value="<?=$rw->id?>" <?=$add?>><?=$rw->nombre_es?></option>
        <? } ?>
      </select>
    </div>
    <div class="col-5">
      <label>Estado</label>
      <select class="form-control" name="estado" id="estado">
        <option value="1" <?=$datos->estado==1?'selected':''?>>Aprobado</option>
        <option value="0" <?=$datos->estado==0?'selected':''?>>Pendiente</option>
      </select>
    </div>
    <div class="card-footer">
      <input type="hidden" name="id" value="<?=$_GET['id']?>">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>
