<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
<form role="form"
id="form"
onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'})"
class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
  <!-- text input -->
  <div class="col-5">
    <label>Nombre</label>
    <input type="text" name="nombre" class="form-control" value='<?=$datos->nombre?>' required="" maxlength="200" autocomplete="off" />
  </div>
  <div class="col-5">
    <label>Pago para el usuario</label>
     <select class="form-control" name="id_usuario" id="id_usuario">
      <option value="0"> - </option>
      <? foreach($usuarioss as $usu){
        $add=$usu->id==$datos->id_usuario?'selected':''?>
      <option value="<?=$usu->id?>" <?=$add?>><?=$usu->nombre.' '.$usu->apellido?></option>
      <? } ?>
    </select>
  </div>
  <div class="col-5">
    <label>Moneda</label>
    <select class="form-control" name="moneda" id="moneda">
      <option value="USD" <?=$datos->moneda=='USD'?'selected':''?>>USD - Dólar Americano</option>
      <option value="COP" <?=$datos->moneda=='COP'?'selected':''?>>COP - Peso Colombiano</option>
      <option value="MXN" <?=$datos->moneda=='MXN'?'selected':''?>>MXN - Peso Mexicano</option>
      <option value="PEN" <?=$datos->moneda=='PEN'?'selected':''?>>PEN - Nuevo Sol Peruano</option>
      <option value="ARS" <?=$datos->moneda=='ARS'?'selected':''?>>ARS - Peso Argentino</option>
      <option value="BRL" <?=$datos->moneda=='BRL'?'selected':''?>>BRL - Real Brasileño</option>
      <option value="CLP" <?=$datos->moneda=='CLP'?'selected':''?>>CLP - Peso Chileno</option>
    </select>
  </div>
  <div class="col-5">
    <label>Valor</label>
    <input type="number" name="valor" class="form-control" value='<?=$datos->valor?>' min="0" required="" maxlength="3" autocomplete="off" />
  </div>
  <div class="col-5">
    <label>Producto</label>
    <select class="form-control" name="producto" id="producto">
      <option value="1" <?=$datos->producto=='1'?'selected':''?>>Habitaciones</option>
      <option value="2" <?=$datos->producto=='2'?'selected':''?>>Tickets</option>
      <option value="3" <?=$datos->producto=='3'?'selected':''?>>Paquetes</option>
      <option value="4" <?=$datos->producto=='4'?'selected':''?>>Workshop</option>
      <option value="5" <?=$datos->producto=='5'?'selected':''?>>Membresía</option>
      <option value="6" <?=$datos->producto=='6'?'selected':''?>>Otro</option>
    </select>
  </div>
  <div class="col-5">
    <label>Estado</label>
    <select class="form-control" name="estado" id="estado" <? if ($datos->estado==1) echo 'disabled'; ?> >
      <option value="0" <?=$datos->estado=='0'?'selected':''?>>Pendiente</option>
      <option value="1" <?=$datos->estado=='1'?'selected':''?>>Comprado</option>
      <option value="2" <?=$datos->estado=='2'?'selected':''?>>Denegado</option>
      <option value="3" <?=$datos->estado=='3'?'selected':''?>>Expirado</option>
    </select>
  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-info">Guardar</button>
    <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
  </div>
</form>