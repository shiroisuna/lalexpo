<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'})"
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Nombre español</label>
      <input type="text" class="form-control" name="nombre_es" id="nombre_es" value="<?=$datos->nombre_es?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Nombre ingles</label>
      <input type="text" class="form-control" name="nombre_en" id="nombre_en" value="<?=$datos->nombre_en?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Estado</label>
      <select class="form-control" name="estado">
        <option value="0" <?=($datos->estado==0)?'selected':''?>>Inactivo</option>        
        <option value="1" <?=($datos->estado==1)?'selected':''?>>Activo</option>
      </select>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
    <input type="hidden" name="env_cat" value="1" />
    <input type="hidden" name="id" value="<?=$datos->id?>" />
  </form>
<script src="js/jquery.form.js"></script>