<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'})"
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Nombre Español</label>
      <input type="text" name="nombre_es" class="form-control" value='<?=$datos->nombre_es?>' required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Nombre Ingles</label>
      <input type="text" name="nombre_en" class="form-control" value='<?=$datos->nombre_en?>' required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Descripción Español</label>
      <textarea name="descrip_es" id="descrip_es" class="form-control" style="height:250px;"><?=$datos->descrip_es?></textarea>
    </div>
    <div class="col-5">
      <label>Descripción Inglés</label>
      <textarea name="descrip_en" id="descrip_en" class="form-control" style="height:250px;"><?=$datos->descrip_en?></textarea>
    </div>
    <div class="col-5">
      <label>Orden</label>
      <input type="number" name="orden" class="form-control" value='<?=$datos->orden?>' required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Habilitado</label>
      <select class="form-control" name="estado" id="estado">
        <option value="1" <?=$datos->estado==1?'selected':''?>>Si</option>
        <option value="0" <?=$datos->estado==0?'selected':''?>>No</option>
      </select>
    </div>
    <div class="card-footer">
      <input type="hidden" name="id" value="<?=$_GET['id']?>">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>
    <script src="js/jquery.form.js"></script>
<script src="dist/ckeditor/ckeditor.js"></script>
<script>
  CKEDITOR.editorConfig = function( config ) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;
  };
  CKEDITOR.inline( 'descrip_es' );
  CKEDITOR.inline( 'descrip_en' );
</script>