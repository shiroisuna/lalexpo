<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>

  <link href="../css/bootstrap-form-helpers.min.css" rel="stylesheet" media="screen">
  <script src="../js/bootstrap-formhelpers.min.js"></script>

  <form role="form"
  id="form"
  onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'})"
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <div class="col-5">
      <label>Foto</label><br/>
      <img src="../<?=$datos->foto?>" style="height: 150px;" />
    </div>
    <div class="col-5">
      <label>Estado Foto</label>
      <select class="form-control" id="estado_foto" name="estado_foto" >
        <option value="0" <?=($datos->estado_foto==0)?'selected':''?>>Pendiente</option>        
        <option value="1" <?=($datos->estado_foto==1)?'selected':''?>>Aprobada</option>
      </select>
    </div>
    <div class="col-5">
      <label>Nombre</label>
      <input type="text" class="form-control" id="nombre" name="nombre" value="<?=$datos->nombre?>" />
    </div>
    <div class="col-5">
      <label>Apellido</label>
      <input type="text" class="form-control" id="apellido" name="apellido" value="<?=$datos->apellido?>" />
    </div>
    <div class="col-5">
      <label>Email</label>
      <input type="text" class="form-control" id="email" name="email" value="<?=$datos->email?>" />
    </div>
    <div class="col-5">
      <label>DNI</label>
      <input type="text" class="form-control" id="dni" name="dni" value="<?=$datos->dni?>" />
    </div>
    <div class="col-5">
      <label>Género</label>
      <select class="form-control" id="genero" name="genero" >
        <option value="1" <?=($datos->genero==1)?'selected':''?>>Masculino</option>        
        <option value="2" <?=($datos->genero==2)?'selected':''?>>Femenino</option>
        <option value="3" <?=($datos->genero==3)?'selected':''?>>Transgénero</option>
      </select>
    </div>
    <div class="col-5">
      <label>Idioma</label>
      <select class="form-control" id="idioma" name="idioma" />
        <option <?=($datos->idioma=='Español')?'selected':''?>>Español</option>        
        <option <?=($datos->idioma=='English')?'selected':''?>>English</option>
      </select>
    </div>    
    <div class="col-5">
      <label>Teléfono</label>
      <input type="text" class="form-control" id="telefono" name="telefono" value="<?=$datos->telefono?>" />
    </div>
    <div class="col-5">
      <label>Pais</label>
      <select class="form-control bfh-countries" id="pais_cod" name="pais_cod" data-country="<?=$datos->pais_cod?>" onchange="$('#pais').val($('#pais_cod option:selected').text())" ></select>
    </div>
    <div class="col-5">
      <label>Estado/Dpto</label>
      <select class="form-control bfh-states" name="estado_cod" id="estado_cod" data-country="pais" onchange="$('#estado').val($('#pais_cod option:selected').text())" ></select>
    </div>  
    <div class="col-5">
      <label>Ciudad</label>
      <input type="text" class="form-control" id="ciudad" name="ciudad" value="<?=$datos->ciudad?>" />
    </div>
    <div class="col-5">
      <label>Expositor</label>
      <select class="form-control" name="expositor">
        <option value="1" <?=($datos->expositor==0)?'selected':''?>>No</option>        
        <option value="2" <?=($datos->expositor>0)?'selected':''?>>Si</option>
      </select>
    </div>
    <div class="col-5">
      <label>Categoria</label>
      <select class="form-control" name="categoria" id="categoria">
        <? while($rw=$rs_categoria->fetch_object()){
          $add=$rw->id==$datos->id_categoria?'selected':'';
          ?>
        <option value="<?=$rw->id?>" <?=$add?>><?=$rw->nombre?></option>
        <? } ?>
      </select>
    </div>
    <div class="col-5">
      <label>Compañia</label>
      <input type="text" class="form-control" id="compania" name="compania" value="<?=$datos->company?>" />
    </div>
    <div class="col-5">
      <label>Sitio Web</label>
      <input type="text" class="form-control" id="website" name="website" value="<?=$datos->website?>" />
    </div>
    <div class="col-5">
      <label>Modelos (Estudio)</label>
      <input type="number" class="form-control" id="models" name="models" min="0" value="<?=$datos->models?>" />
    </div>

    <fieldset>
      <legend>Social</legend>
      <div class="col-5">
        <label>Facebook</label>
        <input type="text" class="form-control" id="face" name="face" value="<?=$datos->face?>" />
      </div>
      <div class="col-5">
        <label>Twitter</label>
        <input type="text" class="form-control" id="twitter" name="twitter" value="<?=$datos->twitter?>" />
      </div>
      <div class="col-5">
        <label>Instagram</label>
        <input type="text" class="form-control" id="instagram" name="instagram" value="<?=$datos->instagram?>" />
      </div>
      <div class="col-5">
        <label>Skype</label>
        <input type="text" class="form-control" id="skype" name="skype" value="<?=$datos->skype?>" />
      </div>
      <div class="col-5">
        <label>Youtube</label>
        <input type="text" class="form-control" id="youtube" name="youtube" value="<?=$datos->youtube?>" />
      </div>    
    </fieldset>

    <div class="col-5">
      <label>Nombre escarapela</label>
      <input type="text" class="form-control" id="nombre_escarapela" name="nombre_escarapela" value="<?=$datos->nombre_escarapela?>" />
    </div>   
    <div class="col-5">
      <label>Estudio</label>
      <input type="text" class="form-control" id="estudio" name="estudio" value="<?=$datos->estudio?>" />
    </div>
    <div class="col-5">
      <label>Descripción profesional</label>
      <textarea class="form-control" id="descrip_profesional" name="descrip_profesional"><?=$datos->descrip_profesional?></textarea>
    </div>
    
    <div class="col-5">
      <label>Visible</label>
      <select class="form-control" name="habilitado">
        <option value="1" <?=($datos->habilitado==0)?'selected':''?>>No</option>        
        <option value="2" <?=($datos->habilitado>0)?'selected':''?>>Si</option>
      </select>
    </div>
    <div class="card-footer">
      <input type="hidden" name="id" value="<?=$datos->id?>" />
      <input type="hidden" name="pais" id="pais" value="" />
      <input type="hidden" name="estado" id="estado" value="" />
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>