<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="
  "
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Nombre Español</label>
      <input type="text" class="form-control" name="nombre_es" id="nombre_es" value="<?=$datos->nombre_es?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Nombre Ingles</label>
      <input type="text" class="form-control" name="nombre_en" id="nombre_en" value="<?=$datos->nombre_en?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Archivo</label>
      <? if(!empty($datos->imagen)){
      $ext=substr(strrchr($datos->imagen, "."), 1);
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 70px;">
          <source src="../images/hoteleria/<?=$datos->imagen?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="../images/hoteleria/<?=$datos->imagen?>" style="max-height: 70px;" />
      <? }else{?>
        <?=$datos->imagen?>
      <? }
      }?>
      <input type="hidden" name="img_ant" value="<?=$datos->imagen?>" />
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg" name="archivo" id="archivo"  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Precio</label>
      <input type="number" class="form-control" min="0" max="9999999" maxlength="8" name="precio" id="precio" value="<?=$datos->precio?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Personas</label>
      <input type="number" class="form-control" min="0" max="999" maxlength="3" name="personas" id="personas" value="<?=$datos->personas?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Categoria</label>
      <select class="form-control" name="id_categoria" id="id_categoria">
        <? while($rw=$rs_categorias->fetch_object()){
          $add=($datos->id_categoria==$rw->id)?'selected':'';
          ?>
        <option value="<?=$rw->id?>" <?=$add?>><?=$rw->nombre_es?></option>
        <? } ?>
      </select>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
    <input type="hidden" name="env_cat" value="1" />
  </form>
<script src="js/jquery.form.js"></script>
  <script>
function inArray(needle, haystack) {
  var length = haystack.length;
  for(var i = 0; i < length; i++) {
      if(haystack[i] == needle) return true;
  }
  return false;
}
(function(){
  $('form').ajaxForm({
    beforeSend: function() {
      if($('#archivo').val()!=''){
        var ext=$('#archivo').val().split('.').pop()
        if(!inArray(ext,['png','gif','jpg','mp4'])){
          msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
          return false
        }
      }
      msg.text('Subiendo archivo...<br /><b id="procentaje">0%</b>').load()
    },
    uploadProgress: function(event, position, total, percentComplete) {
      $('#procentaje').html(percentComplete+'%')
    },
    success: function() {
      msg.text('Procesando archivo...<br /><img src="img/loading.gif" />').load()
    },
    complete: function(d) {
      if(d.responseText==1){
        msg.text('La habitación ha sido guardada exitosamente').load().aceptar(function(){
          document.location.href='listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'
        })
      }else{
        msg.text(d.responseText).load().aceptar()
      }
    }
  })
})();
  </script>