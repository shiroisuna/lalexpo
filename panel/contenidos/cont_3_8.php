<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form
  id="form"
  onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'})"
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Id</label>
      <input type="text" class="form-control" value="<?=$datos->id?>" disabled="" />
    </div>
    <div class="col-5"></div>
    <div class="col-5">
      <label>Titulo Español</label>
      <input type="text" name="titulo_es" class="form-control" value="<?=$datos->titulo_es?>" required="" spellcheck="false" autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Titulo Ingles</label>
      <input type="text" name="titulo_en" class="form-control" value="<?=$datos->titulo_en?>" required="" spellcheck="false" autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Texto Español</label>
      <textarea class="form-control" name="texto_es" id="texto_es" style="height:180px" required=""><?=$datos->texto_es?></textarea>
    </div>
    <div class="col-5">
      <label>Texto Ingles</label>
      <textarea class="form-control" name="texto_en" id="texto_en" style="height:180px" required=""><?=$datos->texto_en?></textarea>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>
    <script src="dist/ckeditor/ckeditor.js"></script>
<script>
  CKEDITOR.editorConfig = function( config ) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;
  };
  CKEDITOR.inline( 'texto_es' );
  CKEDITOR.inline( 'texto_en' );
</script>