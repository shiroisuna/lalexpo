<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
<script>
  var urlRetorno='listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>';
  function estSol() {
    if ($("#estado").val()==2) {
      urlRetorno='contenido.php?cat=3&obj=9';
    } else {
      urlRetorno='listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>';
    }
  }
  estadoAnt=<?=(int)$datos->estado?>;
  function pre(){
    if($('#estado').val()=='4' && $('#estado').val()!=estadoAnt){
      msg.text('<b>Advertencia:</b> Esta a punto de indicar esta solicitud como pagada, esta accion no se podra retroceder').load().confirm(function(){
        guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:urlRetorno})
      })
    }else{
      guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:urlRetorno})
    }
    return false
  }
</script>
  <form role="form"
  id="form"
  onsubmit="return pre()"
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>ID</label>
      <input type="text" name="id" class="form-control" value='<?=$datos->id?>' disabled />
    </div>
    <div class="col-5">
      <label>Fecha</label>
      <p class="form-control"><?=$datos->fecha?></p>
    </div>
    <div class="col-5">
      <label>Usuario</label>
      <p class="form-control"><?=$datos->usuario?></p>
    </div>
    <div class="col-5">
      <label>Categoria</label>
      <p class="form-control"><?=$datos->categoria?></p>
    </div>
    <div class="col-5">
      <label>Paquete</label>
      <p class="form-control"><?=$datos->paquete?></p>
    </div>
    <div class="col-5">
      <label>Precio</label>
      <p class="form-control">$ <?=$datos->precio?></p>
    </div>
    <div class="col-5">
      <label>Logo</label>
      <p class="form-control">
        <img src="../<?=$datos->logo?>" height="120" />
      </p>
    </div>
    <div class="col-5">
      <label>URL</label>
      <p class="form-control"><?=$datos->url?></p>
    </div>
    <div class="col-5">
      <label>Posicion</label><input class="form-control" type="number" name="posicion" value="0">
    </div>
    <div class="col-5">
      <label>Estado</label>
      <? if($datos->estado!=4){?>
      <select class="form-control" name="estado" id="estado" onchange="estSol()">
        <option value="0" style="color:grey;" <?=$datos->estado==0?'selected':''?>>Anulada</option>
        <option value="1" style="color:orange;"  <?=($datos->estado==1 && !empty($_GET['id']))?'selected':''?>>Pendiente</option>
        <option value="2" style="color:green;"  <?=($datos->estado==2 && !empty($_GET['id']))?'selected':''?>>Aprobada</option>
        <option value="3" style="color:red;"  <?=($datos->estado==3 && !empty($_GET['id']))?'selected':''?>>Rechazada</option>
        <option value="4" style="color:blue;"  <?=($datos->estado==4 && !empty($_GET['id']))?'selected':''?>>Pagado</option>
      </select>
      <? }else{ ?>
       <select class="form-control" disabled="">
        <option>Pagado</option>
       </select>
       <input type="hidden" name="estado_ant" id="estado_ant" value="<?=$datos->estado?>" />
       <input type="hidden" name="estado" id="estado" value="4" />
      <? } ?>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>