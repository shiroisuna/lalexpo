<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="
  "
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Titulo</label>
      <input type="text" class="form-control" name="titulo" maxlength="70" id="titulo" value="<?=$datos->titulo?>" required=""  autocomplete="off" />
    </div>
    <br style="clear: both;" />
    <div class="col-5">
      <label>Nombre Español</label>
      <input type="text" class="form-control" name="nombre_es" maxlength="150" id="nombre_es" value='<?=$datos->nombre_es?>' required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Nombre Ingles</label>
      <input type="text" class="form-control" name="nombre_en" maxlength="150" id="nombre_en" value='<?=$datos->nombre_en?>' required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Imagen Español</label>
      <? if(!empty($datos->imagen_es)){
      $ext=substr(strrchr($datos->imagen_es, "."), 1);
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 70px;">
          <source src="../images/hoteleria/<?=$datos->imagen_es?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="../images/hoteleria/<?=$datos->imagen_es?>" style="max-height: 70px;" />
      <? }else{?>
        <?=$datos->imagen_es?>
      <? }
      }?>
      <input type="hidden" name="img_ant_es" value="<?=$datos->imagen_es?>" />
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg" name="imagen_es" id="imagen_es"  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Imagen Ingles</label>
      <? if(!empty($datos->imagen_en)){
      $ext=substr(strrchr($datos->imagen_en, "."), 1);
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 70px;">
          <source src="../images/hoteleria/<?=$datos->imagen_en?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="../images/hoteleria/<?=$datos->imagen_en?>" style="max-height: 70px;" />
      <? }else{?>
        <?=$datos->imagen_en?>
      <? }
      }?>
      <input type="hidden" name="img_ant_en" value="<?=$datos->imagen_en?>" />
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg" name="imagen_en" id="imagen_en"  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Imagen Vuelos Español</label>
      <? if(!empty($datos->imgvuelos_es)){
      $ext=substr(strrchr($datos->imgvuelos_es, "."), 1);
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 70px;">
          <source src="../images/hoteleria/<?=$datos->imgvuelos_es?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="../images/hoteleria/<?=$datos->imgvuelos_es?>" style="max-height: 70px;" />
      <? }else{?>
        <?=$datos->imgvuelos_es?>
      <? }
      }?>
      <input type="hidden" name="img_ant_vuel_es" value="<?=$datos->imgvuelos_es?>" />
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg" name="imgvuelos_es" id="imgvuelos_es"  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Imagen Vuelos Ingles</label>
      <? if(!empty($datos->imgvuelos_en)){
      $ext=substr(strrchr($datos->imgvuelos_en, "."), 1);
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 70px;">
          <source src="../images/hoteleria/<?=$datos->imgvuelos_en?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="../images/hoteleria/<?=$datos->imgvuelos_en?>" style="max-height: 70px;" />
      <? }else{?>
        <?=$datos->imgvuelos_en?>
      <? }
      }?>
      <input type="hidden" name="img_ant_vuel_en" value="<?=$datos->imgvuelos_en?>" />
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg" name="imgvuelos_en" id="imgvuelos_en"  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Subtitulo Español</label>
      <input type="text" class="form-control" maxlength="150" name="subtitulo_es" id="subtitulo_es" value="<?=$datos->subtitulo_es?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Subtitulo Ingles</label>
      <input type="text" class="form-control" maxlength="150" name="subtitulo_en" id="subtitulo_en" value="<?=$datos->subtitulo_en?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Descripción Español</label>
      <textarea class="form-control" name="descripcion_es" id="descripcion_es" required=""><?=$datos->descripcion_es?></textarea>
    </div>
    <div class="col-5">
      <label>Descripción Ingles</label>
      <textarea class="form-control" name="descripcion_en" id="descripcion_en" required=""><?=$datos->descripcion_en?></textarea>
    </div>
    <div class="col-5">
      <label>Adicional Español</label>
      <textarea class="form-control" name="adicional_es" id="adicional_es" required=""><?=$datos->adicional_es?></textarea>
    </div>
    <div class="col-5">
      <label>Adicional Ingles</label>
      <textarea class="form-control" name="adicional_en" id="adicional_en" required=""><?=$datos->adicional_en?></textarea>
    </div>
    <div class="col-5">
      <label>Categoria</label>
      <input type="number" class="form-control" min="0" max="5" maxlength="1" name="categoria" id="categoria" value="<?=$datos->categoria?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Estado</label>
      <select class="form-control" name="estado" id="estado">
        <option value="1" <?=$datos->estado==1?'selected':''?>>Activo</option>
        <option value="0" <?=($datos->estado==0)?'selected':''?>>Inactivo</option>
      </select>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
    </div>
    <input type="hidden" name="env_hotel" value="1" />
  </form>
<script src="js/jquery.form.js"></script>
  <script src="dist/ckeditor/ckeditor.js"></script>
<script>
  CKEDITOR.editorConfig = function( config ) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;
  };
  CKEDITOR.inline( 'descripcion_es' );
  CKEDITOR.inline( 'descripcion_en' );
  CKEDITOR.inline( 'adicional_es' );
  CKEDITOR.inline( 'adicional_en' );
</script>
  <script>
function inArray(needle, haystack) {
  var length = haystack.length;
  for(var i = 0; i < length; i++) {
      if(haystack[i] == needle) return true;
  }
  return false;
}
(function(){
  $('form').ajaxForm({
    beforeSend: function() {
      if($('#imagen_es').val()!=''){
        var ext=$('#imagen_es').val().split('.').pop()
        if(!inArray(ext,['png','gif','jpg','mp4'])){
          msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
          return false
        }
      }
      if($('#imagen_en').val()!=''){
        var ext=$('#imagen_en').val().split('.').pop()
        if(!inArray(ext,['png','gif','jpg','mp4'])){
          msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
          return false
        }
      }
      if($('#imgvuelos_es').val()!=''){
        var ext=$('#imgvuelos_es').val().split('.').pop()
        if(!inArray(ext,['png','gif','jpg','mp4'])){
          msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
          return false
        }
      }
      if($('#imgvuelos_en').val()!=''){
        var ext=$('#imgvuelos_en').val().split('.').pop()
        if(!inArray(ext,['png','gif','jpg','mp4'])){
          msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
          return false
        }
      }
      msg.text('Subiendo archivos...<br /><b id="procentaje">0%</b>').load()
    },
    uploadProgress: function(event, position, total, percentComplete) {
      $('#procentaje').html(percentComplete+'%')
    },
    success: function() {
      msg.text('Procesando archivos...<br /><img src="img/loading.gif" />').load()
    },
    complete: function(d) {
      if(d.responseText==1){
        msg.text('La habitación ha sido guardada exitosamente').load().aceptar(function(){
          document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$_GET['id']?>'
        })
      }else{
        msg.text(d.responseText).load().aceptar()
      }
    }
  })
})();
  </script>