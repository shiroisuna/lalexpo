<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}
function generarCodigo($longitud) {
  return rand(123456123,999999999);
  /*
 $key = '';
 $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
 $max = strlen($pattern)-1;
 for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
 return $key;*/
}
$cod=$datos->codigo;
if(empty($_GET['id'])){
  $cod=generarCodigo(45);
}
?>
  <form role="form"
  id="form"
  onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'})"
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Codigo</label>
      <input type="text" name="codigo" class="form-control" readonly="" style="background: #CCC;" value="<?=$cod?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label style="display: block;">Porcentaje</label>
      <input type="number" name="porcentaje" style="width: 70px;display: inline-block;" class="form-control" value="<?=$datos->porcentaje?>" required=""  autocomplete="off" /> <b>%</b>
    </div>
    <div class="col-5">
      <label>Asignado a:</label>
      <select id="asignado" name="asignado" class="form-control">
        <option value="0">Seleccione</option>
        <? foreach($usuarioss as $usu){
          $add=$usu->id==$datos->id_usuario?'selected':'';?>
        <option value="<?=$usu->id?>" <?=$add?>><?=$usu->nombre.' '.$usu->apellido?></option>
        <? } ?>
      </select>
    </div>
    <br style="clear:both" />
    <div class="col-5">
      <label>Redimido</label>
       <select class="form-control" disabled="">
        <option value="0" <?=($datos->redimido==0)?'selected':''?>>No</option>
        <option value="1" <?=($datos->redimido==1)?'selected':''?>>Si</option>
      </select>
    </div>
    <div class="col-5">
      <label>Redimido por:</label>
       <select class="form-control" disabled="">
        <option value="0"> - </option>
        <? foreach($usuarioss as $usu){
          $add=$usu->id==$datos->id_usuario_redimido?'selected':'';
          ?>
        <option value="<?=$usu->id?>" <?=$add?>><?=$usu->nombre.' '.$usu->apellido?></option>
        <? } ?>
      </select>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>