<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
<script src="https://cdn.ckeditor.com/ckeditor5/10.1.0/classic/ckeditor.js"></script>
  <form role="form"
  id="form"
  onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize()+'&edit='+encodeURIComponent(_editor.getData()),back:'listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'})"
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Nombre Español</label>
      <input type="text" name="nombre_es" class="form-control" value="<?=$datos->nombre_es?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Nombre Ingles</label>
      <input type="text" name="nombre_en" class="form-control" value="<?=$datos->nombre_en?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Orden</label>
      <input type="number" name="orden" maxlength="2" max="99" min="0" class="form-control" value="<?=$datos->orden?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Habilitado</label>
      <select class="form-control" name="habilitado" id="habilitado">
        <option value="1" <?=$datos->habilitado==1?'selected':''?>>Si</option>
        <option value="0" <?=($datos->habilitado==0 && !empty($_GET['id']))?'selected':''?>>No</option>
      </select>
    </div>
    <div class="col-5">
      <label>Tipo</label>
      <select class="form-control" name="tipo" id="tipo" onchange="chgTipo(this.value)">
        <option value="1" <?=$datos->tipo==1?'selected':''?>>Dinamico</option>
        <option value="2" <?=$datos->tipo==2?'selected':''?>>Estatico</option>
      </select>
    </div>
    <div class="col-5">
      <label>Seccion</label>
      <select name="seccion" id="seccion" class="form-control" onchange="chgCat(this.value)">
        <? while($rw=$cat_tradu->fetch_object()){
          $add='';
          if(empty($datos->id) && $_SESSION['ult_cat_tradu']==$rw->id){
            $add='selected';
          }
          if($datos->id_seccion==$rw->id){
            $add='selected';
          }
          ?>
        <option value="<?=$rw->id?>" <?=$add?>><?=$rw->id?> - <?=$rw->nombre?></option>
        <? } ?>
        <option value="-1" style="color:blue;font-weight:bold;">Nuevo</option>
      </select>
    </div>
     <br style="clear: both;" />
    <div class="col-5" id="dv_contenido" <?=($datos->tipo!=2)?'style="display:none"':''?>>
      <label>Contenido</label>
      <textarea name="contenido" id="contenido">
        <?=$datos->contenido?>
      </textarea>
    </div>
    <div class="col-5" id="dv_url" <?=($datos->tipo!=1)?'style="display:none"':''?>>
      <label>Pagina</label>
      <input type="text" name="url" class="form-control" value="<?=$datos->url?>"  autocomplete="off" />
    </div>
    <script>
    editor=ClassicEditor
        .create( document.querySelector( '#contenido' ) )
        .then( editor => {
          _editor=editor
            console.log( editor );
        } )
        .catch( error => {
            console.error( error );
        } );
        $(document).ready(function(){
          chgTipo($('#tipo').val())
        })
        function chgTipo(v){
          if(v==1){
            $('#dv_contenido').hide()
            $('#dv_url').show()
          }else{
            $('#dv_contenido').show()
            $('#dv_url').hide()
          }
        }

  function chgCat(v){
    if(v=='-1'){
      msg.text('<b>Nombre:</b><br /><input type="text" class="form-control" id="nombre_cat" />').load().aceptar(function(){
        if($('#nombre_cat').val()==''){
          chgCat($('#seccion').val())
          return false
        }
        var nombre=$('#nombre_cat').val()
        msg.text('Creando seccion...<br /><img src="img/loading.gif" />').load()
        $.ajax({
          url:'contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>',type:'post',
          data:'addSec=1&nombre='+nombre,dataType:'json',
          success:function(d){
            if(d.estado=='1'){
              var i,t='',add
              for(i in d.lista){
                add=(d.lista[i].id==d.id)?'selected':''
                t+='<option value="'+d.lista[i].id+'" '+add+'>'+d.lista[i].id+' - '+d.lista[i].nombre+'</option>'
              }
              t+='<option value="-1" style="color:blue;font-weight:bold;">Nuevo</option>'
              $('#seccion').html(t)
              msg.close()
            }else{
              msg.text(d.msg).load().aceptar()
            }
          },error:function(d){
            msg.text(d.responseText).load().aceptar()
          }
        })
      })
    }
  }
  
</script>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>