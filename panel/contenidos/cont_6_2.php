<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>
  <form role="form"
  id="form"
  onsubmit="
  "
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Nombre español</label>
      <input type="text" class="form-control" name="nombre_es" id="nombre_es" value="<?=$datos->nombre_es?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Nombre ingles</label>
      <input type="text" class="form-control" name="nombre_en" id="nombre_en" value="<?=$datos->nombre_en?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Categoria</label>
      <select class="form-control" name="categoria" id="categoria">
        <option value=""></option>
        <? while($rw=$rs_categorias->fetch_object()){
          $add=$rw->id==$datos->id_categoria?'selected':'';
          ?>
        <option value="<?=$rw->id?>" <?=$add?>><?=$rw->nombre_es?></option>
        <? } ?>
      </select>
    </div>
    <div class="col-5">
      <label>Disponible</label>
      <input type="text" class="form-control" name="disponible" id="disponible" value="<?=$datos->disponible?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Vendido</label>
      <div class="form-control"><?=$datos->vendido?>&nbsp;</div>
    </div>
    <div class="col-5">
      <label>Valor</label>
      <input type="text" class="form-control" name="valor" id="valor" value="<?=$datos->valor?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Descripción Español</label>
      <textarea class="form-control" name="descripcion_es" id="descripcion_es" required=""><?=$datos->descripcion_es?></textarea>
    </div>
    <div class="col-5">
      <label>Descripción Ingles</label>
      <textarea class="form-control" name="descripcion_en" id="descripcion_en" required=""><?=$datos->descripcion_en?></textarea>
    </div>
    <div class="col-5">
      <label>Archivo</label><br/>
      <img height="80" src="../images/productos/<?=$datos->archivo?>" />
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg" name="archivo" id="archivo"  />
    </div>
    <div class="col-5">
      <label>Activo</label>
      <select class="form-control" name="activo">
        <option value="0" <?=($datos->activo==0)?'selected':''?>>No</option>        
        <option value="1" <?=($datos->activo==1)?'selected':''?>>Si</option>
      </select>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
    <input type="hidden" name="env_prod" value="1" />
  </form>
<script src="js/jquery.form.js"></script>
  <script src="dist/ckeditor/ckeditor.js"></script>
<script>
  CKEDITOR.editorConfig = function( config ) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;
  };
  CKEDITOR.inline( 'descripcion_es' );
  CKEDITOR.inline( 'descripcion_en' );
</script>
  <script>
function inArray(needle, haystack) {
  var length = haystack.length;
  for(var i = 0; i < length; i++) {
      if(haystack[i] == needle) return true;
  }
  return false;
}
(function(){
  $('form').ajaxForm({
    beforeSend: function() {
      if($('#archivo').val()!=''){
        var ext=$('#archivo').val().split('.').pop()
        if(!inArray(ext,['png','gif','jpg','mp4'])){
          msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
          return false
        }
        msg.text('Subiendo archivo...<br /><b id="procentaje">0%</b>').load()
      }
    },
    uploadProgress: function(event, position, total, percentComplete) {
      $('#procentaje').html(percentComplete+'%')
    },
    success: function() {
      msg.text('Procesando archivo...<br /><img src="img/loading.gif" />').load()
    },
    complete: function(d) {
      if(d.responseText==1){
        msg.text('La imagen han sido guardado exitosamente').load().aceptar(function(){
          document.location.href='listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'
        })
      }else{
        msg.text(d.responseText).load().aceptar()
      }
    }
  })
})();
  </script>