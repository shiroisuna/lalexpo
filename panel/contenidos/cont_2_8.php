<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?} ?>
  <form role="form" enctype="multipart/form-data"
  id="form"
  onsubmit=""
  class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
    <!-- text input -->
    <div class="col-5">
      <label>Nombre Español</label>
      <input type="text" name="nombre_es" class="form-control" value='<?=$datos->nombre_es?>' required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Nombre Ingles</label>
      <input type="text" name="nombre_en" class="form-control" value='<?=$datos->nombre_en?>' required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Categoria</label>
      <select class="form-control" name="categoria" id="categoria">
        <? while($rw=$rs_categorias->fetch_object()){
          $add=$rw->id==$datos->id_categoria?'selected':'';
          ?>
        <option value="<?=$rw->id?>" <?=$add?>><?=$rw->nombre_es?></option>
        <? } ?>
      </select>
    </div>
    <div class="col-5">
      <label>Habilitado</label>
      <select class="form-control" name="estado" id="estado">
        <option value="1" <?=$datos->estado==1?'selected':''?>>Si</option>
        <option value="0" <?=$datos->estado==0?'selected':''?>>No</option>
      </select>
    </div>
    <div class="col-5">
      <label>Cantidad</label>
      <input type="number" name="cantidad" class="form-control" value="<?=$datos->cantidad?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Precio</label>
      <input type="number" name="precio" class="form-control" value="<?=$datos->precio?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Nro. Tiquetes</label>
      <input type="number" name="tiquetes" class="form-control" value="<?=$datos->tiquetes?>" required=""  autocomplete="off" />
    </div>
    <div class="col-5">
      <label>Imagen</label>
      <img src="../images/paquetes/<?=$datos->imagen?>" height="80">
      <input type="file" class="form-control" accept=".png,.jpg,.gif,.svg" name="imagen" id="imagen"  />
    </div>
    <div class="col-5">
      <label>Descripción Español</label>
      <textarea name="descrip_es" id="descrip_es" class="form-control" style="height:250px;"><?=$datos->descrip_es?></textarea>
    </div>
    <div class="col-5">
      <label>Descripción Inglés</label>
      <textarea name="descrip_en" id="descrip_en" class="form-control" style="height:250px;"><?=$datos->descrip_en?></textarea>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>
    </div>
  </form>
  <script src="js/jquery.form.js"></script>
<script src="dist/ckeditor/ckeditor.js"></script>
<script>
  CKEDITOR.editorConfig = function( config ) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;
  };
  CKEDITOR.inline( 'descrip_es' );
  CKEDITOR.inline( 'descrip_en' );
function inArray(needle, haystack) {
  var length = haystack.length;
  for(var i = 0; i < length; i++) {
      if(haystack[i] == needle) return true;
  }
  return false;
}
(function(){
  $('form').ajaxForm({
    beforeSend: function() {
      if($('#imagen').val()!=''){
        var ext=$('#imagen').val().split('.').pop()
        if(!inArray(ext,['png','gif','jpg','mp4'])){
          msg.text('Solo se permiten las extenciones jpg,gif,png,mp4').load().aceptar()
          return false
        }
        msg.text('Subiendo archivo...<br /><b id="procentaje">0%</b>').load()
      }
    },
    uploadProgress: function(event, position, total, percentComplete) {
      $('#procentaje').html(percentComplete+'%')
    },
    success: function() {
      msg.text('Procesando archivo...<br /><img src="img/loading.gif" />').load()
    },
    complete: function(d) {
      if(d.responseText==1){
        msg.text('Los datos se han guardado exitosamente.').load().aceptar(function(){
          document.location.href='listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>'
        })
      }else{
        msg.text(d.responseText).load().aceptar()
      }
    }
  })
})();
  </script>