<? if(!empty($msg)){?>
<span style="color: green;"><?=$msg?></span>
<?}?>
<? if(!empty($error)){?>
<span style="color: red;"><?=$error?></span>
<?}?>

<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th>Fecha</th>
    <th>Votante</th>
    <th>Nominado</th>
    <th>Categoría</th>
    <th>Estado</th>
    <th></th>
  </tr>
  <?
  if($rs->num_rows>0){
  $numfil=0; $nulos=0;
  while($rw=$datos->fetch_object()){ ?>
  <tr>
    <td><?=$rw->id?></td>
    <td><?=strftime("%d/%m/%Y %I:%M %p", strtotime($rw->fecha))?></td>
    <td><?=$rw->votante?></td>
    <td><?=$rw->nominado?></td>
    <td><?=$rw->categoria?></td>
    <td><?=($rw->estado==1)?'Activo':'Anulado'?></td>
    <td>
      <? if ($rw->estado==1) { ?>
      <a href="javascript:;" onclick="msg.text('¿Desea anular este voto?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&anular=<?=$rw->id?>&id=<?=$_GET['id']?>'})" title="Anular"><i class="far fa-times-circle"></i></a>
    <? } else { ?>
      <a href="javascript:;" onclick="msg.text('¿Desea activar este voto?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&activar=<?=$rw->id?>&id=<?=$_GET['id']?>'})" title="Activar"><i class="far fa-check-circle"></i></a>      
    <?
      $nulos++; 
      } ?>   
      <a href="javascript:;" onclick="msg.text('¿Desea eliminar este voto?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>&id=<?=$_GET['id']?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a>
    </td>
  </tr>
  <? $numfil++;

   } 
 }else{ ?>
  <tr>
    <td colspan="7">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>
<br/>
<table style="width:250px;" class="table table-bordered">
  <tr>
    <th colspan="2">Resumen</th>
  </tr>
  <tr>
    <td width="50%">Total votos:</td>
    <td><?=$numfil?></td>
  </tr>
  <tr>
    <td>Votos nulos:</td>
    <td> <?=$nulos?></td>
  </tr>
  <tr>
    <td>Votos activos: </td>
    <td><?=($numfil-$nulos)?></td>
  </tr>
</table>
<a class="btn btn-info" href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>">Volver</a>

