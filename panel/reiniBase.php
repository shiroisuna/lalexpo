<?
include("includes/conexion.php");
if($_SESSION['tipo']!=1){
  echo '<b style="color:red">No sos administrador</b>';
  exit;
}
if(!empty($_GET['reini'])){
  $tbs=array('accidentes','empleados','empresas','horas','obras','obras_empleados');
  foreach($tbs as $tb){
    $con->query('TRUNCATE TABLE '.$tb);
    if(!empty($con->error)){
      echo $con->error;
      exit;
    }
  }
  $con->query("DELETE FROM usuarios WHERE usuario!='admin'");
  if(!empty($con->error)){
    echo $con->error;
    exit;
  }
  echo 1;
  exit;
}
include("includes/header.php");
include("includes/top.php");
include("includes/menu.php")?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="nav-icon fas fa-cogs text-info"></i> Reiniciar Base de Datos</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item active">Reiniciar Base de Datos</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
<style>
    .col-5,.col-3{float: left;}
    </style>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6" style="margin: auto;">
            <div class="card">
              <div class="card-header reves">
                <h3 class="card-title">Reiniciar Base de Datos</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <? if(!empty($msg)){?>
              <span style="color: green;"><?=$msg?></span>
              <?}?>
              <? if(!empty($error)){?>
              <span style="color: red;"><?=$error?></span>
              <?}?>
                <form role="form"
                id="form"
                class="form-horizontal"
                onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'reiniBase.php'})"
                method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
                  <!-- text input -->
                  <div class="col-12">
                    <label style="font-weight: normal;font-size: 13px;">
                    <b style="color: red;font-size: 15px;">Esta a punto de reiniciar la base de datos.</b><br /><br />
                    Tenga en cuenta que este proceso eliminar&aacute;:<br />
                    * Registros de accidentes<br />
                    * Empleados<br />
                    * Empresas<br />
                    * Registros de horas<br />
                    * Obras<br />
                    * Relaci&oacute;n de obras y empleados<br /><br />
                    Todos los usuarios ser&aacute;n eliminados menos el administrador, que seguir&aacute; conservando su contrase&ntilde;a actual.
                    </label>
                  </div>
                  <div class="card-footer" style="clear: both; text-align: center;">
                    <button type="button" onclick="preReini()" class="btn btn-info">Reiniciar</button>
                  </div>
                </form>
                <script>
                function preReini(){
                  msg.text('¿Desea realmente reiniciar la base de datos?').load().confirm(function(){
                    reini()
                  })
                }
                function reini(){
                  msg.text('Reiniciando...<br /><img src="img/loading.gif" />').load()
                  $.ajax({
                    url:'reiniBase.php?reini=1',
                    success:function(d){
                      if(d==1){
                        msg.text('La base de datos ha sido reiniciada exitosamente.').load().aceptar()
                      }else{
                        msg.text(d).load().aceptar()
                      }
                    }
                  })
                }
                </script>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include("includes/footer.php")?>