$(document).ready(function(){
    cargaTiposForms()
    setToolTip()
})
function setToolTip(){
  $('.imgAcc').tooltip({
      position: {
        my: "center bottom-20",
        at: "center top",
        using: function( position, feedback ) {
          $( this ).css( position );
          $( "<div>" )
            .addClass( "arrow" )
            .addClass( feedback.vertical )
            .addClass( feedback.horizontal )
            .appendTo( this );
        }
      }
    })
   $('.trCriterios').hover(function(){
  	$(this).find('.imgAcc').parent().css('display','inline').css('opacity','0').animate({opacity:1},700)
  },function(){
  	$(this).find('.imgAcc').parent().css('display','none')
  })
}
function cargaTiposForms(sel){
    $('#tipoforms').html('<option value="0">Cargando...</option>')
    $.ajax({
        url:'newforms.php',type:'POST',data:'getTipoFormularios=1',dataType:'json',
        success:function(d){
            var t='',i
            t+='<option value="0">Seleccione</option>'
            for(i in d){
                t+='<option value="'+d[i].id+'">'+d[i].nombre+'</option>'
            }
            t+='<option value="-1" style="color:#0084b9;font-weight:bold">Crear nuevo...</option>'
            $('#tipoforms').html(t)
            if(typeof sel=='undefined') sel='0'
            $('#tipoforms').val(sel)
            verifAcciones()
        },error:function(d){
            msg.text(d.responseText).load().aceptar()
        }
    })
}
function chgTipoForm(v){
    verifAcciones()
    if(v=='-1'){
        msg.text('Nombre del TIPO de formulario: <br /><input type="text" maxlength="50" id="nombreTipoForm" />').load().confirm(function(){
            var nombreForm=$('#nombreTipoForm').val()
            msg.text('Creando tipo de formulario....<br /><img src="img/loading.gif" />').load()
            $.ajax({
                url:'newforms.php',type:'POST',data:'setNuevoTipoForm=1&nombre='+nombreForm,dataType:'json',
                success:function(d){
                    if(d.estado==1){
                        msg.text('Tipo de formulario creado exitosamente.').load().aceptar(function(){
                            msg.close()
                            cargaTiposForms(d.id)
                        })
                    }else{
                      msg.text(d).load().aceptar()
                    }
                },error:function(d){
                    msg.text(d.responseText).load().aceptar()
                }
            })
        })
    }
}
function chgForm(v){
    verifAccionesForm()
    if(v=='-1'){
        msg.text('Nombre del formulario: <br /><input type="text" maxlength="50" id="nombreForm" />').load().confirm(function(){
            var nombreForm=$('#nombreForm').val()
            msg.text('Creando de formulario....<br /><img src="img/loading.gif" />').load()
            $.ajax({
                url:'newforms.php',type:'POST',data:'setNuevoForm=1&nombre='+nombreForm+'&tipo='+$('#tipoforms').val(),dataType:'json',
                success:function(d){
                    if(d.estado==1){
                        msg.text('Formulario creado exitosamente.').load().aceptar(function(){
                            msg.close()
                            getFormularios(d.id)
                        })
                    }else{
                      msg.text(d).load().aceptar()
                    }
                },error:function(d){
                    msg.text(d.responseText).load().aceptar()
                }
            })
        })
    }
}
function getFormularios(sel){
  $('#forms').html('<option value="0">Cargando...</option>')
  $.ajax({
      url:'newforms.php',type:'POST',data:'getFormularios=1&tipo='+$('#tipoforms').val(),dataType:'json',
      success:function(d){
          var t='',i
          t+='<option value="0">Seleccione</option>'
          for(i in d){
              t+='<option value="'+d[i].id+'">'+d[i].nombre_form+'</option>'
          }
          t+='<option value="-1" style="color:#0084b9;font-weight:bold">Crear nuevo...</option>'
          $('#forms').html(t)
          if(typeof sel=='undefined') sel='0'
          $('#forms').val(sel)
          verifAccionesForm()
      },error:function(d){
          msg.text(d.responseText).load().aceptar()
      }
  })
}
function modifNombreShow(){
  msg.text('Nombre del TIPO de formulario: <br /><input type="text" maxlength="50" id="nombreTipoForm" value="'+$('#tipoforms option:selected').text()+'" />').load().confirm(function(){
    var nombreForm=$('#nombreTipoForm').val()
    msg.text('Modificando nombre....<br /><img src="img/loading.gif" />').load()
    $.ajax({
      url:'newforms.php',type:'POST',data:'modificarTipoForm=1&id='+$('#tipoforms').val()+'&nombre='+nombreForm,
      success:function(d){
          if(d==1){
              msg.text('El nombre del tipo de formulario fue modificado exitosamente.').load().aceptar(function(){
                  msg.close()
                  cargaTiposForms($('#tipoforms').val())
              })
          }else{
            msg.text(d).load().aceptar()
          }
      },error:function(d){
          msg.text(d.responseText).load().aceptar()
      }
    })
  })
}
function modifNombreFormShow(){
  msg.text('Nombre del formulario: <br /><input type="text" maxlength="50" id="nombreForm" value="'+$('#forms option:selected').text()+'" />').load().confirm(function(){
    var nombreForm=$('#nombreForm').val()
    msg.text('Modificando nombre....<br /><img src="img/loading.gif" />').load()
    $.ajax({
      url:'newforms.php',type:'POST',data:'modificarForm=1&id='+$('#forms').val()+'&nombre='+nombreForm,
      success:function(d){
          if(d==1){
              msg.text('El nombre del formulario fue modificado exitosamente.').load().aceptar(function(){
                  msg.close()
                  getFormularios($('#forms').val())
              })
          }else{
            msg.text(d).load().aceptar()
          }
      },error:function(d){
          msg.text(d.responseText).load().aceptar()
      }
    })
  })
}
function avisoElimTipo(){
  var t='<label style="font-weight: normal;font-size: 13px;">'
  t+='<b style="color: red;font-size: 15px;">Esta a punto de eliminar el tipo de formulario de la base de datos.</b><br><br>'
  t+='Tenga en cuenta que este proceso eliminará:<br />'
  t+='* Datos de formularios de este tipos<br />'
  t+='* Formularios de este tipo<br />'
  t+='* Criterios de formularios de este tipo'
  t+='</label>';

  msg.text(t).load().confirm(function(){
    msg.text('Eliminando tipo de formulario...<br /><img src="img/loading.gif" />').load()
    $.ajax({
      url:'newforms.php',type:'POST',data:'elimTipo=1&id='+$('#tipoforms').val(),
      success:function(d){
        if(d==1){
          msg.text('El tipo de formulario ha sido eliminado exitosamente.').load().aceptar(function(){
            msg.close()
            cargaTiposForms()
          })
        }else{
          msg.text(d).load().aceptar()
        }
      },error:function(d){
        msg.text(d.responseText).load().aceptar()
      }
    })
  })
}
function avisoElimForm(){
  msg.text('¿Desea realmente eliminar este formulario?').load().confirm(function(){
    msg.text('Eliminando formulario...<br /><img src="img/loading.gif" />').load()
    $.ajax({
      url:'newforms.php',type:'POST',data:'elimForm=1&id='+$('#forms').val(),
      success:function(d){
        if(d==1){
          msg.text('El formulario ha sido eliminado exitosamente.').load().aceptar(function(){
            msg.close()
            getFormularios()
          })
        }else{
          msg.text(d).load().aceptar()
        }
      },error:function(d){
        msg.text(d.responseText).load().aceptar()
      }
    })
  })
}
function verifAccionesForm(){
  var v=$('#forms').val()
  if(v!='-1'&&v!='0'){
    $('#accionesForm,#contenidoXForm').css('display','inline')
    generarVistaPrevia()
  }else{
    $('#accionesForm,#contenidoXForm').css('display','none')
  }
}
function verifAcciones(){
  var v=$('#tipoforms').val()
  if(v!='-1'&&v!='0'){
    $('#acciones').css('display','inline')
    getFormularios()
  }else{
    $('#acciones').css('display','none')
  }
}
function generarVistaPrevia(){
  msg.text('Generando vista previa...<br /><img src="img/loading.gif" />').load()
  $.ajax({
    url:'newforms.php',type:'POST',data:'getDatos=1&id='+$('#forms').val(),dataType:'json',
    success:function(d){
      msg.close()

      $('#dts_nomForm').html(d.forms.nombre_form)
      $('#nombre_si').val(d.forms.nombre_si)
      $('#nombre_no').val(d.forms.nombre_no)
      $('#tiene_noaplica').prop('checked',(d.forms.tiene_noaplica==1))
      $('#add1').val(d.forms.add1)
      $('#muestra_accionestomar').prop('checked',(d.forms.muestra_accionestomar==1))

      $('#tit_si').html(d.forms.nombre_si)
      $('#tit_no').html(d.forms.nombre_no)
      $('#tit_add1').html(d.forms.add1)

      var e,t='',c=1
      for(e in d.criterios){
        if(d.criterios[e].esDivision=='0'){
          t+='<tr class="trCriterios _act">'
          t+='<td style="width:10px">'+c+'</td><td>'+d.criterios[e].descripcion
          t+=' <span style="display:none" onclick="modifCriterioShow(\''+d.criterios[e].id+'\',\''+d.criterios[e].descripcion+'\',\''+d.criterios[e].orden+'\',\''+d.criterios[e].esDivision+'\')"><img src="img/edit.svg" class="imgAcc" title="Modificar" /></span> '
          t+='<span onclick="elimCriterio(\''+d.criterios[e].id+'\')" style="display:none"><img src="img/dele.svg" class="imgAcc" title="Eliminar" /></span>'
          t+=' </td>'
          t+='<td style="text-align:center"><input type="checkbox" disabled="disabled" /></td>'
          t+='<td style="text-align:center"><input type="checkbox" disabled="disabled" /></td>'
          t+='<td style="text-align:center"><input type="checkbox" disabled="disabled" /></td>'
          t+='<td style="text-align:center"><input type="text" style="width:100%" disabled="disabled" /></td>'
          t+='</tr>'
          c++
        }else{
          t+='<tr class="trCriterios" style="background:#d6ebf4"><td colspan="6" style="font-weight:bold;font-size: 12px">'+d.criterios[e].descripcion
          t+=' <span style="display:none" onclick="modifCriterioShow(\''+d.criterios[e].id+'\',\''+d.criterios[e].descripcion+'\',\''+d.criterios[e].orden+'\',\''+d.criterios[e].esDivision+'\')"><img src="img/edit.svg" class="imgAcc" title="Modificar" /></span> '
          t+='<span onclick="elimCriterio(\''+d.criterios[e].id+'\')" style="display:none"><img src="img/dele.svg" class="imgAcc" title="Eliminar" /></span>'
          t+='</td></tr>'
        }
      }
      $('#tbContent').html(t)
      setToolTip()
      $('#contenidoXForm').css('display','block')
    },error:function(d){
      msg.text(d.responseText).load().aceptar()
    }
  })
}
function modifCriterioShow(_id,_descrip,_orden,_divi){
  msg.text('Criterio: <br /><textarea maxlength="120" id="descripcion" style="height:55px;width:100%">'+_descrip+'</textarea><br />Orden: <input type="number" maxlength="3" value="'+_orden+'" id="orden" /><br />Es division: <input type="checkbox" id="esDivision" value="1" '+(_divi==1?'checked':'')+' />').load().confirm(function(){
    var descripcion=$('#descripcion').val(),orden=$('#orden').val(),esDivision=($('#esDivision').is(':checked')?'1':'0')
    msg.text('Modificando nombre....<br /><img src="img/loading.gif" />').load()
    $.ajax({
      url:'newforms.php',type:'POST',data:'modifCriterio=1&id='+_id+'&descripcion='+descripcion+'&orden='+orden+'&esDivision='+esDivision,
      success:function(d){
          if(d==1){
              msg.text('El criterio fue modificado exitosamente.').load().aceptar(function(){
                  msg.close()
                  generarVistaPrevia()
              })
          }else{
            msg.text(d).load().aceptar()
          }
      },error:function(d){
          msg.text(d.responseText).load().aceptar()
      }
    })
  })
}
function elimCriterio(_id){
  msg.text('¿Desea eliminar este criterio?').load().confirm(function(){
    msg.text('Eliminando criterio...<br /><img src="img/loading.gif" />').load()
    $.ajax({
      url:'newforms.php',type:'POST',data:'elimCriterio=1&id='+_id,
      success:function(d){
        if(d==1){
          msg.text('El criterio ha sido eliminado exitosamente.').load().aceptar(function(){
            msg.close()
            generarVistaPrevia()
          })
        }else{
          msg.text(d).load().aceptar()
        }
      },error:function(d){
        msg.text(d.responseText).load().aceptar()
      }
    })
  })
}

function showAddCriterio(){
  msg.text('Criterio: <br /><textarea maxlength="120" id="descripcion" style="height:55px;width:100%" /><br />Orden: <input type="number" maxlength="3" value="1" id="orden" /><br />Es division: <input type="checkbox" id="esDivision" value="1" />').load().confirm(function(){
    var descripcion=$('#descripcion').val(),orden=$('#orden').val(),esDivision=($('#esDivision').is(':checked')?'1':'0')
    msg.text('Modificando nombre....<br /><img src="img/loading.gif" />').load()
    $.ajax({
      url:'newforms.php',type:'POST',data:'addCriterio=1&idForms='+$('#forms').val()+'&descripcion='+descripcion+'&orden='+orden+'&esDivision='+esDivision,
      success:function(d){
          if(d==1){
              msg.text('El criterio fue creado exitosamente.').load().aceptar(function(){
                  msg.close()
                  generarVistaPrevia()
              })
          }else{
            msg.text(d).load().aceptar()
          }
      },error:function(d){
          msg.text(d.responseText).load().aceptar()
      }
    })
  })
}
function guardaConfigs(){
  msg.text('Guardando configuración....<br /><img src="img/loading.gif" />').load()
 $.ajax({
    url:'newforms.php',type:'POST',data:'guardaConfigs=1&id='+$('#forms').val()+'&'+$('#formConfig').serialize(),
    success:function(d){
      if(d==1){
          msg.text('El configuración para este formulario se ha guardado exitosamente.').load().aceptar(function(){
              msg.close()
              generarVistaPrevia()
          })
      }else{
        msg.text(d).load().aceptar()
      }
    },error:function(d){
        msg.text(d.responseText).load().aceptar()
    }
  })
}