$(document).ready(function(){
  var checkout=$('#desde').datepicker({
    format:'dd/mm/yyyy',
    onRender: function(date) {
      return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
  }).on('changeDate', function(ev) {
    var newDate = new Date(ev.date)
    newDate.setDate(newDate.getDate()+6);
    console.log(newDate);
    var dia=newDate.getDate(),mes=(newDate.getMonth()+1);
    if(parseInt(dia)<10){
      dia='0'+dia;
    }
    if(parseInt(mes)<10){
      mes='0'+mes;
    }
    $('#hasta').val(dia+'/'+mes+'/'+newDate.getFullYear())
    checkout.hide();
    getDatos($('#obra').val())
  }).data('datepicker');
})
_dias=['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado']
function indicaRangoEncabezado(){
var _fDesde=$('#desde').val().split('/')
_fDesde=new Date(_fDesde[2],(parseInt(_fDesde[1])-1),_fDesde[0])
var _fHasta=new Date(_fDesde)
 _fHasta.setDate(_fDesde.getDate()+6),t=''
while(_fDesde.valueOf()<=_fHasta.valueOf()){
  var dia=_fDesde.getDate(),mes=(_fDesde.getMonth()+1);
  if(parseInt(dia)<10){
    dia='0'+dia;
  }
  if(parseInt(mes)<10){
    mes='0'+mes;
  }
  t+='<th class="tituloFecha reves"><div>'+_dias[_fDesde.getDay()]+'<br />'+dia+'/'+mes+'</div>'
  t+='<table style="margin:auto"><tr>'
  t+='<td class="td02">HHE</td>'
  t+='<td class="td02" style="width:5px !important"></td>'
  t+='<td class="td02">HHST</td>'
  t+='</tr></table>'
  t+='</th>'
  _fDesde.setDate(_fDesde.getDate()+1)
}
__t=t
return t
}
function armaCampos(id,obra){
var _fDesde=$('#desde').val().split('/')
_fDesde=new Date(_fDesde[2],(parseInt(_fDesde[1])-1),_fDesde[0])
var _fHasta=new Date(_fDesde)
 _fHasta.setDate(_fDesde.getDate()+6),t=''
while(_fDesde.valueOf()<=_fHasta.valueOf()){
  var dia=_fDesde.getDate(),mes=(_fDesde.getMonth()+1);
  if(parseInt(dia)<10){
    dia='0'+dia;
  }
  if(parseInt(mes)<10){
    mes='0'+mes;
  }
  t+='<td class="td01 data"><table><tr>'
  t+='<td class="td02"><input onkeyup="calculaTotales()" type="text" class="impHs" id="hs_'+id+'_'+obra+'_'+_fDesde.getFullYear()+'-'+mes+'-'+dia+'_hhe" name="hs_'+id+'_'+obra+'_'+_fDesde.getFullYear()+'-'+mes+'-'+dia+'_hhe" maxlength="2" /></td>'
  t+='<td class="td02" style="width:5px !important"></td>'
  t+='<td class="td02"><input onkeyup="calculaTotales()" type="text" class="impHs" id="hs_'+id+'_'+obra+'_'+_fDesde.getFullYear()+'-'+mes+'-'+dia+'_hhst" name="hs_'+id+'_'+obra+'_'+_fDesde.getFullYear()+'-'+mes+'-'+dia+'_hhst" maxlength="2" /></td>'
  t+='</tr></table></td>'
  _fDesde.setDate(_fDesde.getDate()+1)
}
t+='<td class="td01 data subs totales" id="subTotal_'+id+'"></td>'
return t
}
function rearmarCampos(){
$('#thContent').html('<th class="tits reves">Nombre</th><th class="tits reves">Cuil</th>'+indicaRangoEncabezado()+'<th class="tits reves" style="width:41px;padding:5px;">Total</th>')
}
function calculaTotales(){
var totalTodo=0;
$('.empleadosTr').each(function(a,tr){
  var c=0,idEmp=$(tr).attr('id').split('_')[1];
  $('#'+$(tr).attr('id')+' input').each(function(a,inp){
    if($(inp).val()!='') c+=parseInt($(inp).val())
  })
  totalTodo+=c
  $('#subTotal_'+idEmp).html(c)
})
$('#totalTodo').html('Total:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '+totalTodo)
}
function getDatos(v){
  if(v=='0'){
    $('#tbTodo,#btnGuard,#periodo,#nomObraPrint').css('display','none')
    return false
  }
  msg.text('Cargando...<br /><img src="img/loading.gif" />').load()
  $('#desdePrint').html($('#desde').val())
  $('#hastaPrint').html($('#hasta').val())
  $('#nomObraPrint').html($('#obra option:selected').text()).css('display','block')
  $('#tbTodo').css('display','table')
  $('#btnGuard,#periodo').css('display','block')
  $('#tbContent').html('<tr><td colspan="9" style="text-align:center">Cargando...</td></tr>')
  //$('#desde').val('<?=date('d/m/Y')?>')
  //$('#hasta').val('<?=date('d/m/Y',strtotime('+7 day'))?>')
  $.ajax({
    url:'cargardatos.php',type:'post',data:'obDatosObra='+v+'&desde='+$('#desde').val()+'&hasta='+$('#hasta').val(),type:'post',dataType:'json',
    success:function(d){
      msg.close()
      if(d.empleados.length==0){
        $('#tbContent').html('<tr><td colspan="9" style="text-align:center">No se encontraron empleados asignados a esta obra</td></tr>')
        $('#btnGuard').css('display','none')
        return false
      }
      var t='',i
      rearmarCampos()
      for( i in d.empleados){
        t+='<tr class="empleadosTr _act" id="emp_'+d.empleados[i].id+'"><td class="data">'+d.empleados[i].nombre+'</td><td class="data">'+d.empleados[i].cuil+'</td>'
        t+=armaCampos(d.empleados[i].id,v)
        t+='</tr>'
      }
      t+='<tr><td colspan="8" class="totales"></td><td colspan="2" class="totales reves" id="totalTodo"></td></tr>'
      $('#tbContent').html(t)
      buscarDatosGuardados(d.datos)
    }
  })
}
function buscarDatosGuardados(d){
  var i
  for(i in d){
    $('#hs_'+d[i].indice+'_hhe').val(d[i].hhe)
    $('#hs_'+d[i].indice+'_hhst').val(d[i].hhst)
  }
  calculaTotales()
}
function guardar(){
  msg.text('Guardando...<br /><img src="img/loading.gif" />').load()
  $.ajax({
    url:'cargardatos.php',type:'post',data:'guardarDatos=1&'+$('#formCampos').serialize(),type:'post',
    success:function(d){
      if(d==1){
        msg.text('Los datos se han guardado exitosamente.').load().aceptar()
      }else{
        msg.text(d).load().aceptar()
      }
    }
  })
}