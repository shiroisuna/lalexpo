$(document).ready(function(){
  $("#thContent input").on("click", function () {
   $(this).select()
  })
  $("#thContent input").on("keypress keyup blur",function (event) {
     calculaDatos()
  });
})
function getDatos(v){
  if(v=='0'){
    $('#tbTodo,#btnGuard,#periodo,#nomObraPrint').css('display','none')
    return false
  }
  msg.text('Cargando...<br /><img src="img/loading.gif" />').load()
  $('#periodoPrint').html($('#mes option:selected').text()+' /'+$('#anio').val())
  $('#nomObraPrint').html($('#obra option:selected').text()).css('display','block')
  $('#tbTodo').css('display','table')
  $('#btnGuard,#periodo').css('display','block')
  //$('#tbContent').html('<tr><td colspan="9" style="text-align:center">Cargando...</td></tr>')
  //$('#desde').val('<?=date('d/m/Y')?>')
  //$('#hasta').val('<?=date('d/m/Y',strtotime('+7 day'))?>')
  $.ajax({
    url:'cargaraccidentes.php',type:'post',data:'obDatosObra='+v+'&anio='+$('#anio').val()+'&mes='+$('#mes').val(),type:'post',dataType:'json',
    success:function(d){
      msg.close()
      $("#thContent input").val('')
      var i
      for(i in d.obra){
        $('#'+i).html(d.obra[i])
      }
      for(i in d.datos){
        $('#'+i).val(d.datos[i])
      }
      calculaDatos()
    }
  })
}
function calculaDatos(){
  sNum('prop_aspt','nop_aspt','total_aspt')
  sNum('prop_acpt','nop_acpt','total_acpt')
  sNum('prop_acumulados','nop_acumulados','total_acumulados')
  sNum('prop_dias_perdidos','nop_dias_perdidos','total_dias_perdidos')
  sNum('prop_acto_inseg','nop_acto_inseg','total_acto_inseg')
  sNum('prop_cond_inseg','nop_cond_inseg','total_cond_inseg')
  sNum('prop_aspt','prop_acpt','total_acc_propios')
  sNum('nop_aspt','nop_acpt','total_acc_nopropios')

  var ap=parseInt($('#total_acc_propios').html(),10)
  var anp=parseInt($('#total_acc_nopropios').html(),10)
  var r
  $('#total_acc').html((ap+anp))

  var acptprop=parseInt($('#prop_acpt').val(),10)
  var hsprop=parseInt($('#hs_total_propio').html(),10)
  if(hsprop==0){
    $('#ifn_prop').html('--')
  }else{
    r=((acptprop*1000000)/hsprop).toFixed(2)
    if(isNaN(r)) r='--'
    $('#ifn_prop').html(r)
  }
  var acptnop=parseInt($('#nop_acpt').val(),10)
  var hsnop=parseInt($('#hs_total_nopropio').html(),10)
  if(hsnop==0){
    $('#ifn_noprop').html('--')
  }else{
    r=((acptnop*1000000)/hsnop).toFixed(2)
    if(isNaN(r)) r='--'
    $('#ifn_noprop').html(r)
  }
  var acpt=parseInt($('#total_acpt').html(),10)
  var hs=parseInt($('#hs_total').html(),10)
  if(hs==0){
    $('#ifn_total').html('--')
  }else{
    r=((acpt*1000000)/hs).toFixed(2)
    if(isNaN(r)) r='--'
    $('#ifn_total').html(r)
  }
  /////////////////////////////
  var accprop=parseInt($('#total_acc_propios').html(),10)
  if(hsprop==0){
    $('#ifb_prop').html('--')
  }else{
    r=((accprop*1000000)/hsprop).toFixed(2)
    if(isNaN(r)) r='--'
    $('#ifb_prop').html(r)
  }
  var accnop=parseInt($('#total_acc_nopropios').html(),10)
  if(hsnop==0){
    $('#ifb_noprop').html('--')
  }else{
    r=((accnop*1000000)/hsnop).toFixed(2)
    if(isNaN(r)) r='--'
    $('#ifb_noprop').html(r)
  }
  var acc=parseInt($('#total_acc').html(),10)
  if(hs==0){
    $('#ifb_total').html('--')
  }else{
    r=((acc*1000000)/hs).toFixed(2)
    if(isNaN(r)) r='--'
    $('#ifb_total').html(r)
  }
  crearGraficos()
}
function sNum(v1,v2,id){
  $('#'+id).html(num(v1)+num(v2))
}
function num(id){
  if($('#'+id).val()=='') return 0
  return parseInt($('#'+id).val(),10)
}
function guardar(){
  msg.text('Guardando...<br /><img src="img/loading.gif" />').load()
  $.ajax({
    url:'cargaraccidentes.php',
    type:'post',
    data:'guardarDatos=1&obra='+$('#obra').val()+'&anio='+$('#anio').val()+'&mes='+$('#mes').val()+'&'+$('#formCampos').serialize(),
    success:function(d){
      if(d==1){
        msg.text('Los datos se han guardado exitosamente.').load().aceptar()
      }else{
        msg.text(d).load().aceptar()
      }
    }
  })
}
function crearGraficos(){
  var tiposAc = new FusionCharts({
        type: 'doughnut2d',
        renderAt: '_tiposAcc',
        width: '300',
        height: '200',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "TIPOS DE ACCIDENTES PROPIOS",
                /*"subCaption": "",
                "numberPrefix": "",*/
                "startingAngle": "20",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "enableSmartLabels": "1",
                "enableMultiSlicing": "1",
                "decimals": "1",
                //Theme
                "theme": "fint"
            },
            "data": [
              {"label": "ASPT","value": $('#prop_aspt').val()},
              {"label": "ACPT","value": $('#prop_acpt').val()}
            ]
        },
        events:{rendered:function(){sacaPubli()}}
    }).render();
    var causasAc = new FusionCharts({
        type: 'doughnut2d',
        renderAt: '_causasAcc',
        width: '300',
        height: '200',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "CAUSAS DE ACCIDENTES PROPIOS",
                /*"subCaption": "",
                "numberPrefix": "",*/
                "startingAngle": "20",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "enableSmartLabels": "1",
                "enableMultiSlicing": "1",
                "decimals": "1",
                //Theme
                "theme": "fint"
            },
            "data": [
              {"label": "Acto Inseguro","value": $('#prop_acto_inseg').val()},
              {"label": "Condicion Insegura","value": $('#prop_cond_inseg').val()}
            ]
        },
        events:{rendered:function(){sacaPubli()}}
    }).render();
  var tiposAcExt = new FusionCharts({
        type: 'doughnut2d',
        renderAt: '_tiposAccExt',
        width: '300',
        height: '200',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "TIPOS DE ACCIDENTES CONTRATISTAS",
                /*"subCaption": "",
                "numberPrefix": "",*/
                "startingAngle": "20",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "enableSmartLabels": "1",
                "enableMultiSlicing": "1",
                "decimals": "1",
                //Theme
                "theme": "fint"
            },
            "data": [
              {"label": "ASPT","value": $('#nop_aspt').val()},
              {"label": "ACPT","value": $('#nop_acpt').val()}
            ]
        },
        events:{rendered:function(){sacaPubli()}}
    }).render();
    var causasAcExt = new FusionCharts({
        type: 'doughnut2d',
        renderAt: '_causasAccExt',
        width: '300',
        height: '200',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "CAUSAS DE ACCIDENTES CONTRATISTAS",
                /*"subCaption": "",
                "numberPrefix": "",*/
                "startingAngle": "20",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "enableSmartLabels": "1",
                "enableMultiSlicing": "1",
                "decimals": "1",
                //Theme
                "theme": "fint"
            },
            "data": [
              {"label": "Acto Inseguro","value": $('#nop_acto_inseg').val()},
              {"label": "Condicion Insegura","value": $('#nop_cond_inseg').val()}
            ]
        },
        events:{rendered:function(){sacaPubli()}}
    }).render();
    var causasAcExt = new FusionCharts({
        type: 'doughnut2d',
        renderAt: '_tiposAccTotal',
        width: '300',
        height: '200',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "TIPOS DE ACCIDENTES TOTAL",
                /*"subCaption": "",
                "numberPrefix": "",*/
                "startingAngle": "20",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "enableSmartLabels": "1",
                "enableMultiSlicing": "1",
                "decimals": "1",
                //Theme
                "theme": "fint"
            },
            "data": [
              {"label": "ASPT","value": $('#total_aspt').html()},
              {"label": "ACPT","value": $('#total_acpt').html()}
            ]
        },
        events:{rendered:function(){sacaPubli()}}
    }).render();
    var causasAcExt = new FusionCharts({
        type: 'doughnut2d',
        renderAt: '_causasAccTotal',
        width: '300',
        height: '200',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "CAUSAS DE ACCIDENTES TOTAL",
                /*"subCaption": "",
                "numberPrefix": "",*/
                "startingAngle": "20",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "enableSmartLabels": "1",
                "enableMultiSlicing": "1",
                "decimals": "1",
                //Theme
                "theme": "fint"
            },
            "data": [
              {"label": "Acto Inseguro","value": $('#total_acto_inseg').html()},
              {"label": "Condición Insegura","value": $('#total_cond_inseg').html()}
            ]
        },
        events:{rendered:function(){sacaPubli()}}
    }).render();
}
function sacaPubli(){
  $('.grafi tspan').each(function(a,b){
      if($(b).html().indexOf('Trial')!='-1'){
        $(b).parent().parent().remove()
      }
    })
}