$(document).ready(function(){
  //$('#mes').MonthPicker({ Button: false });
})
_mes=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octrubre','Noviembre','Diciembre']
function getDatos(v){
  if(v=='0'){
    $('#tbTodo,#btnGuard,#periodo,#nomObraPrint').css('display','none')
    return false
  }
  $('#periodoPrint').html($('#mes option:selected').text()+' /'+$('#anio').val())
  $('#nomObraPrint').html($('#obra option:selected').text()).css('display','block')
  $('#tbTodo').css('display','table')
  $('#btnGuard,#periodo').css('display','block')
  $('#tbContent').html('<tr><td colspan="9" style="text-align:center">Cargando...</td></tr>')
  //$('#desde').val('<?=date('d/m/Y')?>')
  //$('#hasta').val('<?=date('d/m/Y',strtotime('+7 day'))?>')
  msg.text('Cargando...<br /><img src="img/loading.gif" />').load()
  $.ajax({
    url:'reporteAnual.php',type:'post',cache:false,data:'obDatosObra='+v+'&anio='+$('#anio').val(),type:'post',dataType:'json',
    success:function(d){
      msg.close()
      $('.dats').html('')
      cosas={}
      for(i in d){
      	mes=d[i].mes
      	if(parseInt(mes)<10) mes='0'+mes
      	d2=d[i]
      	for(e in d2){
      		$('#'+mes+'_'+e).html(d2[e])
      		//console.log(d2[e])
          if(typeof cosas[e]=='undefined') cosas[e]={}
          if(d2[e]!=null)
            cosas[e][mes]=d2[e]
        }
      }
      for(i in cosas){
        c=0
        for(i2 in cosas[i]){
          c+=parseInt(cosas[i][i2],10)
        }
        $('#13_'+i).html(c)
      }
      crearGraficos()
    }
  })
}
function crearGraficos(){
  meses=[],aspt=[],acpt=[]
  for(i in _mes){
    meses[meses.length]={"label":_mes[i]}
  }
  for(i=1;i<=12;i++){
  	var e=(i<10)?'0'+i:i
  	var v=parseInt($('#'+e+'_aspt').html())
  	aspt[aspt.length]={"value":((isNaN(v))?0:v)}
  }
  for(i=1;i<=12;i++){
  	var e=(i<10)?'0'+i:i
  	var v=parseInt($('#'+e+'_acpt').html())
  	acpt[acpt.length]={"value":((isNaN(v))?0:v)}
  }
  var energyChart = new FusionCharts({
    type: 'mscolumn2d',
    renderAt: '_grafiTipos',
    width: '300',
    height: '200',
    dataFormat: 'json',
    dataSource: {
      "chart": {
        "caption": "TIPO DE ACCIDENTES",
       "formatnumberscale": "0",
        "showvalues":"0",
         /*"numbersuffix": "TWh",*/
        "theme": "hulk-light"
      },
      "categories": [{
        "category": meses
      }],
      "dataset": [{
        "seriesname": "ASPT",
        "data": aspt
      }, {
        "seriesname": "ACPT",
        "data": acpt
      }]
    }
  });
  energyChart.render();
  acto=[],cond=[]
  for(i=1;i<=12;i++){
  	var e=(i<10)?'0'+i:i
  	var v=parseInt($('#'+e+'_acto_inseg').html())
  	acto[acto.length]={"value":((isNaN(v))?0:v)}
  }
  for(i=1;i<=12;i++){
  	var e=(i<10)?'0'+i:i
  	var v=parseInt($('#'+e+'_cond_inseg').html())
  	cond[cond.length]={"value":((isNaN(v))?0:v)}
  }
  var energyChart = new FusionCharts({
    type: 'mscolumn2d',
    renderAt: '_grafiCausa',
    width: '300',
    height: '200',
    dataFormat: 'json',
    dataSource: {
      "chart": {
        "caption": "CAUSA DE ACCIDENTES",
       "formatnumberscale": "0",
        "showvalues":"0",
         /*"numbersuffix": "TWh",*/
        "theme": "hulk-light"
      },
      "categories": [{
        "category": meses
      }],
      "dataset": [{
        "seriesname": "Acto Inseguro",
        "data": acto
      }, {
        "seriesname": "Condicion Insegura",
        "data": cond
      }]
    }
  });
  energyChart.render();
  nro=[]
  for(i=1;i<=12;i++){
  	var e=(i<10)?'0'+i:i
  	var v=parseInt($('#'+e+'_accidentes').html())
  	nro[nro.length]={"value":((isNaN(v))?0:v)}
  }
  var energyChart = new FusionCharts({
    type: 'mscolumn2d',
    renderAt: '_grafiNro',
    width: '300',
    height: '200',
    dataFormat: 'json',
    dataSource: {
      "chart": {
        "caption": "NRO DE ACCIDENTES",
       "formatnumberscale": "0",
        "showvalues":"0",
         /*"numbersuffix": "TWh",*/
        "theme": "hulk-light"
      },
      "categories": [{
        "category": meses
      }],
      "dataset": [{
        "seriesname": "Nro de Accidentes",
        "data": nro
      }]
    }
  });
  energyChart.render();
  $('tspan').each(function(a,b){
    if($(b).html()=='FusionCharts XT Trial'){
      $(b).parent().parent().css('display','none')
    }
  })
}