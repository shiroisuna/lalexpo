$(document).ready(function(){
  //$('#mes').MonthPicker({ Button: false });
})
_mes=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octrubre','Noviembre','Diciembre']
function indicaRangoEncabezado(){
var _fDesde=new Date($('#anio').val(),$('#mes').val(),1)
var _fHasta=new Date(_fDesde)
 _fHasta.setMonth(_fDesde.getMonth()+1),t=''
while(_fDesde.valueOf()<_fHasta.valueOf()){
  var dia=_fDesde.getDate()
  t+='<th class="tituloFecha reves" style="padding:0px !important">'+dia+'</th>'
  _fDesde.setDate(_fDesde.getDate()+1)
}
__t=t
return t
}
function armaCampos(id,obra){
var _fDesde=new Date($('#anio').val(),$('#mes').val(),1)
var _fHasta=new Date(_fDesde)
 _fHasta.setMonth(_fDesde.getMonth()+1),t='',c=0
while(_fDesde.valueOf()<_fHasta.valueOf()){
  var dia=_fDesde.getDate(),mes=(_fDesde.getMonth()+1);
  if(parseInt(dia)<10){
    dia='0'+dia;
  }
  if(parseInt(mes)<10){
    mes='0'+mes;
  }
  t+='<td class="td01 data num" id="'+id+'_'+obra+'_'+_fDesde.getFullYear()+'-'+mes+'-'+dia+'"></td>'
  _fDesde.setDate(_fDesde.getDate()+1)
  c++
}
$('.dependColspan').attr('colspan',(c+3))
$('.restoTitImp').attr('colspan',(c-12))
__totalTd=(c+3)

t+='<td class="td01 data subs totales num" id="subTotal_'+id+'"></td>'
return t
}
function rearmarCampos(){
$('#thContent').html('<th class="tits reves">Nombre</th><th class="tits reves">Cuil</th>'+indicaRangoEncabezado()+'<th class="tits reves" style="width:41px;padding:5px;">Total</th>')
}
function calculaTotales(){
var totalTodo=0;
$('.empleadosTr').each(function(a,tr){
  var c=0,idEmp=$(tr).attr('id').split('_')[1];
  $('#'+$(tr).attr('id')+' span').each(function(a,inp){
    if($(inp).html()!='') c+=parseInt($(inp).html())
  })
  totalTodo+=c
  $('#subTotal_'+idEmp).html(c)
})
$('#totalTodo').html('Total:&nbsp;'+totalTodo)
}
function getDatos(v){
  if(v=='0'){
    $('#tbTodo,#btnGuard,#periodo,#nomObraPrint').css('display','none')
    return false
  }
  $('#periodoPrint').html($('#mes option:selected').text()+' /'+$('#anio').val())
  $('#nomObraPrint').html($('#obra option:selected').text()).css('display','block')
  $('#tbTodo').css('display','table')
  $('#btnGuard,#periodo').css('display','block')
  $('#tbContent').html('<tr><td colspan="9" style="text-align:center">Cargando...</td></tr>')
  //$('#desde').val('<?=date('d/m/Y')?>')
  //$('#hasta').val('<?=date('d/m/Y',strtotime('+7 day'))?>')
  msg.text('Cargando...<br /><img src="img/loading.gif" />').load()
  $.ajax({
    url:'reporteMensual.php',type:'post',data:'obDatosObra='+v+'&anio='+$('#anio').val()+'&mes='+$('#mes').val(),type:'post',dataType:'json',
    success:function(d){
      msg.close()
      if(d.empleados.length==0){
        $('#tbContent').html('<tr><td colspan="9" style="text-align:center">No se encontraron empleados asignados a esta obra</td></tr>')
        $('#btnGuard').css('display','none')
        return false
      }
      var t='',i
      rearmarCampos()
      for( i in d.empleados){
        t+='<tr class="empleadosTr _act" id="emp_'+d.empleados[i].id+'"><td class="data">'+d.empleados[i].nombre+'</td><td class="data">'+d.empleados[i].cuil+'</td>'
        t+=armaCampos(d.empleados[i].id,v)
        t+='</tr>'
      }
      t+='<tr><td colspan="'+(__totalTd-2)+'" class="totales"></td><td colspan="2" class="totales" id="totalTodo"></td></tr>'
      $('#tbContent').html(t)
      buscarDatosGuardados(d.datos)
    }
  })
}
function buscarDatosGuardados(d){
  var i
  for(i in d){
    $('#'+d[i].indice).html('<span>'+d[i].horas+'</span>')
  }
  calculaTotales()
}
function guardar(){
  $.ajax({
    url:'reporteMensual.php',type:'post',data:'guardarDatos=1&'+$('#formCampos').serialize(),type:'post',dataType:'json',
    success:function(d){

    }
  })
}