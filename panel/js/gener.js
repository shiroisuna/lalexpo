$(document).ready(function(){
  $("#aviso").dialog({
      autoOpen:false,modal:true,resizable:true,
      title:'Mensaje',closeOnEscape:false,open:function(){
          $(this).parent().find('a.ui-dialog-titlebar-close').hide()
      }
  })
})
var msg={
    width:function(t){
        $("#aviso").dialog({width:t});return this
    },
    height:function(t){
        $("#aviso").dialog({height:t});return this
    },
    title:function(t){
        $("#aviso").dialog({title:t});return this
    },
    load:function(){
        $("#aviso").dialog({buttons:{},modal: true})
        $("#aviso").dialog('open')
        return this
    },
    text:function(t){
        $('#avisoTxt').html(t);return this
    },
    close:function(){
        $('#aviso').dialog('close');
    },
    retry:function(f,f2){
        $("#aviso").dialog({
            buttons:{
                "Reintentar":f,
                "Cancelar":function(){msg.close();if(typeof f2=='string'){eval('funsCancel.'+f2+'()')}}
            }
        })
        return this
    },
    confirm:function(f,f2){
        $("#aviso").dialog({
            title:'Confirmación',
            buttons:{
                "Aceptar":f,
                "Cancelar":function(){msg.close();if(typeof f2=='string'){eval('funsCancel.'+f2+'()')}}
            }
        })
        return this
    },
    aceptar:function(f){
        $("#aviso").dialog({
            buttons:{
                "Aceptar":function(){
                    $('#aviso').dialog('close');
                    if(typeof f=='string'){
                        eval('funsCancel.'+f+'()')
                    }
                    if(typeof f=='function'){
                        f()
                    }
                }
            }
        })
		$('.ui-dialog :button').each(function(){
			if($(this).find('.ui-button-text').html()=='Aceptar'){
				$(this).focus()
			}
		})
        return this
    },
    aceptarBack:function(url){
        $("#aviso").dialog({
            buttons:{
                "Aceptar":function(){
                  document.location.href=url
                }
            }
        })
		$('.ui-dialog :button').each(function(){
			if($(this).find('.ui-button-text').html()=='Aceptar'){
				$(this).focus()
			}
		})
        return this
    },
}
function guardar(ob){
  msg.text('Guardando...<br /><img src="img/loading.gif" />').load()
  $.ajax({
    url:ob.pag,type:'post',data:ob.datos,type:'post',
    success:function(d){
      if(d==1){
        msg.text('Los datos se han guardado exitosamente.').load().aceptarBack(ob.back)
      }else{
        msg.text(d).load().aceptar()
      }
    }
  })
  return false
}