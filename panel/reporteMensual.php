<?
include('includes/conexion.php');
include('includes/accionesReporteMes.php');
include('includes/header.php');
include('includes/top.php');
include('includes/menu.php')?>
<link rel="stylesheet" href="css/01.css?<?=rand(0,99999)?>" />
<script src="js/reporteMensual.js?<?=rand(0,99999)?>"></script>
<style>
.tituloFecha{
  width: 24px !important;
}
.num {
    text-align: right;
    padding-right: 3px !important;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="far  fa-calendar-alt nav-icon"></i> Reporte Mensual Horas</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item active">Reporte Mensual Horas</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header no-print">
                <h3 class="card-title">Seleccione la obra:</h3>
                  <select class="form-control" id="obra" onchange="getDatos(this.value)">
                    <option value="0" selected="">Seleccione</option>
                      <? while($obra=$rsObras->fetch_object()){
                      echo '<option value="'.$obra->id.'" '.$add.'>'.$obra->nombre."</option>";
                      }?>
                    </select>
              </div>
              <!-- /.card-header -->
                <!-- daterange picker -->
              <div class="card-body">
                <form id="formCampos">
                <div style="padding-bottom:10px;display:none" class="no-print" id="periodo">
                <b>Mes:
                  <select id="mes" onchange="getDatos($('#obra').val())">
                    <?
                    $mes=date('m');
                    for($i=0;$i<=11;$i++){
                      $id=($i+1);
                      $add=($id==$mes)?'selected="selected"':'';
                      echo '<option value="'.$i.'" '.$add.'>'.$meses[$i].'</option>';
                    }?>
                  </select>/
                  <select id="anio" onchange="getDatos($('#obra').val())">
                    <?
                     for($i=date('Y');$i>=2016;$i--){
                      echo '<option value="'.$i.'">'.$i.'</option>';
                    }?>
                  </select>
                </b>
                </div>
                <table class="table table-bordered" style="display: none;" id="tbTodo">
                  <thead class="only-print">
                    <tr>
                      <td colspan="34" style="padding: 0 !important;" class=" dependColspan">
                        <table  style="width: 100%;">
                          <tr>
                            <th style="width: 50%;font-size: 22px;">
                              REGISTRO MENSUAL DE HORAS HOMBRE DE <br />EXPOSICIÓN HHE
                            </th>
                            <th style="font-size: 22px; text-align: center;vertical-align: middle;"><?=$_SESSION['empresa_nombre']?></th>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td class="txtEnc" colspan="2">
                        NRO. DE OBRA:
                      </td>
                      <td colspan="34" class="txtEnc">
                        DENOMINACI&Oacute;N: <span id="nomObraPrint"></span>
                      </td>
                    </tr>
                    <tr>
                      <td class="txtEnc dependColspan">
                        Mes:  <span id="periodoPrint"></span>
                      </td>
                    </tr>
                  </thead>
                  <thead id="thContent"><tr>
                  </tr>
                  <tbody id="tbContent">
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="34" class="txtEnc">
                      *HHE: <span>HORAS HOMBRE  LABORADAS</span> | *HHST: <span>HORAS HOMBRE DE SOBRETIEMPO</span>
                    </td>
                  </tr>
                  <tr>
                   <td style="padding: 0;" class="dependColspan">
                    <table style="width: 100%;">
                      <tr>
                        <td style="text-align: center;    padding: 2px 12px;" class="txtEnc reves">RESPONSABLE DE HYS</td>
                        <td style="text-align: center;    padding: 2px 12px;" class="txtEnc reves">JEFE DE OBRA</td>
                      </tr>
                      <tr>
                        <td class="txtEnc" style="padding: 2px 12px !important;">Aclaraci&oacute;n</td>
                        <td class="txtEnc" style="padding: 2px 12px !important;">Aclaraci&oacute;n</td>
                      </tr>
                      <tr>
                        <td class="txtEnc" style="height: 120px;    padding: 2px 12px;">Firma</td>
                        <td class="txtEnc" style="height: 120px;    padding: 2px 12px;">Firma</td>
                      </tr>
                    </table>
                   </td>
                  </tr>
                </tfoot>
                </table>
                  <div class="card-footer no-print" id="btnGuard" style="display:none;text-align: center !important;">
                  <button type="button" class="btn btn-info" onclick="window.print()">Imprimir</button>
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include('includes/footer.php')?>