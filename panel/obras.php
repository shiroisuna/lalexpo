<?
include('includes/conexion.php');
$limit=8;
$desde=((int)$_GET['p'])*$limit;
$rs=$con->query('SELECT o.id, o.nro_obra, o.nombre,
(SELECT COUNT(oe.id) FROM obras_empleados oe WHERE oe.id_obra=o.id) empleados
FROM obras o
where o.empresa='.$_SESSION['empresa'].'
 limit '.$desde.','.$limit);
$total=$con->query('SELECT COUNT(id) total FROM obras')->fetch_object()->total;
include('includes/header.php');
include('includes/top.php');
include('includes/menu.php')?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="fa fa-suitcase nav-icon"></i> Obras</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item active">Obras</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <style>
    td{
      vertical-align: middle !important;
    }
    </style>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listado de Obras <a href="newobra.php"  class="btn btn-info" style="position:absolute;right:10px;top:5px;color:#FFF">Crear nueva</a></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <tbody><tr>
                    <th style="width: 10px">#</th>
                    <th>Nro. obra</th>
                    <th>Denominaci&oacute;n</th>
                    <th style="width: 120px">Empleados</th>
                    <th style="width: 120px">&nbsp;</th>
                  </tr>
                  <?
                  if($rs->num_rows>0){
                  while($rw=$rs->fetch_object()){?>
                  <tr>
                    <td><?=$rw->id?>.</td>
                    <td><a href="newobra.php?id=<?=$rw->id?>"><?=$rw->nro_obra?></a></td>
                    <td><?=$rw->nombre?></td>
                    <td><?=$rw->empleados?></td>
                    <td style="padding: 0.3rem !important;"><a href="cargarempleados.php?id=<?=$rw->id?>"  class="btn btn-info" style="float: right;color:#FFF">Cargar empleados</a></td>
                  </tr>
                  <? }}else{ ?>
                  <tr>
                    <td colspan="5">No se encontraron datos.</td>
                  </tr>
                  <? } ?>
                </tbody></table>
              </div>
              <!-- /.card-body -->
              <? $paginas=ceil($total/$limit)-1;
              if($paginas>0){?>
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <?
                  $act=(int)$_GET['p'];
                  if($_GET['p']>0){?>
                  <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.($act-1)?>">Anterior</a></li>
                  <? }
                  for($i=($act-5);$i<$act;$i++){
                    if($i<0) continue;?>
                    <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.$i?>"><?=$i?></a></li>
                  <? }?>
                  <li class="page-item active"><a class="page-link" href="javascript:;"><?=$act?></a></li>
                  <?for($i=$act+1;$i<=$i+5;$i++){
                    if($i>$paginas) break;?>
                    <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.$i?>"><?=$i?></a></li>
                  <? }?>
                  <? if($paginas>$act){?>
                  <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.($act+1)?>">Siguiente</a></li>
                  <? } ?>
                </ul>
              </div>
              <? } ?>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include('includes/footer.php')?>