<?
include('includes/conexion.php');
include('includes/accionesReporteAnual.php');
include('includes/header.php');
include('includes/top.php');
include('includes/menu.php')?>
<link rel="stylesheet" href="css/01.css?<?=rand(0,99999)?>" />
<script src="plugins/fusionChart/fusioncharts.js?<?=rand(0,99999)?>"></script>
<script src="plugins/fusionChart/fusioncharts.charts.js?<?=rand(0,99999)?>"></script>
<script src="plugins/fusionChart/themes/hulk-light.js?<?=rand(0,99999)?>"></script>
<script src="js/reporteAnual.js?<?=rand(0,99999)?>"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="far  fa-calendar-alt nav-icon"></i> Reporte Anual</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item active">Reporte Anual</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header no-print">
                <h3 class="card-title">Seleccione la obra:</h3>
                  <select class="form-control" id="obra" onchange="getDatos(this.value)">
                    <option value="0" selected="">Seleccione</option>
                      <? while($obra=$rsObras->fetch_object()){
                      echo '<option value="'.$obra->id.'" '.$add.'>'.$obra->nombre."</option>";
                      }?>
                    </select>
              </div>
              <!-- /.card-header -->
                <!-- daterange picker -->
              <div class="card-body">
                <form id="formCampos">
                <div style="padding-bottom:10px;display:none" class="no-print" id="periodo">
                <b>A&ntilde;O:
                  <select id="anio" onchange="getDatos($('#obra').val())">
                    <?
                     for($i=date('Y');$i>=2016;$i--){
                      echo '<option value="'.$i.'">'.$i.'</option>';
                    }?>
                  </select>
                </b>
                </div>
                <table class="table table-bordered" style="display: none;" id="tbTodo">
                  <thead class="only-print">
                    <tr>
                      <td colspan="15" style="padding: 0 !important;">
                        <table  style="width: 100%;">
                          <tr>
                            <th style="width: 50%;font-size: 22px;padding: 2px 12px;">
                              REGISTRO ESTAD&Iacute;STICO ANUAL DE <br />ACCIDENTABILIDAD
                            </th>
                            <th style="font-size: 22px; text-align: center;vertical-align: middle;padding: 2px 12px;"><?=$_SESSION['empresa_nombre']?></th>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2" class="txtEnc" style="padding: 2px 12px;">
                        NRO. DE OBRA:
                      </td>
                      <td colspan="13" class="txtEnc" style="padding: 2px 12px;">
                        DENOMINACI&Oacute;N: <span id="nomObraPrint"></span>
                      </td>
                    </tr>
                    <tr>
                      <td class="txtEnc" colspan="15" style="padding: 2px 12px;">
                        A&ntilde;o:  <span id="periodoPrint"></span>
                      </td>
                    </tr>
                  </thead>
                  <style>
                  #thContent{
                    font-size:11px
                  }
                  #thContent td{
                    padding:3px 4px
                  }
                  .dats {
                    text-align: right;
                    padding-right: 4px !important;
                  }
                  </style>
                  <tbody id="thContent">
                   <tr>
                    <td rowspan="14" style="width:100px;">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="reves">Ene.</td>
                    <td class="reves">Feb.</td>
                    <td class="reves">Mar.</td>
                    <td class="reves">Abr.</td>
                    <td class="reves">May.</td>
                    <td class="reves">Jun.</td>
                    <td class="reves">Jul.</td>
                    <td class="reves">Ago.</td>
                    <td class="reves">Sep.</td>
                    <td class="reves">Oct.</td>
                    <td class="reves">Nov.</td>
                    <td class="reves">Dic.</td>
                    <td class="reves">Total.</td>
                   </tr>
                   <tr class="_act">
                    <td>Nº. De Trabajadores</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_nrotrab" class="dats"></td>
                    <? }?>
                   </tr>
                   <tr class="_act">
                    <td>HHE</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_hhe" class="dats"></td>
                    <? }?>
                   </tr>
                   <tr class="_act">
                    <td>HHE S/T</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_hhst" class="dats"></td>
                    <? }?>
                   </tr>
                   <tr class="_act">
                    <td>HHE TOTAL</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_hhe_total" class="dats"></td>
                    <? }?>
                   </tr>
                   <tr class="_act">
                    <td>D&iacute;as Laborados</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_dias_labo" class="dats"></td>
                    <? }?>
                   </tr>
                   <tr class="_act">
                    <td>ASPT</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_aspt" class="dats aspt"></td>
                    <? }?>
                   </tr>
                   <tr class="_act">
                    <td>ACPT</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_acpt" class="dats acpt"></td>
                    <? }?>
                   </tr>
                   <tr class="_act">
                    <td>Nº  DE ACCIDENTES </td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_accidentes" class="dats"></td>
                    <? }?>
                   </tr>
                   <tr class="_act">
                    <td>Días Perdidos por RM</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_dias_perdidos" class="dats"></td>
                    <? }?>
                   </tr>
                   <tr class="_act">
                    <td>Acto Inseguro</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_acto_inseg" class="dats"></td>
                    <? }?>
                   </tr>
                   <tr class="_act">
                    <td>Condici&oacute;n Insegura</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_cond_inseg" class="dats"></td>
                    <? }?>
                   </tr>
                   <tr class="_act">
                    <td>ÍNDICE DE FRECUENCIA NETA         (IFN)</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_frecu_neta" class="dats"></td>
                    <? }?>
                   </tr>
                   <tr class="_act">
                    <td>ÍNDICE DE FRECUENCIA BRUTA         (IFB)</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_frecu_bruta" class="dats"></td>
                    <? }?>
                   </tr>
                   <tr class="_act">
                    <td rowspan="9" class="reves">TIPO DE ACCIDENTE</td>
                    <td>Caidas y Resbalones</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_caidas" class="dats"></td>
                    <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Golpeado Contra</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_golcontra" class="dats"></td>
                    <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Golpeado Por</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_golpor" class="dats"></td>
                    <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Contacto con Objetos Cortantes</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_objcort" class="dats"></td>
                    <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Contacto con partes Calientes</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_calientes" class="dats"></td>
                    <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Atrapamiento</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_atrap" class="dats"></td>
                    <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Partículas en los ojos</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_partojos" class="dats"></td>
                    <? }?>
                    </tr>
                    <tr class="_act">
                      <td>In Itínere</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_itinere" class="dats"></td>
                    <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Otro</td>
                    <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                      <td id="<?=$e?>_otro01" class="dats"></td>
                    <? }?>
                    </tr>
                    <tr class="_act">
                      <td rowspan="8" class="reves">PARTE DEL CUERPO LESIONADA</td>
                      <td>Manos</td>
                      <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                        <td id="<?=$e?>_manos" class="dats"></td>
                      <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Dedos</td>
                      <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                        <td id="<?=$e?>_dedos" class="dats"></td>
                      <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Cara</td>
                      <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                        <td id="<?=$e?>_cara" class="dats"></td>
                      <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Cabeza</td>
                      <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                        <td id="<?=$e?>_cabeza" class="dats"></td>
                      <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Extremidades Superiores</td>
                      <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                        <td id="<?=$e?>_super" class="dats"></td>
                      <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Extremidades Inferiores</td>
                      <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                        <td id="<?=$e?>_infe" class="dats"></td>
                      <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Pie</td>
                      <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                        <td id="<?=$e?>_pie" class="dats"></td>
                      <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Otro</td>
                      <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                        <td id="<?=$e?>_otro02" class="dats"></td>
                      <? }?>
                    </tr>
                    <tr class="_act">
                      <td rowspan="6" class="reves">TIPO DE LESIÓN</td>
                      <td>Contusion</td>
                      <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                        <td id="<?=$e?>_contu" class="dats"></td>
                      <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Herida Cortante</td>
                      <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                        <td id="<?=$e?>_hecort" class="dats"></td>
                      <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Ematoma</td>
                      <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                        <td id="<?=$e?>_ematoma" class="dats"></td>
                      <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Quemadura</td>
                      <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                        <td id="<?=$e?>_quemadura" class="dats"></td>
                      <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Fractura</td>
                      <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                        <td id="<?=$e?>_fractu" class="dats"></td>
                      <? }?>
                    </tr>
                    <tr class="_act">
                      <td>Otro</td>
                      <? for($i=1;$i<=13;$i++){$e=($i<10)?'0'.$i:$i?>
                        <td id="<?=$e?>_otro03" class="dats"></td>
                      <? }?>
                    </tr>
                    <style>
                    .grafi{
                      display: inline-block;
                      border: 1px solid #CCC;
                      border-radius: 5px;
                    }
                    </style>
                    <td colspan="15" style="text-align: center;">
                      <div id="_grafiTipos" class="grafi"></div>
                      <div id="_grafiCausa" class="grafi"></div>
                      <div id="_grafiNro" class="grafi"></div>
                    </td>
                  </tbody>
                  <tfoot>
                    <tr>
                     <td colspan="15" style="padding: 0;">
                      <table style="width: 100%;">
                        <tr>
                          <td style="text-align: center;    padding: 2px 12px;" class="txtEnc reves">RESPONSABLE DE HYS</td>
                          <td style="text-align: center;    padding: 2px 12px;" class="txtEnc reves">COORDINADOR DE HYS</td>
                          <td style="text-align: center;    padding: 2px 12px;" class="txtEnc reves">JEFE DE OBRA</td>
                        </tr>
                        <tr>
                          <td class="txtEnc" style="padding: 2px 12px !important;">Aclaraci&oacute;n</td>
                          <td class="txtEnc" style="padding: 2px 12px !important;">Aclaraci&oacute;n</td>
                          <td class="txtEnc" style="padding: 2px 12px !important;">Aclaraci&oacute;n</td>
                        </tr>
                        <tr>
                          <td class="txtEnc" style="height: 120px;    padding: 2px 12px;">Firma</td>
                          <td class="txtEnc" style="height: 120px;    padding: 2px 12px;">Firma</td>
                          <td class="txtEnc" style="height: 120px;    padding: 2px 12px;">Firma</td>
                        </tr>
                      </table>
                     </td>
                    </tr>
                  </tfoot>
                </table>
                  <div class="card-footer no-print" id="btnGuard" style="display:none;text-align: center !important;">
                  <button type="button" class="btn btn-info" onclick="window.print()">Imprimir</button>
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include('includes/footer.php')?>