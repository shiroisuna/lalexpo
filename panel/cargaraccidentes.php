<?
include('includes/conexion.php');
include('includes/accionesCargaAccidentes.php');
include('includes/header.php');
include('includes/top.php');
include('includes/menu.php')?>
<link rel="stylesheet" href="css/01.css" />
<script type="text/javascript" src="https://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
<script src="plugins/fusionChart/themes/fusioncharts.theme.fint.js"></script>
<script src="js/cargarAccidentes.js?<?=rand(0,99999)?>"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="fa fa-user-md nav-icon"></i> Cargar Accidentes</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item active">Cargar Accidentes</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header no-print">
                <h3 class="card-title">Seleccione la obra:</h3>
                  <select class="form-control" id="obra" onchange="getDatos(this.value)">
                    <option value="0" selected="">Seleccione</option>
                      <? while($obra=$rsObras->fetch_object()){
                      echo '<option value="'.$obra->id.'" '.$add.'>'.$obra->nombre."</option>";
                      }?>
                    </select>
              </div>
              <!-- /.card-header -->
                <!-- daterange picker -->
              <div class="card-body">
                <form id="formCampos">
                <div style="padding-bottom:10px;display:none" class="no-print" id="periodo">
                <b>Mes:
                  <select id="mes" onchange="getDatos($('#obra').val())">
                    <?
                    $mes=date('m');
                    for($i=0;$i<=11;$i++){
                      $id=($i+1);
                      $add=($id==$mes)?'selected="selected"':'';
                      echo '<option value="'.$i.'" '.$add.'>'.$meses[$i].'</option>';
                    }?>
                  </select>/
                  <select id="anio" onchange="getDatos($('#obra').val())">
                    <?
                     for($i=date('Y');$i>=2016;$i--){
                      echo '<option value="'.$i.'">'.$i.'</option>';
                    }?>
                  </select>
                </b>
                </div>
                <table class="table table-bordered" style="display: none;" id="tbTodo">
                  <thead class="only-print">
                    <tr>
                      <td colspan="10" style="padding: 0 !important;">
                        <table  style="width: 100%;">
                          <tr>
                            <th style="width: 50%;font-size: 22px;padding: 2px 12px;">
                              REGISTRO ESTADISTICO MENSUAL <br />DE ACCIDENTABILIDAD
                            </th>
                            <th style="font-size: 22px; text-align: center;padding: 2px 12px;vertical-align: middle;"><?=$_SESSION['empresa_nombre']?></th>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2" class="txtEnc" style="    padding: 2px 12px;">
                        NRO. DE OBRA:
                      </td>
                      <td colspan="8" class="txtEnc" style="    padding: 2px 12px;">
                        DENOMINACI&Oacute;N: <span id="nomObraPrint"></span>
                      </td>
                    </tr>
                    <tr>
                      <td class="txtEnc" colspan="10" style="    padding: 2px 12px;">
                        Mes:  <span id="periodoPrint"></span>
                      </td>
                    </tr>
                  </thead>
                  <style>
                  #thContent input, input[type=text]{
                    width: 40px;
                    font-size: 12px;
                    height: 20px;
                    text-align: right;
                    border-radius: 3px;
                    border: 1px solid #777;
                    padding-right: 5px;
                  }
                  .tits{
                      padding: 3px 5px !important
                  }
                  .dats{
                    text-align: right;
                  }
                  .grafi{
                    display: inline-block;
                    border: 1px solid #CCC;
                    border-radius: 5px;
                  }
                  .tbTipos{
                    margin: auto;
                    width: 80%;
                    font-size: 12px;
                    margin-bottom: 5px;
                  }
                  .tbTipos td{
                    padding:2px;
                    text-align: center;
                  }
                  .tbTipos .tit{
                    font-size: 13px;
                    font-weight: bold;
                  }
                  .tbTipos input{
                    text-align: center !important;
                  }
                  </style>
                  <tbody id="thContent">
                    <tr>
                      <th class="tits borBottom" colspan="2">&nbsp;</th>
                      <th class="tits dats reves" style="width:10%">Propios</th>
                      <th class="tits dats reves" style="width:10%">No Propios</th>
                      <th class="tits dats reves" style="width:10%">Total</th>
                    </tr>
                    <tr class="_act">
                      <th class="tits reves" rowspan="4" style="width:15%">HHE</th>
                      <th class="tits">HHE</th>
                      <th class="tits dats" id="hhe_propio"></th>
                      <th class="tits dats" id="hhe_nopropio"></th>
                      <th class="tits dats" id="hhe_total"></th>
                    </tr>
                    <tr class="_act">
                      <th class="tits">HHE S/T</th>
                      <th class="tits dats" id="hhst_propio"></th>
                      <th class="tits dats" id="hhst_nopropio"></th>
                      <th class="tits dats" id="hhst_total"></th>
                    </tr>
                    <tr class="_act">
                      <th class="tits">HHE TOTAL</th>
                      <th class="tits dats" id="hs_total_propio"></th>
                      <th class="tits dats" id="hs_total_nopropio"></th>
                      <th class="tits dats"  id="hs_total"></th>
                    </tr>
                    <tr class="_act">
                      <th class="tits borBottom">D&iacute;as Laborados</th>
                      <th class="tits dats borBottom" id="dias_propio"></th>
                      <th class="tits dats borBottom" id="dias_nopropio"></th>
                      <th class="tits dats borBottom" id="dias_total"></th>
                    </tr>
                    <tr class="_act">
                      <th class="tits reves" rowspan="5">ACCIDENTABILIDAD</th>
                      <th class="tits">ASPT</th>
                      <th class="tits dats"><input type="text" id="prop_aspt" name="prop_aspt" maxlength="3" /></th>
                      <th class="tits dats"><input type="text" id="nop_aspt" name="nop_aspt" maxlength="3" /></th>
                      <th class="tits dats" id="total_aspt"></th>
                    </tr>
                    <tr class="_act">
                      <th class="tits">ACPT</th>
                      <th class="tits dats"><input type="text" id="prop_acpt" name="prop_acpt" maxlength="3" /></th>
                      <th class="tits dats"><input type="text" id="nop_acpt" name="nop_acpt" maxlength="3" /></th>
                      <th class="tits dats" id="total_acpt"></th>
                    </tr>
                    <tr class="_act">
                      <th class="tits">Nº TOTAL DE ACCIDENTES</th>
                      <th class="tits dats" id="total_acc_propios"></th>
                      <th class="tits dats" id="total_acc_nopropios"></th>
                      <th class="tits dats" id="total_acc"></th>
                    </tr>
                    <tr class="_act">
                      <th class="tits">Nº DE ACCIDENTES  ACUMULADOS</th>
                      <th class="tits dats"><input type="text" id="prop_acumulados" name="prop_acumulados" maxlength="3" /></th>
                      <th class="tits dats"><input type="text" id="nop_acumulados" name="nop_acumulados" maxlength="3" /></th>
                      <th class="tits dats" id="total_acumulados"></th>
                    </tr>
                    <tr class="_act">
                      <th class="tits">D&iacute;as Perdidos por RM</th>
                      <th class="tits dats"><input type="text" id="prop_dias_perdidos" name="prop_dias_perdidos" maxlength="3" /></th>
                      <th class="tits dats"><input type="text" id="nop_dias_perdidos" name="nop_dias_perdidos" maxlength="3" /></th>
                      <th class="tits dats" id="total_dias_perdidos"></th>
                    </tr>
                    <tr class="_act">
                      <th class="tits reves" rowspan="2">CAUSAS</th>
                      <th class="tits">Acto Inseguro</th>
                      <th class="tits dats"><input type="text" id="prop_acto_inseg" name="prop_acto_inseg" maxlength="3" /></th>
                      <th class="tits dats"><input type="text" id="nop_acto_inseg" name="nop_acto_inseg" maxlength="3" /></th>
                      <th class="tits dats" id="total_acto_inseg"></th>
                    </tr>
                    <tr class="_act">
                      <th class="tits borBottom">Condici&oacute;n Insegura</th>
                      <th class="tits dats borBottom"><input type="text" id="prop_cond_inseg" name="prop_cond_inseg" maxlength="3" /></th>
                      <th class="tits dats borBottom"><input type="text" id="nop_cond_inseg" name="nop_cond_inseg" maxlength="3" /></th>
                      <th class="tits dats borBottom" id="total_cond_inseg"></th>
                    </tr>
                    <tr class="_act">
                      <th class="tits reves" rowspan="2">ÍNDICES</th>
                      <th class="tits">&Iacute;NDICE DE FRECUENCIA NETA (IFN)</th>
                      <th class="tits dats" id="ifn_prop"></th>
                      <th class="tits dats" id="ifn_noprop"></th>
                      <th class="tits dats" id="ifn_total"></th>
                    </tr>
                    <tr class="_act">
                      <th class="tits">&Iacute;NDICE DE FRECUENCIA BRUTA (IFB)</th>
                      <th class="tits dats" id="ifb_prop"></th>
                      <th class="tits dats" id="ifb_noprop"></th>
                      <th class="tits dats" id="ifb_total"></th>
                    </tr>
                  <tr>
                    <td colspan="10">
                      <div style="overflow: auto; max-width: 993px;">
                      <table class="tbTipos">
                        <tr>
                          <td colspan="9" class="tit reves">TIPO DE ACCIDENTE</td>
                        </tr>
                        <tr class="rots">
                          <td><span>Caidas y Resbalones</span></td>
                          <td><span>Golpeado Contra</span></td>
                          <td><span>Golpeado por</span></td>
                          <td><span>Contacto con Objetos Cortantes</span></td>
                          <td><span>Contacto con partes Calientes</span></td>
                          <td><span>Atrapamiento</span></td>
                          <td><span>Partículas en los ojos</span></td>
                          <td><span>In Itínere</span></td>
                          <td><span>Otro</span></td>
                        </tr>
                        <tr class="_act">
                          <td><input type="text" maxlength="3" name="caidas" id="caidas" /></td>
                          <td><input type="text" maxlength="3" name="golcontra" id="golcontra" /></td>
                          <td><input type="text" maxlength="3" name="golpor" id="golpor" /></td>
                          <td><input type="text" maxlength="3" name="objcort" id="objcort" /></td>
                          <td><input type="text" maxlength="3" name="calientes" id="calientes" /></td>
                          <td><input type="text" maxlength="3" name="atrap" id="atrap" /></td>
                          <td><input type="text" maxlength="3" name="partojos" id="partojos" /></td>
                          <td><input type="text" maxlength="3" name="itinere" id="itinere" /></td>
                          <td><input type="text" maxlength="3" name="otro01" id="otro01" /></td>
                        </tr>
                      </table>
                      <table class="tbTipos">
                        <tr>
                          <td colspan="8" class="tit reves">PARTE DEL CUERPO LESIONADA</td>
                        </tr>
                        <tr class="rots">
                          <td><span>Manos</span></td>
                          <td><span>Dedos</span></td>
                          <td><span>Cara</span></td>
                          <td><span>Cabeza</span></td>
                          <td><span>Extremidades Superiores</span></td>
                          <td><span>Extremidades Inferiores</span></td>
                          <td><span>Pie</span></td>
                          <td><span>Otro</span></td>
                        </tr>
                        <tr class="_act">
                          <td><input type="text" maxlength="3" name="manos" id="manos" /></td>
                          <td><input type="text" maxlength="3" name="dedos" id="dedos" /></td>
                          <td><input type="text" maxlength="3" name="cara" id="cara" /></td>
                          <td><input type="text" maxlength="3" name="cabeza" id="cabeza" /></td>
                          <td><input type="text" maxlength="3" name="super" id="super" /></td>
                          <td><input type="text" maxlength="3" name="infe" id="infe" /></td>
                          <td><input type="text" maxlength="3" name="pie" id="pie" /></td>
                          <td><input type="text" maxlength="3" name="otro02" id="otro02" /></td>
                        </tr>
                      </table>
                      <table class="tbTipos">
                        <tr>
                          <td colspan="6" class="tit reves">JEFE DE OBRA</td>
                        </tr>
                        <tr class="rots">
                          <td><span>Contusion</span></td>
                          <td><span>Herida Cortante</span></td>
                          <td><span>Ematoma</span></td>
                          <td><span>Quemadura</span></td>
                          <td><span>Fractura</span></td>
                          <td><span>Otro</span></td>
                        </tr>
                        <tr class="_act">
                          <td><input type="text" maxlength="3" name="contu" id="contu" /></td>
                          <td><input type="text" maxlength="3" name="hecort" id="hecort" /></td>
                          <td><input type="text" maxlength="3" name="ematoma" id="ematoma" /></td>
                          <td><input type="text" maxlength="3" name="quemadura" id="quemadura" /></td>
                          <td><input type="text" maxlength="3" name="fractu" id="fractu" /></td>
                          <td><input type="text" maxlength="3" name="otro03" id="otro03" /></td>
                        </tr>
                      </table>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="10" style="text-align: center;">
                      <div id="_tiposAcc" class="grafi"></div>
                      <div id="_causasAcc" class="grafi"></div>
                      <div id="_tiposAccExt" class="grafi"></div>
                      <div id="_causasAccExt" class="grafi"></div>
                      <div id="_tiposAccTotal" class="grafi"></div>
                      <div id="_causasAccTotal" class="grafi"></div>
                    </td>
                  </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                     <td colspan="10" style="padding: 0;">
                      <table style="width: 100%;">
                        <tr>
                          <td style="text-align: center;    padding: 2px 12px;" class="txtEnc">RESPONSABLE DE HYS</td>
                          <td style="text-align: center;    padding: 2px 12px;" class="txtEnc">COORDINADOR DE HYS</td>
                          <td style="text-align: center;    padding: 2px 12px;" class="txtEnc">JEFE DE OBRA</td>
                        </tr>
                        <tr>
                          <td class="txtEnc" style="padding: 2px 12px !important;">Aclaraci&oacute;n</td>
                          <td class="txtEnc" style="padding: 2px 12px !important;">Aclaraci&oacute;n</td>
                          <td class="txtEnc" style="padding: 2px 12px !important;">Aclaraci&oacute;n</td>
                        </tr>
                        <tr>
                          <td class="txtEnc" style="height: 120px;    padding: 2px 12px;">Firma</td>
                          <td class="txtEnc" style="height: 120px;    padding: 2px 12px;">Firma</td>
                          <td class="txtEnc" style="height: 120px;    padding: 2px 12px;">Firma</td>
                        </tr>
                      </table>
                     </td>
                    </tr>
                  </tfoot>
                </table>
                  <div class="card-footer no-print" id="btnGuard" style="display:none;text-align: center !important;">
                  <button type="button" class="btn btn-info" onclick="window.print()">Imprimir</button>
                  <? if($_SESSION['tipo']!=1){?>
                    <button type="button" class="btn btn-info" onclick="guardar()">Guardar</button>
                    <? } ?>
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include('includes/footer.php')?>