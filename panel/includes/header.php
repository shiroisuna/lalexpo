<?
session_start();
if (!isset($_SESSION['id'])) {
    header('location: index.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="es" style="font-size: 14px;" moznomarginboxes mozdisallowselectionprint>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Registro y Estadisticas</title>
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <link rel="stylesheet" href="css/genen.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Bootstrap -->
<!-- <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<script src="js/gener.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<!-- OPTIONAL SCRIPTS -->
<!--<script src="dist/js/demo.js"></script>
<!-- PAGE PLUGINS -->
<!-- SparkLine -->
<!--<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jVectorMap -->
<!--<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<!--<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.2 -->
<!--<script src="plugins/chartjs-old/Chart.min.js"></script>
<!-- PAGE SCRIPTS
<!--<script src="dist/js/pages/dashboard2.js"></script>-->
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">