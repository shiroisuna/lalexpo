<?
$sec=new secciones();
$cat=new stdClass;
$cat->id=1;
$cat->nombre='Administrativo';
$cat->classIco='nav-icon fa fa-th';
$sec->addCategoria($cat);
$cat=new stdClass;
$cat->id=2;
$cat->nombre='Contenido';
$cat->classIco='fas fa-server';
$sec->addCategoria($cat);
$cat=new stdClass;
$cat->id=3;
$cat->nombre='Extras';
$cat->classIco='fas fa-chart-bar';
$sec->addCategoria($cat);
/*$cat=new stdClass;
$cat->id=4;
$cat->nombre='Paquetes';
$cat->classIco='fas fa-chart-bar';
$sec->addCategoria($cat);*/

$cat=new stdClass;
$cat->id=5;
$cat->nombre='Hoteleria';
$cat->classIco='far fa-building';
$sec->addCategoria($cat);
$obj=new stdClass;

$cat=new stdClass;
$cat->id=6;
$cat->nombre='Carrito';
$cat->classIco='fas fa-shopping-cart';
$sec->addCategoria($cat);

$obj->id=1;
$obj->categoria=1;
$obj->nombre='Traducciones';
$obj->nuevo='Nueva traduccion';
$obj->classIco='fas fa-globe-americas';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM traduccion ORDER BY fecha_alta DESC';
$sec->addSeccion($obj);
/*
$obj=new stdClass;
$obj->id=2;
$obj->categoria=1;
$obj->nombre='Usuarios Front';
$obj->nuevo='Nuevo usuario';
$obj->classIco='fas fa-user-friends';
$obj->selectList='SELECT * FROM usuarios';
$obj->enConstruccion=1;
$sec->addSeccion($obj);*/
$obj=new stdClass;
$obj->id=3;
$obj->categoria=1;
$obj->nombre='Usuarios Back';
$obj->nuevo='Nuevo usuario';
$obj->classIco='fas fa-user-tie';
$obj->selectList='SELECT * FROM usuarios_back';
$obj->export=array(
"id"=>'#',
'user'=>'Usuario',
'pass'=>'Password',
'email'=>'Email',
'nombre'=>'Nombre'
);
$sec->addSeccion($obj);


$obj=new stdClass;
$obj->id=4;
$obj->categoria=1;
$obj->nombre='Blacklist';
$obj->nuevo='';
$obj->classIco='fas fa-bars';
$obj->selectList='SELECT * from blacklist';
$obj->export=array(
"id"=>'#',
'nombre'=>'Nombre',
'apellido'=>'Apellido',
'email'=>'Email',
'telefono'=>'Telefono',
'categoria'=>'Categoria',
);
$sec->addSeccion($obj);

$obj=new stdClass;
$obj->id=5;
$obj->categoria=1;
$obj->nombre='Blacklist alert';
$obj->nuevo='';
$obj->classIco='fas fa-user-slash';
$obj->selectList='CALL getAlertBlacklist ';
$obj->export=array(
"id"=>'#',
'nombre'=>'Nombre'
);
$sec->addSeccion($obj);

$obj=new stdClass;
$obj->id=10;
$obj->categoria=2;
$obj->nombre='Menu';
$obj->nuevo='Nuevo menu';
$obj->classIco='fas fa-tv';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS menu.*, sec.nombre seccion FROM menu INNER JOIN traduccion_seccion sec ON sec.id=menu.id_seccion';
$obj->export=array(
"id"=>'#',
'nombre_es'=>'Nombre',
'seccion'=>'Seccion',
'habilitado'=>'Habilidado,No,Si',
'tipo'=>'Tipo,,Dinamico,Estatico'
);
$sec->addSeccion($obj);

$obj=new stdClass;
$obj->id=4;
$obj->categoria=2;
$obj->nombre='Banners';
$obj->nuevo='Nuevo banner';
$obj->classIco='far fa-images';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM banners_principal';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=12;
$obj->categoria=2;
$obj->nombre='Bloques';
$obj->nuevo='Nuevo Bloque';
$obj->classIco='fas fa-clone';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM bloques ';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=14;
$obj->categoria=2;
$obj->nombre='Programación';
$obj->nuevo='Nuevo Evento';
$obj->classIco='fas fa-clock';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS id, DATE_FORMAT(fecha, \'%d/%m%/%y\') fecha, TIME_FORMAT(inicio, \'%h:%m %p\') inicio, TIME_FORMAT(final, \'%h:%m %p\') final, titulo_es, titulo_en, detalle_es, detalle_en, ubicacion_es, ubicacion_en, estado FROM programacion ';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=1;
$obj->categoria=2;
$obj->nombre='Asistentes';
$obj->nuevo=null;
$obj->classIco='fa fa-address-card';
if (!empty($_POST['filtro'])) { $filtro=" AND (us.dni LIKE '%".$_POST['filtro']."%' OR us.nombre LIKE '%".$_POST['filtro']."%' OR us.apellido LIKE '%".$_POST['filtro']."%' OR us.email LIKE '%".$_POST['filtro']."%') "; } else { $filtro=''; }
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS us.id, us.foto, us.estado_foto, us.dni, us.nombre, us.apellido, cat.nombre AS categoria, us.email, us.estado, us.pais, us.company, us.estudio, COALESCE(ex.id, 0) AS expositor, us.habilitado
  FROM usuarios AS us
  INNER JOIN categorias AS cat ON cat.id=us.id_categoria
  LEFT JOIN expositores AS ex ON ex.id_usuario=us.id WHERE us.eliminado=0 '.$filtro.' ORDER BY us.id DESC';
// $obj->export=array(
// "id"=>'#',
// 'nombre'=>'Nombre',
// 'dni'=>'Documento',
// 'estado'=>'Estado',
// 'pais'=>'Pais',
// 'company'=>'Company',
// 'estudio'=>'Estudio',
// 'categoria'=>'Categoria',
// 'expositor'=>'Expositor,No,Si'
// );
$obj->export=array(
'numreg'=>'#',
'nombre'=>'Nombre',
'dni'=>'Documento',
'estado'=>'Estado',
'pais'=>'Pais',
'company'=>'Company',
'estudio'=>'Estudio',
'categoria'=>'Categoria',
'expositor'=>'Expositor,No,Si'
);
$sec->addSeccion($obj);


$obj=new stdClass;
$obj->id=16;
$obj->categoria=2;
$obj->nombre='Tickets';

$obj->classIco='fa fa-address-card';
if (!empty($_GET['filtro']) && empty($_POST['filtro'])) $_POST['filtro']=unserialize($_GET['filtro']);
if (!empty($_POST['filtro'][0])) { $filtro=" AND (us.dni LIKE '%".$_POST['filtro'][0]."%' OR us.nombre LIKE '%".$_POST['filtro'][0]."%' OR us.apellido LIKE '%".$_POST['filtro'][0]."%' OR us.email LIKE '%".$_POST['filtro'][0]."%') "; } else { $filtro=''; }
if (!empty($_POST['filtro'][1])) { $tt=split("_", $_POST['filtro'][1]); $tipo=" AND tk.tipo=".$tt[0]." AND tk.evento=".$tt[1]." "; } else { $tipo=''; }
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS us.id AS id_usuario, us.foto, us.dni, us.nombre, us.apellido, CONCAT(us.nombre, \' \', us.apellido) as usuario , cat.nombre AS categoria, us.email, us.estado, us.pais, us.company, us.estudio, COALESCE(ex.id, 0) AS expositor, us.habilitado, tk.codigo, tk.tipo, tk.id, (CASE WHEN ws.titulo_es IS NULL THEN \'Lalexpo 2019\' ELSE ws.titulo_es END ) evento
  FROM usuarios AS us 
  INNER JOIN categorias AS cat ON cat.id=us.id_categoria
  INNER JOIN ticket AS tk ON tk.id_usuario=us.id
  LEFT JOIN expositores AS ex ON ex.id_usuario=us.id
  LEFT JOIN workshop AS ws ON ws.id=tk.evento AND tk.tipo=2
  WHERE tk.estado=1 AND us.eliminado=0 '.$tipo.$filtro;
$obj->export=array(
"numreg"=>'#',
"codigo"=>'Ticket',
'usuario'=>'Usuario',
'dni'=>'Documento',
'email'=>'Email',
'categoria'=>'Categoría',
'evento'=>'Evento',
'tipo'=>'Tipo,Ticket,Workshop'
);
$sec->addSeccion($obj);


$obj=new stdClass;
$obj->id=20;
$obj->categoria=2;
$obj->nombre='Accesos con Tickets';
$obj->classIco='fa fa-barcode';
if (!empty($_GET['filtro']) && empty($_POST['filtro'])) $_POST['filtro']=unserialize($_GET['filtro']);
if (!empty($_POST['filtro'][0])) { $filtro=" AND (us.dni LIKE '%".$_POST['filtro'][0]."%' OR us.nombre LIKE '%".$_POST['filtro'][0]."%' OR us.apellido LIKE '%".$_POST['filtro'][0]."%' OR us.email LIKE '%".$_POST['filtro'][0]."%') "; } else { $filtro=''; }
if (!empty($_POST['filtro'][1])) { $tt=split("_", $_POST['filtro'][1]); $tipo=" AND tk.tipo=".$tt[0]." AND tk.evento=".$tt[1]." "; } else { $tipo=''; }
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS asi.*, us.foto, us.dni, us.nombre, us.apellido, CONCAT(us.nombre, \' \', us.apellido) AS usuario , cat.nombre AS categoria, us.email, us.estado, us.pais, us.company, us.estudio, us.habilitado, tk.codigo, tk.tipo, tk.id
  FROM usuarios AS us 
  INNER JOIN categorias AS cat ON cat.id=us.id_categoria
  INNER JOIN ticket AS tk ON tk.id_usuario=us.id
  INNER JOIN asistentes AS asi ON us.id=asi.id_usuario  AND tk.codigo=asi.ticket
  WHERE tk.estado=1 AND us.eliminado=0  '.$tipo.$filtro.' ORDER BY asi.fecha DESC';
$obj->export=array(
"numreg"=>'#',
"codigo"=>'Ticket',
'fecha'=>'Fecha',
'descrip'=>'Evento',
'usuario'=>'Usuario',
'email'=>'Email',
'categoria'=>'Categoría'
);
$sec->addSeccion($obj);


$obj=new stdClass;
$obj->id=2;
$obj->categoria=2;
$obj->nombre='Galería';
$obj->nuevo='Subir imagen/video';
$obj->classIco='far fa-file-image';
if (!empty($_POST['filtro'])) { $album=$_POST['filtro']; } elseif (!empty($_GET['filtro'])) { $album=$_GET['filtro']; } else { $album=''; }
if (!empty($album)) { $filtro=" WHERE g.id_album='".$album."' "; } else { $filtro=''; }
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS g.*,ga.nombre_es FROM galeria g
 inner join galeria_album ga on ga.id=g.id_album'.$filtro;
$sec->addSeccion($obj);

$obj=new stdClass;
$obj->id=5;
$obj->categoria=2;
$obj->nombre='Galería Albums';
$obj->nuevo='Nuevo Album';
$obj->classIco='far fa-file-image';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * from galeria_album';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=3;
$obj->categoria=2;
$obj->nombre='FAQ';
$obj->nuevo='Nueva faq';
$obj->classIco='fas fa-list';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS f.*,fs.nombre_es FROM faq f
inner join faq_seccion fs on fs.id=f.id_seccion';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=6;
$obj->categoria=2;
$obj->nombre='FAQ Secciones';
$obj->nuevo='Nueva seccion faq';
$obj->classIco='fas fa-list';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM faq_seccion';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=7;
$obj->categoria=2;
$obj->nombre='Categorias de Patrocinios';
$obj->nuevo='Nueva categoria';
$obj->classIco='fas fa-certificate';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM patrocinador_categoria_paquete';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=8;
$obj->categoria=2;
$obj->nombre='Paquetes de Patrocinios';
$obj->nuevo='Nuevo paquete';
$obj->classIco='fas fa-box-open';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM vw_paquetes_patrocinios';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=9;
$obj->categoria=2;
$obj->nombre='Solucitides de Patrocinios';
$obj->nuevo=null;
$obj->classIco='fas fa-receipt';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM vw_solicitudes_patrocinios ORDER BY id DESC';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=11;
$obj->categoria=2;
$obj->nombre='Logos de Patrocinadores';
$obj->nuevo='Nuevo logo';
$obj->classIco='fas fa-bookmark';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM vw_logos_patrocinios ';
$sec->addSeccion($obj);

$obj=new stdClass;
$obj->id=13;
$obj->categoria=2;
$obj->nombre='Workshop';
$obj->nuevo='Nuevo Workshop';
$obj->classIco='far fa-handshake';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM workshop ';
$obj->url='contenido.php?cat=2&obj=13&id=1';
$sec->addSeccion($obj);

$obj=new stdClass;
$obj->id=15;
$obj->categoria=2;
$obj->nombre='Workshop pagos';
$obj->classIco='far fa-handshake';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS
ws.id,
ws.fecha,
ws.estado,
ws.nomape,
ws.tel,
ws.celu,
ws.docu,
u.nombre,
u.apellido,
u.id id_usuario,
w.fecha fecha_evento,
w.titulo_es,
w.moneda,
w.precio,
c.nombre categoria,
p.transaction_date,
p.value,
p.response_message_pol,
p.reference_sale,
p.billing_country,
p.todo
FROM workshop_solicitudes ws
INNER JOIN workshop w ON w.id=ws.id_workshop
INNER JOIN usuarios u ON u.id=ws.id_usuario
INNER JOIN categorias c ON c.id=u.id_categoria
LEFT JOIN pagos p ON p.id=ws.id_pago
ORDER BY ws.fecha DESC
';
$sec->addSeccion($obj);

$obj=new stdClass;
$obj->id=17;
$obj->categoria=2;
$obj->nombre='Categorías Premios';
$obj->nuevo='Nueva Categoría';
$obj->classIco='fas fa-trophy';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM premios_categorias  ORDER BY orden';
$sec->addSeccion($obj);

$obj=new stdClass;
$obj->id=18;
$obj->categoria=2;
$obj->nombre='Nominados Premios';
$obj->nuevo='Nuevo nominado';
$obj->classIco='fas fa-trophy';
$obj->selectList="SELECT SQL_CALC_FOUND_ROWS pn.id, CONCAT(us.nombre, ' ', us.apellido) usuario, pc.nombre_es categoria, pn.estado FROM premios_nominados pn
  INNER JOIN usuarios us ON us.id=pn.id_usuario
  INNER JOIN premios_categorias pc ON pc.id=pn.id_categoria";
$sec->addSeccion($obj);

$obj=new stdClass;
$obj->id=19;
$obj->categoria=2;
$obj->nombre='Votos Premios';
$obj->classIco='fas fa-chart-pie';
if (!empty($_POST['categoria'])) { $filtro=" AND pn.id_categoria='".$_POST['categoria']."' "; } else { $filtro=''; }
$obj->selectList="SELECT SQL_CALC_FOUND_ROWS pn.id, CONCAT(us.nombre, ' ', us.apellido) usuario, pc.nombre_es categoria, COUNT(*) votos FROM premios_votos pv
  INNER JOIN premios_nominados pn ON pn.id=pv.id_nominado
  INNER JOIN usuarios us ON us.id=pn.id_usuario
  INNER JOIN premios_categorias pc ON pc.id=pn.id_categoria
  WHERE pn.estado=1 AND pv.estado=1 ".$filtro."
  GROUP BY pv.id_nominado, pn.id_categoria
  ORDER BY votos DESC";
$sec->addSeccion($obj);

$obj=new stdClass;
$obj->id=21;
$obj->categoria=2;
$obj->nombre='Ganadores Premios';
$obj->classIco='fas fa-signal';
if (!empty($_POST['categoria'])) { $filtro=" AND pn.id_categoria='".$_POST['categoria']."' "; } else { $filtro=''; }
$obj->selectList="SELECT categoria, usuario, MAX(votos) votos FROM (
SELECT pn.id, CONCAT(us.nombre, ' ', us.apellido) usuario, pc.nombre_es categoria, COUNT(pv.id_nominado) votos FROM premios_votos pv
  INNER JOIN premios_nominados pn ON pn.id=pv.id_nominado
  INNER JOIN usuarios us ON us.id=pn.id_usuario
  INNER JOIN premios_categorias pc ON pc.id=pn.id_categoria
  WHERE pn.estado=1 AND pv.estado=1
  GROUP BY pv.id_nominado, pn.id_categoria) AS ganadores
  GROUP BY ganadores.categoria
  ORDER BY MAX(votos) DESC";
$sec->addSeccion($obj);


//Extras
$obj=new stdClass;
$obj->id=1;
$obj->categoria=3;
$obj->nombre='Suscripciones';
$obj->nuevo='Exportar lista';
$obj->classIco='far fa-paper-plane';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM suscripciones';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=2;
$obj->categoria=3;
$obj->nombre='Nomencladores';
$obj->nuevo='Nuevo nomenclador';
$obj->classIco='fas fa-tv';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM nomencladores';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=3;
$obj->categoria=3;
$obj->nombre='Parametros';
$obj->url='contenido.php?cat=3&obj=3&id=1';
$obj->nuevo='Exportar';
$obj->classIco='far fa-list-alt';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=4;
$obj->categoria=3;
$obj->nombre='Banners enlaces';
$obj->nuevo='Nuevo banner enlace';
$obj->classIco='far fa-share-square';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM banners';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=5;
$obj->categoria=3;
$obj->nombre='Códigos de descuentos';
$obj->nuevo='Nuevo código';
$obj->classIco='far fa-credit-card';
if (!empty($_POST['filtro'])) { $filtro=" AND (u.dni LIKE '%".$_POST['filtro']."%' OR u.nombre LIKE '%".$_POST['filtro']."%' OR u.apellido LIKE '%".$_POST['filtro']."%' OR u.email LIKE '%".$_POST['filtro']."%') "; } else { $filtro=''; }
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS c.*,
u.nombre asig_nombre,
u.apellido asig_apellido,
u2.nombre red_nombre,
u2.apellido red_apellido
 FROM codigos_descuento c
LEFT JOIN usuarios u ON u.id=c.id_usuario
LEFT JOIN usuarios u2 ON u2.id=c.id_usuario_redimido
WHERE u.eliminado=0 '.$filtro.'
ORDER BY c.id DESC
';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=6;
$obj->categoria=3;
$obj->nombre='Pagos de usuarios';
$obj->nuevo='Nueva pedido de pago';
$obj->classIco='fas fa-dollar-sign';
if (!empty($_POST['filtro'])) { $filtro="WHERE u.dni LIKE '%".$_POST['filtro']."%' OR u.nombre LIKE '%".$_POST['filtro']."%' OR u.apellido LIKE '%".$_POST['filtro']."%' OR u.email LIKE '%".$_POST['filtro']."%' OR up.nombre LIKE '%".$_POST['filtro']."%' "; } else { $filtro=''; }
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS up.*,u.nombre usu_nom,u.apellido usu_ape,
p.state_pol            AS state_pol,
  p.transaction_date     AS transaction_date,
  p.billing_city         AS billing_city,
  p.billing_country      AS billing_country,
  p.value                AS VALUE,
  p.response_message_pol AS response_message_pol,
  p.reference_sale       AS reference_sale,
  p.todo                 AS todo
FROM usuarios_pagos up
LEFT JOIN usuarios u ON up.id_usuario=u.id
LEFT JOIN pagos p ON p.id=up.id_pago
'.$filtro.'
ORDER BY up.id DESC';
$obj->export=array(
"id"=>'#',
'transaction_date'=>'Fecha',
'usu_nom'=>'Nombre',
'usu_ape'=>'Apellidos',
'nombre'=>'Descripción',
'moneda'=>'Moneda',
'valor'=>'Monto',
'response_message_pol'=>'Resultado'
);
$sec->addSeccion($obj);

$obj=new stdClass;
$obj->id=9;
$obj->categoria=3;
$obj->nombre='Facturas';
$obj->nuevo='Nueva Factura';
$obj->classIco='fas fa-file-alt';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM facturas ORDER BY num_fac DESC';
$sec->addSeccion($obj);

$obj=new stdClass;
$obj->id=7;
$obj->categoria=3;
$obj->nombre='Descarga de listados';
$obj->nuevo='';
$obj->classIco='fas fa-cloud-download-alt';
$obj->selectList='select 1';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=8;
$obj->categoria=3;
$obj->nombre='Textos email';
$obj->nuevo='';
$obj->classIco='fas fa-at';
$obj->selectList='select SQL_CALC_FOUND_ROWS * FROM textos_email';
$sec->addSeccion($obj);
//Paquetes
$obj=new stdClass;
$obj->id=1;
$obj->categoria=4;
$obj->nombre='Categorias';
$obj->nuevo='Nuevo categoria';
$obj->classIco='fas fa-tv';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM paquetes_categorias';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=2;
$obj->categoria=4;
$obj->nombre='Paquetes';
$obj->nuevo='Nuevo paquete';
$obj->classIco='fas fa-tv';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM paquetes';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=3;
$obj->categoria=4;
$obj->nombre='Reservas';
$obj->nuevo='Nueva reserva';
$obj->classIco='fas fa-tv';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM paquetes_usuario';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=4;
$obj->categoria=4;
$obj->nombre='Reservar paquete';
$obj->nuevo='Nueva reserva';
$obj->classIco='fas fa-tv';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM paquetes_usuario';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=5;
$obj->categoria=4;
$obj->nombre='Reservas';
#$obj->nuevo='Exportar';
$obj->classIco='fas fa-tv';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM paquetes_usuario';
$obj->enConstruccion=1;
$sec->addSeccion($obj);
//Hoteleria
$obj=new stdClass;
$obj->id=1;
$obj->categoria=5;
$obj->nombre='Reservas';
#$obj->nuevo='Exportar';
$obj->classIco='far fa-calendar-alt';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM pagos_hotel order by id desc';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=2;
$obj->categoria=5;
$obj->nombre='Hotel';
$obj->url='contenido.php?cat=5&obj=2&id=1';
$obj->nuevo='Exportar';
$obj->classIco='far fa-building';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM paquetes_usuario';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=6;
$obj->categoria=5;
$obj->nombre='Hotel imagenes';
$obj->nuevo='Nueva imagen';
$obj->classIco='fas fa-camera-retro';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * from hotel_imagenes';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=3;
$obj->categoria=5;
$obj->nombre='Categorias';
$obj->nuevo='Nueva categoria';
$obj->classIco='fas fa-cubes';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM hotel_categorias_hab';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=4;
$obj->categoria=5;
$obj->nombre='Habitaciones';
$obj->nuevo='Nueva habitación';
$obj->classIco='fas fa-bed';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS h.*,hc.nombre_es categoria FROM hotel_habitciones h
inner join hotel_categorias_hab hc on hc.id=h.id_categoria  ';
$sec->addSeccion($obj);
$obj=new stdClass;
$obj->id=5;
$obj->categoria=5;
$obj->nombre='Itinerario de Pasajeros';
$obj->nuevo='Exportar';
$obj->classIco='far fa-clock';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS
hi.id,
hr.contac_nombre,
hr.contac_email,
hr.contac_telef,
hi.llegada,
hi.partida,
hi.llega_aerolinea,
hi.partida_aerolinea,
hi.llega_vuelo,
hi.id_reserva,
hi.partida_vuelo
 FROM hotel_itinerarios hi
INNER JOIN hotel_reservas hr ON hr.id=hi.id_reserva
ORDER BY llegada ASC';
$sec->addSeccion($obj);


$obj=new stdClass;
$obj->id=1;
$obj->categoria=6;
$obj->nombre='Categorias';
$obj->nuevo='Nueva categoria';
$obj->classIco='fas fa-list-alt';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS * FROM productos_categorias';
$sec->addSeccion($obj);

$obj=new stdClass;
$obj->id=2;
$obj->categoria=6;
$obj->nombre='Productos';
$obj->nuevo='Nuevo producto';
$obj->classIco='fas fa-shopping-bag';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS pro.*, cat.nombre_es categoria FROM productos pro LEFT JOIN productos_categorias cat ON cat.id=pro.id_categoria';
$sec->addSeccion($obj);

$obj=new stdClass;
$obj->id=3;
$obj->categoria=6;
$obj->nombre='Ventas';
$obj->nuevo='';
$obj->classIco='fas fa-money-check-alt';
$obj->selectList='SELECT SQL_CALC_FOUND_ROWS ven.*, DATE_FORMAT(ven.fecha_pedido, \'%d/%m/%Y %l:%i %p\') fecha, CONCAT(us.nombre, \' \', us.apellido) usuario FROM productos_ventas ven INNER JOIN usuarios us ON us.id=ven.id_usuario';
$sec->addSeccion($obj);

class secciones{
  var $secciones,$categorias;
  function __construct(){
    $this->secciones=new stdClass;
    $this->categorias=new stdClass;
  }
  function addSeccion($ob){
    if(!isset($this->secciones->{$ob->categoria}))
    $this->secciones->{$ob->categoria}=new stdClass;
    $this->secciones->
    {$ob->categoria}->
    {$ob->id}=$ob;
  }
  function addCategoria($ob){
    $this->categorias->{$ob->id}=$ob;
  }
  function getSecciones(){
    return $this->secciones;
  }
  function getCategorias(){
    return $this->categorias;
  }
  function getSeccion($cat,$obj){
    return $this->secciones->{$cat}->{$obj};
  }
}