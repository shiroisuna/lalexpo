<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th>Banner</th>
    <th>Orden</th>
    <th></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><?=$rw->id?>.</td>
    <td>
      <?
      $ext=substr(strrchr($rw->archivo, "."), 1);
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 70px;">
          <source src="../images/banners/<?=$rw->archivo?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="../images/banners/<?=$rw->archivo?>" style="max-height: 70px;" />
      <? }else{?>
        <?=$rw->archivo?>
      <? }?>
    </td>
    <td><?=$rw->orden?></td>
    <td><a href="javascript:;" onclick="msg.text('Desea realmente eliminar este banner?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a></td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="4">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>