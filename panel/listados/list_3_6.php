<form  method="post" action="<?=$_SERVER["REQUEST_URI"]?>" >
  <div class="col-10">    
    <input type="text" class="form-control" style="display: inline-block !important; width: 90% !important; " id="filtro" name="filtro" placeholder="cedula, nombre, apellido, email, descripcion pago" value="<?=$_POST['filtro']?>" onclick="$(this).select()" />
    <button type="submit" class="btn btn-info" style="float:right;">Buscar</button>
  </div>
</form>
<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th>Descripción</th>
    <th>Usuario</th>
    <th>Moneda</th>
    <th>Valor</th>
    <th>Estado</th>
    <th></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><?=$rw->id?>.</td>
    <? if($rw->estado!=1){?>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>"><?=$rw->nombre?></a></td>
    <? }else{?>
    <td><?=$rw->nombre?></td>
    <? } ?>
    <td><?=$rw->usu_nom.' '.$rw->usu_ape?></td>
    <td><?=$rw->moneda?></td>
    <td><?=$rw->valor?></td>
    <?
    $estado='Pendiente';
    $color='#dcdcdc';
    if($rw->estado==1){
      $estado='Comprado';
      $color='#6ff157';
    }
    if($rw->estado==2){
      $estado='Denegado';
      $color='#ff8888';
    }
    if($rw->estado==3){
      $estado='Expirado';
      $color='#dcdcdc';
    }
    echo '<td style="background:'.$color.'">'.$estado;
    ?></td>
    <td>
    <? if($rw->estado!=1){?>
    <a href="javascript:;" onclick="msg.text('Desea realmente eliminar este link de pago?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a></td>
    <? }else{?>
      <i class="far fa-plus-square" style="cursor: pointer;" title="Ver detalle" onclick="$('.trDets').hide();$('#trDet_<?=$rw->id?>').show();"></i>
    <? } ?>
  </tr>
  <tr style="display: none;" id="trDet_<?=$rw->id?>" class="trDets">
    <td colspan="9" class="detalles">
      <? if($rw->estado==1){?>
      <table>
        <tr>
          <th colspan="5" class="reves" style="background:#558c4b">Información del pago</th>
        </tr>
        <tr>
          <th>Fecha</th>
          <th>Valor</th>
          <th>Mensaje</th>
          <th>Referencia</th>
          <th>Pais</th>
        </tr>
        <tr>
          <td><?=$rw->transaction_date?></td>
          <td>$ <?=$rw->valor?></td>
          <td><?=$rw->response_message_pol?></td>
          <td><?=$rw->reference_sale?></td>
          <td><?=$rw->billing_country?></td>
        </tr>
        <tr>
          <td colspan="5">
            <div style="overflow: auto; max-height: 120px;">
              <? $todo=json_decode($rw->todo);
              if (count($todo)>0) {
              foreach($todo as $k=>$dato){?>
              <b><?=$k?>:</b> <?=$dato?><br />
              <? } }?>
            </div>
          </td>
        </tr>
      </table>
      <? } ?>
    </td>
  </tr>
  <? $c++;}}else{ ?>
  <tr>
    <td colspan="8">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>