<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th>Imagen</th>
    <th>Nombre Español</th>
    <th>Nombre Ingles</th>
    <th>Cantidad</th>
    <th>Vendido</th>
    <th>Tiquetes</th>
    <th>Precio</th>
    <th>Categoria</th>  
    <th>Habilitado</th>
    <th></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><?=$rw->id?>.</td>
    <td align="center"><img src="../images/paquetes/<?=$rw->imagen ?>" height="60"></td>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>"><?=$rw->nombre_es?></a></td>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>"><?=$rw->nombre_en?></a></td>
    <td align="center"><?=($rw->cantidad-$rw->vendido) ?></td>
    <td align="center"><?=$rw->vendido ?></td>
    <td align="center"><?=$rw->tiquetes ?></td>
    <td align="right">$ <?=$rw->precio ?></td>    
    <td><?=$rw->categoria ?></td>
    <td><?=($rw->estado==1)?'Si':'No'?></td>
    <td>
      <a href="javascript:;" onclick="msg.text('Desea realmente eliminar este registro?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a>
      <a href="javascript:;" onclick="msg.text('Indique la cantidad:<br/><input type=\'number\' id=\'cant\' value=1 min=-<?=$rw->cantidad?> max=<?=($rw->cantidad-$rw->vendido) ?> /> ').load().confirm(function(){ var max=<?=($rw->cantidad-$rw->vendido) ?>; if ($('#cant').val()<=max) { document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&vend=<?=$rw->id?>&cant='+$('#cant').val(); } else { alert('Cantidad no valida'); } })" title="Vender paquete"><i class="far fa-check-circle"></i></a>
    </td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="4">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>