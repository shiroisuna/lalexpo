<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th>Usuario</th>
    <th>Fecha</th>
    <th>Valor</th>
    <th>Pago</th>
    <th>Estado</th>
    <th></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><?=$rw->id?>.</td>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>"><?=$rw->usuario?></a></td>
    <td><?=$rw->fecha?></a></td>
    <td><?=$rw->valor?></td>
    <td><?=$rw->id_pago?></td>
    <td><?=($rw->estado=='1')?'Pagado':'Pendiente'?></td>
    <td><a href="javascript:;" onclick="msg.text('¿Desea realmente eliminar este pedido?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a></td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="8">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>