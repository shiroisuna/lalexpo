<?
$rs_albums=$con->query("select * from galeria_album");
?>
<form  method="get" action="<?=$_SERVER["REQUEST_URI"]?>" >
  <div class="col-10">    
      <label style="float: left;">Album</label>
      <select class="form-control" name="filtro" id="filtro" style="width:60%;float: left;">
        <option value="">Todos</option>
        <? while($rw=$rs_albums->fetch_object()){?>
        <option value="<?=$rw->id?>" <?=($_GET['filtro']==$rw->id)?'selected':'' ?> ><?=$rw->nombre_es?></option>
        <? } ?>
      </select>
      <input type="hidden" name="cat" value="<?=$_GET['cat']?>" />
      <input type="hidden" name="obj" value="<?=$_GET['obj']?>" />
      <input type="hidden" name="p" value="<?=$_GET['p']?>" />
    <button type="submit" class="btn btn-info" style="float:left;">Ver</button>
  </div>
</form>

<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th>Imagen/Video.</th>
    <th>Album.</th>
    <th></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>"><?=$rw->id?>.</a></td>
    <td>
      <?
      if (!empty($rw->archivo)) {
      $ext=strtolower(substr(strrchr($rw->archivo, "."), 1));
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 100px;">
          <source src="../images/galeria/<?=$rw->archivo?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="../thumb_120_w/images/galeria/<?=$rw->archivo?>" style="max-height: 100px;" />
      <? }else{?>
        <?=$rw->archivo?>
      <? }
      } elseif (!empty($rw->enlace)) {?>
        <iframe width="200" height="120" src="https://www.youtube.com/embed/<?=$rw->enlace?>"></iframe>
      <? } ?>
    </td>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>"><?=$rw->nombre_es?></a></td>
    <td><a href="javascript:;" onclick="msg.text('Desea realmente eliminar este imagen?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a></td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="5">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>