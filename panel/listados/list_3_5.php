<form  method="post" action="<?=$_SERVER["REQUEST_URI"]?>" >
  <div class="col-10">    
    <input type="text" class="form-control" style="display: inline-block !important; width: 90% !important; " id="filtro" name="filtro" placeholder="cedula, nombre, apellido, email" value="<?=$_POST['filtro']?>" onclick="$(this).select()" />
    <button type="submit" class="btn btn-info" style="float:right;">Buscar</button>
  </div>
</form>
<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th>Codigo</th>
    <th>Porcentaje</th>
    <th>Usuario</th>
    <th>Redimido</th>
    <th>U. redimido</th>
    <th></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><?=$rw->id?>.</td>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>"><?=$rw->codigo?></a></td>
    <td><?=$rw->porcentaje?> %</td>
    <td><?=$rw->asig_nombre.' '.$rw->asig_apellido?></td>
    <td><?=($rw->redimido=='0')?'No':'Si'?></td>
    <td><?=$rw->red_nombre.' '.$rw->red_apellido?></td>
    <td><? if($rw->redimido=='0'){
      ?><a href="javascript:;" onclick="msg.text('Desea realmente eliminar este código de descuento?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a><? } ?></td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="7">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>