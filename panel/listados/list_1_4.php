<h3 class="card-title">Adicionar Item Blacklist</h3>
<div class="col-5">
  <input type="text" class="form-control" style="display:inline-block;width:250px" id="nuevo" />
  <input type="button" class="btn btn-info" style="display:inline-block" value="Agregar" onclick="add()" />
</div>
<br />
<h3 class="card-title">Modificar Blacklist</h3>
<select multiple="" class="form-control" name="list[]" size="10" id="lista" style="display:inline-block;width:250px"></select>
<br />
<input type="button" class="btn btn-info" style="display:inline-block" value="Eliminar seleccionados" onclick="elim()" />
<script>
$(document).ready(function(){
  getLista()
})
function add(){
  var nuevo=$('#nuevo').val()
  msg.text('Agregando a la lista...<br /><img src="img/loading.gif" />').load()
  $.ajax({
    url:'contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>',
    type:'post',data:'add=1&nuevo='+$('#nuevo').val(),
    success:function(d){
      if(d==1){
        $('#nuevo').val('')
        getLista()
      }else{
        msg.text(d).load().aceptar()
      }
    },error:function(d){
      msg.text(d.responseText).load().aceptar()
    }
  })
}
function getLista(){
  msg.text('Actualizando lista...<br /><img src="img/loading.gif" />').load()
  $.ajax({
    url:'contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>',
    type:'post',data:'getLista=1',dataType:'json',
    success:function(d){
      var i,t=''
      for(i in d){
        t+='<option value="'+d[i].id+'">'+d[i].nombre+'</option>'
      }
      $('#lista').html(t)
      msg.close()
    },error:function(d){
      msg.text(d.responseText).load().aceptar()
    }
  })
}
function elim(){
  msg.text('¿Desea eliminar estos datos?').load().confirm(function(){
    msg.text('Eliminando...<br /><img src="img/loading.gif" />').load()
    $.ajax({
      url:'contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>',
      type:'post',data:'elim=1&datos='+$('#lista').val(),
      success:function(d){
        if(d==1){
          getLista()
        }else{
          msg.text(d).load().aceptar()
        }
      },error:function(d){
        msg.text(d.responseText).load().aceptar()
      }
    })
  })
}
</script>