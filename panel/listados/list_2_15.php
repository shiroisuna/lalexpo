<? $estado=array("Pendiente", "Aprobada", "Rechazada", "Pagado");?>
<table class="table table-bordered">
  <tbody>
  <tr>
    <th class="reves" width="80">Fecha</th>
    <th class="reves">Usuario</th>
    <th class="reves">Categoria</th>
    <th class="reves">Evento</th>
    <th class="reves" width="80">Fecha</th>
    <th class="reves">Valor</th>
    <th class="reves">Estado</th>
    <th class="reves"></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr class="_act">
    <td><?=date('d/m/Y',strtotime($rw->fecha))?></td>
    <td><?=$rw->nombre.' '.$rw->nombre?></td>
    <td><?=$rw->categoria?></td>
    <td><?=$rw->titulo_es?></td>
    <td><?=date('d/m/Y',strtotime($rw->fecha_evento))?></td>
    <td>$ <?=number_format($rw->precio,0,',','.').' '.$rw->moneda?></td>
    <?
    $color='#dcdcdc';
    if($rw->estado==3){
      $color='#6ff157';
    }
    echo '<td style="background:'.$color.'">';
    ?>
    <?=$estado[$rw->estado]?></td>
    <td><i class="far fa-plus-square" style="cursor: pointer;" title="Ver detalle" onclick="$('.trDets').hide();$('#trDet_<?=$rw->id?>').show();"></i></td>
  </tr>
  <tr style="display: none;" id="trDet_<?=$rw->id?>" class="trDets">
    <td colspan="9" class="detalles">
      <table>
        <tr>
          <th colspan="4" class="reves">Datos de la reserva</th>
        </tr>
        <tr>
          <th style="text-align: left;">Nombre</th>
          <th style="text-align: left;">Telefono</th>
          <th style="text-align: left;">Celular</th>
          <th style="text-align: left;">Documento</th>
        </tr>
        <tr>
          <td><?=$rw->nomape?></td>
          <td><?=$rw->tel?></td>
          <td><?=$rw->celu?></td>
          <td><?=$rw->docu?></td>
        </tr>
      </table>
      <? if($rw->estado==3){?>
      <table>
        <tr>
          <th colspan="5" class="reves" style="background:#558c4b">Información del pago</th>
        </tr>
        <tr>
          <th>Fecha</th>
          <th>Valor</th>
          <th>Mensaje</th>
          <th>Referencia</th>
          <th>Pais</th>
        </tr>
        <tr>
          <td><?=$rw->transaction_date?></td>
          <td>$ <?=$rw->value?></td>
          <td><?=$rw->response_message_pol?></td>
          <td><?=$rw->reference_sale?></td>
          <td><?=$rw->billing_country?></td>
        </tr>
        <tr>
          <td colspan="5">
            <div style="overflow: auto; max-height: 120px;">
              <? $todo=json_decode($rw->todo);
              foreach($todo as $k=>$dato){?>
              <b><?=$k?>:</b> <?=$dato?><br />
              <? } ?>
            </div>
          </td>
        </tr>
      </table>
      <? } ?>
  <? }}else{ ?>
  <tr>
    <td colspan="6">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>
<script>
function showChgEstado(id,estado,id_usuario,moneda,precio){
  var select='<select id="estado" onchange="setAviso(this.value)">'
  select+='<option value="0">Pendiente</option>'
  select+='<option value="1">Aprobada</option>'
  select+='<option value="2">Rechazada</option>'
  select+='<option value="3">Pagado</option>'
  msg.text('<div id="msgSelect"></div>Estado: '+select).load().confirm(function(){
    var valorEstado=$('#estado').val()
    msg.text('Cambiando estado...').load()
    $.ajax({
      url:'contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id='+id,
      type:'post',data:'estado='+valorEstado+'&id_usuario='+id_usuario+'&moneda='+moneda+'&precio='+precio,
      success:function(d){
        if(d=='1'){
          msg.text('Estado modificado exitosamente').load().aceptar(function(){
            window.location.reload()
          })
        }else{
          msg.text(d).load().aceptar()
        }
      }
    })
  })
  $('#estado').val(estado)
}
function setAviso(v){
  var t=''
  if(v==1){
    t='<b>Atención:</b> Esta acción enviará un mail al usuario con un cupon de pago';
  }
  if(v==3){
    t='<b>Atención:</b> Esta acción enviará un mail al usuario informando que esta acreditado para asistir al workshop y <b>no se puede revertir</b>';
  }
  $('#msgSelect').html(t)
}
</script>
<style>
.detalles .reves{
  background: #446375;
}
.detalles th{
  text-align: center;
}
.detalles td,.detalles th{
  padding:3px
}
.detalles table{
  margin:auto;
  width: 90%;
}
</style>