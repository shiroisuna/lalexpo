<? $estado=array("<span style='color:grey;'>Anulada</span>", "<span style='color:orange;'>Pendiente</span>", "<span style='color:green;'>Aprobada</span>", "<span style='color:red;'>Rechazada</span>", "<span style='color:blue;'>Pagado</span>");?>
<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th>Fecha</th>
    <th>Usuario</th>
    <th>Categoria</th>
    <th>Paquete</th>
    <th>Precio</th>
    <th>Logo</th>
    <th>Estado</th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr style="cursor: pointer;" onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'">
    <td><?=$rw->id?>.</td>
    <td><?=$rw->fecha?></td>
    <td><?=$rw->usuario?></td>
    <td><?=$rw->categoria?></td>
    <td><?=$rw->paquete?></td>
    <td align="right">$ <?=$rw->precio ?></td>
    <td><img src="../<?=$rw->logo?>" height="80"/></td>
    <td><?=$estado[$rw->estado]?></td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="7">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>