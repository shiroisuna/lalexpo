<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px" class="reves">#</th>
    <th class="reves">Nombre</th>
    <th class="reves">Correo</th>
    <th class="reves">Telefono</th>
    <th class="reves"><i class="fas fa-users"></i></th>
    <th class="reves">Fecha</th>
    <th class="reves">Estado</th>
    <th class="reves">Acciones</th>
    <th class="reves"></th>
  </tr>
  <?
  if($rs->num_rows>0){
    $c=1;
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><?=$c?>.</td>
    <td><?=$rw->contac_nombre?></td>
    <td><?=$rw->contac_email?></td>
    <td><?=$rw->contac_telef?></td>
    <td><?=$rw->personas?></td>
    <td><?=date('d/m/Y',strtotime($rw->entrada))?> - <?=date('d/m/Y',strtotime($rw->salida))?></td>
    <?
    $estado='Pendiente';
    $color='#dcdcdc';
    if($rw->estado==1){
      $estado='Comprado';
      $color='#6ff157';
    }
    if($rw->estado==2){
      $estado='Denegado';
      $color='#ff8888';
    }
    if($rw->estado==3){
      $estado='Expirado';
      $color='#dcdcdc';
    }
    echo '<td style="background:'.$color.'">'.$estado;
    ?></td>
    <td>
      <? if($rw->estado!=1){ ?>
        <a href="javascript:;" onclick="msg.text('¿Desea eliminar esta reserva?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a>
      <? } ?>
    </td>
    <td><i class="far fa-plus-square" style="cursor: pointer;" title="Ver detalle" onclick="$('.trDets').hide();$('#trDet_<?=$rw->id?>').show();"></i></td>
  </tr>
  <tr style="display: none;" id="trDet_<?=$rw->id?>" class="trDets">
    <td colspan="9" class="detalles">
      <table>
        <tr>
          <th class="reves">Categoria</th>
          <th class="reves">Habitacion</th>
          <th class="reves">Precio</th>
          <th class="reves">Total por dias</th>
          <th class="reves">Usuario de compra</th>
          <th class="reves">Pais</th>
          <th class="reves">Estado</th>
        </tr>
        <tr>
          <td><?=$rw->cat_habitacion?></td>
          <td><?=$rw->nombre_habitacion?></td>
          <td>$ <?=$rw->habi_precio?></td>
          <td>$ <?=$rw->totalPagar?></td>
          <td><?=$rw->nombre?> <?=$rw->apellido?></td>
          <td> <?=$rw->pais?></td>
          <td> <?=$rw->provincia?></td>
        </tr>
      </table>
      <table>
        <tr>
          <th colspan="2" class="reves">Huéspedes</th>
        </tr>
        <tr>
          <th style="text-align: left;">Nombre</th>
          <th style="text-align: left;">Pasaporte</th>
        </tr>
        <? $rs_huesp=$con->query("select * from hotel_reservas_hospedados where id_reserva=".$rw->id);
        while($rw_huesp=$rs_huesp->fetch_object()){?>
        <tr>
          <td><?=$rw_huesp->nombre?></td>
          <td><?=$rw_huesp->pasaporte?></td>
        </tr>
        <? } ?>
      </table>
      <? if($rw->estado==1){?>
      <table>
        <tr>
          <th colspan="5" class="reves" style="background:#558c4b">Información del pago</th>
        </tr>
        <tr>
          <th>Fecha</th>
          <th>Valor</th>
          <th>Mensaje</th>
          <th>Referencia</th>
          <th>Pais</th>
        </tr>
        <tr>
          <td><?=$rw->transaction_date?></td>
          <td>$ <?=$rw->value?></td>
          <td><?=$rw->response_message_pol?></td>
          <td><?=$rw->reference_sale?></td>
          <td><?=$rw->billing_country?></td>
        </tr>
        <tr>
          <td colspan="5">
            <div style="overflow: auto; max-height: 120px;">
              <? $todo=json_decode($rw->todo);
              foreach($todo as $k=>$dato){?>
              <b><?=$k?>:</b> <?=$dato?><br />
              <? } ?>
            </div>
          </td>
        </tr>
      </table>
      <? } ?>
    </td>
  </tr>
  <? $c++;}}else{ ?>
  <tr>
    <td colspan="9">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>
<style>
.detalles .reves{
  background: #446375;
}
.detalles th{
  text-align: center;
}
.detalles td,.detalles th{
  padding:3px
}
.detalles table{
  margin:auto;
  width: 90%;
}
</style>