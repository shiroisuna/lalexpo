<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th>Nombre.</th>
    <th>Imagen.</th>
    <th>Disponibles</th>
    <th>Vendidos</th>
    <th>Estado</th>
    <th></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><?=$rw->id?>.</td>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>"><?=$rw->nombre_es?></a></td>
    <td>
      <?
      $ext=substr(strrchr($rw->imagen, "."), 1);
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 70px;">
          <source src="../images/hoteleria/<?=$rw->imagen?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="../images/hoteleria/<?=$rw->imagen?>" style="max-height: 70px;" />
      <? }else{?>
        <?=$rw->imagen?>
      <? }?>
    </td>
    <td><?=$rw->disponibles?></td>
    <td><?=$rw->vendidos?></td>
    <td><?=($rw->estado==1)?'Activo':'Inactivo'?></td>
    <td><a href="javascript:;" onclick="msg.text('Desea realmente eliminar este imagen?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a></td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="7" style="text-align: center;">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>