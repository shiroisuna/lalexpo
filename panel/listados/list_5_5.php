<table class="table table-bordered">
  <tbody><tr>
    <th>Nombre</th>
    <th>Correo</th>
    <th>F. llegada</th>
    <th>Hora de llegada</th>
    <th>Nombre de Aerolinea</th>
    <th>Nro de Vuelo</th>
    <th>F. Partida</th>
    <th>Hora de Partida</th>
    <th>Nombre de Aerolinea</th>
    <th>Nro de Vuelo</th>
    <th></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><?=$rw->contac_nombre?></td>
    <td><?=$rw->contac_email?></td>
    <td><?
    if(!empty($rw->llegada)){
      echo date('d/m/Y',strtotime($rw->llegada));
    }
    ?></td>
    <td><?
    if(!empty($rw->llegada)){
      echo date('H:i',strtotime($rw->llegada));
    }
    ?></td>
    <td><?=$rw->llega_aerolinea?></td>
    <td><?=$rw->llega_vuelo?></td>
    <td><?
    if(!empty($rw->partida)){
      echo date('d/m/Y',strtotime($rw->partida));
    }
    ?></td>
    <td><?
    if(!empty($rw->partida)){
      echo date('H:i',strtotime($rw->partida));
    }
    ?></td>
    <td><?=$rw->partida_aerolinea?></td>
    <td><?=$rw->partida_vuelo?></td>
    <td>
      <a href="javascript:;" onclick="msg.text('Desea realmente eliminar este registro?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a>
      &nbsp;&nbsp;&nbsp;
      <i class="far fa-plus-square" style="cursor: pointer;" title="Ver detalle" onclick="$('.trDets').hide();$('#trDet_<?=$rw->id?>').show();"></i>
    </td>
  </tr>
  <tr style="display: none;" id="trDet_<?=$rw->id?>" class="trDets">
    <td colspan="11" class="detalles">
      <table>
        <tr>
          <th colspan="2" class="reves">Pasajeros</th>
        </tr>
        <tr>
          <th style="text-align: left;">Nombre</th>
          <th style="text-align: left;">Pasaporte</th>
        </tr>
        <? $rs_huesp=$con->query("select * from hotel_reservas_hospedados where id_reserva=".$rw->id_reserva);
        while($rw_huesp=$rs_huesp->fetch_object()){?>
        <tr>
          <td><?=$rw_huesp->nombre?></td>
          <td><?=$rw_huesp->pasaporte?></td>
        </tr>
        <? } ?>
      </table>
    </td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="11" style="text-align: center;">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>
<style>
.detalles .reves{
  background: #446375;
}
.detalles th{
  text-align: center;
}
.detalles td,.detalles th{
  padding:3px
}
.detalles table{
  margin:auto;
  width: 90%;
}
</style>