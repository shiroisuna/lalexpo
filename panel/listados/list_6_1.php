<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th>Nombre Esp.</th>
    <th>Nombre Ing.</th>
    <th>Estado</th>
    <th></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><?=$rw->id?>.</td>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>"><?=$rw->nombre_es?></a></td>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>"><?=$rw->nombre_en?></a></td>
    <td><?=($rw->estado=='1')?'Activo':'Inactivo'?></td>
    <td><a href="javascript:;" onclick="msg.text('¿Desea realmente eliminar esta categoria?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a></td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="5">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>