<? 
$rs_workshops=$con->query("SELECT id, titulo_es FROM workshop ");
?>
<form  method="post" action="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>" >
  <div class="col-10">    
    <input type="text" class="form-control" style="display: inline-block !important; width: 50% !important; " id="filtro" name="filtro[0]" placeholder="cedula, nombre, apellido, email" value="<?=$_POST['filtro'][0]?>" onclick="$(this).select()" />
    <select name="filtro[1]" id="tipo" class="form-control" style="display: inline-block !important; width: 180px !important; ">
      <option value="">Tipo: Todos</option>
      <option value="1_1" <?=($_POST['filtro'][1]==1)?'selected':''?>>Lalexpo 2019</option>
      <? while ($rw=$rs_workshops->fetch_object()) { ?>
        <option value="2_<?=$rw->id?>" <?=($_POST['filtro'][1]=='2_'.$rw->id)?'selected':''?>><?=$rw->titulo_es?></option>
      <? } ?>
    </select>
    <button type="submit" class="btn btn-info" >Buscar</button>
  </div>
</form>
<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">Ticket</th>
    <th>Nombre</th>
    <th>DNI</th>
    <th>Email</th>
    <th>Origen</th>
    <th>Categoria</th> 
    <th>Evento</th>    
    <th></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>"><?=$rw->codigo?></a></td>
    <td><?=$rw->nombre?> <?=$rw->apellido?></td>
    <td><?=$rw->dni?></td>
    <td><?=$rw->email?></td>
    <td><?=$rw->estado?> - <?=$rw->pais?></td>
    <td><?=$rw->categoria?></td>
    <td><?=strip_tags($rw->evento)?></td>
    <td><a href="javascript:;" onclick="msg.text('¿Desea realmente eliminar este ticket?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a></td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="7">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>