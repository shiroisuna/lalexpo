<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th width="80">Fecha</th>
    <th width="50">Inicio</th>
    <th width="50">Fin</th>
    <th>Titulo Español</th>
<!--    <th>Detalle Español</th>-->
    <th>Titulo Inglés</th>
<!--    <th>Detalle Inglés</th>-->
    <th>Ubicación Español</th>
    <th>Ubicación Inglés</th>
    <th>Estado</th>
    <th></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr style="cursor: pointer;">
    <td onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'"><?=$rw->id?>.</td>
    <td onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'"><?=$rw->fecha ?></td>
    <td onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'"><?=$rw->inicio ?> </td>
    <td onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'"><?=$rw->final?></td>
    <td onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'"><?=$rw->titulo_es?></td>
<!--    <td>--><?//=$rw->detalle_es?><!--</td>-->
    <td onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'"><?=$rw->titulo_en?></td>
<!--    <td>--><?//=$rw->detalle_en?><!--</td>-->
    <td onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'"><?=$rw->ubicacion_es?></td>
    <td onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'"><?=$rw->ubicacion_en?></td>
    <td onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'"><?=($rw->estado==1)?'Activo':'Inactivo'?></td>
    <td><a href="javascript:;" onclick="msg.text('Desea realmente eliminar este registro?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a></td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="6">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>