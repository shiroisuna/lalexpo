<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 50px">#</th>
    <th>Fecha</th>
    <th>Cliente</th>
    <th>Descripcion</th>
    <th>Monto</th>
    <th>Estado</th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><a href="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->num_fac?>"><?=$rw->num_fac?></a></td>
    <td><?=$rw->fec_emi?></td>
    <td><?=$rw->nombre?></td>
    <td><?=(strlen($rw->descrip)>70)?substr($rw->descrip,0,70).'...':$rw->descrip?></td>
    <td><?=number_format($rw->mon_net, 2, ",", ".")?></td>
    <td><?=($rw->estado==1)?'Pendiente':'Pagada'?></td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="6">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>