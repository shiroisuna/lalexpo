<?
$ganadores=$con->query("SELECT estado_ganadores FROM parametros LIMIT 1")->fetch_object()->estado_ganadores;
?>

  <div class="col-10"> 
    <form  method="post" action="<?=$_SERVER["REQUEST_URI"]?>" >  
      <label>Categoria</label>
      <select class="form-control" name="categoria" id="categoria"  style="display: inline-block !important; width: 50% !important; ">
        <option value="">Todas</option>
        <? 
        $rs_categorias=$con->query("SELECT id, nombre_es, nombre_en FROM premios_categorias WHERE estado=1");
        while($rw2=$rs_categorias->fetch_object()){
          $add=$rw2->id==$_POST['categoria']?'selected':'';
        ?>
        <option value="<?=$rw2->id?>" <?=$add?>><?=$rw2->nombre_es?></option>
        <? } ?>
      </select>
      <button type="submit" class="btn btn-info" >Ver</button>
    </form>
    <form method="post" id="frm2" action="contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>" >
      <input type="hidden" name="ganadores" id="pargan" value="<?=$ganadores?>">
      <label for="ganadores" style="float: right;"  ><input type="checkbox" id="ganadores" style="width: 16px;height: 16px;" <?=($ganadores?'checked':'')?>>  Activar ganadores</label>
    </form>      
  </div>

<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th>Nominado</th>
    <th>Categoría</th>
    <th>Votos</th>
  </tr>
  <?
  if($rs->num_rows>0){
  $numfil=1;
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td><?=$numfil?></td>
    <td><?=$rw->usuario?></td>
    <td><?=$rw->categoria?></td>
    <td><?=$rw->votos?></td>
  </tr>
  <? $numfil++; } }else{ ?>
  <tr>
    <td colspan="4">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>
<script type="text/javascript">
  $("#ganadores").on("click", function(){
      $("#pargan").val(($(this).prop("checked")?1:0));
      $("#frm2").submit();
  });
</script>