<table class="table table-bordered">
  <tbody><tr>
    <th style="width: 10px">#</th>
    <th width="80">Banner</th>
    <th width="210">Archivo Español.</th>
    <th width="210">Archivo Ingles.</th>
    <th>Fecha Alta</th>
    <th>Link URL</th>
    <th>Estado</th>
    <th width="10"></th>
  </tr>
  <?
  if($rs->num_rows>0){
  while($rw=$rs->fetch_object()){?>
  <tr>
    <td style="cursor: pointer;" onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'"><?=$rw->id?>.</td>
    <td style="cursor: pointer;" onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'"><?=$rw->banner ?></td>
    <td style="cursor: pointer;" onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'">
      <?
      $ext=strtolower(substr(strrchr($rw->archivo_es, "."), 1));
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 70px;max-width: 208px;" >
          <source src="../images/banners/<?=$rw->archivo_es?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="../thumb_208_w/images/banners/<?=$rw->archivo_es?>" style="max-height: 70px;max-width: 208px;" />
      <? }else{?>
        <?=$rw->archivo_es?>
      <? }?>
    </td>
    <td style="cursor: pointer;" onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'">
      <?
      $ext=substr(strrchr($rw->archivo_en, "."), 1);
      if($ext=='mp4'){?>
        <video autoplay="" muted="" loop="" id="myVideo" style="max-height: 70px;max-width: 208px;" >
          <source src="../images/banners/<?=$rw->archivo_en?>" type="video/mp4" />
        </video>
      <? }elseif(in_array($ext,array('png','gif','jpg'))){?>
        <img src="../thumb_208_w/images/banners/<?=$rw->archivo_en?>" style="max-height: 70px;max-width: 208px;"  />
      <? }else{?>
        <?=$rw->archivo_en?>
      <? }?>
    </td>
    <td style="cursor: pointer;" onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'"><?=date('Y-m-d H:i',strtotime($rw->fecha_alta))?></td>
    <td onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'"><?=$rw->link?></td>
    <td onclick="window.location='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&id=<?=$rw->id?>'"><?=($rw->activo==1)?'Activo':'Inactivo'?></td>
    <td><a href="javascript:;" onclick="msg.text('Desea realmente eliminar este banner?').load().confirm(function(){document.location.href='contenido.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>&elim=<?=$rw->id?>'})" title="Eliminar"><i class="far fa-trash-alt"></i></a></td>
  </tr>
  <? }}else{ ?>
  <tr>
    <td colspan="5">No se encontraron datos.</td>
  </tr>
  <? } ?>
</tbody></table>