<?
include("../includes/conexion.php");
session_start();
include('includes/config.php');
$secAct=$sec->getSeccion($_GET['cat'],$_GET['obj']);
$archivo='acciones/acc_'.$_GET['cat'].'_'.$_GET['obj'].'.php';
if(!is_file($archivo)){
  echo '<div style="text-align:right">No se encontró el archivo: '.$archivo.'</div>';
}else{
  include($archivo);
}
include("includes/header.php");
include("includes/top.php");
include("includes/menu.php")?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="<?=$secAct->classIco?> nav-icon"></i>  <?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ".$secAct->nombre:$secAct->nuevo?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item"><a href="listado.php?cat=<?=$_GET['cat']?>&obj=<?=$_GET['obj']?>"><?=$secAct->nombre?></a></li>
              <li class="breadcrumb-item active"><?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ".$secAct->nombre:$secAct->nuevo?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <style>
    .col-5{
      display: inline-block;
      text-align: left;
    }
    .card-footer{
      margin-top: 15px;
      text-align: center;
    }
    form{
      text-align: center;
    }
    </style>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12" style="margin: auto;">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="<?=$secAct->classIco?> nav-icon"></i>  <?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ".$secAct->nombre:$secAct->nuevo?> </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <? if($secAct->enConstruccion!=1){
                  $archivo='contenidos/cont_'.$_GET['cat'].'_'.$_GET['obj'].'.php';
                  if(!is_file($archivo)){?>
                  <center style="font-size:2rem">No se encontró el archivo: <?=$archivo?></center>
                  <? }else{
                    include($archivo);
                  }
                }else{?>
                  <center style="font-size:2rem">En construccion</center>
                <? }?>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include("includes/footer.php")?>