<?
include('includes/conexion.php');
$limit=8;
$desde=((int)$_GET['p'])*$limit;
$rs=$con->query('SELECT * FROM nomencladores
 limit '.$desde.','.$limit);
$total=$con->query('SELECT COUNT(id) total FROM nomencladores')->fetch_object()->total;
include('includes/header.php');
include('includes/top.php');
include('includes/menu.php')?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="fas fa-tv nav-icon"></i> Nomencladores</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item active">Nomencladores</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listado de Nomencladores <a href="newnomencladores.php"  class="btn btn-info" style="float: right;color:#FFF">Crear nuevo</a></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <tbody><tr>
                    <th style="width: 10px">#</th>
                    <th>Nombre</th>
                    <th>URL</th>
                    <th>Habilitado</th>
                  </tr>
                  <?
                  if($rs->num_rows>0){
                  while($rw=$rs->fetch_object()){?>
                  <tr>
                    <td><?=$rw->id?>.</td>
                    <td><a href="newusuario.php?id=<?=$rw->id?>"><?=$rw->nombre?></a></td>
                    <td><?=$rw->email?></td>
                    <td><?=$rw->tipo?></td>
                  </tr>
                  <? }}else{ ?>
                  <tr>
                    <td colspan="4">No se encontraron datos.</td>
                  </tr>
                  <? } ?>
                </tbody></table>
              </div>
              <!-- /.card-body -->
              <? $paginas=ceil($total/$limit)-1;
              if($paginas>0){?>
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <?
                  $act=(int)$_GET['p'];
                  if($_GET['p']>0){?>
                  <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.($act-1)?>">Anterior</a></li>
                  <? }
                  for($i=($act-5);$i<$act;$i++){
                    if($i<0) continue;?>
                    <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.$i?>"><?=$i?></a></li>
                  <? }?>
                  <li class="page-item active"><a class="page-link" href="javascript:;"><?=$act?></a></li>
                  <?for($i=$act+1;$i<=$i+5;$i++){
                    if($i>$paginas) break;?>
                    <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.$i?>"><?=$i?></a></li>
                  <? }?>
                  <? if($paginas>$act){?>
                  <li class="page-item"><a class="page-link" href="<?=$_SERVER['PHP_SELF'].'?p='.($act+1)?>">Siguiente</a></li>
                  <? } ?>
                </ul>
              </div>
              <? } ?>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include('includes/footer.php')?>