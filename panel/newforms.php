<?
include('includes/conexion.php');
include('includes/accionesFormularios.php');
include('includes/header.php');
include('includes/top.php');
include('includes/menu.php')?>
<link rel="stylesheet" href="css/01.css" />
<link rel="stylesheet" href="css/forms.css?<?=rand(0,99999)?>" />
<script type="text/javascript" src="https://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
<script src="plugins/fusionChart/themes/fusioncharts.theme.fint.js"></script>
<script src="js/forms.js?<?=rand(0,99999)?>"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="fa fa-clipboard nav-icon"></i> Formularios de inspecciones</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item active">Cargar formularios</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header no-print">
                <h3 class="card-title" style="display: inline;width: auto;">Tipo de formulario:</h3>
                  <select class="form-control" id="tipoforms" onchange="chgTipoForm(this.value)" style="display: inline;width: auto;">
                    </select>
                  &nbsp;&nbsp;<span id="acciones" class="acciones" style="display:none;">
                    <span onclick="modifNombreShow()"><img src="img/edit.svg" class="imgAcc" title="Modificar nombre" /></span> |
                    <span onclick="avisoElimTipo()"><img src="img/dele.svg" class="imgAcc" title="Eliminar" /></span>
                  </span>
              </div>
              <div class="card-header no-print">
                <h3 class="card-title" style="display: inline;width: auto;">Formulario:</h3>
                  <select class="form-control" id="forms" onchange="chgForm(this.value)" style="display: inline;width: auto;">
                    </select>
                  &nbsp;&nbsp;<span id="accionesForm" class="acciones" style="display:none;">
                    <span onclick="modifNombreFormShow()"><img src="img/edit.svg" class="imgAcc" title="Modificar nombre" /></span> |
                    <span onclick="avisoElimForm()"><img src="img/dele.svg" class="imgAcc" title="Eliminar" /></span>
                  </span>
              </div>
              <!-- /.card-header -->
                <!-- daterange picker -->
<<<<<<< HEAD
              <div class="card-body" id="contenidoXForm" style="display: none;">
=======
              <div class="card-body">
<<<<<<< HEAD
                <form id="formCampos">
                <div class="card-footer no-print" id="btnGuard" style="text-align: center !important;">
                    <button type="button" class="btn btn-info" onclick="showAddCriterio()">Añadir Criterio</button>
                    <button type="button" class="btn btn-info" onclick="showConfig()">Configuración</button>
=======
>>>>>>> 365f4f9777576fb192057d05918c8a54387cba0f
                <form id="formConfig">
                  <table class="table table-bordered" style="width: auto; margin: auto;">
                    <thead>
                      <tr><th class="tits dats reves" style="text-align: center;" colspan="3">Configuraciones</th></tr>
                    </thead>
                    <tbody class="tbContent2">
                      <tr>
                        <td>1</td>
                        <td>
                        Texto afimativo
                        </td>
                        <td><input type="text" id="nombre_si" name="nombre_si" /></td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>
                        Texto negativo
                        </td>
                        <td><input type="text" id="nombre_no" name="nombre_no" /></td>
                      </tr>
                      <!--<tr>
                        <td>3</td>
                        <td>
                        Tiene no aplica
                        </td>
                        <td><input type="checkbox" id="tiene_noaplica" name="tiene_noaplica" value="1" /></td>
                      </tr>-->
                      <tr>
                        <td>3</td>
                        <td>
                        Text Observaciones
                        </td>
                        <td><input type="text" id="add1" name="add1" /></td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>
                        Muestra acciones a tomar
                        </td>
                        <td><input type="checkbox" id="muestra_accionestomar" name="muestra_accionestomar" value="1" /></td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td style="text-align: center;" colspan="3">
                          <button type="button" class="btn btn-info" onclick="guardaConfigs()">Guardar configuración</button>
                          <button type="button" class="btn btn-info" onclick="showAddCriterio()">Añadir Criterio</button>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </form>
                <form id="formCampos">
<<<<<<< HEAD
=======
                <div class="card-footer no-print" id="btnGuard" style="text-align: center !important;">
                    <button type="button" class="btn btn-info" onclick="showAddCriterio()">Añadir Criterio</button>
>>>>>>> e67293abddb0034b4d8980badacb381efe389974
                  </div>
>>>>>>> 365f4f9777576fb192057d05918c8a54387cba0f
                <table class="table table-bordered"  id="tbTodo">
                  <thead class="only-print">
                    <tr>
                      <td colspan="10" style="padding: 0 !important;">
                        <table  style="width: 100%;">
                          <tr>
                            <th style="width: 50%;font-size: 22px;padding: 20px 12px" id="dts_nomForm"></th>
                            <th style="font-size: 22px; text-align: center;padding: 2px 12px;vertical-align: middle;">Nombre Empresa</th>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2" class="txtEnc" style="    padding: 2px 12px;">
                        NRO. DE OBRA:
                      </td>
                      <td colspan="8" class="txtEnc" style="    padding: 2px 12px;">
                        DENOMINACI&Oacute;N: <span id="nomObraPrint"></span>
                      </td>
                    </tr>
                    <tr>
                      <td class="txtEnc" colspan="10" style="    padding: 2px 12px;">
                        Mes:  <span id="periodoPrint"></span>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 10px" class="reves">&nbsp;</td>
                      <th class="tits borBottom reves" style="width: 65%">Criterios a evaluar</th>
                      <th class="tits dats reves" id="tit_si" style="width:50px;text-align:center">Si</th>
                      <th class="tits dats reves" id="tit_no" style="width:50px;text-align:center">No</th>
                      <th class="tits dats reves" id="tit_nap" style="width:50px;text-align:center">N/A</th>
                      <th class="tits dats reves" id="tit_add1" style="text-align:center">Observaciones</th>
                    </tr>
                  </thead>
                  <style>
                  #thContent input, input[type=text]{
                    width: 40px;
                    font-size: 12px;
                    height: 20px;
                    text-align: right;
                    border-radius: 3px;
                    border: 1px solid #777;
                    padding-right: 5px;
                  }
                  .tits{
                      padding: 3px 5px !important
                  }
                  .dats{
                    text-align: right;
                  }
                  .grafi{
                    display: inline-block;
                    border: 1px solid #CCC;
                    border-radius: 5px;
                  }
                  .tbTipos{
                    margin: auto;
                    width: 80%;
                    font-size: 12px;
                    margin-bottom: 5px;
                  }
                  .tbTipos td{
                    padding:2px;
                    text-align: center;
                  }
                  .tbTipos .tit{
                    font-size: 13px;
                    font-weight: bold;
                  }
                  .tbTipos input{
                    text-align: center !important;
                  }
                  .foot01 td{
                    text-align:right;
                    font-size:14px;
                    font-weight: bold;
                    padding: 5px 15px;
                  }
                  .center{
                    text-align:center !important
                  }
<<<<<<< HEAD
=======
                  #tbContent td,.tbContent2 td {
                    font-size: 12px;
                  }
                  .tbContent2 td {
                    padding: 2px 5px;
                  }
                  .tbContent2 input[type=text]{
                    width: 110px;
                    text-align: left;
                  }
                  .foot01 td {
                    font-size: 12px;
                  }
>>>>>>> e67293abddb0034b4d8980badacb381efe389974
                  </style>
                  <tbody id="tbContent">
                  </tbody>
                  <tfoot>
                    <tr class="foot01">
                      <td colspan="2">TOTAL DE ÍTEMS AFIRMATIVOS</td>
                      <td colspan="3" class="center">13</td>
                      <td rowspan="3" class="center"></td>
                    </tr>
                    <tr class="foot01">
                      <td colspan="2">TOTAL DE ÍTEMS EVALUADOS</td>
                      <td colspan="3" class="center">13</td>
                    </tr>
                    <tr class="foot01">
                      <td colspan="2" class="reves">CALIFICACIÓN PARCIAL UNITARIA</td>
                      <td colspan="3" class="center">100%</td>
                    </tr>
                    <tr>
                     <td colspan="10" style="padding-top: 25px;">
                      <table style="width: 100%; max-width: 700px;margin:auto">
                        <tr>
                          <td style="text-align: center;    padding: 2px 12px;width: 50%;" class="txtEnc reves">RESPONSABLE DE HYS</td>
                          <td style="text-align: center;    padding: 2px 12px;" class="txtEnc reves">JEFE DE OBRA</td>
                        </tr>
                        <tr>
                          <td class="txtEnc" style="padding: 2px 12px !important;">Aclaraci&oacute;n</td>
                          <td class="txtEnc" style="padding: 2px 12px !important;">Aclaraci&oacute;n</td>
                        </tr>
                        <tr>
                          <td class="txtEnc" style="height: 120px;    padding: 2px 12px;">Firma</td>
                          <td class="txtEnc" style="height: 120px;    padding: 2px 12px;">Firma</td>
                        </tr>
                      </table>
                     </td>
                    </tr>
                  </tfoot>
                </table>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include('includes/footer.php')?>