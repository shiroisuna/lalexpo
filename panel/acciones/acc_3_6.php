<?
include('../includes/funciones.php');
if(!empty($_POST["moneda"])){
  if ($_POST['estado']==1) {
    $con->query("INSERT INTO pagos set
    state_pol='4',
    test='0',
    transaction_date=NOW(),
    billing_country='CO',
    description='".$_POST['nombre']."',
    value='".$_POST['valor']."',
    response_message_pol='ADMIN_APPROVED',
    currency='".$_POST['moneda']."',
    transaction_id='',
    payment_method_name='',
    payment_method_id='',
    billing_city='',
    reference_sale='lalexpo|".$_POST['id_usuario']."|".$_POST['producto']."|".date("Ymd_His")."|".$_GET['id']."_1',
    todo=''");
    $pagoid=$con->insert_id;
  }

  if(!empty($_GET['id'])){
    $con->query("UPDATE usuarios_pagos SET
    id_usuario='".$_POST['id_usuario']."',
    id_pago='".$pagoid."',
    valor='".$_POST['valor']."',
    moneda='".$_POST['moneda']."',
    producto='".$_POST['producto']."',
    nombre='".$_POST['nombre']."',
    estado='".$_POST['estado']."'
    WHERE id='".$_GET['id']."'");
  }else{
    $con->query("INSERT INTO usuarios_pagos SET
    id_usuario='".$_POST['id_usuario']."',
    id_pago='".$pagoid."',
    valor='".$_POST['valor']."',
    moneda='".$_POST['moneda']."',
    producto='".$_POST['producto']."',
    nombre='".$_POST['nombre']."',
    estado='".$_POST['estado']."'");
    $_GET["id"]=$con->insert_id;
  }

  if ($_POST['estado']>1) {


    if(empty($con->error)){
      echo 1;
    }else{
      echo $con->error;
    }
      exit();
  }

    
  if ($_POST['producto']==2) {
    crearTicket('../', $_POST['id_usuario']);
    $rw2=$con->query("SELECT * FROM usuarios WHERE id='".$_POST['id_usuario']."'")->fetch_object();
    $con->query("UPDATE ticket SET estado='1' WHERE id_usuario='".$_POST['id_usuario']."' AND tipo=1");
    $cant_tickets=$con->query("SELECT * FROM usuarios_pagos WHERE id='".($_GET["id"])."'")->fetch_object()->cant_tickets;
    $ticket=$con->query("SELECT estado, codigo FROM ticket WHERE id_usuario='".$_POST['id_usuario']."' AND tipo=1")->fetch_object();
    $text_compra=$con->query("SELECT * FROM textos_email WHERE id='compra_ticket'")->fetch_object();
    $text_compra_extras=$con->query("select * from textos_email where id='compra_tickets_extras'")->fetch_object();
    $cant_tickets--;
    if($cant_tickets>0){
      if($rw2->lengua=='Español'){
        $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra_extras->titulo_es);
        $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra_extras->texto_es);
      }else{
        $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra_extras->titulo_en);
        $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra_extras->texto_en);
      }
      for($i=1;$i<=$cant_tickets;$i++){
        $con->query("INSERT INTO codigos_descuento SET
        id_usuario='".$_POST['id_usuario']."',
        codigo='".generarCodigo(45)."',
        porcentaje='100',
        redimido='0'");
      }
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"https://lalexpo.com/test/mail2.php");
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$rw2->email."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje)));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      echo curl_exec ($ch);
      curl_close ($ch);
    }

      if($rw2->lengua=='Español'){
        $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->titulo_es);
        $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->texto_es);
      }else{
        $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->titulo_en);
        $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->texto_en);
      }
      $add='<br /><br /><a href="http://'.$_SERVER['SERVER_NAME'].'/getTicket.php?cod='.$ticket->codigo.'&e=1">http://'.$_SERVER['SERVER_NAME'].'/getTicket.php?cod='.$ticket->codigo.'&e=1</a>';
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"https://lalexpo.com/test/mail2.php");
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$rw2->email."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje).$add));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      echo curl_exec ($ch);
      curl_close ($ch);    
  }

  if ($_POST['producto']==4) {
    crearTicketWorkshop('../', $_POST['id_usuario']);
    $id_workshop=$con->query("SELECT id_workshop FROM workshop WHERE estado=1 LIMIT 1");
    $rw2=$con->query("SELECT * FROM usuarios WHERE id='".$_POST['id_usuario']."'")->fetch_object();
    $con->query("UPDATE ticket SET estado='1' WHERE id_usuario='".$_POST['id_usuario']."' AND tipo=2");
    $ticket=$con->query("SELECT estado, codigo FROM ticket WHERE id_usuario='".$_POST['id_usuario']."' AND tipo=2")->fetch_object();

    $text_email=$con->query("select * from textos_email where id='workshop_registro'")->fetch_object();
    if($rw2->lengua=='Español'){
      $tit=$text_email->titulo_es;
      $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_email->texto_es);
    }else{
      $tit=$text_email->titulo_en;
      $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_email->texto_en);
    }

     $mensaje=str_replace('%link%', '<br /><br /><a href="https://lalexpo.com/getTicket.php?cod='.$ticket->codigo.'&e='.$id_workshop.'&desc=D" target="_blank">https://lalexpo.com/getTicket.php?cod='.$ticket->codigo.'&e='.$id_workshop.'&desc=D</a>', $mensaje);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://lalexpo.com/test/mail2.php");
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$rw2->email."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);
    curl_close($ch);

    $text_email=$con->query("select * from textos_email where id='workshop_pago_exitoso'")->fetch_object();
    if($rw2->lengua=='Español'){
      $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_email->titulo_es);
      $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_email->texto_es);
    }else{
      $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_email->titulo_en);
      $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_email->texto_en);
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://lalexpo.com/test/mail2.php");
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$rw2->email."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);
    curl_close($ch);
   
  }

  if ($_POST['producto']==5 && $_POST['estado']) {
    crearTicket('../', $_POST['id_usuario']);
    $rw2=$con->query("SELECT * FROM usuarios WHERE id='".$_POST['id_usuario']."'")->fetch_object();
    $con->query("UPDATE ticket SET estado='1' WHERE id_usuario='".$_POST['id_usuario']."' AND tipo=1");
    $ticket=$con->query("SELECT estado, codigo FROM ticket WHERE id_usuario='".$_POST['id_usuario']."' AND tipo=1")->fetch_object();

    $con->query("INSERT INTO codigos_descuento SET
    id_usuario='".$_POST['id_usuario']."',
    codigo='".generarCodigo(45)."',
    porcentaje='50',
    redimido='0'");  

    $text_compra=$con->query("SELECT * FROM textos_email WHERE id='compra_ticket'")->fetch_object();
    if($rw2->lengua=='Español'){
      $tit=$text_email->titulo_es;
      $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_email->texto_es);
    }else{
      $tit=$text_email->titulo_en;
      $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_email->texto_en);
    }

     $mensaje=$mensaje.'<br /><br /><a href="https://lalexpo.com/getTicket.php?cod='.$ticket->codigo.'&e=1&desc=D" target="_blank">https://lalexpo.com/getTicket.php?cod='.$ticket->codigo.'&e=1&desc=D</a>';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://lalexpo.com/test/mail2.php");
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$rw2->email."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);
    curl_close($ch);
   
  }


  if(empty($con->error)){
    echo 1;
  }else{
    echo $con->error;
  }
  exit;
}
if(!empty($_GET["id"])){
  $datos=$con->query("SELECT * from usuarios_pagos where id='".($_GET["id"])."'")->fetch_object();
}
if(!empty($_GET['elim'])){
  $sql="delete from usuarios_pagos where id='".$_GET['elim']."'";
  $con->query($sql);
  header('location: listado.php?cat='.$_GET['cat'].'&obj='.$_GET['obj']);
  exit;
}
$rs=$con->query("select id,nombre,apellido from usuarios where eliminado=0 ORDER BY LTRIM(nombre)");
$usuarioss=array();
while($rw=$rs->fetch_object()){
  $usuarioss[]=$rw;
}

function generarCodigo($longitud) {
  return rand(123456123,999999999);
}