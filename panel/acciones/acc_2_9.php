<?
session_start();
if(!empty($_POST['estado'])){
  if(!empty($_GET['id'])){
    $con->query("UPDATE patrocinador_solicitud SET estado='".$_POST['estado']."' WHERE id='".$_GET['id']."'");
    $datos=$con->query("SELECT
      ps.id_usuario,
      u.nombre,
      u.apellido,
      u.dni,
      u.email,
      u.pais,
      u.estado,
      u.ciudad,
      u.telefono,
      u.lengua,
      pq.nombre_en,
      pq.nombre_es,
      pq.precio,
      pq.tiquetes,
      id_paquete
      FROM patrocinador_solicitud ps
      INNER JOIN patrocinador_paquete pq ON pq.id=ps.id_paquete
      INNER JOIN usuarios u ON u.id=ps.id_usuario
      where ps.id='".$_GET['id']."'")->fetch_object();
    if ($_POST['estado']==2) {
      $con->query("INSERT INTO patrocinador_logo SET id_solicitud='".$_GET['id']."', posicion='".$_POST['posicion']."', estado=0");
      $text_email=$con->query("select * from textos_email where id='solicitud_sponsor_aprovada'")->fetch_object();
      if($datos->lengua=='Español'){
        $tit=str_replace('%nombre%',$datos->nombre.' '.$datos->apellido,$text_email->titulo_es);
        $mensaje=str_replace('%nombre%',$datos->nombre.' '.$datos->apellido,$text_email->texto_es);
      }else{
        $tit=str_replace('%nombre%',$datos->nombre.' '.$datos->apellido,$text_email->titulo_en);
        $mensaje=str_replace('%nombre%',$datos->nombre.' '.$datos->apellido,$text_email->texto_en);
      }
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"http://lalexpo.com/test/mail2.php");
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$datos->email."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje)));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_exec($ch);
      curl_close($ch);
      $_SESSION['factura']['nit']=$datos->dni;
      $_SESSION['factura']['nombre']=$datos->nombre.' '.$datos->apellido;
      $_SESSION['factura']['email']=$datos->email;
      $_SESSION['factura']['telefono']=$datos->telefono;
      $_SESSION['factura']['concepto']=$datos->nombre_es;
      $_SESSION['factura']['precio']=$datos->precio;
    }
    if ($_POST['estado']==3) {
      $text_email=$con->query("select * from textos_email where id='solicitud_sponsor_rechazada'")->fetch_object();
      if($datos->lengua=='Español'){
        $tit=str_replace('%nombre%',$datos->nombre.' '.$datos->apellido,$text_email->titulo_es);
        $mensaje=str_replace('%nombre%',$datos->nombre.' '.$datos->apellido,$text_email->texto_es);
      }else{
        $tit=str_replace('%nombre%',$datos->nombre.' '.$datos->apellido,$text_email->titulo_en);
        $mensaje=str_replace('%nombre%',$datos->nombre.' '.$datos->apellido,$text_email->texto_en);
      }
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"http://lalexpo.com/test/mail2.php");
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$datos->email."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje)));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_exec($ch);
      curl_close($ch);
    }
    if ($_POST['estado']==4 && $_POST['estado_ant']!='4') {
      $rw=$con->query("select id_paquete,id_usuario from patrocinador_solicitud where id=".$_GET['id'])->fetch_object();
      $id_paquete=$rw->id_paquete;
      $id_usuario=$rw->id_usuario;
      $rw2=$con->query("select * from usuarios where id='".$id_usuario."'")->fetch_object();
      $cant_tickets=$con->query("SELECT tiquetes FROM patrocinador_paquete WHERE id=".$id_paquete)->fetch_object()->tiquetes;
      $text_compra=$con->query("select * from textos_email where id='compra_ticket'")->fetch_object();
      $text_compra_extras=$con->query("select * from textos_email where id='compra_tickets_extras'")->fetch_object();
      $con->query("update patrocinador_logo SET estado=1 where id_solicitud='".$_GET['id']."'");
      $con->query("update patrocinador_paquete set cantidad=cantidad-1, vendido=vendido+1 where id='".$id_paquete."'");
      //$rs=$con->query("select * from ticket where id_usuario='".$id_usuario."' and estado=1");
      $ticket=$con->query("select estado,codigo from ticket where id_usuario='".$id_usuario."'")->fetch_object();
      if($ticket->estado==0){
        $cant_tickets--;
        $con->query("update ticket SET estado='1' where id_usuario='".$id_usuario."'");
        if($rw2->lengua=='Español'){
          $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->titulo_es);
          $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->texto_es);
        }else{
          $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->titulo_en);
          $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra->texto_en);
        }
        $add='<br /><br /><a href="http://'.$_SERVER['SERVER_NAME'].'/getTicket.php?cod='.$ticket->codigo.'">http://'.$_SERVER['SERVER_NAME'].'/getTicket.php?cod='.$ticket->codigo.'</a>';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://lalexpo.com/test/mail2.php");
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$rw2->email."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje).$add));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec ($ch);
        curl_close ($ch);
      }
      if($cant_tickets>0){
        if($rw2->lengua=='Español'){
          $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra_extras->titulo_es);
          $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra_extras->texto_es);
        }else{
          $tit=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra_extras->titulo_en);
          $mensaje=str_replace('%nombre%',$rw2->nombre.' '.$rw2->apellido,$text_compra_extras->texto_en);
        }
        for($i=1;$i<=$cant_tickets;$i++){
          $con->query("INSERT INTO codigos_descuento SET
          id_usuario='".$id_usuario."',
          codigo='".generarCodigo(45)."',
          porcentaje='100',
          redimido='0'");
          if(!empty($con->error)){
            echo $con->error.'<br /><br />';
          }
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://lalexpo.com/test/mail2.php");
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".$rw2->email."&asunto=".urlencode($tit)."&mensaje=".urlencode(nl2br($mensaje)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec ($ch);
        curl_close ($ch);
      }
    }
  }
  if(empty($con->error)){
    echo 1;
  }else{
    echo $con->error;
  }
  exit;
}
function generarCodigo($longitud) {
 $key = '';
 $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
 $max = strlen($pattern)-1;
 for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
 return $key;
}
if(!empty($_GET["id"])){
  $datos=$con->query("SELECT  * FROM vw_solicitudes_patrocinios where id='".($_GET["id"])."'")->fetch_object();
}