<?
include("../includes/conexion.php");
$rsEmpre=$con->query("SELECT id,razonsocial from empresas");
if(!empty($_POST["usuario"])){
  $id=(int)$_GET["id"];
  $empresa=(int)$_POST["empresa"];
  $usuario=addslashes($_POST["usuario"]);
  $password=addslashes($_POST["password"]);
  $email=addslashes($_POST["email"]);
  if(!empty($id)){
    $con->query("UPDATE usuarios_back SET empresa='".$empresa."',
    user='".$usuario."',
    pass='".$password."',
    email='".$email."'
    WHERE id=".$id);
  }else{
    $total=$con->query("select count(*) total from usuarios where usuario='".$usuario."'")->fetch_object()->total;
    if($total>0){
      echo '<span style="color:red">El usuario ingresado ya existe en la base de datos, por favor ingrese uno diferente</span>';
      exit;
    }
    $con->query("INSERT INTO usuarios SET empresa='".$empresa."',
    usuario='".$usuario."',
    password='".$password."',
    email='".$email."'");
    $_GET["id"]=$con->insert_id;
  }
  if(empty($con->error)){
    echo 1;
  }else{
    echo $con->error;
  }
  exit;
}
if(!empty($_GET["id"])){
  $datos=$con->query("SELECT * from usuarios_back where id=".((int)$_GET["id"]))->fetch_object();
}
include("includes/header.php");
#include("includes/top.php");
include("includes/menu.php")?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ":"Nuevo"?> Usuario</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dash.php">Home</a></li>
              <li class="breadcrumb-item"><a href="usuarios.php">Usuarios</a></li>
              <li class="breadcrumb-item active"><?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ":"Nuevo"?> Usuario</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6" style="margin: auto;">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?=(!empty($_GET["id"]))?"Modificaci&oacute;n de ":"Nuevo"?> Usuario</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <? if(!empty($msg)){?>
              <span style="color: green;"><?=$msg?></span>
              <?}?>
              <? if(!empty($error)){?>
              <span style="color: red;"><?=$error?></span>
              <?}?>
                <form role="form"
                id="form"
                onsubmit="return guardar({pag:'<?=$_SERVER["REQUEST_URI"]?>',datos:$('#form').serialize(),back:'usuarios.php'})"
                class="form-horizontal" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
                  <!-- text input -->
                  <div class="col-5">
                    <label>Usuario</label>
                    <input type="text" class="form-control" name="usuario" value="<?=$datos->usuario?>" required=""  autocomplete="off" />
                  </div>
                  <div class="col-5">
                    <label>Contrase&ntilde;a</label>
                    <input type="password" class="form-control" name="password" value="<?=$datos->password?>" required="" />
                  </div>
                  <div class="col-5">
                    <label>Email</label>
                    <input type="text" class="form-control" name="email" value="<?=$datos->email?>" />
                  </div>
                  <div class="col-5">
                    <label>Nombre</label>
                    <input type="text" class="form-control" name="nombre" value="<?=$datos->nombre?>" required="" />
                  </div>
                  <div class="card-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>
                    <a type="submit" class="btn btn-default float-right" href="usuarios.php">Volver</a>
                  </div>
                </form>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<? include("includes/footer.php")?>