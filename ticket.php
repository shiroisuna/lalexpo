<?php
    include('includes/conexion.php');
    session_start();
include('includes/idioma.php');
$lg=new idioma($_GET['lg']);
$lg->seccion(8);
     include('includes/header.php');
     if ($_SESSION['login']!='1') {
        header('location: index.php');
        exit();
     }
     ?>
    <link rel="stylesheet" href="css/theme.css" />
    <style>
    .content{
      font-size:1rem;
    }
    </style>
    <div class="content">
  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <center>
            <h1 class="display-4"><?=$lg->seccion->t_bienv?></h1>
          </center>
        </div>
      </div>
    </div>
  </div>
  <div class="py-6 p-0">
    <div id="vtick" class="container">
      <div class="row">
        <div class="col-sm-12">
          <img class="img-fluid" src="<? include('servicios/ticket.php'); ?>">
        </div>
        <div class="col-sm-6">
            <a class="btn btn-primary w-50 pull-right" href="dashboard.php"><?=$lg->seccion->t_volv?></a>
        </div>
        <div class="col-sm-6">
            <a class="btn btn-primary w-50 pull-left" href="/getTicket.php?cod=<?=$ticket->codigo?>&desc=D"><?=$lg->seccion->guarPdf?></a>
        </div>
        <div class="col-sm-12">
          <p class="px-3">
          <?=$lg->seccion->t_contactick01?>
          <?=$lg->seccion->t_contactick02?>
          <?=$lg->seccion->t_contactick03?>
          <?=$lg->seccion->t_contactick04?>
          <?=$lg->seccion->t_contactick05?>
        </div>
<div class="">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <h1 class="">
            <b class="text-primary">Lalexpo</b> internacional
            <br />
          </h1>
          <h5>Cali - Colombia
            <br />
          </h5>
        </div>
        <div class="col-md-7">
          <!--<img class="img-fluid d-block mx-auto py-4" src="../../2.JPG">-->
        </div>
      </div>
      <div class="row text-left   bg-light px-2">
        <div class="col-md-3">
          <div class="row mb-3">
            <div class="text-center col-2">
              <i class="d-block mx-auto fa fa-3x fa-phone text-primary"></i>
            </div>
            <div class="align-self-center col-10">
              <h5 class="text-secondary">
                <b>
                  <i>USA</i>
                </b>
                <br>
                <b>+1 347 568 0230</b>
              </h5>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="row mb-3">
            <div class="text-center col-2">
              <i class="d-block mx-auto fa fa-3x fa-phone-square text-primary"></i>
            </div>
            <div class="align-self-center col-10">
              <h5 class="text-secondary">
                <b>
                  <i>Colombia</i>
                  <br />+57 3135253836</b>
              </h5>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="row mb-3">
            <div class="text-center col-2">
              <i class="d-block mx-auto fa fa-3x fa-skype text-primary"></i>
            </div>
            <div class="align-self-center col-10">
              <h5 class="text-secondary">
                <b>Lalexpo</b>
              </h5>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2">
              <i class="d-block mx-auto fa fa-3x fa-facebook-square text-secondary"></i>
            </div>
            <div class="col-md-2">
              <i class="d-block mx-auto fa fa-3x fa-youtube-square text-secondary"></i>
            </div>
            <div class="col-md-1">
              <i class="d-block mx-auto fa fa-3x fa-twitter-square text-secondary"></i>
            </div>
          </div>
        </div>
        <div class="my-3 col-md-3">
          <div class="row mb-3">
            <div class="text-center col-2">
              <i class="d-block mx-auto fa fa-3x fa-phone text-primary"></i>
            </div>
            <div class="align-self-center col-10">
              <h5 class="text-secondary">
                <b>
                  <i>Colombia</i>
                  <br />+ 57 (2) 380 97 03 EXT 102</b>
              </h5>
            </div>
          </div>
        </div>
        <div class="my-3 col-md-3">
          <div class="row mb-3">
            <div class="text-center col-2">
              <i class="d-block mx-auto fa fa-3x fa-envelope-square text-primary"></i>
            </div>
            <div class="align-self-center col-10">
              <h5 class="text-secondary">
                <b>info@lalexpo.com</b>
              </h5>
            </div>
          </div>
        </div>
        <div class="col-md-4 my-3">
          <div class="row mb-3">
            <div class="col-md-12">
              <!--<img class="img-fluid d-block" src="../../logo_11.png">-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
      </div>
    </div>
  </div>
  </div>
      </div>
      <script type="text/javascript">
    </script>
    <?php include('includes/footer.php')?>
  <!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>