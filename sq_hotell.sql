/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.1.33-community : Database - lalexpo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`lalexpo` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `lalexpo`;

/*Table structure for table `hotel` */

DROP TABLE IF EXISTS `hotel`;

CREATE TABLE `hotel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(70) DEFAULT NULL,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `imagen_en` varchar(100) DEFAULT NULL,
  `imagen_es` varchar(100) DEFAULT NULL,
  `imgvuelos_es` varchar(100) DEFAULT NULL,
  `imgvuelos_en` varchar(100) DEFAULT NULL,
  `subtitulo_es` varchar(150) DEFAULT NULL,
  `subtitulo_en` varchar(150) DEFAULT NULL,
  `descripcion_es` longtext,
  `descripcion_en` longtext,
  `adicional_es` longtext,
  `adicional_en` longtext,
  `categoria` int(1) DEFAULT NULL,
  `estado` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `hotel` */

insert  into `hotel`(`id`,`titulo`,`nombre_es`,`nombre_en`,`imagen_en`,`imagen_es`,`imgvuelos_es`,`imgvuelos_en`,`subtitulo_es`,`subtitulo_en`,`descripcion_es`,`descripcion_en`,`adicional_es`,`adicional_en`,`categoria`,`estado`) values (1,'Hotel NH Cali Royal','<span class=\"title-span-ctg-lalexpo\">19-21/FEB/2017 <span class=\"box-span-red-ctg-lalexpo\">Cali /Col</span>','<span class=\"title-span-ctg-lalexpo\">19-21/FEB/2017 <span class=\"box-span-red-ctg-lalexpo\">Cali /Col</span>','0.4781230015318978882.jpg','0.4766570015318978881.jpg','0.0273510015318978663.jpg','0.0287950015318978664.jpg','Tarifas especiales para los asistentes y expositores del Show LALEXPO 2018','Special rates for attendees and exhibitors of the 2018 LALEXPO Show','Cali es la capital del departamento del Valle del Cauca, también conocida como la capital mundial de la salsa, la sultana del Valle o la sucursal del cielo, es la tercera ciudad de la República de Colombia. Con más de 477 años de existencia es una de las ciudades más antiguas de América.</br></br>\n Es uno de los principales centros económicos e industriales de Colombia, además de ser el principal centro urbano, cultural, económico, industrial y agrario del suroccidente del país. </br></br>\nCali es una ciudad de grandes espacios para el turismo y la recreación,  abundan en ella las mujeres hermosas, los sitios de valor histórico y espacios para la diversión diurna y nocturna que hacen de ella una meca del turismo.  De igual manera la sucursal del valle se identifica por su gastronomía tradicional, una cocina que funde las herencias española, indígena y africana, dando un sabor único. Así nacen el sancocho de gallina, el arroz atollado, la sopa de tortillas, el aborrajado, las tostadas de plátano verde con hogao y los tamales.','Cali is the capital of the State of Valle del Cauca, also known as the world capital of Salsa music, La Sultana del Valle or “Sucursal del cielo” (heaven’s branch), is the third  most populated City of the Republic of Colombia. With more than 477 years of existence it is one of the oldest cities in America.</br></br>\nIt is one of the main economic and industrial centers of Colombia, as well as being the main urban, cultural, economic, Industrial and Agrarian Center of the southwest of the country.</br></br>\nCali is a City of great spaces for tourism and recreation, there are plenty of beautiful women, sites of historical value and spaces for day and night fun that makes it a mecca of tourism. Similarly, it is also identified by its traditional cuisine, a cuisine that melts together the Spanish, indigenous and African heritages, creating a unique taste. This is how the Sancocho, the atollado rice, the tortilla soup, the aborrajado, the green plantain toast with Hogao and the tamales are born.','Está situado en una agradable zona residencial, justo al lado del centro comercial Holguines Trade Center. Los mejores restaurantes y bares de la Avenida San Joaquín quedan a pocos minutos caminando, mientras que el animado centro de Cali se encuentra a solo 20 minutos en automóvil.</br></br>\n\n• Junto al complejo Holguines Trade Center</br>\n• Cerca de los mejores centros comerciales de la ciudad: Unicentro y Jardín Plaza</br>\n• A poca distancia a pie de uno de los mejores campos de golf de Cali</br></br>\nLas 145 habitaciones del hotel son más grandes que las de la mayoría de los hoteles de la ciudad. Limpias, modernas y con una decoración en tonos suaves, las alcobas son perfectas tanto para turistas como para viajeros de negocios. Las habitaciones Suite Royal dan a la piscina, mientras que las Ejecutivas tienen vista panorámica de la ciudad.','The hotel is in a charming residential area, right next to the Holguines Trade Center. It’s only a short walk to the best restaurants and bars on the Avenida San Joaquin, while the bustling centre of Cali is only 20 minutes’ drive away.</br></br>\n\n• Next to the Holguines Trade Center complex</br>\n• Close to the city’s best shopping malls: Unicentro and Jardin Plaza</br>\n• Short walk to one of Cali’s best golf courses</br></br>\nThe hotel’s 145 rooms are bigger than most hotels in the city. Clean, modern and with muted décor, rooms are perfect for both tourists and those on business. Suite Royal rooms overlook the pool, while Executive Rooms have panoramic views of the city.',5,1);

/*Table structure for table `hotel_categorias_hab` */

DROP TABLE IF EXISTS `hotel_categorias_hab`;

CREATE TABLE `hotel_categorias_hab` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `precio` int(8) unsigned DEFAULT NULL,
  `adicional` int(4) unsigned DEFAULT NULL,
  `disponibles` int(4) unsigned DEFAULT NULL,
  `descripcion_es` longtext,
  `descripcion_en` longtext,
  `estado` int(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `hotel_categorias_hab` */

insert  into `hotel_categorias_hab`(`id`,`nombre_es`,`nombre_en`,`imagen`,`precio`,`adicional`,`disponibles`,`descripcion_es`,`descripcion_en`,`estado`) values (1,'Habitaciones Standard','Habitaciones Standard','0.812720001531892831.png',10,2,100,'des ','des ',0),(2,'Cuarto Superior','Cuarto Superior',NULL,124,2,20,'sd','sd',1),(3,'Junior Suite','Junior Suite',NULL,130,20,50,'asd','asd',1),(4,'Suite Familiar','Suite Familiar',NULL,143,2,20,'asd','asd',1);

/*Table structure for table `hotel_habitciones` */

DROP TABLE IF EXISTS `hotel_habitciones`;

CREATE TABLE `hotel_habitciones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_categoria` int(11) unsigned DEFAULT NULL,
  `nombre_es` varchar(150) DEFAULT NULL,
  `nombre_en` varchar(150) DEFAULT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `precio` int(8) unsigned DEFAULT NULL,
  `personas` int(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `hotel_habitciones` */

insert  into `hotel_habitciones`(`id`,`id_categoria`,`nombre_es`,`nombre_en`,`imagen`,`precio`,`personas`) values (2,1,'hab test','test','0.315482001531893960.png',150,3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
