/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.6.24 : Database - lalexpo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `lalexpo`;

/*Table structure for table `patrocinador_logo` */

DROP TABLE IF EXISTS `patrocinador_logo`;

CREATE TABLE `patrocinador_logo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_solicitud` int(11) DEFAULT NULL,
  `posicion` int(2) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `solicitud` (`id_solicitud`),
  CONSTRAINT `solicitud` FOREIGN KEY (`id_solicitud`) REFERENCES `patrocinador_solicitud` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Table structure for table `vw_logos_patrocinios` */

DROP TABLE IF EXISTS `vw_logos_patrocinios`;

/*!50001 DROP VIEW IF EXISTS `vw_logos_patrocinios` */;
/*!50001 DROP TABLE IF EXISTS `vw_logos_patrocinios` */;

/*!50001 CREATE TABLE  `vw_logos_patrocinios`(
 `id` int(11) ,
 `posicion` int(2) ,
 `logo` varchar(200) ,
 `id_categoria` int(11) ,
 `categoria` varchar(150) ,
 `id_paquete` int(11) ,
 `paquete` varchar(150) ,
 `id_usuario` int(11) unsigned ,
 `usuario` varchar(101) ,
 `estado` tinyint(4) 
)*/;

/*View structure for view vw_logos_patrocinios */

/*!50001 DROP TABLE IF EXISTS `vw_logos_patrocinios` */;
/*!50001 DROP VIEW IF EXISTS `vw_logos_patrocinios` */;

/*!50001 CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_logos_patrocinios` AS (select `pl`.`id` AS `id`,`pl`.`posicion` AS `posicion`,`ps`.`logo` AS `logo`,`pp`.`id_categoria` AS `id_categoria`,`pc`.`nombre_es` AS `categoria`,`ps`.`id_paquete` AS `id_paquete`,`pp`.`nombre_es` AS `paquete`,`ps`.`id_usuario` AS `id_usuario`,concat(`us`.`nombre`,' ',`us`.`apellido`) AS `usuario`,`pl`.`estado` AS `estado` from ((((`patrocinador_logo` `pl` join `patrocinador_solicitud` `ps` on((`ps`.`id` = `pl`.`id_solicitud`))) join `patrocinador_paquete` `pp` on((`ps`.`id_paquete` = `pp`.`id`))) join `patrocinador_categoria_paquete` `pc` on((`pc`.`id` = `pp`.`id_categoria`))) join `usuarios` `us` on((`us`.`id` = `ps`.`id_usuario`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
