<?
include('includes/conexion.php');
include('includes/idioma.php');
$lg=new idioma($_GET['lg']);
$lg->seccion(8);
include('includes/header.php')?>
<style>
  body, html {
    min-width: 350px !important;
  }
  .bkg{
    background-color: #2d2d2d;
    display: table;
  }
  .bkg .txt01{
    width: 49%;
    position: relative;
    display:table-cell;
    vertical-align: top;
    padding-top: 4em;
  }
  .bkg .txt03{
    text-align: center;
  }
  .bkg h1{
    font-family:AspiraBlack,Verdana;
    font-size:8em;
    color:#d0a951;
    margin:0px;
    padding:0px;
    display:inline-block;
    line-height: 80%;
    text-align: center;
    letter-spacing: -0.1em;
    width: 100%;
  }
  .bkg h1 span{
    color:#FFF;
  }
  .bkg .txt03{
    font-family: AspiraLight,Verdana;
    font-size: 5em;
    color: #FFF;
    display: inline-block;
    padding: 0 9%;
  }
  .bkg .padd{
    text-align: center;
  }
  .section{
    background: #FFF;
    min-height: 500px;
  }
  section{
    display: block;
    max-width: 1233px;
    width: 85%;
    margin: auto;
    padding-top: 50px;
  }
  section header{
    background: url(img/fdo_enc_sec.jpg) left repeat-y;
    height: 78px;
    display: block;
    max-width: 1233px;
    width: 100%;
    border-radius: 25px;
    text-align: center;
    color:#FFF;
    font-family: AspiraBold,Verdana;
    font-size:5em;
    vertical-align: middle;
    padding-top: 30px;
  }
  section ul{
    list-style: none;
    display: block;
    margin: auto;
    border:0px solid #969696;
    padding:0;
    background: #FFF;
    width: 85%;
    margin-top: -30px;
    -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    border-bottom-left-radius: 40px;
    border-bottom-right-radius: 40px;
    position: relative;
  }
  section li{
    width: 100%;
    background: url('img/fdo_bottom_ofer.jpg') center bottom no-repeat;
  }
  .sinBg{
    background: none !important;
  }
  .cell{
    display: table-cell;
    vertical-align: middle;
    cursor: pointer;
  }
  .he01{
    width: 10px;
  }
  .he02{
    width: 94%;
    font-size: 3em;
    font-family: Aspira,Verdana;
    padding-left: 40px;
  }
  .he03{
    max-width: 134px;
    text-align: left;
  }
  .he03 img{
    max-width: 43px;
    width: 90%;
  }
  .tabl{
    display: table;
    width: 100%;
    color:#2d2d2d;
    height: 140px;
  }
  .descrip{
    font-family: Aspira,Verdana;
    font-size: 2.4em;
    color:#2d2d2d;
    line-height: 1.2em;
    position: relative;
  }
  .descrip p {
    margin-left: 60px;
    margin-right: 60px;
  }
  .descrip div{
    position: absolute;
    top: 15px;
    right: 100px;
      -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    width: 280px;
    padding:30px
  }
  .descrip table{
    width: 100%;
  }
  .descrip td{
    height: 50px;
    vertical-align: middle;
    border-bottom: 1px solid #606060;
  }
  .descrip div a{
    width: 265px;
    padding: 10px 0;
    background: #a81d26;
    color:#FFF;
    display: inline-block;
    text-align: center;
    vertical-align: middle;
    margin: auto;
    text-decoration: none;
    margin-top: 40px;
    border-radius: 25px;
  }
  /*************************************/
  .fechas{
    text-align: right;
    padding-right: 20px;
    padding-top: 40px;
  }
  .fechas a,.fechas span{
    font-size:2em;
    text-decoration:none;
    color:#8f1526;
    font-family:Aspira,Verdana;
  }
  .fechas a:hover{
    text-decoration: underline;
  }
  .gral{
    -webkit-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    -moz-box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    box-shadow: 2px 9px 18px 4px rgba(150,150,150,0.47);
    width: 90%;
    margin: auto;
    position: relative;
    background: #FFF;
  }
  .gral td{
    height: 150px;
    border-bottom: 1px solid #dddddd;
  }
  .gral table{
    width: 100%;
  }
  .gral td.txt{
    color:#000;
    font-size: 2.7em;
    font-family:Aspira,Verdana;
  }
  .awe{
    color:#821126;
    font-size:2.5em;
    font-family: AwesomeSolid,Verdana;
  }
  .dvBusc2{
    text-align: left;
    padding-left: 20px;
    font-family:Aspira,Verdana;
    color:#000;
    font-size:2.2em;
    width: 90%;
    margin: auto;
    padding-bottom: 15px;
  }
  .dvBusc2 .buscWho{
  }
  .dvBusc2 .red{
    color:#821126;
    display: inline-block;
    padding-right: 20px;
    background: url(img/fle04.jpg) no-repeat right 5px;
    cursor:pointer
  }
  .txt01 div{
    display: inline-block;
  }
  .he04,.he05{
    font-family: AspiraBlack,Verdana;
    font-size: 2em;
    color:#a91d26;
    padding:0 4%
  }
  .he05{
    font-size: 1em;
    color:#2d2d2d;
    display:inline
  }
  ul.tabs {
    margin: 0;
    padding: 0;
    float: left;
    list-style: none;
    height: 32px;
    width: 100%;
    text-align: center;
  }
  ul.tabs li {
    display: inline-block;
    margin: 0;
    padding: 0;
    height: 31px; /*--Subtract 1px from the height of the unordered list--*/
    line-height: 31px; /*--Vertically aligns the text within the tab--*/
    border: 1px solid #2d2d2d;
    border-left: none;
    margin-bottom: -1px; /*--Pull the list item down 1px--*/
    overflow: hidden;
    position: relative;
    background: #232323;
    color: #d0a951;
    font-family: Aspira,Verdana;
    font-size: 2em;
    border-top-left-radius: 15px;
    border-top-right-radius: 15px;
  }
  ul.tabs li a {
    text-decoration: none;
    color: #d0a951;
    display: block;
    font-size: 1.2em;
    padding: 0 20px;
    outline: none;
  }
  ul.tabs li a:hover {
    background: #2d2d2d;
  }
  html ul.tabs li.active, html ul.tabs li.active a:hover  {
    background: #2d2d2d;
  }
  .tab_container {
    border: 1px solid #999;
    border-top: none;
    overflow: hidden;
    clear: both;
    float: left;
    width: 100%;
    height:380px;
    background: #2d2d2d;
    border-radius: 15px;
  }
  .tab_content {
    padding: 20px;
    font-size: 1.2em;
    display: table;
  }
.infoCont {
  margin-top: 50px;
  width: 90%;
  margin-left: auto;
  margin-right: auto;
}
  .preguntas > *, .respuesta > * { font-family: Aspira,Verdana; }
  .preguntas {
    display: table-cell;
    width: 45%;
    /*float: left;*/
    padding: 10px;
  }
  .preguntas:after {
    border-right: solid 1px #5d6974;
  }
  .preguntas h1, .respuesta h1 {
    font-size: 4em;
    color: #fff;
  }
  .cajaPregunta {
    display: block;
    width: 100%;
    height: 200px;
    max-height: 200px;
    overflow-y: auto;
    border-radius: 6px;
    border: solid 1px #d0a951;
  }
  .cajaPregunta p {
    display: block;
    padding: 10px;
    color: #fff;
    background: #545454;
    font-size: 1.5em;
    cursor: pointer;
  }
  .cajaPregunta p:hover {
    background: #606060;
  }
  .cajaPregunta p.active {
    background: #2d2d2d;
    color: #d0a951;
  }
  .respuesta {
    display: table-cell;
    width: 45%;
    /*float: right;*/
    padding: 10px;
  }
  .cajaRespuesta {
    height: 200px;
    max-height: 200px;
    overflow-y: auto;
  }
  .cajaRespuesta p {
    display: none;
    color: #d0a951;
    font-size: 1.8em;
    text-align: justify;
  }
  </style>
  <div class="content">
  <div class="separador"></div>
  <div class="bkg">
    <div class="txt01 padd">
      <h1><?=$lg->general->menu_info?></h1>
          <span class="txt03">
            <?php echo cargarBloque('info-descrip', $lg->idioma); ?>
          </span>
    </div>
  </div>
      <div class="section">
      <div class="section">
      <div class="infoCont">
        <?php include('servicios/faq.php'); ?>
      </div>
      </div>
    </div>
    <script type="text/javascript">
      $(document).ready(function() {
        $(".tab_content").hide();
        $("ul.tabs li:first").addClass("active").show();
        $(".tab_content:first").show();
        $("ul.tabs li").click(function() {
          $("ul.tabs li").removeClass("active");
          $(this).addClass("active");
          $(".tab_content").hide();
          var activeTab = $(this).find("a").attr("href");
          $(activeTab).fadeIn();
          return false;
        });
      });
      function verResp(pregunta){
        $(".cajaPregunta p").removeClass("active");
        $("#sp"+pregunta).addClass("active");
        $(".cajaRespuesta p").hide();
        $("#rsp"+pregunta).show();
      }
    </script>
    <? include('includes/footer.php')?>